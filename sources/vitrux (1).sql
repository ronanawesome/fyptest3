-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2021 at 09:05 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vitrux`
--

-- --------------------------------------------------------

--
-- Table structure for table `AboutUs`
--

CREATE TABLE `AboutUs` (
  `id` int(11) NOT NULL,
  `photo` varchar(512) NOT NULL,
  `description` longtext NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `priority_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `AboutUs`
--

INSERT INTO `AboutUs` (`id`, `photo`, `description`, `is_active`, `priority_order`) VALUES
(1, '2df4u3n4o7mss08w48.jpg', 'VITRUX, a company jointly established by multi groups of experts. With a unique business model that integrates four main core sectors into one, which is biotechnology, e-commerce, digital marketing media, and education sector. VITRUX is dedicated in creating a new form of business culture and global business platform, not only bringing people the extraordinary benefits of sustainable income, but also generating quality and healthy life for everyone.\n\nTo build a globally integrated health platform, VITRUX is committed to used natural and nutritional formula, cutting-edge life technology, and stringent quality management. Providing people with the highest quality health products and the most scientific health management that can be consumed with peace of mind and confidence.\n\nVITRUX always upholds our corporate core values of ‘firm belief-resolute action-persistence in learning’, Committed to the in-depth research and development of biological peptides, maintaining advanced technology advantages in the industry, continuous expanding awareness in the domestic market and creating a business opportunity to the international market, VITRUX will continue to strives to create a benchmark in the peptide industry.', 1, 100);

-- --------------------------------------------------------

--
-- Table structure for table `AboutUsItem`
--

CREATE TABLE `AboutUsItem` (
  `id` int(11) NOT NULL,
  `photo` varchar(512) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` longtext NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `priority_order` int(11) NOT NULL,
  `created_datatime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `AboutUsItem`
--

INSERT INTO `AboutUsItem` (`id`, `photo`, `title`, `description`, `is_active`, `priority_order`, `created_datatime`) VALUES
(1, '', '', 'asfsfafafdfafsfsfsd', 1, 100, '2021-05-19 03:14:04'),
(2, '', '', 'sfsfsssgsghshh', 1, 100, '2021-05-19 03:23:43');

-- --------------------------------------------------------

--
-- Table structure for table `Admin`
--

CREATE TABLE `Admin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(84) NOT NULL,
  `type` varchar(50) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Admin`
--

INSERT INTO `Admin` (`id`, `name`, `username`, `password`, `type`, `is_active`, `last_login`) VALUES
(4, 'masteradmin', 'masteradmin', 'AQAAAAEAACcQAAAAELG+U9l+8HReQRbvImkvnq0Eg9vSfmhHi5aHxfl2U06IP6FYCVYZ8FrQL2G5f9pinw==', 'master', 1, '2021-05-15 11:46:57');

-- --------------------------------------------------------

--
-- Table structure for table `Banner`
--

CREATE TABLE `Banner` (
  `id` int(11) NOT NULL,
  `photo` varchar(512) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `priority_order` int(11) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Banner`
--

INSERT INTO `Banner` (`id`, `photo`, `is_active`, `priority_order`, `created_datetime`) VALUES
(1, 'pvetjxokkusskw8gso.jpg', 1, 100, '2021-05-19 04:36:42'),
(2, 're7224hzi74ks0o00o.jpg', 1, 100, '2021-05-19 04:36:48'),
(3, 'fulbi26z4q8sk4c0k4.jpg', 1, 100, '2021-05-19 04:36:54');

-- --------------------------------------------------------

--
-- Table structure for table `Blog`
--

CREATE TABLE `Blog` (
  `id` int(11) NOT NULL,
  `photo` varchar(512) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` longtext NOT NULL,
  `priority_order` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Blog`
--

INSERT INTO `Blog` (`id`, `photo`, `title`, `description`, `priority_order`, `is_active`, `created_datetime`) VALUES
(2, '2x3hn99ordmo4so8ko.png', 'Why choose [Peptide]?', '1902, physiologist-William Baylis\n\nDiscovered \"secretin\", he pointed out that when acid penetrates into the body from the stomach, Will release a \"chemical messenger\" in the jejunum mucosa, also known as hormones.\nThese messengers use the blood to stimulate the pancreas to secrete pancreatic juice.\n \nThis is the first time a \"peptide\" has been discovered in the records of human history.\nThis major discovery created the powerful function of \"peptides\" in endocrinology. \nHe won the Nobel Prize in Medicine for this.\n\nIt also opened a new chapter in the history of human science of cell nutrition balance!\nPeptide is a revolutionary product that will lead the human nutrition revolution in the 21st century.', 100, 1, '2021-05-19 06:07:20'),
(3, '5zgwjs4lgnwg4g4kc.png', 'What is a peptide?', 'The human body is made up of cells, which are composed of five major substances: water, protein, fat, carbohydrate and minerals. Therefore, apart from water, protein is the most important substance in the human body. \n\nThe small molecule active peptide is a kind of biochemical substance between amino acid and protein. It has a smaller molecular weight than protein and a larger molecular weight than an amino acid. It is a fragment of protein. Two or more amino acids are connected by peptide bonds, and the \"amino acid chain\" or \"amino acid string\" formed is called a peptide.\n\nBesides this, A peptide composed of more than 10-15 amino acids is called a polypeptide, and a peptide composed of 2 to 9 amino acids is called a small molecule peptide or small peptide. \n\nPeptides are the basic material that constitutes all cells of the human body, all human muscle tissues and connective tissues, promote the body\'s biochemical reflect acid, regulating physiological hormone, carriers for transporting oxygen or other ions, antibodies against germs are all peptides.\nSo, our whole body is full of peptides, in addition, the renewal, metabolism, growth and repair of cells are inseparable from peptides. \n\nPeptides are a form of life, without peptides, cells are inactive;\nWithout peptides, there is no vitality of the organ \nWithout peptides, life would not be possible to survive.', 100, 1, '2021-05-19 06:24:16');

-- --------------------------------------------------------

--
-- Table structure for table `ContactUs`
--

CREATE TABLE `ContactUs` (
  `id` int(11) NOT NULL,
  `phone` varchar(64) NOT NULL,
  `address` varchar(256) NOT NULL,
  `email` varchar(128) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ContactUs`
--

INSERT INTO `ContactUs` (`id`, `phone`, `address`, `email`, `description`) VALUES
(1, '+60-145962358', 'No. 3,Jalan 19/1, Seksyen 19,46300 Petaling Jaya, Selangor.', 'info@vitrux.com', 'We are a team of produce that high quality product .');

-- --------------------------------------------------------

--
-- Table structure for table `CourierService`
--

CREATE TABLE `CourierService` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `state_coverage` varchar(64) NOT NULL,
  `base_west` float NOT NULL,
  `incremental_west` float NOT NULL,
  `base_east` float NOT NULL,
  `incremental_east` float NOT NULL,
  `priority_order` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `CourierService`
--

INSERT INTO `CourierService` (`id`, `name`, `state_coverage`, `base_west`, `incremental_west`, `base_east`, `incremental_east`, `priority_order`, `is_active`) VALUES
(1, 'J&T express', 'adfa', 1.21, 20.32, 10, 45.21, 100, 1),
(2, 'Poslaju', 'Sabah', 1.24, 7.65, 5.12, 34.2, 100, 1);

-- --------------------------------------------------------

--
-- Table structure for table `GroupSalesBonus`
--

CREATE TABLE `GroupSalesBonus` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `group_bonus` int(11) NOT NULL,
  `level` varchar(64) NOT NULL,
  `level_bonus` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `GroupSalesBonus`
--

INSERT INTO `GroupSalesBonus` (`id`, `title`, `group_bonus`, `level`, `level_bonus`, `is_active`) VALUES
(1, '100,000 P *Direct Sponsor Group Sales', 3, '1st Generation', 5, 1),
(2, '200,000 P *Direct Sponsor Group Sales', 7, '2nd Generation', 5, 1),
(3, '', 0, '3rd Generation', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `LeaderSalesBonus`
--

CREATE TABLE `LeaderSalesBonus` (
  `id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` longtext NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `LeaderSalesBonus`
--

INSERT INTO `LeaderSalesBonus` (`id`, `title`, `description`, `is_active`) VALUES
(1, 'sponsor 20 Diamond', 'Additional 5% for Sales Bonus', 1);

-- --------------------------------------------------------

--
-- Table structure for table `PersonalSalesBonus`
--

CREATE TABLE `PersonalSalesBonus` (
  `id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `req_start` int(11) NOT NULL,
  `req_end` int(11) NOT NULL,
  `requirement` varchar(64) NOT NULL,
  `commission` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PersonalSalesBonus`
--

INSERT INTO `PersonalSalesBonus` (`id`, `title`, `req_start`, `req_end`, `requirement`, `commission`, `price`, `is_active`) VALUES
(1, 'Normal Member**', 3, 59, '3-59', 20, 278, 1),
(2, 'Silver', 60, 119, '60-119', 40, 258, 1),
(3, 'Gold', 120, 239, '120-239', 60, 238, 1),
(4, 'Diamond', 240, 0, '240 and above', 80, 218, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ProductCategory`
--

CREATE TABLE `ProductCategory` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `priority_order` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ProductGalley`
--

CREATE TABLE `ProductGalley` (
  `id` int(11) NOT NULL,
  `image` varchar(512) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `priority_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ProductGalley`
--

INSERT INTO `ProductGalley` (`id`, `image`, `is_active`, `priority_order`) VALUES
(1, 'ie8b1ad8gk8ccossoc.jpeg', 1, 100),
(2, '2lisnpt556ckwoc88.jpeg', 1, 100),
(3, 'cp7d3ephyzccs40g0.jpeg', 1, 100);

-- --------------------------------------------------------

--
-- Table structure for table `ProductManage`
--

CREATE TABLE `ProductManage` (
  `id` int(11) NOT NULL,
  `category` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `quantity` int(11) NOT NULL,
  `stock_available` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `photo` varchar(512) NOT NULL,
  `cost` decimal(16,2) NOT NULL,
  `sell_price` decimal(16,2) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `priority_order` int(11) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ProductManage`
--

INSERT INTO `ProductManage` (`id`, `category`, `name`, `sku`, `quantity`, `stock_available`, `description`, `photo`, `cost`, `sell_price`, `is_active`, `priority_order`, `created_datetime`, `last_modified`) VALUES
(1, 'food', 'vitrux (1 Box package)', '5KM-600', 20, 10, 'The health care product is often loosely defined as a service. Health products are vitamins, minerals, herbal medicines, homeopathic preparations, probiotics and even some traditional medicines are also prescribed by doctor or any equivalent physician.', '2whg4b07gl4w0c404k.jpeg', '2.00', '298.00', 1, 100, '2021-05-17 09:47:09', '2021-05-17 09:47:09'),
(2, 'food', 'Vitrux (Buy 1 free 1)', '123', 30, 0, 'The health care product is often loosely defined as a service. Health products are vitamins, minerals, herbal medicines, homeopathic preparations, probiotics and even some traditional medicines are also prescribed by doctor or any equivalent physician.', '14fpn3e7qveos4cc40.jpeg', '5.12', '524.00', 1, 100, '2021-05-19 10:00:48', '2021-05-19 10:00:48'),
(3, 'food', 'Vitrux (3 box package)', '452', 40, 0, 'The health care product is often loosely defined as a service. Health products are vitamins, minerals, herbal medicines, homeopathic preparations, probiotics and even some traditional medicines are also prescribed by doctor or any equivalent physician.', 'vt6xn5roh1wo448os0.jpeg', '6.12', '1000.00', 1, 100, '2021-05-19 10:02:45', '2021-05-19 10:02:45');

-- --------------------------------------------------------

--
-- Table structure for table `SalesManage`
--

CREATE TABLE `SalesManage` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `status` varchar(64) NOT NULL,
  `courier_service` varchar(64) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `grand_total` float NOT NULL,
  `agent_info` varchar(64) CHARACTER SET utf32 NOT NULL,
  `customer_info` varchar(64) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `SalesManage`
--

INSERT INTO `SalesManage` (`id`, `sale_id`, `status`, `courier_service`, `transaction_id`, `grand_total`, `agent_info`, `customer_info`, `created_datetime`) VALUES
(1, 12, 'processing', '1', 123, 49.12, 'agent 0', 'josh', '2021-05-17 16:00:00'),
(3, 122, 'processing', '2', 103, 45.58, 'agent 0', 'josh exson', '2021-05-18 05:30:14');

-- --------------------------------------------------------

--
-- Table structure for table `Testimonial`
--

CREATE TABLE `Testimonial` (
  `id` int(11) NOT NULL,
  `photo` varchar(512) CHARACTER SET utf8mb4 NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `priority_order` int(11) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Testimonial`
--

INSERT INTO `Testimonial` (`id`, `photo`, `is_active`, `priority_order`, `created_datetime`) VALUES
(10, 'jrz5ljlzvq8kwgkgoo.jpg', 1, 100, '2021-05-19 04:04:19'),
(11, 'mey3orrd97kkk0ssws.jpg', 1, 100, '2021-05-19 04:04:39'),
(12, 'odkbug5tkvksw8g4c0.jpg', 1, 100, '2021-05-19 04:04:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AboutUs`
--
ALTER TABLE `AboutUs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `AboutUsItem`
--
ALTER TABLE `AboutUsItem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Admin`
--
ALTER TABLE `Admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Banner`
--
ALTER TABLE `Banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Blog`
--
ALTER TABLE `Blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ContactUs`
--
ALTER TABLE `ContactUs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `CourierService`
--
ALTER TABLE `CourierService`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `GroupSalesBonus`
--
ALTER TABLE `GroupSalesBonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `LeaderSalesBonus`
--
ALTER TABLE `LeaderSalesBonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `PersonalSalesBonus`
--
ALTER TABLE `PersonalSalesBonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ProductCategory`
--
ALTER TABLE `ProductCategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ProductGalley`
--
ALTER TABLE `ProductGalley`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ProductManage`
--
ALTER TABLE `ProductManage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `SalesManage`
--
ALTER TABLE `SalesManage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Testimonial`
--
ALTER TABLE `Testimonial`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `AboutUs`
--
ALTER TABLE `AboutUs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `AboutUsItem`
--
ALTER TABLE `AboutUsItem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Banner`
--
ALTER TABLE `Banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Blog`
--
ALTER TABLE `Blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ContactUs`
--
ALTER TABLE `ContactUs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `CourierService`
--
ALTER TABLE `CourierService`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `GroupSalesBonus`
--
ALTER TABLE `GroupSalesBonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `LeaderSalesBonus`
--
ALTER TABLE `LeaderSalesBonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `PersonalSalesBonus`
--
ALTER TABLE `PersonalSalesBonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ProductCategory`
--
ALTER TABLE `ProductCategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ProductGalley`
--
ALTER TABLE `ProductGalley`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ProductManage`
--
ALTER TABLE `ProductManage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `SalesManage`
--
ALTER TABLE `SalesManage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Testimonial`
--
ALTER TABLE `Testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
