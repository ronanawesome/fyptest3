<strong>STUDENT MANAGEMENT SYSTEM FOR PRIMARY SCHOOLS (SMSP)</strong>

#Pre-requisite knowledge
<ul>
	<li>Object Relational Mapping (ORM)</li>
	<li>How Framework works</li>
	<li>What is a LAMP/WAMP stack</li>
</ul>

#Software required
- WAMP, To host the project locally so you can access it. URL: https://sourceforge.net/projects/wampserver/
- PHP (Integrated development environment)IDE, (Developer Choice: PHPstorm v2021.1.2)

#Third party API, in 'module' folder
- xCrud 1.6
- select2 - 4.0.13

#Software Architecture of this Web Application
The architecture model for this application is Model-View-Controller (MVC)
Where the View and Controller is a combined file which resides the pages itself in 'apps' folder.
The 'model' folder contains all the classes which is similar to the database created.

#Database with MySQL
The fyp3rd.sql file is the database for this web application, you can import it using MySQL in phpMyAdmin
- Things to do before importing fyp3rd.sql
- Look for a file 'my.ini' scroll to the bottom under [mysqld] paste the following, the lower_case_table_names is to allow the sql to have capital letter table.

lower_case_table_names=2

Gitlab repositories URL: https://gitlab.com/ronanawesome/fyptest3.git