<?php

class Exam {
    public function searchExam($exam_name)
    {
        $row = $this->dbc->select("SELECT id FROM Exam WHERE Exam_Name = :exam_name", array(':exam_name' => $exam_name));
        if (!empty($row)) {
            return $this->load($row[0]->id);
        } else {
            return false;
        }
    }
    private $dbc=NULL;
    private $id="";
    private $Exam_Name="";
    private $Subject_Id="";
    private $Exam_Date="";
    private $Intake_Id="";

    function __construct($dbc=NULL) {
        $this->dbc = $dbc;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getExamName() {
        return $this->Exam_Name;
    }

    public function setExamName($Exam_Name) {
        $this->Exam_Name = $Exam_Name;
    }

    public function getSubjectId() {
        return $this->Subject_Id;
    }

    public function setSubjectId($Subject_Id) {
        $this->Subject_Id = $Subject_Id;
    }

    public function getExamDate() {
        return $this->Exam_Date;
    }

    public function setExamDate($Exam_Date) {
        $this->Exam_Date = $Exam_Date;
    }

    public function getIntakeId() {
        return $this->Intake_Id;
    }

    public function setIntakeId($Intake_Id) {
        $this->Intake_Id = $Intake_Id;
    }

    public function load($id) {
        $row = $this->dbc->select("SELECT * FROM Exam WHERE id = :id", array(':id' => $id));

        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setExamName($row[0]->Exam_Name);
            $this->setSubjectId($row[0]->Subject_Id);
            $this->setExamDate($row[0]->Exam_Date);
            $this->setIntakeId($row[0]->Intake_Id);
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = [
            "Exam_Name" => $this->getExamName(),
            "Subject_Id" => $this->getSubjectId(),
            "Exam_Date" => $this->getExamDate(),
            "Intake_Id" => $this->getIntakeId(),
        ];
        $this->dbc->insert("Exam", $data);

        return $this->dbc->lastInsertId();
    }

    public function save() {
        $data = [
            "id" => $this->getId(),
            "Exam_Name" => $this->getExamName(),
            "Subject_Id" => $this->getSubjectId(),
            "Exam_Date" => $this->getExamDate(),
            "Intake_Id" => $this->getIntakeId(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("Exam", $data, $where);
    }

    public function remove() {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("Exam", $data);
    }


}
