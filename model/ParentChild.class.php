<?php

class ParentChild {
    public  function checkChildAssigned($user_ID){
        $row = $this->dbc->select("SELECT Child_Id FROM ParentChild WHERE 
                                      Child_Id = :user_ID", array(':user_ID' => $user_ID));
        if (!empty($row)) {
            return true;
        } else {
            return false;
        }
    }

    public  function checkParentChild($user_ID){
        $row = $this->dbc->select("SELECT User_Id FROM ParentChild WHERE 
                                      User_Id = :user_ID", array(':user_ID' => $user_ID));
        if (!empty($row)) {
            return true;
        } else {
            return false;
        }
    }
    public  function checkParentAndChild($user_ID){
        $row = $this->dbc->select("SELECT User_Id FROM ParentChild WHERE 
                                      User_Id = :user_ID", array(':user_ID' => $user_ID));
        if (!empty($row)) {
            return true;
        } else {
            return false;
        }
    }
    public function loadparentchildinfo($id) {
        $row = $this->dbc->select("SELECT * FROM ParentChild WHERE User_Id = :id", array(':id' => $id));
        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setUserId($row[0]->User_Id);
            return true;
        } else {
            return false;
        }
    }
    private $dbc=NULL;
    private $id="";
    private $User_Id="";
    private $Child_Id="";
    private $Relationship="";

    function __construct($dbc=NULL) {
        $this->dbc = $dbc;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getUserId() {
        return $this->User_Id;
    }

    public function setUserId($User_Id) {
        $this->User_Id = $User_Id;
    }

    public function getChildId() {
        return $this->Child_Id;
    }

    public function setChildId($Child_Id) {
        $this->Child_Id = $Child_Id;
    }

    public function getRelationship() {
        return $this->Relationship;
    }

    public function setRelationship($Relationship) {
        $this->Relationship = $Relationship;
    }

    public function load($id) {
        $row = $this->dbc->select("SELECT * FROM ParentChild WHERE id = :id", array(':id' => $id));

        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setUserId($row[0]->User_Id);
            $this->setChildId($row[0]->Child_Id);
            $this->setRelationship($row[0]->Relationship);
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = [
            "User_Id" => $this->getUserId(),
            "Child_Id" => $this->getChildId(),
            "Relationship" => $this->getRelationship(),
        ];
        $this->dbc->insert("ParentChild", $data);

        return $this->dbc->lastInsertId();
    }

    public function save() {
        $data = [
            "id" => $this->getId(),
            "User_Id" => $this->getUserId(),
            "Child_Id" => $this->getChildId(),
            "Relationship" => $this->getRelationship(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("ParentChild", $data, $where);
    }

    public function remove() {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("ParentChild", $data);
    }


}
