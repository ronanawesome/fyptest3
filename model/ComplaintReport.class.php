<?php

class ComplaintReport {
    public function createComplaintReport($data, $user_id){
        $tempstudent_Ids = "";

        foreach($data['Student_Ids'] as $sid){
            $tempstudent_Ids = $sid . "," . $tempstudent_Ids;
        }

        $this->setTitle($data['Title']);
        $this->setType($data['Type']);

        $this->setCreatorId($user_id);
        $this->setUserIds(rtrim($tempstudent_Ids,','));
        $this->setContent($data['Content']);
        $this->setIsDelete(0);
        $this->setCreatedDate($data['Complaint_Date']);
        $this->create();
    }
    private $dbc=NULL;
    private $id="";
    private $Creator_Id="";
    private $User_Ids="";
    private $Type="";
    private $Content="";
    private $Title="";
    private $CreatedDate="";
    private $is_delete="";

    function __construct($dbc=NULL) {
        $this->dbc = $dbc;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getCreatorId() {
        return $this->Creator_Id;
    }

    public function setCreatorId($Creator_Id) {
        $this->Creator_Id = $Creator_Id;
    }

    public function getUserIds() {
        return $this->User_Ids;
    }

    public function setUserIds($User_Ids) {
        $this->User_Ids = $User_Ids;
    }

    public function getType() {
        return $this->Type;
    }

    public function setType($Type) {
        $this->Type = $Type;
    }

    public function getContent() {
        return $this->Content;
    }

    public function setContent($Content) {
        $this->Content = $Content;
    }

    public function getTitle() {
        return $this->Title;
    }

    public function setTitle($Title) {
        $this->Title = $Title;
    }

    public function getCreatedDate() {
        return $this->CreatedDate;
    }

    public function setCreatedDate($CreatedDate) {
        $this->CreatedDate = $CreatedDate;
    }

    public function getIsDelete() {
        return $this->is_delete;
    }

    public function setIsDelete($is_delete) {
        $this->is_delete = $is_delete;
    }

    public function load($id) {
        $row = $this->dbc->select("SELECT * FROM ComplaintReport WHERE id = :id", array(':id' => $id));

        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setCreatorId($row[0]->Creator_Id);
            $this->setUserIds($row[0]->User_Ids);
            $this->setType($row[0]->Type);
            $this->setContent($row[0]->Content);
            $this->setTitle($row[0]->Title);
            $this->setCreatedDate($row[0]->CreatedDate);
            $this->setIsDelete($row[0]->is_delete);
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = [
            "Creator_Id" => $this->getCreatorId(),
            "User_Ids" => $this->getUserIds(),
            "Type" => $this->getType(),
            "Content" => $this->getContent(),
            "Title" => $this->getTitle(),
            "is_delete" => $this->getIsDelete(),
        ];
        $this->dbc->insert("ComplaintReport", $data);

        return $this->dbc->lastInsertId();
    }

    public function save() {
        $data = [
            "id" => $this->getId(),
            "Creator_Id" => $this->getCreatorId(),
            "User_Ids" => $this->getUserIds(),
            "Type" => $this->getType(),
            "Content" => $this->getContent(),
            "Title" => $this->getTitle(),
            "CreatedDate" => $this->getCreatedDate(),
            "is_delete" => $this->getIsDelete(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("ComplaintReport", $data, $where);
    }

    public function remove() {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("ComplaintReport", $data);
    }


}
