<?php

class Attendance {
    public function createAttendance($data, $user_id){
        $tempstudentIds = "";
        foreach($data['Student_Ids'] as $iid){
            $tempstudentIds = $iid . "," .  $tempstudentIds ;
        }
        $this->setClassId($data['Class_Id']);
        $this->setDate($data['Date']);
        $this->setStudentIds(rtrim( $tempstudentIds,','));

//        $this->setDate(date("Y-m-d h:i:sa"));
        $this->create();
    }







    public function recordAttendance($coach_id){
        $this->setAttendance(1);
        $this->setAttendTakerUserId($coach_id);
        if(is_null($this->getAttendanceDatetime())):
            $this->setAttendanceDatetime(today_format(false));
        endif;
        $this->save();
    }
    private $dbc=NULL;
    private $id="";
    private $Date="";
    private $Student_ids="";
    private $Class_Id="";

    function __construct($dbc=NULL) {
        $this->dbc = $dbc;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getDate() {
        return $this->Date;
    }

    public function setDate($Date) {
        $this->Date = $Date;
    }

    public function getStudentIds() {
        return $this->Student_ids;
    }

    public function setStudentIds($Student_ids) {
        $this->Student_ids = $Student_ids;
    }

    public function getClassId() {
        return $this->Class_Id;
    }

    public function setClassId($Class_Id) {
        $this->Class_Id = $Class_Id;
    }

    public function load($id) {
        $row = $this->dbc->select("SELECT * FROM Attendance WHERE id = :id", array(':id' => $id));

        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setDate($row[0]->Date);
            $this->setStudentIds($row[0]->Student_ids);
            $this->setClassId($row[0]->Class_Id);
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = [
            "Date" => $this->getDate(),
            "Student_ids" => $this->getStudentIds(),
            "Class_Id" => $this->getClassId(),
        ];
        $this->dbc->insert("Attendance", $data);

        return $this->dbc->lastInsertId();
    }

    public function save() {
        $data = [
            "id" => $this->getId(),
            "Date" => $this->getDate(),
            "Student_ids" => $this->getStudentIds(),
            "Class_Id" => $this->getClassId(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("Attendance", $data, $where);
    }

    public function remove() {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("Attendance", $data);
    }


}
