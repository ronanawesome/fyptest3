<?php

class Teacher {
//    private $class = null;
//    public function getClass($refresh = false)
//    {
//        if (is_null($this->class) || $refresh) {
//            $rows = $this->dbc->select("SELECT id FROM ClassRoom WHERE id = :id", array(':id' => $this->get()));
//            $this->class = false;
//            $obj = new ClassRoom($this->dbc);
//
//            if (isset($rows) && !empty($rows)) {
//                $obj->load($rows[0]->id);
//                $this->class = clone $obj;
//            } else {
//                return false;
//            }
//        }
//
//        return $this->class;
//    }
    public function createSubjectsTaught($data, $user_id){

        $temptarget_Ids = "";
        foreach($data['Subjects_Taught'] as $iid){
            $temptarget_Ids = $iid . "," .  $temptarget_Ids ;
        }

        $this->setUserId($user_id);
//        $this->setFile($data['File']);
        $this->setSubjectsTaught(rtrim($temptarget_Ids,','));
        $this->create();
    }
    public function loadteacherinfo($id) {
        $row = $this->dbc->select("SELECT * FROM Teacher WHERE User_Id = :id", array(':id' => $id));
        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setUserId($row[0]->User_Id);
            $this->setSubjectsTaught($row[0]->Class_Id);
            return true;
        } else {
            return false;
        }
    }


    public  function checkTeacher($user_ID){
        $row = $this->dbc->select("SELECT User_Id FROM Teacher WHERE
                                      User_Id = :user_ID", array(':user_ID' => $user_ID));
        if (!empty($row)) {
            return true;
        } else {
            return false;
        }
    }
    private $dbc=NULL;
    private $id="";
    private $User_Id="";
    private $Subjects_Taught="";

    function __construct($dbc=NULL) {
        $this->dbc = $dbc;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getUserId() {
        return $this->User_Id;
    }

    public function setUserId($User_Id) {
        $this->User_Id = $User_Id;
    }

    public function getSubjectsTaught() {
        return $this->Subjects_Taught;
    }

    public function setSubjectsTaught($Subjects_Taught) {
        $this->Subjects_Taught = $Subjects_Taught;
    }

    public function load($id) {
        $row = $this->dbc->select("SELECT * FROM Teacher WHERE id = :id", array(':id' => $id));

        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setUserId($row[0]->User_Id);
            $this->setSubjectsTaught($row[0]->Subjects_Taught);
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = [
            "User_Id" => $this->getUserId(),
            "Subjects_Taught" => $this->getSubjectsTaught(),
        ];
        $this->dbc->insert("Teacher", $data);

        return $this->dbc->lastInsertId();
    }

    public function save() {
        $data = [
            "id" => $this->getId(),
            "User_Id" => $this->getUserId(),
            "Subjects_Taught" => $this->getSubjectsTaught(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("Teacher", $data, $where);
    }

    public function remove() {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("Teacher", $data);
    }


}
