<?php

class Subject
{
    public function searchSubject($Subject_Name)
    {
        $row = $this->dbc->select("SELECT id FROM Subject WHERE Subject_Name = :Subject_Name", array(':Subject_Name' => $Subject_Name));
        if (!empty($row)) {
            return $this->load($row[0]->id);
        } else {
            return false;
        }
    }

    private $dbc = NULL;
    private $id = "";
    private $Subject_Name = "";
    private $Subject_Level = "";
    private $Subject_Code = "";

    function __construct($dbc = NULL)
    {
        $this->dbc = $dbc;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getSubjectName()
    {
        return $this->Subject_Name;
    }

    public function setSubjectName($Subject_Name)
    {
        $this->Subject_Name = $Subject_Name;
    }

    public function getSubjectLevel()
    {
        return $this->Subject_Level;
    }

    public function setSubjectLevel($Subject_Level)
    {
        $this->Subject_Level = $Subject_Level;
    }

    public function getSubjectCode()
    {
        return $this->Subject_Code;
    }

    public function setSubjectCode($Subject_Code)
    {
        $this->Subject_Code = $Subject_Code;
    }

    public function load($id)
    {
        $row = $this->dbc->select("SELECT * FROM Subject WHERE id = :id", array(':id' => $id));
        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setSubjectName($row[0]->Subject_Name);
            $this->setSubjectLevel($row[0]->Subject_Level);
            $this->setSubjectCode($row[0]->Subject_Code);
            return true;
        } else {
            return false;
        }
    }

    public function create()
    {
        $data = [
            "Subject_Name" => $this->getSubjectName(),
            "Subject_Level" => $this->getSubjectLevel(),
            "Subject_Code" => $this->getSubjectCode(),
        ];
        $this->dbc->insert("Subject", $data);

        return $this->dbc->lastInsertId();
    }

    public function save()
    {
        $data = [
            "id" => $this->getId(),
            "Subject_Name" => $this->getSubjectName(),
            "Subject_Level" => $this->getSubjectLevel(),
            "Subject_Code" => $this->getSubjectCode(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("Subject", $data, $where);
    }

    public function remove()
    {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("Subject", $data);
    }








}
