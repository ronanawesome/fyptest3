<?php

class Student {

    private $class = null;
    private $intake=null;
    public function getIntake($refresh = false)
    {
        if (is_null($this->intake) || $refresh) {
            $rows = $this->dbc->select("SELECT id FROM Intakes WHERE id = :id", array(':id' => $this->getIntakeId()));
            $this->intake = false;
            $obj = new Intakes($this->dbc);

            if (isset($rows) && !empty($rows)) {
                $obj->load($rows[0]->id);
                $this->intake = clone $obj;
            } else {
                return false;
            }
        }

        return $this->intake;
    }
    public function loadstudentinfo($id) {
        $row = $this->dbc->select("SELECT * FROM Student WHERE User_Id = :id", array(':id' => $id));
        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setUserId($row[0]->User_Id);
            $this->setClassId($row[0]->Class_Id);
            $this->setIntakeId($row[0]->Intake_Id);
            return true;
        } else {
            return false;
        }
    }

    public function getClass($refresh = false)
    {
        if (is_null($this->class) || $refresh) {
            $rows = $this->dbc->select("SELECT id FROM ClassRoom WHERE id = :id", array(':id' => $this->getClassId()));
            $this->class = false;
            $obj = new ClassRoom($this->dbc);

            if (isset($rows) && !empty($rows)) {
                $obj->load($rows[0]->id);
                $this->class = clone $obj;
            } else {
                return false;
            }
        }

        return $this->class;
    }


    public  function checkStudent($user_ID){
        $row = $this->dbc->select("SELECT User_Id FROM Student WHERE
                                      User_Id = :user_ID", array(':user_ID' => $user_ID));
        if (!empty($row)) {
            return true;
        } else {
            return false;
        }
    }

    public function updateStudentInfo($data){

        $this->setClassId($data['Class_Id']);
        $this->setIntakeId($data['Intake_Id']);
        $this->save();
    }


    private $dbc=NULL;
    private $id="";
    private $User_Id="";
    private $Class_Id="";
    private $Intake_Id="";

    function __construct($dbc=NULL) {
        $this->dbc = $dbc;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getUserId() {
        return $this->User_Id;
    }

    public function setUserId($User_Id) {
        $this->User_Id = $User_Id;
    }

    public function getClassId() {
        return $this->Class_Id;
    }

    public function setClassId($Class_Id) {
        $this->Class_Id = $Class_Id;
    }

    public function getIntakeId() {
        return $this->Intake_Id;
    }

    public function setIntakeId($Intake_Id) {
        $this->Intake_Id = $Intake_Id;
    }

    public function load($id) {
        $row = $this->dbc->select("SELECT * FROM Student WHERE id = :id", array(':id' => $id));

        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setUserId($row[0]->User_Id);
            $this->setClassId($row[0]->Class_Id);
            $this->setIntakeId($row[0]->Intake_Id);
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = [
            "User_Id" => $this->getUserId(),
            "Class_Id" => $this->getClassId(),
            "Intake_Id" => $this->getIntakeId(),
        ];
        $this->dbc->insert("Student", $data);

        return $this->dbc->lastInsertId();
    }

    public function save() {
        $data = [
            "id" => $this->getId(),
            "User_Id" => $this->getUserId(),
            "Class_Id" => $this->getClassId(),
            "Intake_Id" => $this->getIntakeId(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("Student", $data, $where);
    }

    public function remove() {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("Student", $data);
    }


}
