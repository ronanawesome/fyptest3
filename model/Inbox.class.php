<?php

class Inbox {

    private $dbc=NULL;
    private $id="";
    private $Announcement_Id="";
    private $Checked="";

    function __construct($dbc=NULL) {
        $this->dbc = $dbc;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getAnnouncementId() {
        return $this->Announcement_Id;
    }

    public function setAnnouncementId($Announcement_Id) {
        $this->Announcement_Id = $Announcement_Id;
    }

    public function getChecked() {
        return $this->Checked;
    }

    public function setChecked($Checked) {
        $this->Checked = $Checked;
    }

    public function load($id) {
        $row = $this->dbc->select("SELECT * FROM Inbox WHERE id = :id", array(':id' => $id));

        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setAnnouncementId($row[0]->Announcement_Id);
            $this->setChecked($row[0]->Checked);
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = [
            "Announcement_Id" => $this->getAnnouncementId(),
            "Checked" => $this->getChecked(),
        ];
        $this->dbc->insert("Inbox", $data);

        return $this->dbc->lastInsertId();
    }

    public function save() {
        $data = [
            "id" => $this->getId(),
            "Announcement_Id" => $this->getAnnouncementId(),
            "Checked" => $this->getChecked(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("Inbox", $data, $where);
    }

    public function remove() {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("Inbox", $data);
    }


}
