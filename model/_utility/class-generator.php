<?php

/**
 *  -------------------------------------------------------------------------------------
 *  - Please read this before using it                                                  -
 *  -------------------------------------------------------------------------------------
 *  1.  Ensure that database connection is setup
 *  2.  Make sure this script is always stored under <PROJECT_DIR>/model/_utility/ folder
 *  2.  Access via http://<project>/model/_utility/class-generator.php
 *  3.  Use at your own risks.
 *  4.  Good Luck Have Fun!
 *
 *  Query String Accept:
 *  target  =   STRING (CASE SENSITIVE)
 *
 *  Eg:
 *  ?target=<Exact_TableName>
 *
 **/

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', $_SERVER['DOCUMENT_ROOT']);

require_once(ROOT . DS . 'package' . DS . 'vendor' . DS . 'autoload.php');
require_once(ROOT . DS . 'include' . DS . 'shared_function.inc.php');
require_once(ROOT . DS . 'config' . DS . 'app.config.inc.php');
require_once(ROOT . DS . 'config' . DS . 'config.' . SERVER_STATE . '.inc.php');

require_once(ROOT . DS . 'include' . DS . 'common.inc.php');
require_once(ROOT . DS . 'include' . DS . 'mysql.inc.php');

global $dbc;

$tables = $dbc->select("SHOW TABLES;");
$tables = json_decode(json_encode($tables), true);
$table_names = get_db_table_names($tables);

echo "Class Generator 101<br>";
echo "Database : " . DB_NAME . "<br>";
echo "Number of table found : " . count($table_names) . "<br>";
echo "<br><br>";

$target = (isset($_GET['target']) ? $_GET['target'] : "");
if (!empty($target)) {
    if (!in_array($target, $table_names)) exit("Target Not Found - Case Sensitive");

    $table_names = [];
    $table_names[] = $target;
}

foreach ($table_names as $table_name) {

    $m_file_name = ROOT . DS . 'model' . DS . $table_name . '.class.php';
    $file_exist = (file_exists($m_file_name)) ? intval(true) : intval(false);

    $u_fields = $dbc->select("DESCRIBE $table_name;");
    $u_fields = json_decode(json_encode($u_fields), true);
    $f_fields = get_db_table_fields($u_fields);

    $append = replace_string_in_file($m_file_name);

    $content = '';
    if (!$append) {
        $content .= generate_class_header($table_name);
    }
    $content .= generate_class_common($table_name, $f_fields);
    $content .= generate_class_closing();

    write_to_file($m_file_name, $content, $append);

    echo "COMPLETED - " . $table_name . "<br>";

}

function replace_string_in_file($filename, $keyword = 'private $dbc=NULL;')
{
    if (!file_exists($filename)) return false;

    $results = [];
    foreach (file($filename) as $key => $line) :
        $results[] = $line;
        if (strpos($line, $keyword) !== false) break;
//        if (str_contains($line, $keyword)) break;
    endforeach;

    $prev_str = '';
    array_pop($results);
    foreach ($results as $result) :
        $prev_str .= $result;
    endforeach;

    file_put_contents($filename, $prev_str);
    return true;
}

function get_db_table_names($sql_results)
{
    $results = [];
    foreach ($sql_results as $arr_content) :
        foreach ($arr_content as $value) {
            $results[] = $value;
        }
    endforeach;
    return $results;
}

function get_db_table_fields($sql_results)
{
    $results = [];
    foreach ($sql_results as $arr_content) :
        $item = [];

        $item['name'] = $arr_content['Field'];
        $item['cc_name'] = str_replace(' ', '', ucwords(str_replace('_', ' ', $arr_content['Field'])));
        $item['default'] = $arr_content['Default'];

        $results[] = $item;
    endforeach;
    return $results;
}

function generate_class_header($tbl_name)
{
    $result = '';
    $result .= '<?php' . PHP_EOL;
    $result .= PHP_EOL;
    $result .= 'class ' . $tbl_name . ' {' . PHP_EOL;
    $result .= PHP_EOL;
    return $result;
}

function generate_class_fields($fields)
{
    $result = '';
    $result .= '    private $dbc=NULL;' . PHP_EOL;
    foreach ($fields as $field):
        $result .= '    private $' . $field['name'] . '="";' . PHP_EOL;
    endforeach;
    $result .= PHP_EOL;
    return $result;
}

function generate_class_setter_getters($fields)
{
    $result = '';

    // Constructor
    $result .= '    function __construct($dbc=NULL) {' . PHP_EOL;
    $result .= '        $this->dbc = $dbc;' . PHP_EOL;
    $result .= '    }' . PHP_EOL;

    foreach ($fields as $field):
        // Getter
        $result .= '    public function get' . $field['cc_name'] . '() {' . PHP_EOL;
        $result .= '        return $this->' . $field['name'] . ';' . PHP_EOL;
        $result .= '    }' . PHP_EOL;
        $result .= PHP_EOL;

        // Setter
        $result .= '    public function set' . $field['cc_name'] . '($' . $field['name'] . ') {' . PHP_EOL;
        $result .= '        $this->' . $field['name'] . ' = $' . $field['name'] . ';' . PHP_EOL;
        $result .= '    }' . PHP_EOL;
        $result .= PHP_EOL;
    endforeach;
    return $result;
}

function generate_class_common($table_name, $fields)
{
    $result = '';

    $result .= generate_class_fields($fields);
    $result .= generate_class_setter_getters($fields);
    $result .= generate_class_load_section($table_name, $fields);
    $result .= generate_class_create_section($table_name, $fields);
    $result .= generate_class_save_section($table_name, $fields);
    $result .= generate_class_remove_section($table_name, $fields);

    return $result;
}

function generate_class_load_section($table_name, $fields)
{
    $result = '';
    $result .= '    public function load($id) {' . PHP_EOL;
    $result .= '        $row = $this->dbc->select("SELECT * FROM ' . $table_name . ' WHERE id = :id", array(\':id\' => $id));' . PHP_EOL;
    $result .= PHP_EOL;
    $result .= '        if (!empty($row)) {' . PHP_EOL;
    foreach ($fields as $field) :
        $result .= '            $this->set' . $field['cc_name'] . '($row[0]->' . $field['name'] . ');' . PHP_EOL;
    endforeach;
    $result .= '            return true;' . PHP_EOL;
    $result .= '        } else {' . PHP_EOL;
    $result .= '            return false;' . PHP_EOL;
    $result .= '        }' . PHP_EOL;
    $result .= '    }' . PHP_EOL;
    $result .= PHP_EOL;

    return $result;
}

function generate_class_create_section($table_name, $fields)
{
    $result = '';
    $result .= '    public function create() {' . PHP_EOL;
    $result .= '        $data = [' . PHP_EOL;
    foreach ($fields as $key => $field) :
        if ($field['name'] == 'id') continue;
        if ($field['default'] == "CURRENT_TIMESTAMP") continue;
        $result .= '            "' . $field['name'] . '" => $this->get' . $field['cc_name'] . '(),' . PHP_EOL;
    endforeach;
    $result .= '        ];' . PHP_EOL;
    $result .= '        $this->dbc->insert("' . $table_name . '", $data);' . PHP_EOL;
    $result .= PHP_EOL;
    $result .= '        return $this->dbc->lastInsertId();' . PHP_EOL;
    $result .= '    }' . PHP_EOL;
    $result .= PHP_EOL;

    return $result;
}

function generate_class_save_section($table_name, $fields)
{
    $result = '';
    $result .= '    public function save() {' . PHP_EOL;
    $result .= '        $data = [' . PHP_EOL;
    foreach ($fields as $field) :
        $result .= '            "' . $field['name'] . '" => $this->get' . $field['cc_name'] . '(),' . PHP_EOL;
    endforeach;
    $result .= '        ];' . PHP_EOL;
    $result .= '        $where = array("' . $fields[0]['name'] . '" => $this->get' . $fields[0]['cc_name'] . '());' . PHP_EOL;
    $result .= PHP_EOL;
    $result .= '        return $this->dbc->update("' . $table_name . '", $data, $where);' . PHP_EOL;
    $result .= '    }' . PHP_EOL;
    $result .= PHP_EOL;

    return $result;
}

function generate_class_remove_section($table_name, $fields)
{
    $result = '';
    $result .= '    public function remove() {' . PHP_EOL;
    $result .= '        $data = array("' . $fields[0]['name'] . '" => $this->get' . $fields[0]['cc_name'] . '());' . PHP_EOL;
    $result .= PHP_EOL;
    $result .= '        return $this->dbc->delete("' . $table_name . '", $data);' . PHP_EOL;
    $result .= '    }' . PHP_EOL;
    $result .= PHP_EOL;

    return $result;
}

function generate_class_closing()
{
    $result = '';
    $result .= PHP_EOL;
    $result .= '}';

    return $result;
}

function write_to_file($file, $content, $append = false)
{
    if (!file_exists(dirname($file))) mkdir(dirname($file), 0777, true);
    if ($append) {
        file_put_contents($file, $content . PHP_EOL, FILE_APPEND | LOCK_EX);
    } else {
        file_put_contents($file, $content);
    }
}