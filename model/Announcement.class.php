<?php

class Announcement {
    public function createAnnouncement($data, $user_id,$file){
        $tempYtarget_Ids = "";
        foreach($data['Year_Target'] as $iid){
            $tempYtarget_Ids = $iid . "," .  $tempYtarget_Ids ;
        }
        if (!is_null($file)){
            $this->setFile(str_replace($_SERVER['DOCUMENT_ROOT'] . DS, "",$file));
        }
        $this->setTitle($data['Title']);
   $this->setCreatorId($user_id);
//        $this->setFile($data['File']);

        $this->setYearTarget(rtrim( $tempYtarget_Ids,','));
        $this->setDescription($data['Description']);
        $this->setCreatedDatetime(date("Y-m-d h:i:sa"));
        $this->create();
    }
//    public function checkTarget($id){
//        $row = $this->dbc->select("SELECT * FROM Announcement WHERE id IN complete = 1", array(':pid' => $player_id, ':tid' =>$this->getId()));
//        if(!empty($row)){
//            return true;
//        } else {
//            return false;
//        }
//    }

    private $dbc=NULL;
    private $id="";
    private $Creator_Id="";
    private $Title="";
    private $Description="";
    private $Created_Datetime="";
    private $File="";
    private $Year_Target="";

    function __construct($dbc=NULL) {
        $this->dbc = $dbc;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getCreatorId() {
        return $this->Creator_Id;
    }

    public function setCreatorId($Creator_Id) {
        $this->Creator_Id = $Creator_Id;
    }

    public function getTitle() {
        return $this->Title;
    }

    public function setTitle($Title) {
        $this->Title = $Title;
    }

    public function getDescription() {
        return $this->Description;
    }

    public function setDescription($Description) {
        $this->Description = $Description;
    }

    public function getCreatedDatetime() {
        return $this->Created_Datetime;
    }

    public function setCreatedDatetime($Created_Datetime) {
        $this->Created_Datetime = $Created_Datetime;
    }

    public function getFile() {
        return $this->File;
    }

    public function setFile($File) {
        $this->File = $File;
    }

    public function getYearTarget() {
        return $this->Year_Target;
    }

    public function setYearTarget($Year_Target) {
        $this->Year_Target = $Year_Target;
    }

    public function load($id) {
        $row = $this->dbc->select("SELECT * FROM Announcement WHERE id = :id", array(':id' => $id));

        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setCreatorId($row[0]->Creator_Id);
            $this->setTitle($row[0]->Title);
            $this->setDescription($row[0]->Description);
            $this->setCreatedDatetime($row[0]->Created_Datetime);
            $this->setFile($row[0]->File);
            $this->setYearTarget($row[0]->Year_Target);
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = [
            "Creator_Id" => $this->getCreatorId(),
            "Title" => $this->getTitle(),
            "Description" => $this->getDescription(),
            "File" => $this->getFile(),
            "Year_Target" => $this->getYearTarget(),
        ];
        $this->dbc->insert("Announcement", $data);

        return $this->dbc->lastInsertId();
    }

    public function save() {
        $data = [
            "id" => $this->getId(),
            "Creator_Id" => $this->getCreatorId(),
            "Title" => $this->getTitle(),
            "Description" => $this->getDescription(),
            "Created_Datetime" => $this->getCreatedDatetime(),
            "File" => $this->getFile(),
            "Year_Target" => $this->getYearTarget(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("Announcement", $data, $where);
    }

    public function remove() {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("Announcement", $data);
    }


}
