<?php

class Absence {

    private $dbc=NULL;
    private $id="";
    private $Attendance_Id="";
    private $Reason="";

    function __construct($dbc=NULL) {
        $this->dbc = $dbc;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getAttendanceId() {
        return $this->Attendance_Id;
    }

    public function setAttendanceId($Attendance_Id) {
        $this->Attendance_Id = $Attendance_Id;
    }

    public function getReason() {
        return $this->Reason;
    }

    public function setReason($Reason) {
        $this->Reason = $Reason;
    }

    public function load($id) {
        $row = $this->dbc->select("SELECT * FROM Absence WHERE id = :id", array(':id' => $id));

        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setAttendanceId($row[0]->Attendance_Id);
            $this->setReason($row[0]->Reason);
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = [
            "Attendance_Id" => $this->getAttendanceId(),
            "Reason" => $this->getReason(),
        ];
        $this->dbc->insert("Absence", $data);

        return $this->dbc->lastInsertId();
    }

    public function save() {
        $data = [
            "id" => $this->getId(),
            "Attendance_Id" => $this->getAttendanceId(),
            "Reason" => $this->getReason(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("Absence", $data, $where);
    }

    public function remove() {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("Absence", $data);
    }


}
