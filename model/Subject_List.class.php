<?php

class Subject_List {

    private $dbc=NULL;
    private $id="";
    private $Student_id="";
    private $Subject_ids="";

    function __construct($dbc=NULL) {
        $this->dbc = $dbc;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getStudentId() {
        return $this->Student_id;
    }

    public function setStudentId($Student_id) {
        $this->Student_id = $Student_id;
    }

    public function getSubjectIds() {
        return $this->Subject_ids;
    }

    public function setSubjectIds($Subject_ids) {
        $this->Subject_ids = $Subject_ids;
    }

    public function load($id) {
        $row = $this->dbc->select("SELECT * FROM Subject_List WHERE id = :id", array(':id' => $id));

        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setStudentId($row[0]->Student_id);
            $this->setSubjectIds($row[0]->Subject_ids);
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = [
            "Student_id" => $this->getStudentId(),
            "Subject_ids" => $this->getSubjectIds(),
        ];
        $this->dbc->insert("Subject_List", $data);

        return $this->dbc->lastInsertId();
    }

    public function save() {
        $data = [
            "id" => $this->getId(),
            "Student_id" => $this->getStudentId(),
            "Subject_ids" => $this->getSubjectIds(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("Subject_List", $data, $where);
    }

    public function remove() {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("Subject_List", $data);
    }


}
