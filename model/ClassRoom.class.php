<?php

class ClassRoom {

    public function loadclassinfo($id) {
        $row = $this->dbc->select("SELECT * FROM ClassRoom WHERE CTeacher_Id = :id", array(':id' => $id));
        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setClassLevel($row[0]->Class_Level);
            $this->setClassName($row[0]->Class_Name);
            $this->setCapacity($row[0]->Capacity);
            $this->setCTeacherId($row[0]->CTeacher_Id);
            return true;
        } else {
            return false;
        }
    }

    public  function checkTeacherClass($user_ID){
        $row = $this->dbc->select("SELECT CTeacher_Id FROM ClassRoom WHERE
                                      CTeacher_Id = :user_ID", array(':user_ID' => $user_ID));
        if (!empty($row)) {
            return true;
        } else {
            return false;
        }
    }

    public function searchClassroom($Class_Name) {
        $row = $this->dbc->select("SELECT id FROM ClassRoom WHERE Class_Name = :Class_Name", array(':Class_Name' => $Class_Name));
        if (!empty($row)) {
            return $this->load($row[0]->id);
        } else {
            return false;
        }
    }
    public function numberOfStudentsInClass($Class_Name) {
        $row = $this->dbc->select("SELECT student_Id, COUNT(*) FROM ClassRoom WHERE Class_Name = :Class_Name GROUP BY Class_Name", array(':Class_Name' => $Class_Name));
        if (!empty($row)) {
            return $this->load($row[0]->id);
        } else {
            return false;
        }
    }
    public function updateClassTeacher($data){
        if($data['CTeacher_Id']=="NULL"){
            $this->setCTeacherId(null);
        }else{
            $this->setCTeacherId($data['CTeacher_Id']);
        }
        $this->save();
    }



    private $dbc=NULL;
    private $id="";
    private $Class_Name="";
    private $Capacity="";
    private $Class_Level="";
    private $CTeacher_Id="";

    function __construct($dbc=NULL) {
        $this->dbc = $dbc;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getClassName() {
        return $this->Class_Name;
    }

    public function setClassName($Class_Name) {
        $this->Class_Name = $Class_Name;
    }

    public function getCapacity() {
        return $this->Capacity;
    }

    public function setCapacity($Capacity) {
        $this->Capacity = $Capacity;
    }

    public function getClassLevel() {
        return $this->Class_Level;
    }

    public function setClassLevel($Class_Level) {
        $this->Class_Level = $Class_Level;
    }

    public function getCTeacherId() {
        return $this->CTeacher_Id;
    }

    public function setCTeacherId($CTeacher_Id) {
        $this->CTeacher_Id = $CTeacher_Id;
    }

    public function load($id) {
        $row = $this->dbc->select("SELECT * FROM ClassRoom WHERE id = :id", array(':id' => $id));

        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setClassName($row[0]->Class_Name);
            $this->setCapacity($row[0]->Capacity);
            $this->setClassLevel($row[0]->Class_Level);
            $this->setCTeacherId($row[0]->CTeacher_Id);
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = [
            "Class_Name" => $this->getClassName(),
            "Capacity" => $this->getCapacity(),
            "Class_Level" => $this->getClassLevel(),
            "CTeacher_Id" => $this->getCTeacherId(),
        ];
        $this->dbc->insert("ClassRoom", $data);

        return $this->dbc->lastInsertId();
    }

    public function save() {
        $data = [
            "id" => $this->getId(),
            "Class_Name" => $this->getClassName(),
            "Capacity" => $this->getCapacity(),
            "Class_Level" => $this->getClassLevel(),
            "CTeacher_Id" => $this->getCTeacherId(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("ClassRoom", $data, $where);
    }

    public function remove() {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("ClassRoom", $data);
    }


}
