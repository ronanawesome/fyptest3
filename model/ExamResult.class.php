<?php

class ExamResult {

    private $class = null;
    public function getStudent($refresh = false)
    {
        if (is_null($this->class) || $refresh) {
            $rows = $this->dbc->select("SELECT id FROM Student WHERE User_id = :id", array(':id' => $this->getStudentID()));
            $this->class = false;
            $obj = new ClassRoom($this->dbc);

            if (isset($rows) && !empty($rows)) {
                $obj->load($rows[0]->id);
                $this->class = clone $obj;
            } else {
                return false;
            }
        }}






    private $dbc=NULL;
    private $id="";
    private $Exam_ID="";
    private $User_ID="";
    private $Exam_Grade="";
    private $Exam_Marks="";

    function __construct($dbc=NULL) {
        $this->dbc = $dbc;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getExamID() {
        return $this->Exam_ID;
    }

    public function setExamID($Exam_ID) {
        $this->Exam_ID = $Exam_ID;
    }

    public function getUserID() {
        return $this->User_ID;
    }

    public function setUserID($User_ID) {
        $this->User_ID = $User_ID;
    }

    public function getExamGrade() {
        return $this->Exam_Grade;
    }

    public function setExamGrade($Exam_Grade) {
        $this->Exam_Grade = $Exam_Grade;
    }

    public function getExamMarks() {
        return $this->Exam_Marks;
    }

    public function setExamMarks($Exam_Marks) {
        $this->Exam_Marks = $Exam_Marks;
    }

    public function load($id) {
        $row = $this->dbc->select("SELECT * FROM ExamResult WHERE id = :id", array(':id' => $id));

        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setExamID($row[0]->Exam_ID);
            $this->setUserID($row[0]->User_ID);
            $this->setExamGrade($row[0]->Exam_Grade);
            $this->setExamMarks($row[0]->Exam_Marks);
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = [
            "Exam_ID" => $this->getExamID(),
            "User_ID" => $this->getUserID(),
            "Exam_Grade" => $this->getExamGrade(),
            "Exam_Marks" => $this->getExamMarks(),
        ];
        $this->dbc->insert("ExamResult", $data);

        return $this->dbc->lastInsertId();
    }

    public function save() {
        $data = [
            "id" => $this->getId(),
            "Exam_ID" => $this->getExamID(),
            "User_ID" => $this->getUserID(),
            "Exam_Grade" => $this->getExamGrade(),
            "Exam_Marks" => $this->getExamMarks(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("ExamResult", $data, $where);
    }

    public function remove() {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("ExamResult", $data);
    }


}
