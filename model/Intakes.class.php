<?php

class Intakes {


    public function searchIntakes($intake) {
        $row = $this->dbc->select("SELECT id FROM Intakes WHERE Intake_Code = :intake", array(':intake' => $intake));
        if (!empty($row)) {
            return $this->load($row[0]->id);
        } else {
            return false;
        }
    }
    private $dbc=NULL;
    private $id="";
    private $Intake_Code="";
    private $Year="";
    private $No_of_Students="";
    private $Active_Status="";
    private $is_delete="";

    function __construct($dbc=NULL) {
        $this->dbc = $dbc;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getIntakeCode() {
        return $this->Intake_Code;
    }

    public function setIntakeCode($Intake_Code) {
        $this->Intake_Code = $Intake_Code;
    }

    public function getYear() {
        return $this->Year;
    }

    public function setYear($Year) {
        $this->Year = $Year;
    }

    public function getNoOfStudents() {
        return $this->No_of_Students;
    }

    public function setNoOfStudents($No_of_Students) {
        $this->No_of_Students = $No_of_Students;
    }

    public function getActiveStatus() {
        return $this->Active_Status;
    }

    public function setActiveStatus($Active_Status) {
        $this->Active_Status = $Active_Status;
    }

    public function getIsDelete() {
        return $this->is_delete;
    }

    public function setIsDelete($is_delete) {
        $this->is_delete = $is_delete;
    }

    public function load($id) {
        $row = $this->dbc->select("SELECT * FROM Intakes WHERE id = :id", array(':id' => $id));

        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setIntakeCode($row[0]->Intake_Code);
            $this->setYear($row[0]->Year);
            $this->setNoOfStudents($row[0]->No_of_Students);
            $this->setActiveStatus($row[0]->Active_Status);
            $this->setIsDelete($row[0]->is_delete);
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = [
            "Intake_Code" => $this->getIntakeCode(),
            "Year" => $this->getYear(),
            "No_of_Students" => $this->getNoOfStudents(),
            "Active_Status" => $this->getActiveStatus(),
            "is_delete" => $this->getIsDelete(),
        ];
        $this->dbc->insert("Intakes", $data);

        return $this->dbc->lastInsertId();
    }

    public function save() {
        $data = [
            "id" => $this->getId(),
            "Intake_Code" => $this->getIntakeCode(),
            "Year" => $this->getYear(),
            "No_of_Students" => $this->getNoOfStudents(),
            "Active_Status" => $this->getActiveStatus(),
            "is_delete" => $this->getIsDelete(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("Intakes", $data, $where);
    }

    public function remove() {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("Intakes", $data);
    }


}
