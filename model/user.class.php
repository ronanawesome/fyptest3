<?php

class user {
    public function searchUser($username) {
        $row = $this->dbc->select("SELECT id FROM user WHERE Username = :username", array(':username' => $username));
        if (!empty($row)) {
            return $this->load($row[0]->id);
        } else {
            return false;
        }
    }

    public function searchEmail($email) {
        $row = $this->dbc->select("SELECT id FROM user WHERE email = :email", array(':email' => $email));
        if (!empty($row)) {
            return $this->load($row[0]->id);
        } else {
            return false;
        }
    }
    public function checkGender($userID) {
    $row = $this->dbc->select("SELECT Gender FROM user WHERE Id = :userID", array(':userID' => $userID));
    if (!empty($row)) {
        return $this->load($row[0]->id);
    } else {
        return false;
    }
}
    public function checkApproved($username) {
        $row = $this->dbc->select("SELECT id FROM user WHERE Username = :username AND RegistrationApproval= ''", array(':username' => $username));
        if (!empty($row)) {
            return $this->load($row[0]->id);
        } else {
            return false;
        }
    }

    public function approve($data) {

        $this->setRegistrationApproval($data['RegistrationApproval']);
        $this->save();

    }

    public function reject($admin_remark) {
        $this->setRegistrationApproval("rejected");
           $this->save();
    }

    private $user = null;
    public function getUser($refresh = false) {
        if (is_null($this->user) || $refresh) {
            $rows = $this->dbc->select("SELECT id FROM user WHERE id = :id", [':id' => $this->getId()]);
            $this->user = false;
            $obj = new user($this->dbc);
            if (!empty($rows)) {
                $obj->load($rows[0]->id);
                $this->user = clone $obj;
            }
        }

        return $this->user;
    }


    private $dbc=NULL;
    private $id="";
    private $Username="";
    private $Fname="";
    private $Lname="";
    private $Email="";
    private $Password="";
    private $Birthday="";
    private $Gender="";
    private $PhoneNo="";
    private $City="";
    private $PostCode="";
    private $Street="";
    private $CreatedDate="";
    private $Role="";
    private $RegistrationApproval="";
    private $Religion="";
    private $is_delete="";

    function __construct($dbc=NULL) {
        $this->dbc = $dbc;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getUsername() {
        return $this->Username;
    }

    public function setUsername($Username) {
        $this->Username = $Username;
    }

    public function getFname() {
        return $this->Fname;
    }

    public function setFname($Fname) {
        $this->Fname = $Fname;
    }

    public function getLname() {
        return $this->Lname;
    }

    public function setLname($Lname) {
        $this->Lname = $Lname;
    }

    public function getEmail() {
        return $this->Email;
    }

    public function setEmail($Email) {
        $this->Email = $Email;
    }

    public function getPassword() {
        return $this->Password;
    }

    public function setPassword($Password) {
        $this->Password = $Password;
    }

    public function getBirthday() {
        return $this->Birthday;
    }

    public function setBirthday($Birthday) {
        $this->Birthday = $Birthday;
    }

    public function getGender() {
        return $this->Gender;
    }

    public function setGender($Gender) {
        $this->Gender = $Gender;
    }

    public function getPhoneNo() {
        return $this->PhoneNo;
    }

    public function setPhoneNo($PhoneNo) {
        $this->PhoneNo = $PhoneNo;
    }

    public function getCity() {
        return $this->City;
    }

    public function setCity($City) {
        $this->City = $City;
    }

    public function getPostCode() {
        return $this->PostCode;
    }

    public function setPostCode($PostCode) {
        $this->PostCode = $PostCode;
    }

    public function getStreet() {
        return $this->Street;
    }

    public function setStreet($Street) {
        $this->Street = $Street;
    }

    public function getCreatedDate() {
        return $this->CreatedDate;
    }

    public function setCreatedDate($CreatedDate) {
        $this->CreatedDate = $CreatedDate;
    }

    public function getRole() {
        return $this->Role;
    }

    public function setRole($Role) {
        $this->Role = $Role;
    }

    public function getRegistrationApproval() {
        return $this->RegistrationApproval;
    }

    public function setRegistrationApproval($RegistrationApproval) {
        $this->RegistrationApproval = $RegistrationApproval;
    }

    public function getReligion() {
        return $this->Religion;
    }

    public function setReligion($Religion) {
        $this->Religion = $Religion;
    }

    public function getIsDelete() {
        return $this->is_delete;
    }

    public function setIsDelete($is_delete) {
        $this->is_delete = $is_delete;
    }

    public function load($id) {
        $row = $this->dbc->select("SELECT * FROM user WHERE id = :id", array(':id' => $id));

        if (!empty($row)) {
            $this->setId($row[0]->id);
            $this->setUsername($row[0]->Username);
            $this->setFname($row[0]->Fname);
            $this->setLname($row[0]->Lname);
            $this->setEmail($row[0]->Email);
            $this->setPassword($row[0]->Password);
            $this->setBirthday($row[0]->Birthday);
            $this->setGender($row[0]->Gender);
            $this->setPhoneNo($row[0]->PhoneNo);
            $this->setCity($row[0]->City);
            $this->setPostCode($row[0]->PostCode);
            $this->setStreet($row[0]->Street);
            $this->setCreatedDate($row[0]->CreatedDate);
            $this->setRole($row[0]->Role);
            $this->setRegistrationApproval($row[0]->RegistrationApproval);
            $this->setReligion($row[0]->Religion);
            $this->setIsDelete($row[0]->is_delete);
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = [
            "Username" => $this->getUsername(),
            "Fname" => $this->getFname(),
            "Lname" => $this->getLname(),
            "Email" => $this->getEmail(),
            "Password" => $this->getPassword(),
            "Birthday" => $this->getBirthday(),
            "Gender" => $this->getGender(),
            "PhoneNo" => $this->getPhoneNo(),
            "City" => $this->getCity(),
            "PostCode" => $this->getPostCode(),
            "Street" => $this->getStreet(),
            "Role" => $this->getRole(),
            "RegistrationApproval" => $this->getRegistrationApproval(),
            "Religion" => $this->getReligion(),
            "is_delete" => $this->getIsDelete(),
        ];
        $this->dbc->insert("user", $data);

        return $this->dbc->lastInsertId();
    }

    public function save() {
        $data = [
            "id" => $this->getId(),
            "Username" => $this->getUsername(),
            "Fname" => $this->getFname(),
            "Lname" => $this->getLname(),
            "Email" => $this->getEmail(),
            "Password" => $this->getPassword(),
            "Birthday" => $this->getBirthday(),
            "Gender" => $this->getGender(),
            "PhoneNo" => $this->getPhoneNo(),
            "City" => $this->getCity(),
            "PostCode" => $this->getPostCode(),
            "Street" => $this->getStreet(),
            "CreatedDate" => $this->getCreatedDate(),
            "Role" => $this->getRole(),
            "RegistrationApproval" => $this->getRegistrationApproval(),
            "Religion" => $this->getReligion(),
            "is_delete" => $this->getIsDelete(),
        ];
        $where = array("id" => $this->getId());

        return $this->dbc->update("user", $data, $where);
    }

    public function remove() {
        $data = array("id" => $this->getId());

        return $this->dbc->delete("user", $data);
    }


}
