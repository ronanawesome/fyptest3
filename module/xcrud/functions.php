<?php

function complaint_soft_delete($primary_key, $xcrud)
{
    global $dbc;

    $obj = new ComplaintReport($dbc);
    $obj->load($primary_key);
    $obj->setIsDelete(1);
    $obj->save();
}
function user_soft_delete($primary_key, $xcrud)
{
    global $dbc;

    $obj = new user($dbc);
    $obj->load($primary_key);
    $obj->setIsDelete(1);

    $obj->save();
}
function intake_soft_delete($primary_key, $xcrud)
{
    global $dbc;
    $obj = new Intakes($dbc);
    $obj->load($primary_key);
    $obj->setIsDelete(1);

    $obj->save();
}
//function class_intactive($primary_key, $xcrud)
//{
//    global $dbc;
//    $obj = new ClassRoom($dbc);
//    $obj->load($primary_key);
//    $obj->set(1);
//
//    $obj->save();
//}


function convert_percentage($value, $fieldname, $primary_key, $row, $xcrud)
{
    $string = "%";
    return number_format($value*100,0) . $string;
}

function hash_password($postdata, $xcrud)
{	$postdata->set('password', PasswordHasher::HashPassword($postdata->get('password')));
}

function round_off_2digit_helper($value, $fieldname, $primary_key, $row, $xcrud)
{
    return number_format($value, 2);
}

function slugify_string($value, $fieldname, $primary_key, $row, $xcrud)
{
    return ucwords(slugify_with_space($value));
}

function slugify_with_space($url)
{
	# Prep string with some basic normalization
	$url = strtolower($url);
	$url = strip_tags($url);
	$url = stripslashes($url);
	$url = html_entity_decode($url);

	# Remove quotes (can't, etc.)
	$url = str_replace('\'', '', $url);

	# Replace non-alpha numeric with hyphens
	$match = '/[^a-z0-9]+/';
	$replace = ' ';
	$url = preg_replace($match, $replace, $url);

	$url = trim($url, '-');

	return $url;
}

function slugify($url)
{
	# Prep string with some basic normalization
	$url = strtolower($url);
	$url = strip_tags($url);
	$url = stripslashes($url);
	$url = html_entity_decode($url);

	# Remove quotes (can't, etc.)
	$url = str_replace('\'', '', $url);

	# Replace non-alpha numeric with hyphens
	$match = '/[^a-z0-9]+/';
	$replace = '-';
	$url = preg_replace($match, $replace, $url);

	$url = trim($url, '-');

	return $url;
}

function slugify_before_insert($postdata, $xcrud)
{
	if ($postdata->get('name')) {
		$url = slugify($postdata->get('name'));
		$postdata->set('slug', $url);
	}
}

function slugify_before_update($postdata, $primary_key, $xcrud)
{
	if ($postdata->get('name')) {
		$url = slugify($postdata->get('name'));
		$postdata->set('slug', $url);
	}
}

function after_language_insert($postdata,$primary_key, $xcrud){

    $new_language = $primary_key;
    $db = Xcrud_db::get_instance();
    $sql = 'SELECT id FROM ZSettingLanguage WHERE is_default=1 LIMIT 1';
    $db->query($sql);
    $row=$db->row();
    $default_language=$row['id'];

    $sql = "INSERT INTO ZSettingVocabulary(language_id,key_value,key_text)SELECT '$new_language' as language_id,key_value,key_text FROM  SettingVocabulary  WHERE language_id='$default_language'";
    $db->query($sql);

}//function for this crud

function after_language_remove($primary_key){
    $db = Xcrud_db::get_instance();
    $sql = 'DELETE FROM ZSettingVocabulary WHERE language_id='.$primary_key.'';
    $db->query($sql);
}

function after_vocabulary_insert($postdata,$primary_key, $xcrud){

    $last_id = $primary_key;

    $db = Xcrud_db::get_instance();
    $sql = 'SELECT key_value FROM ZSettingVocabulary WHERE id = '.$last_id;
    $db->query($sql);
    $row=$db->row();
    $key_value=$row['key_value'];
    $sql = 'SELECT key_text FROM ZSettingVocabulary WHERE id = '.$last_id;
    $db->query($sql);
    $row=$db->row();
    $key_text=addslashes($row['key_text']);


    $sql = 'SELECT id FROM ZSettingLanguage WHERE ZSettingLanguage.id!=1';
    $db->query($sql);

    $q=array();

    while ($row = $db->result->fetch_assoc()) {
        $q[]='INSERT INTO ZSettingVocabulary (language_id,key_value,key_text) VALUES ('.$row['id'].',\''.$key_value.'\',\''.$key_text.'\')';
    }//end of while

    foreach ($q as $query) {
        $db->query($query);
    }

    global $dbc;
    $language = new Language($dbc);
    $language->rewrite_from_xcrud();
}

function vocabulary_remove_replacer($primary_key, $xcrud){

    $new_language = $primary_key;
    $db = Xcrud_db::get_instance();
    $sql = 'SELECT key_value FROM ZSettingVocabulary WHERE id='.$primary_key;
    $db->query($sql);
    $row=$db->row();
    $sql = 'DELETE FROM ZSettingVocabulary WHERE key_value=\''.$row['key_value'].'\'';
    $db->query($sql);

    global $dbc;
    $language = new Language($dbc);
    $language->rewrite_from_xcrud();
}

function after_vocabulary_update($postdata, $primary, $xcrud)
{

    global $dbc;
    $language = new Language($dbc);
    $language->rewrite_from_xcrud();

}

function after_insert_debug($postdata,$primary_key, $xcrud){
    echo 'Postdata <pre>'.print_r($postdata).'</pre>';
    echo 'Primary_key : '.$primary_key.'';
}
function after_update_debug($postdata,$primary_key, $xcrud){
    echo 'Postdata <pre>'.print_r($postdata).'</pre>';
    echo 'Primary_key : '.$primary_key.'';
}

function after_insert_update_category($postdata, $primary_key, $xcrud) {
    global $dbc;

    $category = new ZCommonCategory($dbc);
    $category->load($primary_key);

    $slug = text_2_slug($category->getName());

    $category->setSlug($slug);
    $category->save();
}

function wyswyg_input($value, $field, $primary_key, $list, $xcrud)
{
    $name = $xcrud->fieldname_encode($field);
    echo "<script>
            $(document).ready(function() {
               $('.". $name ."').summernote({
                    tabsize: 2,
                    height: 300,
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                        ['fontname', ['fontname']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ol', 'ul', 'paragraph', 'height']],
                        ['table', ['table']],
                        ['insert', ['link', 'video', 'picture']],
                        ['view', ['undo', 'redo', 'fullscreen', 'codeview', 'help']]
                    ],
                    fontSizes: ['8', '9', '10', '11', '12', '14', '15', '16', '18', '19', '20'],
                    lang: 'zh-TW', 
                }).on('summernote.enter', function(we, e) {
                      $(this).summernote('pasteHTML', '<br>&VeryThinSpace;');
                      e.preventDefault();
                });
                
                var fontPalatte = $('#foreColorPalette > div > div > button:nth-child(1)');
                var primaryColor = '#27b7eb';
                fontPalatte.css('background-color', primaryColor);
                fontPalatte.attr('data-value', primaryColor);
                fontPalatte.attr('aria-label', primaryColor);
                fontPalatte.attr('data-original-title', primaryColor);
            })
          </script>";

    if (!empty($value)) {
        $newValue = str_replace("'", "\\'", $value);
        $newValue = preg_replace('/\s+/S', " ", $newValue);

        echo "<script>
            $(document).ready(function()    {
               $('.". $name ."').summernote('code', '" . $newValue . "');
            })
          </script>";
    }

    return '<textarea name="' . $name . '" class="xcrud-input '. $name .'"></textarea>';
}

function product_description_insert($postdata, $xcrud){
    $longDesc = $postdata->get('long_description');
    $specification = $postdata->get('specification');

    switch (getLang()) {
        case 'zs':

            $extraDesc = $postdata->get('extra_description_zs');
            $postdata->set('extra_description_zs', htmlspecialchars_decode($extraDesc));
            break;

        default:
           $extraDesc = $postdata->get('extra_description_en');
           $postdata->set('extra_description_en', htmlspecialchars_decode($extraDesc));
    };


    $postdata->set('long_description', htmlspecialchars_decode($longDesc));
    $postdata->set('specification', htmlspecialchars_decode($specification));
}

function after_product_insert($postdata, $primary_id, $xcrud)
{
    global $dbc;

    $default_attribute = new ProductAttribute($dbc);
    $default_attribute->setName("Default");
    $default_attribute->setProductId($primary_id);
    $default_attribute->setStock(-1);
    $default_attribute->setIsDefault(1);
    $default_attribute->setPriorityOrder(100);
    $default_attribute->setIsActive(1);

    $default_attribute->create();
}

function after_courier_insert($postdata, $xcrud)
{
    global $dbc;

    $common = new ZCommonCategory($dbc);
    $common->load($postdata->get('courier_service_id'));

    $postdata->set('courier_name', $common->getName());
    $postdata->set('courier_slug', $common->getSlug());
}

function courier_service_soft_delete($primary_key, $xcrud)
{
    global $dbc;

    $obj = new ZCourierService($dbc);
    $obj->load($primary_key);
    $obj->setIsDelete(1);

    $obj->save();
}

function group_category_retrieve($value, $fieldname, $primary_key, $row, $xcrud)
{
    if (empty($value)) {
        return '-- NONE --';
    }

    return $value;
}

function shipping_status_label($value, $fieldname, $primary_key, $row, $xcrud)
{
    $label = '';
    $str = '<span class="label label-lg label-light-%s label-inline">%s</span>';

    switch ($value) {
        case 'Processing':
            $label = 'warning';
            break;

        case 'Packed':
            $label = 'primary';
            break;

        case 'Shipped':
            $label = 'success';
            break;

        case 'Cancelled':
            $label = 'danger';
            break;

        case 'Store In Warehouse':
            $label = 'info';
            break;

        case 'Walk In':
            $label = 'info';
            break;

        default:
            return '';
            break;
    }

    return sprintf($str, $label ,$value);
}

function payment_status_label($value, $fieldname, $primary_key, $row, $xcrud)
{
    $label = '';
    $str = '<span class="label label-lg label-light-%s label-inline">%s</span>';

    switch ($value) {
        case 'Pending Payment':
            $label = 'warning';
            break;

        case 'Paid':
            $label = 'success';
            break;

        case 'Store In Warehouse':
            $label = 'info';
            break;

        case 'Rejected':
            $label = 'danger';
            break;

        default:
            return '';
            break;
    }

    return sprintf($str, $label ,$value);
}

function status_label($value, $fieldname, $primary_key, $row, $xcrud)
{
    $label = '';
    $str = '<span class="label label-lg label-light-%s label-inline">%s</span>';

    switch ($value) {
        case 'Pending':
            $label = 'warning';
            break;

        case 'Approved':
            $label = 'success';
            break;

        case 'Rejected':
            $label = 'danger';
            break;

        default:
            return '';
            break;
    }

    return sprintf($str, $label ,$value);
}

function enquiry_option_is_default($xcrud)
{
    global $dbc;

    $primary = $xcrud->get('primary');

    $dbc->update('YAboutUsPhoto', array('is_default' => 0), array(1 => 1));

    $option = new YAboutUsPhoto($dbc);
    $option->load($primary);
    $option->setIsDefault(1);
    $option->save();
}

function done_contact($xcrud)
{
    global $dbc;

    $primary = $xcrud->get('primary');

    $option = new FormSubmission($dbc);
    $option->load($primary);
    $option->setIsDone(1);
    $option->save();
}
