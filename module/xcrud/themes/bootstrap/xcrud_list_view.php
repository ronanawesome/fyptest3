<?php echo $this->render_table_name(); ?>
<?php if ($this->is_create or $this->is_csv or $this->is_print){?>
        <div class="xcrud-top-actions">
            <div class="btn-group pull-right">
                <?php echo $this->print_button('btn btn-default','glyphicon glyphicon-print');
                echo $this->csv_button('btn btn-default','glyphicon glyphicon-file'); ?>
            </div>
            <?php echo $this->add_button('btn btn-success','fas fa-plus'); ?>
            <?php echo $this->render_search(); ?>
            <div class="clearfix"></div>
        </div>
<?php }else{ ?>
	
	<div class="xcrud-top-actions">
	<?php echo $this->render_search(); ?>
	<div class="clearfix"></div>
	</div>
<?php	} ?>
        
        <div class="xcrud-list-container">
        <table class="xcrud-list table table-bordered table-hover table-bordered m-table m-table--head-bg-brand">
            <thead>
                <?php echo $this->render_grid_head('tr', 'th'); ?>
            </thead>
            <tbody class="text-dark">
                <?php echo $this->render_grid_body('tr', 'td'); ?>
            </tbody>
            <tfoot>
                <?php echo $this->render_grid_footer('tr', 'td'); ?>
            </tfoot>
        </table>
        </div>
        
        <div class="xcrud-nav m-datatable__pager m-datatable--paging-loaded clearfix">
        
        	<div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer" style="display:inline;margin-top:10px;margin-left:14px;margin-right:20px;">
              <div class="row" style="display:inline;">
                <div class="dataTables_pager" style="display:inline;">
                  <div class="dataTables_paginate paging_simple_numbers" id="m_table_1_paginate" ><?php echo $this->render_pagination(); ?></div>
                </div>
              </div>
            </div>
        
            <?php echo $this->render_limitlist(true); ?>
            
            
            
            <?php echo $this->render_benchmark(); ?>
            
        </div>
