<?php echo $this->render_table_name($mode); ?>

<div class="xcrud-top-actions btn-group w-100">
    <?php
    echo $this->render_button('return','list','','btn btn-primary btn-block text-light w-100 py-4'); 
	?>
</div>

<div class="xcrud-view" style="margin-top:20px;">
	
<?php
	
$primary_key = $this->result_row['primary_key'];

global $dbc;
	
$draw = new Draw($dbc);
$draw->load($primary_key);
	
$results=$draw->getDrawResult();
	
echo '<div class="row">';
foreach($results as $result){
		
?>
<div class="col-sm-6 col-md-6 col-lg-4">

    <table class="table table-bordered table-condensed text-center font-weight-bold">
        <tr>
            <th class="text-light bg-warning" colspan="5"><?php echo $result->getProvider()->getName(); ?></th>
        </tr>
        
        <tr>
            <td colspan="5"><?php echo $draw->getNiceDate() ?></td>
        </tr>
        
        <tr>
            <td class="bg-dark text-light text-center" colspan="3">First</td>
            <td colspan="2"><?php echo $result->getData()['First']; ?></td>
        </tr>
        
        <tr>
            <td class="bg-dark text-light text-center" colspan="3">Second</td>
            <td colspan="2"><?php echo $result->getData()['Second']; ?></td>
        </tr>
        
        <tr>
            <td class="bg-dark text-light text-center" colspan="3">Third</td>
            <td colspan="2"><?php echo $result->getData()['Third']; ?></td>
        </tr>
        
        <tr>
            <td class="bg-dark text-light text-center" colspan="5">Special</td>
        </tr>
        
        <tr>
            <td><?php echo $result->getData()['Special01']; ?></td>
            <td><?php echo $result->getData()['Special02']; ?></td>
            <td><?php echo $result->getData()['Special03']; ?></td>
            <td><?php echo $result->getData()['Special04']; ?></td>
            <td><?php echo $result->getData()['Special05']; ?></td>
        </tr>
        
        <tr>
            <td><?php echo $result->getData()['Special06']; ?></td>
            <td><?php echo $result->getData()['Special07']; ?></td>
            <td><?php echo $result->getData()['Special08']; ?></td>
            <td><?php echo $result->getData()['Special09']; ?></td>
            <td><?php echo $result->getData()['Special10']; ?></td>
        </tr>
        
        <tr>
            <td class="bg-dark text-light text-center" colspan="5">Consolation</td>
        </tr>
        
        <tr>
            <td><?php echo $result->getData()['Consolation01']; ?></td>
            <td><?php echo $result->getData()['Consolation02']; ?></td>
            <td><?php echo $result->getData()['Consolation03']; ?></td>
            <td><?php echo $result->getData()['Consolation04']; ?></td>
            <td><?php echo $result->getData()['Consolation05']; ?></td>
        </tr>
        
        <tr>
            <td><?php echo $result->getData()['Consolation06']; ?></td>
            <td><?php echo $result->getData()['Consolation07']; ?></td>
            <td><?php echo $result->getData()['Consolation08']; ?></td>
            <td><?php echo $result->getData()['Consolation09']; ?></td>
            <td><?php echo $result->getData()['Consolation10']; ?></td>
        </tr>
        
    </table>
</div>
<?php		
		
}

echo '</div>';

?>
	
</div>
<div class="xcrud-nav">
    <?php echo $this->render_benchmark(); ?>
</div>
