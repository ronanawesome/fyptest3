<?php

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));

spl_autoload_register('my_autoloader');

include_once('..' . DS . '..' . DS . 'config' . DS . 'app.config.inc.php');
include_once('..' . DS . '..' . DS . 'include' . DS . 'shared_function.inc.php');
include_once('..' . DS . '..' . DS . 'config' . DS . 'config.' . SERVER_STATE . '.inc.php');
require_once('..' . DS . '..' . DS . 'module' . DS . 'kint' . DS . 'Kint.class.php');
include_once('..' . DS . '..' . DS . 'include' . DS . 'mysql.inc.php');


function my_autoloader($className)
{

    if (file_exists('..' . DS . '..' . DS . 'classes' . DS . $className . '.class.php')) {
        require_once('..' . DS . '..' . DS . 'classes' . DS . $className . '.class.php');
    } else if (file_exists('..' . DS . '..' . DS . 'template' . DS . $className . '.class.php')) {
        require_once('..' . DS . '..' . DS . 'template' . DS . $className . '.class.php');
    } else if (file_exists('..' . DS . '..' . DS . 'model' . DS . $className . '.class.php')) {
        require_once('..' . DS . '..' . DS . 'model' . DS . $className . '.class.php');
    } else if (file_exists('..' . DS . '..' . DS . 'model' . DS . 'helper' . DS . $className . '.class.php')) {
        require_once('..' . DS . '..' . DS . 'model' . DS . 'helper' . DS . $className . '.class.php');
    } else if (file_exists('..' . DS . '..' . DS . 'model' . DS . 'entity' . DS . $className . '.class.php')) {
        require_once('..' . DS . '..' . DS . 'model' . DS . 'entity' . DS . $className . '.class.php');
    } else {
        echo 'Class doesn\'t exist : ' . $className;
    }

}//add more else if(file_exist)(__classes_folder__) if needed

include('xcrud.php');
header('Content-Type: text/html; charset=' . Xcrud_config::$mbencoding);
echo Xcrud::get_requested_instance();
