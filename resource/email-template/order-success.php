<?php
global $dbc;
global $url;

$role = $url['call_parts'][0];

$contactUs = new YContactUs($dbc);
$contactUs->load(1);

if (isset($_SESSION['sale_id'])) {
    $obj = new UserSale($dbc);
    $obj->load($_SESSION['sale_id']);

    unset($_SESSION['sale_id']);
}
?><!doctype html>
<html lang="en-US">

<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <title>Order Successfully</title>
    <meta name="description" content="Order Successfully">
</head>
<body style="font-family: Helvetica,sans-serif;padding: 0;margin: 0;-webkit-font-smoothing: antialiased;color: #616161;">

<style>
    .check-container {
        width: 6.25rem;
        height: 7.5rem;
        display: flex;
        flex-flow: column;
        align-items: center;
        justify-content: space-between;
        margin: auto;
        padding: 15px 0;
    }

    .check-container .check-background {
        width: 100%;
        height: calc(100% - 1.25rem);
        background: linear-gradient(to bottom right, #5de593, #41d67c);
        box-shadow: 0px 0px 0px 65px rgba(255, 255, 255, 0.25) inset, 0px 0px 0px 65px rgba(255, 255, 255, 0.25) inset;
        transform: scale(0.84);
        border-radius: 50%;
        animation: animateContainer 0.75s ease-out forwards 0.75s;
        display: flex;
        align-items: center;
        justify-content: center;
        opacity: 0;
    }

    .check-container .check-background svg {
        width: 65%;
        transform: translateY(0.25rem);
        stroke-dasharray: 80;
        stroke-dashoffset: 80;
        animation: animateCheck 0.35s forwards 1.25s ease-out;
    }

    .check-container .check-shadow {
        bottom: calc(-15% - 5px);
        left: 0;
        border-radius: 50%;
        background: radial-gradient(closest-side, #49da83, transparent);
        animation: animateShadow 0.75s ease-out forwards 0.75s;
    }

    @keyframes animateContainer {
        0% {
            opacity: 0;
            transform: scale(0);
            box-shadow: 0px 0px 0px 65px rgba(255, 255, 255, 0.25) inset, 0px 0px 0px 65px rgba(255, 255, 255, 0.25) inset;
        }
        25% {
            opacity: 1;
            transform: scale(0.9);
            box-shadow: 0px 0px 0px 65px rgba(255, 255, 255, 0.25) inset, 0px 0px 0px 65px rgba(255, 255, 255, 0.25) inset;
        }
        43.75% {
            transform: scale(1.15);
            box-shadow: 0px 0px 0px 43.334px rgba(255, 255, 255, 0.25) inset, 0px 0px 0px 65px rgba(255, 255, 255, 0.25) inset;
        }
        62.5% {
            transform: scale(1);
            box-shadow: 0px 0px 0px 0px rgba(255, 255, 255, 0.25) inset, 0px 0px 0px 21.667px rgba(255, 255, 255, 0.25) inset;
        }
        81.25% {
            box-shadow: 0px 0px 0px 0px rgba(255, 255, 255, 0.25) inset, 0px 0px 0px 0px rgba(255, 255, 255, 0.25) inset;
        }
        100% {
            opacity: 1;
            box-shadow: 0px 0px 0px 0px rgba(255, 255, 255, 0.25) inset, 0px 0px 0px 0px rgba(255, 255, 255, 0.25) inset;
        }
    }

    @keyframes animateCheck {
        from {
            stroke-dashoffset: 80;
        }
        to {
            stroke-dashoffset: 0;
        }
    }

    @keyframes animateShadow {
        0% {
            opacity: 0;
            width: 100%;
            height: 15%;
        }
        25% {
            opacity: 0.25;
        }
        43.75% {
            width: 40%;
            height: 7%;
            opacity: 0.35;
        }
        100% {
            width: 85%;
            height: 15%;
            opacity: 0.25;
        }
    }
</style>
<table class="wrapper" style="margin: 0;background: #fff;border: 0;width: 100%;text-align: center;">
    <tbody>
    <tr>
        <td class="content-wrap" style="border: 0;width: 600px;text-align: center;background: #f5f5f5;">
            <table class="content" style="margin: 0 auto;border: 0;width: 600px;text-align: center;">
                <tbody>
                <tr>
                    <td class="em-spacer-1" style="height: 26px;"></td>
                </tr>
                <tr class="casper-header-row">
                    <td class="casper-header-td" style="text-align: center;">
                        <a href="javascript:void(0)">
                            <img src="<?php echo getDomain() ?>/resource/branding/logo3.png"
                                 class="casper-header-logo" margin="0 auto"
                                 style="display: block;width:50%;margin: 0 auto;border: 0!important;outline: 0!important;">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="em-spacer-1" style="height: 26px;"></td>
                </tr>

                <tr>
                    <td class="box" style="background: #fff;padding: 0 25px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                               style="margin: 0 auto;">
                            <tbody>
                            <tr>
                                <td align="center">
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                                           style="margin: 0 auto;">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <div class="check-container">
                                                    <div class="check-background">
                                                        <svg viewBox="0 0 65 51" fill="none"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M7 25L27.3077 44L58.5 7" stroke="white"
                                                                  stroke-width="13" stroke-linecap="round"
                                                                  stroke-linejoin="round"/>
                                                        </svg>
                                                    </div>
                                                    <div class="check-shadow"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="lead-wrap" style="text-align: center;width: 100%;">
                                            <td class="lead"
                                                style="font-family: Helvetica,sans-serif;text-align: center;border-collapse: collapse;font-size: 18px;letter-spacing: .045em;line-height: 24px;mso-line-height-rule: exactly;">

                                                <?php echo $obj->getUser()->getFullName() ?>,your order is purchased
                                                successfully

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="em-spacer-1" style="height: 26px;"></td>
                                        </tr>
                                        <tr>
                                            <td class="order-link-wrap" style="text-align: center;font-size: 14px;">
                                                <strong>
                                                    Order: <a href="javascript:void(0)"
                                                              style="color: #0047be;"><?php echo invoice_num($obj->getId(), 7, 'I-') ?></a>
                                                    <span class="pipe">
                                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                                    </span>
                                                    Date ordered: <?php echo $obj->getPaidDatetime() ?>
                                                </strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="em-spacer-1" style="height: 26px;"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="em-spacer-1" style="height: 26px;"></td>
                </tr>
                <tr>
                    <td class="box box-ext order-summary-box"
                        style="background: #fff;padding: 0 25px;text-align: center;">
                        <table class="order-summary"
                               style="margin: 0 auto;text-align: left;width: 546px;border-top: 2px solid #eee;">
                            <tbody>
                            <tr>
                                <td class="em-spacer-0" style="height: 20px;"></td>
                            </tr>

                            <?php foreach ($obj->getItems() as $key => $item) : ?>
                                <?php
                                $product = $item->getProduct(true);
                                $attribute = $item->getProductAttribute(true);
                                ?>
                                <tr class="line-item">
                                    <td class="line-item product-thumb-column" style="width: 80px;">
                                        <img class="product-thumb" height="80" width="80"
                                             src="<?php echo getDomain() . '/upload/product/' . $product->getPhoto() ?>"
                                             style="width: 80px;height: 80px;">
                                        <div class="row-spacer" style="height: 8px;"></div>
                                    </td>
                                    <td class="line-item product-info-column" style="width: 200px;">
                                        <div class="title" style="margin-bottom: 5px;">
                                            <?php echo $product->getName() ?>
                                        </div>
                                        <div class="sub" style="font-size: 13px;margin-bottom: 3px;">
                                            <?php echo $attribute->getName() ?>
                                        </div>
                                        <div class="sub" style="font-size: 13px;margin-bottom: 3px;">
                                            Qty: <?php echo $item->getQuantity() ?>
                                        </div>
                                    </td>
                                    <td class="line-item product-price-column"
                                        style="width: 100px;text-align: right;vertical-align: top;">
                                        <div class="sub price"
                                             style="font-size: 13px;margin-bottom: 3px;margin-top: 18px;">
                                            <?php echo CURRENCY . ' ' . $item->getProductSingleSelling() ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            <tr>
                                <td class="em-spacer-0" style="height: 20px;"></td>
                            </tr>
                            <tr>
                                <td class="divider" colspan="3" style="border-bottom: 2px solid #eee;"></td>
                            </tr>
                            <tr>
                                <td class="em-spacer-1" style="height: 26px;"></td>
                            </tr>
                            <tr class="total-row" style="font-size: 14px;">
                                <td class="copy" colspan="2" style="text-align: right;">
                                    Subtotal
                                </td>
                                <td class="value" style="text-align: right;">
                                    <?php echo CURRENCY . ' ' . $obj->getTotalSelling() ?>
                                </td>
                            </tr>
                            <tr class="total-row" style="font-size: 14px;">
                                <td class="copy" colspan="2" style="text-align: right;">
                                    Discount
                                </td>
                                <td class="value" style="text-align: right;">
                                    <?php echo CURRENCY . ' ' . $obj->getTotalDiscount() ?>
                                </td>
                            </tr>
                            <tr class="total-row total" style="font-size: 14px;">
                                <td class="copy" colspan="2" style="text-align: right;padding-top: 10px;">
                                    <strong>
                                        Total </strong>
                                </td>
                                <td class="value" style="text-align: right;padding-top: 10px;">
                                    <strong>
                                        <?php echo CURRENCY . ' ' . $obj->getGrandTotal() ?>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="em-spacer-2" style="height: 52px;"></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="em-spacer-1" style="height: 26px;"></td>
                </tr>
                <tr>
                    <td class="box box-ext referral-box"
                        style="background: white;padding: 0 25px;text-align: center;color: #fff;font-family: Helvetica">
                        <table class="referral-info" style="margin: 0 auto;">
                            <tbody>
                            <tr>
                                <td class="em-spacer-1" style="height: 26px;text-align: center;"></td>
                            </tr>
                            <tr>
                                <td class="title"
                                    style="color:black;text-align: center;padding-bottom: 10px;font-weight: 600;text-transform: uppercase;font-size: 19px;">
                                    Click <a href="<?php echo getDomain() . '/public/order-invoice/' . $obj->getId() . '?bypass=true' ?>" target="_blank">Here</a> To Generate
                                    Your Order Invoice.
                                </td>
                            </tr>
                            <tr>
                                <td class="em-spacer-1" style="height: 26px;text-align: center;"></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>


                </tbody>
            </table>
            <table style="margin: 0 auto;">
                <tbody>
                <tr>
                    <td class="em-spacer-2" style="height: 52px;"></td>
                </tr>
                <tr>
                    <td class="footer footer-1" style="color: #999;font-size: 13px;">
                        <a style="text-decoration: none" href="tel:<?php echo $contactUs->getPhone() ?>"><?php echo $contactUs->getPhone() ?></a>
                        <span class="pipe">
                            &nbsp;|&nbsp;
                        </span>
                        <a style="text-decoration: none" href="mailto:<?php echo $contactUs->getEmail() ?>"><?php echo $contactUs->getEmail() ?></a>
                    </td>
                </tr>
                <tr>
                    <td class="em-spacer-0" style="height: 20px;"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>


<table style="margin: 0 auto;">
    <tbody>
    <tr>
        <td class="copyright-footer" style="padding-top: 15px;padding-bottom: 20px;font-size: 11px;color: #b2b2b2;">
            <?php echo $contactUs->getAddress() ?> <span class="pipe">
      &nbsp;|&nbsp;
    </span>
            © <?php echo date('Y') . ' ' . SYS_NAME ?>
        </td>
    </tr>
    </tbody>
</table>

<!-- class content -->


</body>
