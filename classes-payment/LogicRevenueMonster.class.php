<?php

require_once 'package/vendor/autoload.php';

use RevenueMonster\SDK\RevenueMonster;
use RevenueMonster\SDK\Exceptions\ApiException;
use RevenueMonster\SDK\Exceptions\ValidationException;
use RevenueMonster\SDK\Request\WebPayment;
use RevenueMonster\SDK\Request\QRPay;
use RevenueMonster\SDK\Request\QuickPay;

class LogicRevenueMonster {

    protected $client = NULL;

    protected $PRIVATE_KEY = NULL;
    protected $CLIENT_ID = NULL;
    protected $CLIENT_SECRET = NULL;

    protected $STORE_ID = NULL;
    protected $LAYOUT_VER = "v3";

    protected $A_URL_TOKEN = null;
    protected $A_URL_WEBPYMT = null;

    protected $TOKEN = "";
    protected $REFRESH_TOKEN = "";

    public function __construct($config = null, $development = false) {
        if (!empty($config) && !is_null($config)) {
            $this->client = new RevenueMonster([
                'clientId' => $config['client_id'],
                'clientSecret' => $config['client_secret'],
                'privateKey' => "-----BEGIN RSA PRIVATE KEY-----" . "\r\n" . wordwrap(trim($config['private_key']), 64, "\r\n", true) . "\r\n" . "-----END RSA PRIVATE KEY-----",
                'isSandbox' => $development,
            ]);
            $this->STORE_ID = $config['store_id'];
        }
    }

    public function testConnectivity() {
        // Get merchant profile
        try {
            $response = $this->client->merchant->profile();
            return $response;
        } catch(ApiException $e) {
            echo "statusCode : {$e->getCode()}, errorCode : {$e->getErrorCode()}, errorMessage : {$e->getMessage()}";
        } catch(Exception $e) {
            echo $e->getMessage();
        }
    }

    public function newRegisterPayment($agent_id, $amount, $user_id, $redirect_url, $notify_url) {
        try {
            $wp = new WebPayment;
            $wp->order->id = $agent_id."";
            $wp->order->title = $agent_id."";
            $wp->order->currencyType = 'MYR';
            $wp->order->amount = intval(number_format($amount, 2, "", ""));
            $wp->order->detail = $agent_id."";
            $wp->order->additionalData = "";
            $wp->storeId = $this->STORE_ID;
            $wp->redirectUrl = $redirect_url;
            $wp->notifyUrl = $notify_url;
            $wp->layoutVersion = $this->LAYOUT_VER;

            $response = $this->client->payment->createWebPayment($wp);
            return $response;
        } catch (ApiException $e) {
            return ['statusCode' => $e->getCode(), 'errorCode' => $e->getErrorCode(), 'errorMessage' => $e->getMessage()];
        } catch (ValidationException $e) {
            return ['errorMessage' => $e->getMessage()];
        } catch (Exception $e) {
            return ['errorMessage' => $e->getMessage()];
        }
    }

    public function newPayment($sale_id, $amount, $user_id, $redirect_url, $notify_url) {
        try {
            $wp = new WebPayment;
            $wp->order->id = $sale_id."";
            $wp->order->title = $sale_id."";
            $wp->order->currencyType = 'MYR';
            $wp->order->amount = intval(number_format($amount, 2, "", ""));
            $wp->order->detail = $sale_id."";
            $wp->order->additionalData = "";
            $wp->storeId = $this->STORE_ID;
            $wp->redirectUrl = $redirect_url;
            $wp->notifyUrl = $notify_url;
            $wp->layoutVersion = $this->LAYOUT_VER;

            $response = $this->client->payment->createWebPayment($wp);
            return $response;
        } catch (ApiException $e) {
            return ['statusCode' => $e->getCode(), 'errorCode' => $e->getErrorCode(), 'errorMessage' => $e->getMessage()];
        } catch (ValidationException $e) {
            return ['errorMessage' => $e->getMessage()];
        } catch (Exception $e) {
            return ['errorMessage' => $e->getMessage()];
        }
    }

    public function checkTxn($sale_id) {
        try {
            $response = $this->client->payment->findByOrderId($sale_id);
            return $response;
        } catch (ApiException $e) {
            return ['statusCode' => $e->getCode(), 'errorCode' => $e->getErrorCode(), 'errorMessage' => $e->getMessage()];
        } catch (Exception $e) {
            return ['errorMessage' => $e->getMessage()];
        }
    }

}