<?php

class FormInput{

    private $default_attribute = array(
        "class" => "form-control"
    );

    private $attribute;

    function __construct(){
        $this->reset();
    }

    private function reset(){
        $this->attribute=$this->default_attribute;
    }

    public function setId($id){
        $current_attribute = isset($this->attribute['id'])?$this->attribute['id']:'';
        $this->attribute['id']=$current_attribute.' '.$id;
    }

    public function setClass($class){
        $current_attribute = isset($this->attribute['class'])?$this->attribute['class']:'';
        $this->attribute['class']=$current_attribute.' '.$class;
    }

    public function setExtra($key, $value){
        $this->attribute[$key] = $value;
    }

    public function overrideClass($class){
        $this->attribute['class']=$class;
    }

    public function setPlaceholder($ph){
        $this->attribute['placeholder']=$ph;
    }

    public function createText($name,$default){
        $this->createInput('text',$name,$default);
    }

    public function createEmail($name,$default){
        $this->createInput('email',$name,$default);
    }

    public function createNumber($name,$default){
        $this->createInput('number',$name,$default);
    }

    public function createPassword($name,$default){
        $this->createInput('password',$name,$default);
    }

    public function createHidden($name,$default){
        $this->createInput('hidden',$name,$default);
    }

    private function createInput($type,$name,$default){

        $this->setId($name);

        $value = "";
        $value = isset($_POST[$name])?$_POST[$name]:$value;
        $value = isset($_GET[$name])?$_GET[$name]:$value;
        $value = empty($value)&&!isset($_POST[$name])&&!isset($_GET[$name])?$default:$value;

        echo '<input type="'.$type.'" name="'.$name.'" id="input_'.$name.'" value="'.$value.'"';
        foreach($this->attribute as $attribute_name => $attribute_value){
            if($attribute_name==''){
                echo $attribute_value.' ';
            }else{
                echo $attribute_name.'="'.$attribute_value.'" ';
            }
        }
        echo '/>';

        $this->reset();

    }

    public function createTextarea($name,$default){

        $this->setId($name);

        $value = "";
        $value = isset($_POST[$name])?$_POST[$name]:$value;
        $value = isset($_GET[$name])?$_GET[$name]:$value;
        $value = empty($value)&&!isset($_POST[$name])&&!isset($_GET[$name])?$default:$value;

        echo '<textarea type="text" name="'.$name.'" id="input_'.$name.'"';
        foreach($this->attribute as $attribute_name => $attribute_value){
            if($attribute_name==''){
                echo $attribute_value.' ';
            }else{
                echo $attribute_name.'="'.$attribute_value.'" ';
            }
        }
        echo '>';
        echo $value;
        echo '</textarea>';

        $this->reset();

    }

    public function createSelect($name,$choice,$default){

        $this->setId($name);

        $value = "";
        $value = isset($_POST[$name])?$_POST[$name]:$value;
        $value = isset($_GET[$name])?$_GET[$name]:$value;
        $value = empty($value)&&!isset($_POST[$name])&&!isset($_GET[$name])?$default:$value;

        $option = $choice;

        echo '<select name="'.$name.'" id="'.$name.'" ';
        foreach($this->attribute as $attribute_name => $attribute_value){
            if($attribute_name==''){
                echo $attribute_value.' ';
            }else{
                echo $attribute_name.'="'.$attribute_value.'" ';
            }
        }
        echo '">';

        // Create each option:
        foreach ($option as $k => $v) {
            echo '<option value="'.$k.'" ';
            // Select the existing value:
            echo ($value==$k)?'selected="selected"':'';
            echo '>'.$v.'</option>';
        } // End of FOREACH.

        echo '</select>';

        $this->reset();

    }

    //third para must be array
    public function createMultiSelect($name,$choice,$default_array){

        $this->setId($name);

        $value = array();
        $value = isset($_POST[$name])?$_POST[$name]:$value;
        $value = isset($_GET[$name])?$_GET[$name]:$value;
        $value = empty($value)&&!isset($_POST[$name])&&!isset($_GET[$name])?$default_array:$value;

        $option = $choice;

        echo '<select name="'.$name.'[]" id="'.$name.'" ';
        foreach($this->attribute as $attribute_name => $attribute_value){
            if($attribute_name==''){
                echo $attribute_value.' ';
            }else{
                echo $attribute_name.'="'.$attribute_value.'" ';
            }
        }
        echo '">';

        // Create each option:
        foreach ($option as $k => $v) {
            echo '<option value="'.$k.'" ';
            // Select the existing value:
            if(in_array($k,$value)){
                echo 'selected="selected"';
            }
            echo '>'.$v.'</option>';
        } // End of FOREACH.

        echo '</select>';

        $this->reset();

    }

    public function createCheckbox($name,$bool){

        $this->setId($name);

        $checked_string = "";

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            if (isset($_POST[$name])){
                if($_POST[$name]=="on"){
                    $checked_string="checked";
                }
            }
        }else if(isset($_GET[$name])){
            if($_GET[$name]=="on"){
                $checked_string="checked";
            }
        }else{
            if($bool){
                $checked_string="checked";
            }
        }

        //super fix to always have this field submmited
        echo '<input type="hidden" name="'.$name.'" value="off"/>';

        echo '<input type="checkbox" name="'.$name.'" '.$checked_string.' value="on" ';


        foreach($this->attribute as $attribute_name => $attribute_value){
            if($attribute_name==''){
                echo $attribute_value.' ';
            }else{
                echo $attribute_name.'="'.$attribute_value.'" ';
            }
        }

        echo '/>';

        $this->reset();

    }

    public function createRadio($name,$value,$bool){

        $this->setId($value);

        $checked_string="";

        if (isset($_POST[$name])){
            if($_POST[$name]==$value){
                $checked_string="checked";
            }
        }else if(isset($_GET[$name])){
            if($_GET[$name]==$value){
                $checked_string="checked";
            }
        }else{
            if($bool){
                $checked_string="checked";
            }
        }

        echo '<input type="radio" value="'.$value.'" name="'.$name.'" ';

        foreach($this->attribute as $attribute_name => $attribute_value){
            if($attribute_name==''){
                echo $attribute_value.' ';
            }else{
                echo $attribute_name.'="'.$attribute_value.'" ';
            }
        }

        echo $checked_string.' />';

        $this->reset();

    }

}
