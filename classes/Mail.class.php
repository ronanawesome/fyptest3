<?php

require_once 'package/vendor/autoload.php';

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class Mail {

    public static $SMTP_HOST = null;
    public static $SMTP_PORT = null;
    public static $SMTP_NAME = null;
    public static $SMTP_UN = null;
    public static $SMTP_PW = null;

    public static function send_with_attachments($recipients, $subject, $body, $files = array()) {
        try {

            $mail = new PHPMailer(true);

            $mail->isSMTP();
            $mail->Host = SMTP_HOST;
            $mail->SMTPAuth = true;
            $mail->Username = SMTP_USERNAME;
            $mail->Password = SMTP_PASSWORD;
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            $mail->Port = SMTP_PORT;

            $mail->SetFrom(SMTP_USERNAME, SMTP_NAME);

            if (is_array($recipients)) {
                foreach ($recipients as $recipient) {
                    $mail->addAddress(trim($recipient));
                }
            } else {
                $mail->addAddress(trim($recipients));
            }

            $mail->isHTML(true);                                    // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $body;

            foreach ($files as $name => $file) {
                if(file_exists($file)) {
                    $ext = pathinfo($file, PATHINFO_EXTENSION);
                    $mail->addAttachment($file, $name . "." . $ext);
                }
            }

            if (!$mail->Send()) {
                return ['success' => false, 'error' => 1, 'message' => $mail->ErrorInfo];
            } else {
                return ['success' => true, 'error' => 2, 'message' => $mail->ErrorInfo];
            }

        } catch (Exception $e) {
            return ['success' => false, 'error' => 2, 'message' => $mail->ErrorInfo];
        }
    }

    public static function send_multiple($recipients, $subject, $body) {

        try {

            $mail = new PHPMailer(true);

            $mail->isSMTP();
            $mail->Host = self::$SMTP_HOST;
            $mail->SMTPAuth = true;
            $mail->Username = self::$SMTP_UN;
            $mail->Password = self::$SMTP_PW;
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = self::$SMTP_PORT;
            $mail->CharSet = "UTF-8";

            $mail->SetFrom(self::$SMTP_UN, self::$SMTP_NAME);

            if (is_array($recipients)) {
                foreach ($recipients as $recipient) {
                    $mail->addAddress(trim($recipient));
                }
            } else {
                $mail->addAddress(trim($recipients));
            }

            $mail->isHTML(true);                                    // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $body;

            if (!$mail->Send()) {
                return ['success' => false, 'error' => 1, 'message' => $mail->ErrorInfo];
            } else {
                return ['success' => true, 'error' => 2, 'message' => $mail->ErrorInfo];
            }

        } catch (Exception $e) {
            return ['success' => false, 'error' => 2, 'message' => $mail->ErrorInfo];
        }

    }

    public static function test_mail($recipient, $debug = true) {

        try {

            $mail = new PHPMailer(true);

            if ($debug) $mail->SMTPDebug = 2;
            $mail->isSMTP();
            $mail->Host = SMTP_HOST;
            $mail->SMTPAuth = true;
            $mail->Username = SMTP_USERNAME;
            $mail->Password = SMTP_PASSWORD;
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            $mail->Port = SMTP_PORT;

            $mail->SetFrom(SMTP_USERNAME, SMTP_NAME);

            $mail->addAddress(trim($recipient));

            $mail->isHTML(true);                                    // Set email format to HTML
            $mail->Subject = "Test Subject";
            $mail->Body = "Test Body";

            if (!$mail->Send()) {
                return ['success' => false, 'error' => 1, 'message' => $mail->ErrorInfo];
            } else {
                return ['success' => true, 'error' => 2, 'message' => $mail->ErrorInfo];
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }

}