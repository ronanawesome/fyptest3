<?php

class CustomPostcode {

    private static $data = array(
        "peninsular" => array(
            "peninsular"=>array(
                "Perlis"=> array(
                    array (
                        "from" => "01000","to" => "02000",
                    )
                ),
                "Kedah" => array(
                    array(
                        "from" => "05000", "to" => "09810",
                    )
                ),
                "Pulau Pinang" => array(
                    array(
                        "from" => "10000", "to" => "14400",
                    )
                ),
                "Kelatan" => array(
                    array(
                        "from" => "15000", "to" => "18500",
                    )
                ),
                "Terengganu" => array(
                    array(
                        "from" => "20000", "to" => "24300",
                    )
                ),
                "Pahang" => array(
                    array("from" => "25000", "to" => "28800",),
                    array("from" => "39000", "to" => "39200",),
                    array("from" => "49000", "to" => "49000",),
                    array("from" => "69000", "to" => "69000",),
                ),
                "Perak" => array(
                    array(
                        "from" => "30000", "to" => "36810",
                    )
                ),
                "Selangor" => array(
                    array("from" => "40000", "to" => "48300",),
                    array("from" => "63000", "to" => "68100",),
                ),
                "Kuala Lumpur" => array(
                    array("from" => "50000", "to" => "60000",),
                    array("from" => "50000", "to" => "60000",),
                ),
                "Putrajaya" => array(
                    array(
                        "from" => "62000", "to" => "62988",
                    )
                ),
                "Negeri Sembilan" => array(
                    array(
                        "from" => "70000", "to" => "73509",
                    )
                ),
                "Melaka" => array(
                    array(
                        "from" => "75000", "to" => "78309",
                    )
                ),
                "Johor" => array(
                    array(
                        "from" => "79000", "to" => "86900",
                    )
                ),
            ),
        ),
        "sabah" => array(
            "sabah(city)"=>array(
                "Labuan" => array(
                    array(
                        "from" => "87000", "to" => "87099",
                    )
                ),
                "Kota Kinabalu" => array(
                    array(
                        "from" => "88000", "to" => "88999"
                    )
                ),
                "Keningau" => array(
                    array(
                        "from" => "89000", "to" => "89999"
                    )
                ),
                "Sandakan" => array(
                    array(
                        "from" => "90000", "to" => "90099"
                    )
                ),
                "Tawau" => array(
                    array(
                        "from" => "91000", "to" => "91099"
                    )
                ),
                "Lahad Datu" => array(
                    array(
                        "from" => "91100", "to" => "91199"
                    )
                ),
            ),
            "sabah(oda)"=>array(
                "Sabah (ODA)" => array (
                    array(
                        "from" => "91200", "to" => "91309"
                    )
                ),
            ),
        ),
        "sarawak"=>array(
            "sarawak(city)"=>array(
                "Kuching"=>array(
                    array(
                        "from"=>"93000", "to"=>"93999"
                    )
                ),
                "Sibu"=>array(
                    array(
                        "from"=>"96000","to"=>"96099"
                    )
                ),
                "Bintulu"=>array(
                    array(
                        "from"=>"97000","to"=>"97099"
                    )
                ),
                "Miri"=>array(
                    array(
                        "from"=>"98000","to"=>"98049"
                    )
                ),
            ),
            "sarawak(oda)"=>array(
                "Sarawak (ODA)" => array(
                    array(
                        "from"=>"98050","to"=>"98859"
                    )
                ),
            ),
        ),
    );

    public static function getRegions()
    {
        return self::$data;
    }

    public static function check($postcode){
        foreach(self::$data as $region_name => $locations){
            foreach($locations as $location_type => $states){
                foreach($states as $state_name => $state_info){
                    foreach($state_info as $state_range){
                        if (in_array($postcode, range($state_range['from'],$state_range['to']))){
                                return $location_type;
                        }
                    }
                }
            }
        }
        return false;
    }
}