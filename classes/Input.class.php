<?php


//        $choice=array();
//
//        foreach($current_agent->getChildIds() as $child_agent){
//            $choice[$child_agent->id]=$child_agent->fullname;
//
//        }
//
//        $input->setClass('selectpicker form-control');
//        $input->setExtra('multiple');
//        $input->createSelectMultiple('agent-test[]',$choice,null);


class Input {

	public $errors=array();
	public $extra_classes="";
	public $placeholder="";
	public $extra="";

	public function __construct($errors=array()) {

		$this->errors=$errors;

	}//end of constructor

	public function setClass($string){

		$this->extra_classes=$string;
	}

	public function setPH($string){

		$this->placeholder=$string;
	}

	public function setExtra($string){

		$this->extra=$string;
	}

	// - The name to be given to the element.
	// - The type of element (text, password, textarea).
	public function createFormInput($name, $type , $size , $cols ="50" , $rows = "2") {

		$value = false;

		if(isset($_POST[$name])){

			$value = $_POST[$name];

		}else if(isset($_GET[$name])){

			$value = $_GET[$name];

		}

		if ($value && get_magic_quotes_gpc()) $value = stripslashes($value);

		//if ($value && get_magic_quotes_gpc()) $value = stripslashes($value);

		if ( ($type == 'text') || ($type == 'password') ) {
			echo '<input type="'. $type .'" name="'.$name.'" size="'.$size.'" id="'.$name.'" ';

			if (isset($value)) echo ' value="' . htmlspecialchars($value) . '"';

			echo ' '.$this->extra.' ';

			$this->extra="";

			echo ' placeholder="'.$this->placeholder.'" ';

			$this->placeholder="";

			echo ' class="';

			echo $this->extra_classes.' ';

			$this->extra_classes="";

			if (array_key_exists($name, $this->errors)) {
				echo ' " /> <span class="error">' . $this->errors[$name] . '</span>';
			} else {
				echo ' " />';
			}

		} elseif ($type == 'textarea') {

			echo '<textarea name="' . $name . '" id="' . $name . '" id="comments" cols="'.$cols.'" rows="'.$rows.'"';

			echo ' '.$this->extra.' ';

			$this->extra="";

			echo ' placeholder="'.$this->placeholder.'" ';

			$this->placeholder="";

			echo ' class="';


			echo $this->extra_classes.' ';

			$this->extra_classes="";

			if (array_key_exists($name, $this->errors)) {
				echo ' error">';
			} else {
				echo '" >';
			}

			if (isset($value)) echo $value;

			echo '</textarea>';

			if (array_key_exists($name, $this->errors)) echo '<br/><span class="error">' . $this->errors[$name] . '</span>';

		}

	} // End of the create_form_input() function.

	public function createFormInputWithDefault($name, $type , $size , $value , $cols ="50" , $rows = "2"){

		if(isset($_POST[$name])){

			$value = $_POST[$name];

		}else if(isset($_GET[$name])){

			$value = $_GET[$name];

		}

		if ($value && get_magic_quotes_gpc()) $value = stripslashes($value);

		if ( ($type == 'text') || ($type == 'password') ) {

			echo '<input type="'.$type.'" name="'.$name.'" size="'.$size.'" id="'.$name.'" ';

			echo ' '.$this->extra.' ';

			$this->extra="";

			echo ' placeholder="'.$this->placeholder.'" ';

			$this->placeholder="";

			if (isset($value)) echo ' value="'.htmlspecialchars($value).'"';

			echo ' class="';

			echo $this->extra_classes.' ';

			$this->extra_classes="";

			if (array_key_exists($name, $this->errors)) {
				echo 'error" /> <span class="error">' . $this->errors[$name] . '</span>';
			} else {
				echo '" />';
			}

		} elseif ($type == 'textarea') {

			echo '<textarea name="' . $name . '" id="' . $name . '" cols="'.$cols.'" rows="'.$rows.'"';

			echo ' '.$this->extra.' ';

			$this->extra="";

			echo ' placeholder="'.$this->placeholder.'" ';

			$this->placeholder="";

			echo ' class="';

			echo $this->extra_classes.' ';

			$this->extra_classes="";

			if (array_key_exists($name, $this->errors)) {
				echo 'error">';
			} else {
				echo '">';
			}

			if (isset($value)) echo $value;

			echo '</textarea>';

			if (array_key_exists($name, $this->errors)) echo '<br/><span class="error">' . $this->errors[$name] . '</span>';

		}

	}//end of create_input_with_default

	public function createSelect($name,$choice,$first_option="Please select an option",$size=1){

		$value='';

		if (isset($_POST[$name])){
			$value = $_POST[$name];
		}else if (isset($_GET[$name])) {
			$value = $_GET[$name];
		}

		$default_option=($first_option==null?array():array(''=>$first_option));
		$option = $default_option+$choice;

		//$this->errors[$name]
		echo '<select name="'.$name.'" id="'.$name.'" size="'.$size.'" ';

		echo ' '.$this->extra.' ';

		$this->extra="";

		echo ' class="';

		echo $this->extra_classes.' ';

		$this->extra_classes="";


        if (array_key_exists($name, $this->errors)) {
            echo 'error">';
        } else {
            echo '">';
        }

		// Create each option:
		foreach ($option as $k => $v) {

			echo "<option value=\"$k\"";

			// Select the existing value:
			if ($value === (string)$k) echo ' selected="selected"';

			if ($value === '' && $k === '' ){echo ' selected="selected"';}

			echo ">$v</option>\n";

		} // End of FOREACH.

		echo '</select>';

		if (array_key_exists($name, $this->errors)) echo ' <span class="error">' . $this->errors[$name] . '</span>';

	}

	public function createSelectWithDefault($name,$choice,$first_option="Please select an option",$value , $size=1){

		if (isset($_POST[$name])){
			$value = $_POST[$name];
		}else if (isset($_GET[$name])) {
			$value = $_GET[$name];
		}

		$default_option=($first_option==null?array():array(''=>$first_option));
		$option = $default_option+$choice;

		//$this->errors[$name]
		echo '<select name="'.$name.'" id="'.$name.'" size="'.$size.'" ';

		echo ' '.$this->extra.' ';

		$this->extra="";

		echo ' class="';

		echo $this->extra_classes.' ';

		$this->extra_classes="";

        if (array_key_exists($name, $this->errors)) {
            echo 'error">';
        } else {
            echo '">';
        }

		// Create each option:
		foreach ($option as $k => $v) {

			echo "<option value=\"$k\"";

            // Select the existing value:
            if(is_array($value)) {

                if (in_array((string)$k,$value)) echo ' selected="selected"';

            } else {
                if ($value === (string)$k) echo ' selected="selected"';

                if ($value === '' && $k === '' ){echo ' selected="selected"';}
            }

            echo ">$v</option>\n";

		} // End of FOREACH.

		echo '</select>';

		if (array_key_exists($name, $this->errors)) echo ' <span class="error">' . $this->errors[$name] . '</span>';

	}

	public function createCheckBox($name,$checked){

		$checked_string="";

		if ($_SERVER['REQUEST_METHOD'] == 'POST'){

			if (isset($_POST[$name]))$checked_string="checked";

		}else if(isset($_GET[$name])){

			if($_GET[$name]=="on"){
				$checked_string="checked";
			}

		}else{

			if($checked){
				$checked_string="checked";
			}

		}

		echo '<input type="checkbox" name="'.$name.'" '.$checked_string.'>';

	}

    public function createRadio($name,$value,$checked){

        $checked_string="";


        if (isset($_POST[$name])){
            if($_POST[$name]==$value){
                $checked_string="checked";
            }
        }else if(isset($_GET[$name])){
            if($_GET[$name]==$value){
                $checked_string="checked";
            }
        }else{
            if($checked){
                $checked_string="checked";
            }
        }

        echo '<input type="radio" value="'.$value.'" name="'.$name.'"';

        echo ' ' . $this->extra . ' ';

        $this->extra = "";

        echo $checked_string.' />';

    }

}//end of class
