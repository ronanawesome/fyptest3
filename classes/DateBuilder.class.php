<?php
class DateBuilder{

    private static $builder_count = 0;

    private static $date_format = "yyyy-mm-dd";
    private static $time_format = "hh:ii:00";

    private static $datetime_separator = " ";

    function __construct(){
        $this->loadJS();
    }

    public function loadJS(){
    }

    public function loadBS(){

    }

    public static function getDateFormat() {
        return self::$date_format;
    }

    public static function setDateFormat($date_format){
        self::$date_format = $date_format;
    }

    public static function getTimeFormat() {
        return self::$time_format;
    }

    public static function setTimeFormat($time_format){
        self::$time_format = $time_format;
    }

    public static function getDateTimeSeparator() {
        return self::$datetime_separator;
    }

    public static function setDateTimeSeparator($datetime_separator){
        self::$datetime_separator = $datetime_separator;
    }

    public static function getDateTimeFormat() {
        return self::$date_format . self::$datetime_separator . self::$time_format;
    }

    public function build($input_name){

        self::$builder_count++;

        $value = '';

        if(isset($_POST[$input_name])){
            $value = 'value="' . $_POST[$input_name] . '"';
        }else if(isset($_GET[$input_name])){
            $value = 'value="' . $_GET[$input_name] . '"';
        }

        ?>
        <div class="input-append success date no-padding <?php echo $input_name; ?>">
            <input name="<?php echo $input_name; ?>" type="text"
                   class="form-control text-center" <?php echo $value; ?> />
            <span class="add-on"><span class="arrow"></span><i class="icon-th"></i></span>
        </div>
        <script type="text/javascript">
            //Date Pickers
            $( document ).ready(function() {
                $('.input-append.date.<?php echo $input_name; ?>').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    format: '<?php echo self::$date_format; ?>'
                });
            });

        </script>
        <?php

    }

    public function build_with_default($input_name,$value){

        self::$builder_count++;

        if(isset($_POST[$input_name])){
            $value = $_POST[$input_name];
        }else if(isset($_GET[$input_name])){
            $value = $_GET[$input_name];
        }

        ?>
        <div class="input-append success date no-padding <?php echo $input_name; ?>">
            <input name="<?php echo $input_name; ?>" type="text" class="form-control text-center"
                   value="<?php echo $value; ?>"/>
            <span class="add-on"><span class="arrow"></span><i class="icon-th"></i></span>
        </div>
        <script type="text/javascript">
            //Date Pickers
            $( document ).ready(function() {
                $('.input-append.date.<?php echo $input_name; ?>').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    format: '<?php echo self::$date_format; ?>'
                });
            });

        </script>
        <?php
    }

    public function build_range($input_1,$input_2){

        self::$builder_count++;

        $today = date('Y-m-d', time());
        $yesterday = date('Y-m-d', time() - ((60 * 60 * 24) * 1));
        $last_week = date('Y-m-d', time() - ((60 * 60 * 24) * 7));
        $last_month = date('Y-m-d', time() - ((60 * 60 * 24) * 30));
        $old_date = "2020-01-01";

        $value_1 = '';
        $value_2 = '';

        if (isset($_POST[$input_1])) {
            $value_1 = $_POST[$input_1];
        } else if (isset($_GET[$input_1])) {
            $value_1 = $_GET[$input_1];
        }else{
            $value_1 = $today;
        }

        if (isset($_POST[$input_2])) {
            $value_2 = $_POST[$input_2];
        } else if (isset($_GET[$input_2])) {
            $value_2 = $_GET[$input_2];
        } else {
            $value_2 = $today;
        }

        ?>
        <script type="text/javascript">

            $(document).ready(function () {
                $(".dp-today").click(function () {
                    $("input[name='start_date']").val("<?php echo $today; ?>");
                    $("input[name='end_date']").val("<?php echo $today; ?>");
                    //$( "#filter_form" ).submit();
                    $(this).closest('form').submit();
                });
                $(".dp-yesterday").click(function () {
                    $("input[name='start_date']").val("<?php echo $yesterday; ?>");
                    $("input[name='end_date']").val("<?php echo $yesterday; ?>");
                    $(this).closest('form').submit();
                });
                $(".dp-last-week").click(function () {
                    $("input[name='start_date']").val("<?php echo $last_week; ?>");
                    $("input[name='end_date']").val("<?php echo $today; ?>");
                    $(this).closest('form').submit();
                });
                $(".dp-last-month").click(function () {
                    $("input[name='start_date']").val("<?php echo $last_month; ?>");
                    $("input[name='end_date']").val("<?php echo $today; ?>");
                    $(this).closest('form').submit();
                });
                $(".dp-all-time").click(function () {
                    $("input[name='start_date']").val("<?php echo $old_date; ?>");
                    $("input[name='end_date']").val("<?php echo $today; ?>");
                    $(this).closest('form').submit();
                });
            });

            function submitForm() {

            }

        </script>
        <div style="margin-bottom:5px;">
            <a class="btn btn-default dp-today"><?php echo _diary_today; ?></a>
            <a class="btn btn-default dp-yesterday"><?php echo _yesterday; ?></a>
            <a class="btn btn-default dp-last-week"><?php echo _last_week; ?></a>
            <a class="btn btn-default dp-last-month"><?php echo _last_month; ?></a>
            <a class="btn btn-default dp-all-time"><?php echo _all_time; ?></a>
        </div>
        <div class="input-daterange input-group success date <?php echo $input_1 . $input_2; ?>" id="datepicker">
            <div class="input-group-text"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
            <input type="text" autocomplete="off" class="form-control text-center"
                   name="<?php echo $input_1; ?>" value="<?php echo $value_1; ?>"/>
            <div class="input-group-text"><span class="input-group-text"><i class="fa fa-arrow-right"></i></span>
            </div>
            <input type="text" autocomplete="off" class="form-control text-center"
                   name="<?php echo $input_2; ?>" value="<?php echo $value_2; ?>"/>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                $('.input-daterange.<?php echo $input_1 . $input_2; ?>').datepicker({
                    todayBtn: "linked",
                    autoclose: true,
                    todayHighlight: true,
                    format: '<?php echo self::$date_format; ?>',
                    orientation: "bottom"
                });
            });
        </script>
        <?php

    }

    public function build_range_with_default($input_1,$default_1,$input_2,$default_2){

        self::$builder_count++;

        $value_1 = '';
        $value_2 = '';

        if (isset($_POST[$input_1])) {
            $value_1 = $_POST[$input_1];
        } else if (isset($_GET[$input_1])) {
            $value_1 = $_GET[$input_1];
        } else {
            $value_1 = $default_1;
        }

        if (isset($_POST[$input_2])) {
            $value_2 = $_POST[$input_2];
        } else if (isset($_GET[$input_2])) {
            $value_2 = $_GET[$input_2];
        } else {
            $value_2 = $default_2;
        }

        ?>
        <div class="input-daterange input-group success date no-padding <?php echo $input_1 . $input_2; ?>"
             id="datepicker">
            <div class="col-lg-6 col-12" style="padding: 0 !important;margin: 0 !important;">
                <div class="input-group no-padding">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                    <input type="text" autocomplete="off" class="form-control text-center"
                           name="<?php echo $input_1; ?>" value="<?php echo $value_1; ?>"/>
                </div>
            </div>
            <div class="col-lg-6 col-12" style="padding: 0 !important;margin: 0 !important;">
                <div class="input-group no-padding">
                    <span class="input-group-text"><i class="fa fa-arrow-right"></i></span>
                    <input type="text" autocomplete="off" class="form-control text-center"
                           name="<?php echo $input_2; ?>" value="<?php echo $value_2; ?>"/>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.input-daterange.<?php echo $input_1 . $input_2; ?>').datepicker({
                    todayBtn: "linked",
                    autoclose: true,
                    todayHighlight: true,
                    format: '<?php echo self::$date_format; ?>',
                    orientation: 'bottom'
                });
            });
        </script>
        <?php

    }

    public function build_time($input_1)
    {

        self::$builder_count++;

        $value_1 = '';

        if (isset($_POST[$input_1])) {
            $value_1 = $_POST[$input_1];
        } else if (isset($_GET[$input_1])) {
            $value_1 = $_GET[$input_1];
        }

        ?>
        <div class="input-daterange input-group success date no-padding <?php echo $input_1; ?>" id="datepicker">
            <div class="input-group-text"><span class="input-group-text"><i class="fa fa-clock-o"></i></span></div>
            <input type="text" autocomplete="off" class="form-control text-center" name="<?php echo $input_1; ?>"
                   id="<?php echo $input_1; ?>" value="<?php echo $value_1; ?>"/>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#<?php echo $input_1; ?>").timepicker({
                    minuteStep: 1,
                    showMeridian: false,
                    showSeconds: true,
                    defaultTime: '<?php echo $value_1; ?>'
                });
            });
        </script>
        <?php

    }

    public function build_time_range($input_1, $input_2)
    {

        self::$builder_count++;

        $value_1 = '';
        $value_2 = '';

        if (isset($_POST[$input_1])) {
            $value_1 = $_POST[$input_1];
        } else if (isset($_GET[$input_1])) {
            $value_1 = $_GET[$input_1];
        }

        if (isset($_POST[$input_2])) {
            $value_2 = $_POST[$input_2];
        } else if (isset($_GET[$input_2])) {
            $value_2 = $_GET[$input_2];
        }
        ?>
        <div class="input-daterange input-group success date no-padding <?php echo $input_1 . $input_2; ?>"
             id="datepicker">
            <div class="col-lg-6" style="padding: 0 !important;margin: 0 !important;">
                <div class="input-group no-padding">
                    <div class="input-group-text"><span class="input-group-text"><i class="fa fa-clock-o"></i></span></div>
                    <input type="text" autocomplete="off" class="form-control text-center" name="<?php echo $input_1; ?>"
                           id="<?php echo $input_1; ?>" value="<?php echo $value_1; ?>"/>
                </div>
            </div>
            <div class="col-lg-6" style="padding: 0 !important;margin: 0 !important;">
                <div class="input-group no-padding">
                    <div class="input-group-text"><span class="input-group-text"><i class="fa fa-arrow-right"></i></span></div>
                    <input type="text" autocomplete="off" class="form-control text-center" name="<?php echo $input_2; ?>"
                           id="<?php echo $input_2; ?>" value="<?php echo $value_2; ?>"/>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#<?php echo $input_1; ?>").timepicker({
                    minuteStep: 1,
                    showMeridian: false,
                    showSeconds: true,
                    defaultTime: '<?php echo $value_1; ?>'
                });
                $("#<?php echo $input_2; ?>").timepicker({
                    minuteStep: 1,
                    showMeridian: false,
                    showSeconds: true,
                    defaultTime: '<?php echo $value_2; ?>'
                });
            });
        </script>
        <?php
    }

    public function build_time_range_with_default($input_1, $input_2, $default_1, $default_2)
    {

        self::$builder_count++;

        $value_1 = '';
        $value_2 = '';

        if (isset($_POST[$input_1])) {
            $value_1 = $_POST[$input_1];
        } else if (isset($_GET[$input_1])) {
            $value_1 = $_GET[$input_1];
        } else {
            $value_1 = $default_1;
        }

        if (isset($_POST[$input_2])) {
            $value_2 = $_POST[$input_2];
        } else if (isset($_GET[$input_2])) {
            $value_2 = $_GET[$input_2];
        } else {
            $value_2 = $default_2;
        }

        ?>
        <div class="input-daterange input-group success date no-padding <?php echo $input_1 . $input_2; ?>"
             id="datepicker">
            <div class="col-lg-6" style="padding: 0 !important;margin: 0 !important;">
                <div class="input-group no-padding">
                    <span class="input-group-text"><i class="far fa-clock"></i></span>
                    <input type="text" autocomplete="off" class="form-control text-center" name="<?php echo $input_1; ?>"
                           id="<?php echo $input_1; ?>" value="<?php echo $value_1; ?>"/>
                </div>
            </div>
            <div class="col-lg-6" style="padding: 0 !important;margin: 0 !important;">
                <div class="input-group no-padding">
                    <span class="input-group-text"><i class="fa fa-arrow-right"></i></span>
                    <input type="text" autocomplete="off" class="form-control text-center" name="<?php echo $input_2; ?>"
                           id="<?php echo $input_2; ?>" value="<?php echo $value_2; ?>"/>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#<?php echo $input_1; ?>").timepicker({
                    minuteStep: 1,
                    showMeridian: false,
                    showSeconds: true,
                    defaultTime: '<?php echo $value_1; ?>'
                });
                $("#<?php echo $input_2; ?>").timepicker({
                    minuteStep: 1,
                    showMeridian: false,
                    showSeconds: true,
                    defaultTime: '<?php echo $value_2; ?>'
                });
            });
        </script>
        <?php

    }

}