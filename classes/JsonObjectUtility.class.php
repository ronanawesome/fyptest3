<?php

class JsonObjectUtility {

    //this method translate object that create from class
    public static function translate($object, $exception = array()) {
        $predefined_exception = array('dbc');
        $exception = array_merge($exception, $predefined_exception);
        try {
            $reflectionClass = new ReflectionClass(get_class($object));

            $array = array();
            foreach ($reflectionClass->getProperties() as $property) {
                $property->setAccessible(true);
                if (in_array($property->getName(), $exception)) {
                    $property->setAccessible(false);
                    continue;
                }
                $array[$property->getName()] = $property->getValue($object);
                $property->setAccessible(false);
            }
            return json_encode($array);
        } catch (ReflectionException $e) {
            echo "Exception thrown: " . $e;
        }
        return json_encode(array());
    }

}