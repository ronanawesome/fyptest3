<?php

class Collection {

    public static function getParentChild($dbc) {
        $rows = $dbc->select('SELECT id FROM ParentChild');
        $result = array();
        foreach ($rows as $v) {
            $obj = new ParentChild($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }
    public static function getParents($dbc) {
        $rows = $dbc->select('SELECT id FROM user WHERE Role="Parent"');
        $result = array();
        foreach ($rows as $v) {
            $obj = new user($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }
    public static function getTeacherfromClass($dbc) {
        $rows = $dbc->select('SELECT CTeacher_Id FROM ClassRoom ');
        $result = array();
        foreach ($rows as $v) {
            $obj = new user($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }
    public static function getStudentfromClass($data, $dbc)
    {
        $rows = $dbc->select("SELECT id FROM Student WHERE Class_Id = :classid ", array(':classid' => $data['class_id']));
        $result = array();
        foreach ($rows as $v) {
            $obj = new Student($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }
    public static function getStudentsfromclass($dbc) {
        $rows = $dbc->select('SELECT user.id FROM user INNER JOIN Student ON user.id=Student.User_Id WHERE user,Role="Student" AND Student.Class_ID= ');
        $result = array();
        foreach ($rows as $v) {
            $obj = new user($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }
    public static function getStudents($dbc) {
        $rows = $dbc->select('SELECT id FROM user WHERE Role="Student"');
        $result = array();
        foreach ($rows as $v) {
            $obj = new user($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }
    public static function getTeachers($dbc) {
    $rows = $dbc->select('SELECT id FROM user WHERE Role="Teacher"');
    $result = array();
    foreach ($rows as $v) {
        $obj = new user($dbc);
        if ($obj->load($v->id)) {
            $result[] = clone $obj;
        }
    }
    return $result;
}
    public static function getTeachersNotInClass($dbc) {
        $rows = $dbc->select('SELECT id FROM user WHERE Role="Teacher" AND id NOT IN (SELECT CTeacher_Id FROM ClassRoom)');
        $result = array();
        foreach ($rows as $v) {
            $obj = new user($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }
    public static function getTeachersInClass($dbc) {
        $rows = $dbc->select('SELECT id FROM user WHERE Role="Teacher" AND id IN (SELECT CTeacher_Id FROM ClassRoom)');
        $result = array();
        foreach ($rows as $v) {
            $obj = new user($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }
    public static function getStudentDetails($dbc) {
        $rows = $dbc->select('SELECT id FROM Student  ');
        $result = array();
        foreach ($rows as $v) {
            $obj = new Student($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }







    public static function getClassRoom($dbc) {
    $rows = $dbc->select('SELECT id FROM ClassRoom');
    $result = array();
    foreach ($rows as $v) {
        $obj = new ClassRoom($dbc);
        if ($obj->load($v->id)) {
            $result[] = clone $obj;
        }
    }
    return $result;
}
    public static function getIntake($dbc)
    {
        $rows = $dbc->select('SELECT id FROM Intakes WHERE Active_Status=1 OR is_delete=0 ');
        $result = array();
        foreach ($rows as $v) {
            $obj = new Intakes($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }


    public static function getSubject($dbc)
    {
        $rows = $dbc->select('SELECT id FROM Subject');
        $result = array();
        foreach ($rows as $v) {
            $obj = new Subject($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }
    public static function getAnnouncement($dbc)
    {
        $rows = $dbc->select('SELECT id FROM Announcement');
        $result = array();
        foreach ($rows as $v) {
            $obj = new Announcement($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }
    public static function getReports($dbc)
    {
        $rows = $dbc->select('SELECT id FROM ComplaintReport');
        $result = array();
        foreach ($rows as $v) {
            $obj = new ComplaintReport($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }





    public static function getYear($dbc)
    {
        $rows = $dbc->select('SELECT id FROM Subject');
        $result = array();
        foreach ($rows as $v) {
            $obj = new Subject($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }
    public static function getTopSelling($dbc) {
        $rows = $dbc->select('SELECT id FROM SaleItem group by product_id order by SUM(total_nett) desc limit 1');
        $result = array();
        foreach ($rows as $v) {
            $obj = new SaleItem($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getAgentIncentives($dbc){
        $sql = '';
        $sql .= "Select id ";
        $sql .= "FROM AgentGroup ";

        $rows = $dbc->select($sql);
        $result = array();
        foreach($rows as $v){
            $obj = new AgentGroup($dbc);
            if ($obj->load($v->id)){
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getWholesalePackageStats($dbc, $s_date, $e_date) {
        $s_dt = new DateTime($s_date);
        $e_dt = new DateTime($e_date);

        $sql = '';
        $sql .= "SELECT ZProduct.id, ZProduct.product_name, ZPackageWholesale.tier, ZPackageWholesale.package_name, COUNT(Wholesale.id) as total_subscription ";
        $sql .= "FROM ZPackageWholesale ";
        $sql .= "JOIN ZProduct ON ZProduct.id = ZPackageWholesale.product_id ";
        $sql .= "LEFT JOIN Wholesale ON ";
        $sql .= '   Wholesale.package_id = ZPackageWholesale.id ';
        $sql .= '   AND DATE(Wholesale.created_datetime) >= "' . $s_dt->format('Y-m-d') . '"';
        $sql .= '   AND DATE(Wholesale.created_datetime) <= "' . $e_dt->format('Y-m-d') . '"';
        $sql .= "GROUP BY ZProduct.id, ZPackageWholesale.id ";

        $rows = $dbc->select($sql);
        $result = json_decode(json_encode($rows),true);

        return $result;
    }

    public static function getAgentPackagesStats($dbc, $s_date, $e_date) {
        $s_dt = new DateTime($s_date);
        $e_dt = new DateTime($e_date);

        $sql = '';
        $sql .= "SELECT ZProduct.id, ZProduct.product_name, ZPackageAgent.tier, ZPackageAgent.package_name, COUNT(Agent.id) as total_subscription ";
        $sql .= "FROM ZPackageAgent ";
        $sql .= "JOIN ZProduct ON ZProduct.id = ZPackageAgent.product_id ";
        $sql .= "LEFT JOIN Agent ON ";
        $sql .= '   Agent.package_id = ZPackageAgent.id ';
        $sql .= '   AND DATE(Agent.created_datetime) >= "' . $s_dt->format('Y-m-d') . '"';
        $sql .= '   AND DATE(Agent.created_datetime) <= "' . $e_dt->format('Y-m-d') . '"';
        $sql .= "GROUP BY ZProduct.id, ZPackageAgent.id ";


        $rows = $dbc->select($sql);
        $result = json_decode(json_encode($rows),true);

        return $result;
    }

    public static function getWholesalePackagesCount($dbc, $s_date, $e_date) {
        $s_dt = new DateTime($s_date);
        $e_dt = new DateTime($e_date);

        $sql = '';
        $sql .= "SELECT product_name, package_id, package_name, COUNT(package_id) ";
        $sql .= "FROM Wholesale ";
        $sql .= "JOIN ZPackageWholesale ON ZPackageWholesale.id = Wholesale.package_id ";
        $sql .= "JOIN ZProduct ON ZProduct.id = ZPackageWholesale.product_id ";
        $sql .= "AND DATE(Wholesale.created_datetime) >= '" . $s_dt->format('Y-m-d') . "' ";
        $sql .= "AND DATE(Wholesale.created_datetime) <= '" . $e_dt->format('Y-m-d') . "' ";
        $sql .= "GROUP BY package_id ";

        $rows = $dbc->select($sql);
        $result = json_decode(json_encode($rows),true);

        return $result;
    }

    public static function getWholesaleRestockCount($dbc) {
        $rows = $dbc->select(
            "SELECT COUNT(id) as num_rows FROM Sale WHERE refer_type = 'Wholesale' AND is_paid = 0 AND is_reject = 0 AND is_restock = 1"
        );
        return (!empty($rows) ? $rows[0]->num_rows : 0);
    }

    public static function getWholesaleApprovalCount($dbc) {
        $rows = $dbc->select(
            "SELECT COUNT(id) as num_rows FROM Wholesale WHERE is_approval = 0 AND is_reject = 0"
        );
        return (!empty($rows) ? $rows[0]->num_rows : 0);
    }

    public static function getSettings($dbc, $group) {
        $sql = '';
        $sql .= 'SELECT id FROM ZSetting ';
        $sql .= 'WHERE field_group = "' . $group . '" ';

        $rows = $dbc->select($sql);
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZSetting($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getShippingStatusCount($dbc, $status) {
        $rows = $dbc->select(
            "SELECT COUNT(id) as num_rows FROM Sale WHERE shipping_status = :s",
            [':s' => $status]
        );
        return (!empty($rows) ? $rows[0]->num_rows : 0);
    }


    public static function getAgentPackages($dbc) {
        $sql = "";
        $sql .= "SELECT id FROM ZPackageAgent";
        $rows = $dbc->select($sql);
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZPackageAgent($dbc);
            if ($obj->load($v->id)) {
                $result[] = $obj;
            }
        }
        return $result;
    }

    public static function getWholesalePackages($dbc) {
        $sql = "";
        $sql .= "SELECT id FROM ZPackageWholesale";
        $rows = $dbc->select($sql);
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZPackageWholesale($dbc);
            if ($obj->load($v->id)) {
                $result[] = $obj;
            }
        }
        return $result;
    }

//    public static function getProducts($dbc) {
//        $sql = "";
//        $sql .= "SELECT id FROM ZProduct";
//        $rows = $dbc->select($sql);
//        $result = array();
//        foreach ($rows as $v) {
//            $obj = new ZProduct($dbc);
//            if ($obj->load($v->id)) {
//                $result[] = $obj;
//            }
//        }
//        return $result;
//    }

    public static function getWholesalePackage($dbc) {
        $sql = "";
        $sql .= "SELECT id, package_name, package_price FROM ZPackageWholesale";
        $rows = $dbc->select($sql);
        $result = array();
        foreach ($rows as $v) {
            $result[$v->id]['id'] = $v->id;
            $result[$v->id]['package_name'] = $v->package_name;
            $result[$v->id]['package_price'] = $v->package_price;
        }
        return $result;
    }


    public static function ProductGallery($dbc, $product_id) {

        $sql = '';
        $sql .= 'SELECT id ';
        $sql .= 'FROM ZProductGallery ';
        if(!empty($product_id)) $sql .= 'WHERE product_id = ' . $product_id . ' ';

        $rows = $dbc->select($sql);
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZProductGallery($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }


    public static function filterProducts($dbc, $tag) {

        $sql = '';
        $sql .= 'SELECT id ';
        $sql .= 'FROM ZProduct ';
        if(!empty($tag)) $sql .= 'AND product_tags LIKE "%' . $tag . '%" ';
        $sql .= "GROUP BY id";

        $rows = $dbc->select($sql);
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZProduct($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function customerFilterProducts($dbc, $tag, $only_customer = 0) {

        $sql = '';
        $sql .= 'SELECT id ';
        $sql .= 'FROM ZProduct ';
        $sql .= 'WHERE only_customer = "' . $only_customer . '" ';
        $sql .= 'AND is_delete = 0 ';
        if(!empty($tag)) $sql .= 'AND product_tags LIKE "%' . $tag . '%" ';
        $sql .= "GROUP BY id ";
        $sql .= 'ORDER BY priority_order DESC';

        $rows = $dbc->select($sql);
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZProduct($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getCategoryObjs($dbc) {
        $sql = "";
        $sql .= "SELECT id, name_en,name_zs,name_my FROM ZProductTag ORDER BY priority_order desc";
        $rows = $dbc->select($sql);
        $result = array();
        foreach ($rows as $v) {
            $result[$v->id]['id'] = $v->id;
            if($_SESSION['lang']=='en'){
                $result[$v->id]['name_en'] = $v->name_en;
            }elseif ($_SESSION['lang']=='my'){
                $result[$v->id]['name_my'] = $v->name_my;
            }else{
                $result[$v->id]['name_zs'] = $v->name_zs;
            }
        }
        return $result;
    }

    public static function getGroupCategories($dbc) {
        $rows = $dbc->select('SELECT id FROM ZGroupCategory order by level_index ASC');
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZGroupCategory($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getUserCategories($dbc) {
        $rows = $dbc->select('SELECT id FROM ZUserCategory order by level_index ASC');
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZUserCategory($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getPaymentGateways($dbc) {
        $rows = $dbc->select('SELECT id FROM ZPaymentGateway');
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZPaymentGateway($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getCertification($dbc) {
        $rows = $dbc->select('SELECT id FROM YCertification WHERE is_active = 1 ORDER BY priority_order DESC');
        $result = array();
        foreach ($rows as $v) {
            $obj = new YCertification($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getBanners($dbc) {
        $rows = $dbc->select('SELECT id FROM YBanner WHERE is_active = 1 ORDER BY priority_order DESC');
        $result = array();
        foreach ($rows as $v) {
            $obj = new YBanner($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getAboutUsItem($dbc) {
        $rows = $dbc->select('SELECT id FROM YAboutUsItem WHERE is_active = 1 ORDER BY priority_order DESC');
        $result = array();
        foreach ($rows as $v) {
            $obj = new YAboutUsItem($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getAboutUsPhoto($dbc) {
        $rows = $dbc->select('SELECT id FROM YAboutUsPhoto WHERE is_active = 1 ORDER BY priority_order DESC');
        $result = array();
        foreach ($rows as $v) {
            $obj = new YAboutUsPhoto($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getAboutUsDefault($dbc)
    {
        $rows = $dbc->select("SELECT id FROM YAboutUsPhoto WHERE is_default = 1");
        $result = array();
        foreach ($rows as $v) {
            $obj = new YAboutUsPhoto($dbc);
            if ($obj->load($v->id)) {
                $result[] = $obj;
            }
        }
        return $result;
    }

    public static function getBlog($dbc) {
        $rows = $dbc->select('SELECT id FROM YBlog Where priority_order < 100 ORDER BY priority_order DESC');
        $result = array();
        foreach ($rows as $v) {
            $obj = new YBlog($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getProducts($dbc) {
        $rows = $dbc->select('SELECT id FROM ZProduct WHERE is_active = 1');
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZProduct($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getProductGalley($dbc) {
        $rows = $dbc->select('SELECT id FROM ProductGalley WHERE is_active = 1 ORDER BY priority_order DESC');
        $result = array();
        foreach ($rows as $v) {
            $obj = new ProductGalley($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getTestimonial($dbc) {
        $rows = $dbc->select('SELECT id FROM YTestimonial WHERE is_active = 1 ORDER BY priority_order DESC');
        $result = array();
        foreach ($rows as $v) {
            $obj = new YTestimonial($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getAdmins($dbc) {
        $rows = $dbc->select('SELECT id FROM Admin');
        $result = array();
        foreach ($rows as $v) {
            $obj = new Admin($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getCourierService($dbc) {
        $rows = $dbc->select('SELECT id FROM ZCourierService WHERE is_active = 1 AND is_delete = 0 ORDER BY priority_order DESC');
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZCourierService($dbc);
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }

    public static function getZCommonCategories($dbc, $type)
    {
        $rows = $dbc->select('SELECT id FROM ZCommonCategory WHERE type = "' . $type . '" ORDER BY priority_order DESC');
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZCommonCategory($dbc);
            if ($obj->load($v->id)) {
                $result[] = $obj;
            }
        }
        return $result;
    }

    public static function getZGroupCategory($dbc)
    {
        $rows = $dbc->select('SELECT id FROM ZGroupCategory');
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZGroupCategory($dbc);
            if ($obj->load($v->id)) {
                $result[] = $obj;
            }
        }
        return $result;
    }

    public static function getZUserCategory($dbc)
    {
        $rows = $dbc->select('SELECT id FROM ZUserCategory');
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZUserCategory($dbc);
            if ($obj->load($v->id)) {
                $result[] = $obj;
            }
        }
        return $result;
    }

    public static function getAllUser($dbc, $category_id = '')
    {
        $sql = "";

        $sql .= "SELECT id FROM User ";

        if (!empty($category_id)) {
            $sql .= "WHERE user_category_id = {$category_id}";
        }


        $rows = $dbc->select($sql);
        $result = array();
        foreach ($rows as $v) {
            $obj = new User($dbc);
            if ($obj->load($v->id)) {
                $result[] = $obj;
            }
        }
        return $result;
    }

    public static function getAllSales($dbc)
    {
        $sql = "";

        $sql .= "SELECT id FROM UserSale WHERE sale_status = 'Paid'";

        $rows = $dbc->select($sql);
        $result = array();
        foreach ($rows as $v) {
            $obj = new UserSale($dbc);
            if ($obj->load($v->id)) {
                $result[] = $obj;
            }
        }
        return $result;
    }

    public static function getPendingReviewCount($dbc)
    {
        $rows = $dbc->select(
            "SELECT COUNT(id) as num_rows FROM CustomerReview",
            [':s' => "0"]
        );
        return (!empty($rows) ? $rows[0]->num_rows : 0);
    }

    public static function getPendingEnquiryCount($dbc)
    {
        $rows = $dbc->select(
            "SELECT COUNT(id) as num_rows FROM YEnquiry WHERE is_read = :s",
            [':s' => "0"]
        );
        return (!empty($rows) ? $rows[0]->num_rows : 0);
    }

    public static function getPendingEngagementCount($dbc)
    {
        $rows = $dbc->select(
            "SELECT COUNT(id) as num_rows FROM AgentMarketing WHERE status = :s",
            [':s' => "pending"]
        );
        return (!empty($rows) ? $rows[0]->num_rows : 0);
    }

    public static function getWholesalePendingWithdrawCount($dbc)
    {
        $rows = $dbc->select(
            "SELECT COUNT(id) as num_rows FROM WholesaleWithdraw WHERE status = :s",
            [':s' => "pending"]
        );
        return (!empty($rows) ? $rows[0]->num_rows : 0);
    }

    public static function getPendingWithdrawCount($dbc)
    {
        $rows = $dbc->select(
            "SELECT COUNT(id) as num_rows FROM AgentWithdraw WHERE status = :s",
            [':s' => "pending"]
        );
        return (!empty($rows) ? $rows[0]->num_rows : 0);
    }

    public static function filterSalesStatusCount($dbc, $status = "") {
        $sql = '';
        $sql .= 'SELECT COUNT(id) as num_rows ';
        $sql .= 'FROM UserSale ';
        $sql .= 'WHERE is_paid = 1 ';
        $sql .= (!empty($status)) ? "AND shipping_status = :s " : " ";
        $rows = $dbc->select($sql, [':s' => $status]);

        return (!empty($rows) ? $rows[0]->num_rows : 0);
    }

    public static function getPendingPaymentTransaction($dbc)
    {
        $rows = $dbc->select("SELECT id FROM ZPaymentGatewayTransaction WHERE status = 'Pending Payment'");
        $result = array();
        $obj = new ZPaymentGatewayTransaction($dbc);
        foreach ($rows as $v) {
            if ($obj->load($v->id)) {
                $result[] = clone $obj;
            }
        }
        return $result;
    }
    public static function getBanks($dbc) {
        $sql = "";
        $sql .= "SELECT id FROM ZCommonCategory WHERE type = 'bank'";
        $rows = $dbc->select($sql);
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZCommonCategory($dbc);
            if ($obj->load($v->id)) {
                $result[] = $obj;
            }
        }
        return $result;
    }

}