<?php

class Common {

    public static function getComplaintType() {
        return array(
            "Bullying",
            "Vandalism",
            "Cheating",
            "Skipping",
            "Disruptive Behaviour",
            "Other",
        );
    }

    public static function getRegistrationStatus() {
        return array(
            "pending",
            "rejected",
            "approved",

        );
    }

    public static function getSaleTypes() {
        return array(
            'Agent' => "Agent Sales",
            'Customer' => "Customer Sales",
            'Wholesale' => "Wholesale Sales",
        );
    }

    public static function getUserSaleStatus() {
        return array(
            "Pending Payment",
            "Walk In",
            "Processing",
            "Packed",
            "Shipped",
            "Cancelled",
        );
    }

    public static function getStatementBalanceType() {
        return array(
            "balance",
            "commission",
        );
    }

    public static function getAgentRankingOrderBys() {
        return array(
            'total_no_of_sale' => 'Total Sale',
            'total_revenue' => 'Total Revenue',
        );
    }

    public static function getProductBestSellingOrderBys() {
        return array(
            'total_quantity' => 'Total Quantity',
            'total_cost' => 'Total Cost',
            'total_selling' => 'Total Selling',
            'total_profit' => 'Total Profit',
        );
    }

    public static function getSalePerformanceOrderBys(){
        return array(
          'total_sales' => 'Total sales',
          'total_commission' => 'Total Commission',
          'number_of_sales' => 'Number of Sales',
        );
    }

    public static function getWholesaleSalePerformanceOrderBys(){
        return array(
            'total_sales' => 'Total sales',
            'number_of_sales' => 'Number of Sales',
        );
    }

    public static function getCountryCode()
    {
        return array(
            "+60" => "(+60) Malaysia",
            "+65" => "(+65) Singapore"
        );
    }

    public static function getCountry()
    {
        return array(
            "my" => "Malaysia",
            "sg" => "Singapore"
        );
    }

    public static function getAgentSaleStatus() {
        return array(
            "Pending Payment",
            "Paid",
            "Cancelled",
        );
    }

    public static function getTransactionStatus() {
        return array(
            "Pending Payment",
            "Paid",
            "Cancelled",
        );
    }

    public static function getStatementStatus() {
        return array(
            "commission",
            "balance",
        );
    }

    public static function getAgentStatementRemark() {
        return array(
            "open balance",
            "buddy incentives",
            "sale commission",
        );
    }

    public static function getWholesaleStatementRemark() {
        return array(
            "open balance",
            "referral bonus",
            "withdraw success",
            "withdraw rejected",
        );
    }

    public static function getAgentCommissionType() {
        return array(
            "DIRECT PARTNER",
            "BUDDY INCENTIVES",
        );
    }

    public static function getAgentTopupWithdrawStatuses() {
        return array(
            "pending" => "Pending",
            "approved" => "Approved",
            "rejected" => "Rejected",
        );
    }


    public static function getUserCommissionType() {
        return array(
            "sales_bonus" => "Sales Bonus",
            "level_bonus" => "Level Bonus",
            "leadership_bonus" => "Leadership Bonus",
            "group_sales_bonus" => "Group Sales Bonus",
        );
    }

    public static function getMerchantSaleStatus() {
        return array(
            _status_processing => "Processing",
            _status_packed => "Packed",
            _status_shipped => "Shipped",
//            "Cancelled",
        );
    }
    public static function getApprovalStatus() {
        return array(
            "pending" => "pending",
            "approved" => "approved",
            "rejected" => "rejected",
        );
    }
}