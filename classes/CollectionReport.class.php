<?php

class CollectionReport {

    public static function getReferralSales($dbc, $user_ids, $start_date, $end_date) {
        $sql = '';
        $sql .= "SELECT ReferralSale.referral_id, Count(ReferralSale.id) as no_of_sale, SUM(CustomerSale.grand_total) as total_sale, SUM(ReferralSale.commission_earn) as total_commission ";
        $sql .= "FROM CustomerSale ";
        $sql .= "JOIN ReferralSale ON ReferralSale.customer_sale_id = CustomerSale.id ";
        $sql .= "JOIN Referral ON Referral.id = ReferralSale.referral_id ";
        $sql .= "WHERE ReferralSale.id >= 0 ";
        $sql .= "AND CustomerSale.is_delete = 0 ";
        $sql .= "AND CustomerSale.transaction_status IN (" . implode(",", [5]) . ") ";
        if (!empty($user_ids)) {
            if (is_array($user_ids)) {
                if(!empty($user_ids[0])) {
                    $sql .= "AND Referral.created_by IN (" . implode(",", $user_ids) . ") ";
                }
            } else  {
                $sql .= "AND Referral.created_by = " . $user_ids . " ";
            }
        }
        $sql .= (!empty($start_date)) ? "AND CustomerSale.transaction_date >= '{$start_date}' " : "";
        if (!empty($end_date)) {
            $dt = new DateTime($end_date);
            $dt->modify('+1 day');
            $sql .= "AND CustomerSale.transaction_date <= '{$dt->format('Y-m-d')}' ";
        }
        $sql .= "GROUP BY ReferralSale.referral_id ";

        $results = [];
        $rows = $dbc->select($sql, []);
        foreach ($rows as $v) {
            $results[$v->referral_id] = json_decode(json_encode($v), true);
        }

        return $results;
    }

    public static function getProductCategorySales($dbc, $user_ids = "", $start_date = "", $end_date = "") {
        $category_groups = [];
        foreach (Collection::getZCommonCategories($dbc, "product-category") as $category) {
            $category_groups[$category->getName()] = ['condition' => $category->getId() , 'priority_order' => $category->getPriorityOrder(), 'total_count' => 0, 'total_sale' => 0];
        }

        $s_dt = new DateTime($start_date);
        $e_dt = new DateTime($end_date);

        foreach ($category_groups as &$category_group) {
            $sql = '';
            $sql .= "SELECT COALESCE(SUM(product_quantity),0) as total_quantity, COALESCE(SUM(product_total),0) as total_sale ";
            $sql .= "FROM CustomerSaleItem ";
            $sql .= "JOIN CustomerSale ON CustomerSale.id = CustomerSaleItem.sale_id ";
            $sql .= "WHERE CustomerSale.is_delete = 0 ";
            if (!empty($user_ids)) {
                if (is_array($user_ids)) {
                    if(!empty($user_ids[0])) {
                        $sql .= "AND CustomerSale.created_by IN (" . implode(",", $user_ids) . ") ";
                    }
                } else {
                    $sql .= "AND CustomerSale.created_by = " . $user_ids . " ";
                }
            }
            $sql .= "AND CustomerSale.transaction_status IN (" . implode(",", [5]) . ") ";
            $sql .= "AND CustomerSaleItem.product_category = :cid ";
            $sql .= (!empty($start_date)) ? "AND DATE(CustomerSale.transaction_date) >= '{$s_dt->format("Y-m-d")}' " : "";
            $sql .= (!empty($end_date)) ? "AND DATE(CustomerSale.transaction_date) <= '{$e_dt->format("Y-m-d")}' " : "";

            $rows = $dbc->select($sql, [':cid' => $category_group['condition']]);
            if (!empty($rows)) {
                $category_group['total_count'] = $rows[0]->total_quantity;
                $category_group['total_sale'] = $rows[0]->total_sale;
            }
        }

        return $category_groups;

    }

    public static function getCustomerTotalSales($dbc, $user_ids = "", $month_period = 12) {
        $months = [];
        for ($i = 0; $i < $month_period; $i++) {
            $curr_month = date("Y-m", strtotime(date('Y-m-01') . "-{$i} months"));
            $s_date = date('Y-m-01', strtotime($curr_month . "-01"));
            $e_date = date('Y-m-t', strtotime($curr_month . "-01"));
            $months[$curr_month] = ['s_date' => $s_date, 'e_date' => $e_date, 'total_count' => 0, 'total_sale' => 0];
        }

        foreach ($months as &$month) {
            $sql = '';
            $sql .= "SELECT COUNT(id) as num_rows, COALESCE(SUM(grand_total),0) as total_sale ";
            $sql .= "FROM CustomerSale ";
            $sql .= "WHERE CustomerSale.is_delete = 0 ";
            if (!empty($user_ids)) {
                if (is_array($user_ids)) {
                    if(!empty($user_ids[0])) {
                        $sql .= "AND CustomerSale.created_by IN (" . implode(",", $user_ids) . ") ";
                    }
                } else {
                    $sql .= "AND CustomerSale.created_by = " . $user_ids . " ";
                }
            }
            $sql .= "AND transaction_status IN (" . implode(",", [5]) . ") ";
            $sql .= "AND DATE(CustomerSale.transaction_date) >= '{$month['s_date']}' ";
            $sql .= "AND DATE(CustomerSale.transaction_date) <= '{$month['e_date']}' ";

            $rows = $dbc->select($sql, []);
            if (!empty($rows)) {
                $month['total_count'] = $rows[0]->num_rows;
                $month['total_sale'] = $rows[0]->total_sale;
            }
        }

        $months = array_reverse($months);

        return $months;

    }

    public static function getCustomerGrowth($dbc, $user_ids = "", $month_period = 12) {
        $months = [];
        for ($i = 0; $i < $month_period; $i++) {
            $curr_month = date("Y-m", strtotime(date('Y-m-01') . "-{$i} months"));
            $s_date = date('Y-m-01', strtotime($curr_month . "-01"));
            $e_date = date('Y-m-t', strtotime($curr_month . "-01"));
            $months[$curr_month] = ['s_date' => $s_date, 'e_date' => $e_date, 'total_count' => 0];
        }

        foreach ($months as &$month) {
            $sql = '';
            $sql .= "SELECT COUNT(id) as num_rows ";
            $sql .= "FROM Customer ";
            $sql .= "WHERE Customer.is_delete = 0 ";
            if (!empty($user_ids)) {
                if (is_array($user_ids)) {
                    if(!empty($user_ids[0])) {
                        $sql .= "AND Customer.created_by IN (" . implode(",", $user_ids) . ") ";
                    }
                } else {
                    $sql .= "AND Customer.created_by = " . $user_ids . " ";
                }
            }
            $sql .= "AND DATE(Customer.created_datetime) >= '{$month['s_date']}' ";
            $sql .= "AND DATE(Customer.created_datetime) <= '{$month['e_date']}' ";

            $rows = $dbc->select($sql, []);
            if (!empty($rows)) {
                $month['total_count'] = $rows[0]->num_rows;
            }
        }

        $months = array_reverse($months);

        return $months;

    }

    public static function getCustomerNationality($dbc, $user_ids = "", $length = 10) {
        $nationality_groups = [];
        foreach (Common::getNationalities() as $key => $nationality) {
            $nationality_groups[$nationality] = ['condition' => $nationality, 'total_count' => 0];
        }

        foreach ($nationality_groups as &$nationality_group) {
            $sql = '';
            $sql .= "SELECT COUNT(id) as num_rows ";
            $sql .= "FROM Customer ";
            $sql .= "WHERE nationality = '{$nationality_group['condition']}' ";
            $sql .= "AND is_delete = 0 ";
            if (!empty($user_ids)) {
                if (is_array($user_ids)) {
                    if(!empty($user_ids[0])) {
                        $sql .= "AND Customer.created_by IN (" . implode(",", $user_ids) . ") ";
                    }
                } else {
                    $sql .= "AND Customer.created_by = " . $user_ids . " ";
                }
            }
            $rows = $dbc->select($sql, []);
            if (!empty($rows)) {
                $nationality_group['total_count'] = $rows[0]->num_rows;
            }
        }

        usort($nationality_groups, function ($item1, $item2) {
            return $item2['total_count'] <=> $item1['total_count'];
        });

        $nationality_groups = array_slice($nationality_groups, 0, 5, true);

        return $nationality_groups;
    }

    public static function getCustomerType($dbc, $user_ids = "") {
        $type_groups = [
            _type_company => ['condition' => 'company', 'total_count' => 0],
            _type_customer => ['condition' => 'customer', 'total_count' => 0],
        ];

        foreach ($type_groups as &$type_group) {
            $sql = '';
            $sql .= "SELECT COUNT(id) as num_rows ";
            $sql .= "FROM Customer ";
            $sql .= "WHERE customer_type = '{$type_group['condition']}' ";
            $sql .= "AND is_delete = 0 ";
            if (!empty($user_ids)) {
                if (is_array($user_ids)) {
                    if(!empty($user_ids[0])) {
                        $sql .= "AND Customer.created_by IN (" . implode(",", $user_ids) . ") ";
                    }
                } else {
                    $sql .= "AND Customer.created_by = " . $user_ids . " ";
                }
            }
            $rows = $dbc->select($sql, []);
            if (!empty($rows)) {
                $type_group['total_count'] = $rows[0]->num_rows;
            }
        }

        return $type_groups;
    }

    public static function getCustomerGender($dbc, $user_ids = "") {
        $gender_groups = [
            _none => ['condition' => '', 'total_count' => 0],
            _gender_male => ['condition' => 'male', 'total_count' => 0],
            _gender_female => ['condition' => 'female', 'total_count' => 0],
        ];

        foreach ($gender_groups as &$gender_group) {
            $sql = '';
            $sql .= "SELECT COUNT(id) as num_rows ";
            $sql .= "FROM Customer ";
            $sql .= "WHERE gender = '{$gender_group['condition']}' ";
            $sql .= "AND is_delete = 0 ";
            if (!empty($user_ids)) {
                if (is_array($user_ids)) {
                    if(!empty($user_ids[0])) {
                        $sql .= "AND Customer.created_by IN (" . implode(",", $user_ids) . ") ";
                    }
                } else {
                    $sql .= "AND Customer.created_by = " . $user_ids . " ";
                }
            }
            $rows = $dbc->select($sql, []);
            if (!empty($rows)) {
                $gender_group['total_count'] = $rows[0]->num_rows;
            }
        }

        return $gender_groups;
    }

    public static function getCustomerAgeGroup($dbc, $user_ids = "") {
        $age_groups = [
            '18 to 22' => ['c_start' => 18, 'c_end' => 22, 'total_count' => 0], '23 to 27' => ['c_start' => 23, 'c_end' => 27, 'total_count' => 0], '28 to 32' => ['c_start' => 28, 'c_end' => 32, 'total_count' => 0], '33 to 37' => ['c_start' => 33, 'c_end' => 37, 'total_count' => 0], '38 to 42' => ['c_start' => 38, 'c_end' => 42, 'total_count' => 0], '43 to 47' => ['c_start' => 43, 'c_end' => 47, 'total_count' => 0], '48 to 52' => ['c_start' => 48, 'c_end' => 52, 'total_count' => 0], '53 to 57' => ['c_start' => 53, 'c_end' => 57, 'total_count' => 0], '58 to 62' => ['c_start' => 58, 'c_end' => 62, 'total_count' => 0], '63 to 67' => ['c_start' => 63, 'c_end' => 67, 'total_count' => 0], '68 to 72' => ['c_start' => 68, 'c_end' => 72, 'total_count' => 0],
        ];

        foreach ($age_groups as &$age_group) {
            $sql = '';
            $sql .= "SELECT COUNT(id) as num_rows ";
            $sql .= "FROM Customer ";
            $sql .= "WHERE TIMESTAMPDIFF(YEAR, dob, CURDATE()) BETWEEN {$age_group['c_start']} AND {$age_group['c_end']} ";
            $sql .= "AND is_delete = 0 ";
            if (!empty($user_ids)) {
                if (is_array($user_ids)) {
                    if(!empty($user_ids[0])) {
                        $sql .= "AND Customer.created_by IN (" . implode(",", $user_ids) . ") ";
                    }
                } else {
                    $sql .= "AND Customer.created_by = " . $user_ids . " ";
                }
            }
            $rows = $dbc->select($sql, []);
            if (!empty($rows)) {
                $age_group['total_count'] = $rows[0]->num_rows;
            }
        }

        return $age_groups;
    }

    public static function getItemSales($dbc, $user_ids, $start_date, $end_date) {
        $sql = '';
        $sql .= "SELECT CustomerSaleItem.product_id, SUM(CustomerSaleItem.product_quantity) as total_quantity, SUM(CustomerSaleItem.product_total) as total_amount ";
        $sql .= "FROM CustomerSale ";
        $sql .= "JOIN CustomerSaleItem ON CustomerSaleItem.sale_id = CustomerSale.id ";
        $sql .= "WHERE CustomerSale.id >= 0 ";
        $sql .= "AND CustomerSale.is_delete = 0 ";
        if (!empty($user_ids)) {
            if (is_array($user_ids)) {
                if(!empty($user_ids[0])) {
                    $sql .= "AND CustomerSale.created_by IN (" . implode(",", $user_ids) . ") ";
                }
            } else {
                $sql .= "AND CustomerSale.created_by = " . $user_ids . " ";
            }
        }
        $sql .= (!empty($start_date)) ? "AND CustomerSale.transaction_date >= '{$start_date}' " : "";
        if (!empty($end_date)) {
            $dt = new DateTime($end_date);
            $dt->modify('+1 day');
            $sql .= "AND CustomerSale.transaction_date <= '{$dt->format('Y-m-d')}' ";
        }
        $sql .= "GROUP BY CustomerSaleItem.product_id ";

        $results = [];
        $rows = $dbc->select($sql, []);
        foreach ($rows as $v) {
            $results[$v->product_id] = json_decode(json_encode($v), true);
        }

        return $results;
    }

    public static function getCustomerSales($dbc, $user_ids, $start_date, $end_date) {
        $sql = '';
        $sql .= "SELECT CustomerSale.customer_id, Count(CustomerSale.id) as no_of_sale, SUM(CustomerSale.grand_total) as total_sale, SUM(CustomerSale.payment_amount) as total_paid ";
        $sql .= "FROM CustomerSale ";
        $sql .= "WHERE CustomerSale.id >= 0 ";
        $sql .= "AND CustomerSale.is_delete = 0 ";
        $sql .= "AND CustomerSale.transaction_status IN (" . implode(",", [5]) . ") ";
        if (!empty($user_ids)) {
            if (is_array($user_ids)) {
                if(!empty($user_ids[0])) {
                    $sql .= "AND CustomerSale.created_by IN (" . implode(",", $user_ids) . ") ";
                }
            } else  {
                $sql .= "AND CustomerSale.created_by = " . $user_ids . " ";
            }
        }
        $sql .= (!empty($start_date)) ? "AND CustomerSale.transaction_date >= '{$start_date}' " : "";
        if (!empty($end_date)) {
            $dt = new DateTime($end_date);
            $dt->modify('+1 day');
            $sql .= "AND CustomerSale.transaction_date <= '{$dt->format('Y-m-d')}' ";
        }
        $sql .= "GROUP BY CustomerSale.customer_id ";

        $results = [];
        $rows = $dbc->select($sql, []);
        foreach ($rows as $v) {
            $results[$v->customer_id] = json_decode(json_encode($v), true);
        }

        return $results;
    }

    public static function getUsers($dbc) {
        $rows = $dbc->select('SELECT id FROM User ORDER BY id ASC');
        $result = array();
        foreach ($rows as $v) {
            $result[$v->id] = $v->id;
        }
        return $result;
    }

    public static function getUserObjs($dbc) {
        $rows = $dbc->select('SELECT id FROM User ORDER BY id ASC');
        $result = array();
        foreach ($rows as $v) {
            $obj = new User($dbc);
            if ($obj->load($v->id)) {
                $result[$v->id] = $obj;
            }
        }
        return $result;
    }

    public static function getProductCategories($dbc) {
        $rows = $dbc->select('SELECT id FROM ZCommonCategory WHERE type = "product-category" ORDER BY priority_order DESC');
        $result = array();
        foreach ($rows as $v) {
            $result[$v->id] = $v->id;
        }
        return $result;
    }

    public static function getProductCategoryObjs($dbc) {
        $rows = $dbc->select('SELECT id FROM ZCommonCategory WHERE type = "product-category" ORDER BY priority_order DESC');
        $result = array();
        foreach ($rows as $v) {
            $obj = new ZCommonCategory($dbc);
            if ($obj->load($v->id)) {
                $result[$v->id] = $obj;
            }
        }
        return $result;
    }

    public static function foo() {
    }

}