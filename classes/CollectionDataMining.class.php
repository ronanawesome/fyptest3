<?php

class CollectionDataMining {

    public static function getCustomerData($dbc, $start_date, $end_date, $age_from, $age_to, $gender, $nationalities, $customer_types, $races, $religion, $marital_status, $income_ranges, $pref_langs, $sale_from, $sale_to, $customer_cats, $sources, $referrals, $product_cats, $product_skus) {

        $s_dt = new DateTime($start_date);
        $e_dt = new DateTime($end_date);

        $sql = '';
        $sql .= "SELECT Customer.id as customer_id, Customer.customer_category_id ";
        $sql .= ($product_cats !== "") ? ", CustomerSaleItem.product_category as product_category_id " : "";
        $sql .= ($product_skus !== "") ? ", CustomerSaleItem.product_id as product_id " : "";
        $sql .= (!empty($referrals)) ? ", ReferralSale.referral_id as referral_id " : "";
        $sql .= "FROM Customer ";
        $sql .= "JOIN CustomerSale ON CustomerSale.customer_id = Customer.id ";
        $sql .= (!empty($sources)) ? "JOIN CustomerSource ON CustomerSource.customer_id = Customer.id " : "";
        $sql .= (!empty($referrals)) ? "JOIN ReferralSale ON ReferralSale.customer_sale_id = CustomerSale.id " : "";
        $sql .= ($product_cats !== "" || $product_skus !== "") ? "JOIN CustomerSaleItem ON CustomerSaleItem.sale_id = CustomerSale.id " : "";
        $sql .= "WHERE Customer.id >= 0 ";
        $sql .= "AND Customer.is_delete = 0 ";
        $sql .= "AND CustomerSale.is_delete = 0 ";
        $sql .= "AND CustomerSale.transaction_status IN (" . implode(",", [5]) . ") ";
        $sql .= (!empty($start_date)) ? "AND DATE(CustomerSale.transaction_date) >= '{$s_dt->format("Y-m-d")}' " : "";
        $sql .= (!empty($end_date)) ? "AND DATE(CustomerSale.transaction_date) <= '{$e_dt->format("Y-m-d")}' " : "";
        $sql .= (!empty($age_from) && !empty($age_to)) ? "AND ROUND(datediff(now(), Customer.dob) / 365.25) BETWEEN {$age_from} AND {$age_to} " : "";
        $sql .= (!empty($gender)) ? "AND Customer.gender = '{$gender}' " : "";
        $sql .= (!empty($nationalities)) ? "AND Customer.nationality REGEXP '" . implode('|', $nationalities) . "' " : "";
        $sql .= (!empty($customer_types)) ? "AND Customer.customer_type REGEXP '" . implode('|', $customer_types) . "' " : "";
        $sql .= (!empty($races)) ? "AND Customer.race IN (" . implode(",", $races) . ") " : "";
        $sql .= (!empty($religion)) ? "AND Customer.religion IN (" . implode(",", $religion) . ") " : "";
        $sql .= (!empty($marital_status)) ? "AND Customer.marital_status REGEXP '" . implode('|', $marital_status) . "' " : "";
        $sql .= (!empty($income_ranges)) ? "AND Customer.income_range IN ('" . implode("','", $income_ranges) . "') " : "";
        $sql .= (!empty($pref_langs)) ? "AND Customer.preferable_language IN ('" . implode("','", $pref_langs) . "') " : "";
        $sql .= (!empty($sale_from) && !empty($sale_to)) ? "AND CustomerSale.grand_total BETWEEN {$sale_from} AND {$sale_to} " : "";
        $sql .= (!empty($customer_cats)) ? "AND Customer.customer_category_id IN (" . implode(",", $customer_cats) . ") " : "";
        if (!empty($sources)) {
            $sql .= "AND ( CustomerSource.id >= 0 ";
            foreach ($sources as $source) {
                $ids = explode(";", $source);
                $sql .= (isset($ids[0]) && !empty($ids[0])) ? "OR CustomerSource.source_main = {$ids[0]} " : "";
                $sql .= (isset($ids[1]) && !empty($ids[1])) ? "OR CustomerSource.source_sub = {$ids[1]} " : "";
            }
            $sql .= ") ";
        }
        if (!empty($referrals)) {
            $sql .= "AND ReferralSale.referral_id IN (" . implode(",", $referrals) . ") ";
        }
        if ($product_cats !== "") {
            $sql .= "AND CustomerSaleItem.product_category IN (" . $product_cats . ") ";
        }
        if ($product_skus !== "") {
            $sql .= "AND CustomerSaleItem.product_id IN (" . $product_skus . ") ";
        }
        $sql .= "GROUP BY Customer.id ";

//        echo "$sql<br>";

        $rows = $dbc->select($sql, []);

        return json_decode(json_encode($rows), true);
    }

}