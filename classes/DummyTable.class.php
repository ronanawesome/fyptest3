<?php

class DummyTable {

    private $title = "";
    private $checkbox = true;

    private $columns = array();//id,name,
    private $rows =array();

    private $has_add_button    = true;
    private $has_edit_button   = true;
    private $has_list_button   = true;
    private $has_remove_button = true;

	public function __construct($title="Table Title") {
        $this->title = $title;
	}

    public function columns($string){
        $this->columns = explode(',',$string);
    }

    public function row($row){
        $this->rows[] = $row;
    }

    public function unsetAdd(){
        $this->has_add_button=false;
    }

    public function unsetEdit(){
        $this->has_edit_button=false;
    }

    public function unsetList(){
        $this->has_list_button=false;
    }

    public function unsetRemove(){
        $this->has_remove_button=false;
    }

    public function drawAddButton(){
        if($this->has_add_button) echo '<a href="#" class="btn btn-light btn-sm pl-4"><i class="fa fa-plus fa-sm text-primary"></i></a>';
    }

    public function drawEditButton(){
        if($this->has_edit_button) echo '<a href="#" class="mr-3"><i class="fa fa-pen text-primary"></i></a>';
    }

    public function drawListButton(){
        if($this->has_list_button) echo '<a href="#" class="mr-3"><i class="fa fa-list text-primary"></i></a>';
    }

    public function drawRemoveButton(){
        if($this->has_remove_button) echo '<a href="#" class=""><i class="fa fa-trash text-danger"></i></a>';
    }

    public function drawPaginationPanel(){
        ?>
        <div class="col text-center justify-content-center align-self-center mt-5">
            <div class="float-left text-muted mt-2">Showing <b>5</b> out of <b>25</b> entries</div>
            <div class="float-right">
                <ul class="pagination">
                    <li class="page-item"><a href="#" class="page-link">Previous</a></li>
                    <li class="page-item active"><a href="#" class="page-link">1</a></li>
                    <li class="page-item"><a href="#" class="page-link">2</a></li>
                    <li class="page-item"><a href="#" class="page-link">3</a></li>
                    <li class="page-item"><a href="#" class="page-link">4</a></li>
                    <li class="page-item"><a href="#" class="page-link">5</a></li>
                    <li class="page-item"><a href="#" class="page-link">Next</a></li>
                </ul>
            </div>
        </div>
        <?php
    }

    public function render(){
        ?>
        <div class="table-responsive">
            <div>
                <div class="float-left">
                    <h2 class="font-weight-bold mt-1 mr-5"><?php echo $this->title; ?></h2>
                </div>
                <div class="float-left">
                    <?php $this->drawAddButton(); ?>
                </div>
    		</div>
            <table class="table table-striped table-hover">
    			<thead>
    				<tr>
    					<?php
                            if($this->checkbox){
                                ?>
                                <th>
            						<span class="custom-checkbox">
            							<input type="checkbox" id="selectAll">
            							<label for="selectAll"></label>
            						</span>
            					</th>
                                <?php
                            }
                        ?>
                        <?php

                            foreach($this->columns as $column){
                                echo '<th>'.$column.'</th>';
                            }

                        ?>
    					<th>Actions</th>
    				</tr>
    			</thead>
    			<tbody>
                    <?php
                        foreach($this->rows as $row){
                            ?>
                            <tr>
                                <?php

                                    if($this->checkbox){
                                        ?>
                                        <td>
                    						<span class="custom-checkbox">
                    							<input type="checkbox" id="checkbox1" name="options[]" value="1">
                    							<label for="checkbox1"></label>
                    						</span>
                    					</td>
                                        <?php
                                    }

                                    foreach($row as $col){
                                        echo '<td>'.$col.'</td>';
                                    }

                                ?>

            					<td>
                                    <?php
                                        $this->drawEditButton();
                                        $this->drawListButton();
                                        $this->drawRemoveButton();
                                    ?>
            					</td>

            				</tr>
                            <?php
                        }
                    ?>


    			</tbody>
    		</table>
            <?php $this->drawPaginationPanel(); ?>
        </div>
        <?php
    }

}
