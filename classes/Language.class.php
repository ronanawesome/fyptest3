<?php

class Language
{


    private $dbc = null;
    private $sessionName = null;

    function __construct($dbc = null)
    {
        $this->dbc = $dbc;

        $url = parse_path();

        if ($url['call_parts'][0] == 'admin') {
            $this->sessionName = 'admin_lang';
        } else {
            $this->sessionName = 'lang';
        }
    }

    public function getLangaugechoice()
    {

        $rows = $this->dbc->select("SELECT * From ZSettingLanguage WHERE is_active=1");

        return $rows;

    }

    public function call()
    {

        if (!isset($_SESSION[$this->sessionName])) {
            $_SESSION[$this->sessionName] = 'en';
        }

        include_once("language/messages." . $_SESSION[$this->sessionName] . ".inc.php");

    }

    public function callFromXcrud()
    {

        if (!isset($_SESSION[$this->sessionName])) {
            $_SESSION[$this->sessionName] = 'en';
        }

        include_once("../../language/messages." . $_SESSION[$this->sessionName] . ".inc.php");

    }

    public function rewrite()
    {

        $this->deleteFile();

        $result = $this->dbc->select("SELECT * FROM ZSettingLanguage");

        foreach ($result as $row) {

            $vocabulary = "";
            $result1 = $this->dbc->select("SELECT * FROM ZSettingVocabulary WHERE language_id =" . $row->id . " ORDER BY key_value ASC");

            foreach ($result1 as $col) {
                $vocabulary .= "define(\"" . stripslashes($col->key_value) . "\",\"" . stripslashes($col->key_text) . "\");\n";
            }//end of get all value and text into $string

            $string = "";
            $string = "<?php\n\n";
            $string .= $vocabulary;
            $string .= "\n\n?>";
            $my_file = 'language/messages.' . $row->abbreviation . '.inc.php';
            $handle = fopen($my_file, 'w');
            fwrite($handle, $string);
            fclose($handle);

        }


    }


    public function deleteFile()
    {
        $files = glob("/language/messages.*.inc.php");
        array_map('unlink', $files);
    }

    public function grid()
    {

        require_once('module/xcrud/xcrud.php');
        $xcrud = Xcrud::get_instance();
        $xcrud->table_name('Language');
        $xcrud->table('ZSettingLanguage');
        $xcrud->order_by('priority_order', 'desc');

        $xcrud->change_type('priority_order', 'int', 0);
        $xcrud->change_type('is_active', 'bool', 1);

        $xcrud->validation_required('lang_name,priority_order');
        $xcrud->validation_required('abbreviation', 2);

        $xcrud->after_insert('after_language_insert');
        $xcrud->after_remove('after_language_remove');


        echo $xcrud->render();

    }

    public function grid_vocabulary()
    {

        require_once('module/xcrud/xcrud.php');
        $xcrud = Xcrud::get_instance();
        $xcrud->table_name('Vocabulary');
        $xcrud->table('ZSettingVocabulary');
        //$xcrud->order_by('key_value,language_id','asc');
        $xcrud->order_by('id', 'desc');

        $xcrud->relation('language_id', 'ZSettingLanguage', 'id', 'lang_name');

        $xcrud->readonly('language_id');
//		$xcrud->disabled('language_id');

        $xcrud->change_type('language_id', 'int', 1);
        $xcrud->change_type('key_text', 'textarea');

        $xcrud->validation_required('key_value,text_value');

        $xcrud->hide_button('save_new');
        $xcrud->hide_button('save_edit');

        $xcrud->after_insert('after_vocabulary_insert');
        $xcrud->after_update('after_vocabulary_update');
        $xcrud->replace_remove('vocabulary_remove_replacer');

        $xcrud->unset_csv();
        $xcrud->unset_print();

        echo $xcrud->render();

    }

    public function rewrite_from_xcrud()
    {

        $this->deleteFile();

        $result = $this->dbc->select("SELECT * FROM ZSettingLanguage");

        foreach ($result as $row) {

            $vocabulary = "";
            $result1 = $this->dbc->select("SELECT * FROM ZSettingVocabulary WHERE language_id =" . $row->id . " ORDER BY key_value ASC");

            foreach ($result1 as $col) {
                $vocabulary .= "define(\"" . stripslashes($col->key_value) . "\",\"" . stripslashes($col->key_text) . "\");\n";
            }//end of get all value and text into $string

            $string = "";
            $string = "<?php\n\n";
            $string .= $vocabulary;
            $string .= "\n\n?>";
            $my_file = '../../language/messages.' . $row->abbreviation . '.inc.php';
            $handle = fopen($my_file, 'w');
            fwrite($handle, $string);
            fclose($handle);

        }


    }


}