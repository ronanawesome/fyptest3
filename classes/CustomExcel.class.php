<?php

require_once 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class CustomExcel {

    public $reader = NULL;

    public $spreadsheet = NULL;
    public $sheet = NULL;

    public function __construct() {
        $this->spreadsheet = new Spreadsheet();
        $this->sheet = $this->spreadsheet->getActiveSheet();
    }

    public function dynamic_load($filename, $ext) {
        switch ($ext) {
            case "csv":
                $this->loadCsv($filename);
                break;
            case "xls":
                $this->loadXls($filename);
                break;
            default:
                $this->load($filename);
                break;
        }
    }

    public function load($filename) {
        $this->spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($filename);
        $this->sheet = $this->spreadsheet->getActiveSheet();
    }

    public function loadXls($filename) {
        $this->reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($filename);
        $this->reader->setReadDataOnly(true);
        $this->spreadsheet = $this->reader->load($filename);
        $this->sheet = $this->spreadsheet->getActiveSheet();
    }

    public function loadCsv($filename) {
        $this->reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
        $this->spreadsheet = $this->reader->load($filename);
        $this->sheet = $this->spreadsheet->getActiveSheet();
    }

    public function write($cell_value, $content) {
        $this->sheet->setCellValue($cell_value, $content);
    }

    public function writeAll($top_cell_value,$contents) {
        $this->sheet->fromArray($contents, NULL, $top_cell_value);
    }

    public function export_download($filename, $ext="xlsx") {
        $writer = new Xlsx($this->spreadsheet);
        $writer->setPreCalculateFormulas(false);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . $filename . '.' . $ext . '"');

        $writer->save("php://output");
        exit();
    }

}