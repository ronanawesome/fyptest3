<?php
global $dbc, $url;
$role = $url['call_parts'][0];

if (!empty($url['call_parts'][2])) {
    $id = $url['call_parts'][2];
    $t = new ComplaintReport($dbc);
    $t->load($id);
} else {
    redirect($role . '/tournament');
}


$user = new User($dbc);
$user->load($_SESSION['parent_id']);


//---------- page info --------------
//    must include before header
$title = "Complaint Details:: " . $t->getTitle();
$breadcrumbs = array(
    $title => '/' . $role . '/' . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------

$validator = new Rakit\Validation\Validator;
$fields = array();

$error = array();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $validation = $validator->make($_POST + $_GET + $_FILES, $fields);

    $validation->validate();
    $problem = $validation->errors();

    $data = $validation->getValidatedData();
}



include(PARENT_HEADER);
include(PARENT_NAVBAR);
?>

    <style>
        .entry-image {
            display: block;
            position: relative;
            width: 100%;
            height: 100% !important;
        }

        .entry-image img{
            display: block;
            position: relative;
            width: 100%;
            height: 100% !important;
            border: .5px solid #c8c8c8;
        }
        .h1{
            font-family: "Lato", sans-serif;
        }
    </style>

    <!-- Page Title
    ============================================= -->
    <section id="page-title">
        <div class="container clearfix">
            <h1 class="h1"> <?php echo $t->getTitle(); ?></h1>

        </div>

    </section>
    <!-- #page-title end -->

    <!-- Content
    ============================================= -->

    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="error-output">
                    <?php
                    getAlert();
                    ?>
                </div>
                <div class="single-event">
                    <div class=" align-self-center ">
                        <h3 class="mb-0">Complaint Details</h3>
                        <hr>
                        <div class="card bg-light col-6 col-md-12 col-lg-12 col-12 my-1">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6 col-sm-6 col-lg-5 p-1"><strong>Date: </strong>
                                    </div>
                                    <div class="col-12 col-sm-6 col-lg-7 p-1"> <?php echo $t->getCreatedDate() ?></div>
                                    <div class="col-6 col-sm-6 col-lg-5 p-1"><strong>Student(s) Involved:</strong></div>
<!--                                    --><?php
                                                $studentids=$t->getUserIds();
                                                $tempstudentlist="";
                                                $arraystudents=explode(",", $studentids);
                                                foreach (Collection::getStudents($dbc) as $x):
                                                    if(in_array($x->getId(),$arraystudents,TRUE)){
                                                        $tempstudentlist=$x->getUsername().", ". $tempstudentlist;
                                                    }
                                            endforeach;
                                    ?>
                                    <div class="col-12 col-sm-6 col-lg-7 p-1"> <?php echo  $tempstudentlist ?></div>


                                    <div class="col-6 col-sm-6 col-lg-5 p-1"><strong>Type of misconduct:</strong></div>
                                    <div class="col-12 col-sm-6 col-lg-7 p-1"> <?php echo $t->getType() ?></div>
                                    <div class="col-6 col-sm-6 col-lg-5 p-1"><strong>Details: </strong></div>
                                    <div class="col-12 col-sm-6 col-lg-7 p-1"> <?php echo $t->getContent() ?></div>

                                </div>
                            </div>
                        </div>
                    </div>


    </section>

    <!-- #content end -->


<?php
include(PARENT_FOOTER);
