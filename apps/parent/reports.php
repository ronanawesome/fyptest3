<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$page_title = _REGISTER;

$breadcrumbs = [
    _REGISTER => '/' . $role . '/registration'
];

$user = new user($dbc);
$user->load($_SESSION['parent_id']);
$userid=$user->getId();
$parentchild=new ParentChild($dbc);
$parentchild->loadparentchildinfo($userid);
$childID=$parentchild->getChildId();

//$parentC=Collection::getParentChild($dbc,$user);
//$array=explode(",",$parentC);
//
//print_array($parentC);


$child=new user($dbc);

//
//$students=Collection::getStudents($dbc);



include(PARENT_HEADER);
include(PARENT_NAVBAR);



$error = array();
$dbuilder = new DateBuilder();
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $validator = new Validator;

    // fields info container
    $fields = array();


    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values



}
$form = new FormInput();
?>
<title>Announcements</title>
<style>
    .container{
        /*background-color: white;*/
        border-radius: 10px;

    }
    .grid-inner {
        background-color: hotpink;
        border-radius: 10px;
    }



</style>
<section id="page-title">

</section>
<!-- Content
============================================= -->
<section id="content">
    <div class="container clearfix">
        <div class="col-md-12 mb-10">
            <div class="row">
                <?php
                if (empty(Collection::getReports($dbc))):
                    ?>
                    <h3>NO REPORTS ON YOUR CHILD</h3>
                <?php
//                if(empty(Collection::getAnnouncement($dbc))):
//                    ?>
<!--                    <h3>NO ANNOUNCEMENTS</h3>-->
                <?php
                else:
                    foreach (Collection::getReports($dbc) as $t):
                       $studentids=$t->getUserIds();
                        $arraystudents=explode(",", $studentids);
//                        print_r($arraystudents);
                        foreach (Collection::getParentChild($dbc) as $x):
                            if ($x->getUserId()==$user->getId()):
                                if(in_array($x->getChildId(),$arraystudents,TRUE)){
                                    $child=new user($dbc);
                                    $child->load($x->getChildId());
                                    ?>
                                    <div class="entry event col-12" >
                                        <div class="grid-inner row align-items-center g-0 p-4">
                                        <div class="col-md-8 ps-md-4">
                                        <div class="entry-title title-sm">
                                            <h2>
                                                <a href="<?php echo "/$role/report-details/" . $t->getId() ?>">
                                                    <?php echo $t->getTitle().' - '.$child->getUsername() ?>
                                                </a>
                                            </h2>
                                        </div>
                                        <div class="entry-content">
                                         <div class="row">
                                                <div class="col-12 my-2 "><strong>Report Created on:</strong>
                                                    <?php echo date("d/M/Y H:i:s", strtotime($t->getCreatedDate()))?></div>
                                                <div class="col-6 my-2 "><strong>Misconduct:</strong>
                                                    <?php echo $t->getType() ?>
                                             </div>
                                         </div>
                                        </div>
                                    </div>
                                        </div>
                                        <br/>
                                    </div>
                                    <?php
                                }
                            endif;
                        endforeach;
                        ?>
                    <?php
                    endforeach;
                endif;
                ?>

            </div>
        </div>
    </div>
</section><!-- #content end -->


<script type="text/javascript">

    function announcement_details($id) {
        var hiddenBtn = $("#hidden_big_button");
        hiddenBtn.attr('data-remote', '<?php echo "/$role/announcement-details" ?>?id=' + $id);
        hiddenBtn.attr('data-modal-title', 'Announcement Details');
        hiddenBtn.attr('data-loading-text', 'Announcement Details Loading...');
        hiddenBtn.click();
    }

</script>

<?php
include(PARENT_FOOTER);
?>
