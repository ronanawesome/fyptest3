<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$page_title = _REGISTER;
$students=Collection::getStudents($dbc);
$breadcrumbs = [
    _REGISTER => '/' . $role . '/registration'
];

$session_user = new user($dbc);
$session_user->load($_SESSION['parent_id']);


include(PARENT_HEADER);

$userID=$session_user->getId();
$error = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $validator = new Validator;

    // fields info container
    $fields = array();


      $fields[] = array('index' => 'Child_Id', 'label' => 'Child ID', 'required' => true);


    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values


    $gender=$session_user->getGender();


    $parentchild=new ParentChild($dbc);
    if (!$problem) {
        do {
            
            $parentchild->setUserId($session_user->getId());
            $parentchild->setChildId($cv['Child_Id']);
            if( $session_user->getGender()=="Female"){
                $parentchild->setRelationship("Mother");
            }
            else{
                $parentchild->setRelationship("Father");
            }
            $parentchild->create();
            echo "<script>
                        window.location.href=('assign-child?alert-success=Assignment Successfully');
                  </script>";
        } while (0);
    } else {
        $error['problem'] = $problem;
    }

}


$form = new FormInput();
?>
<head>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <style>

        .log-form form {
            display: block;
            width: 80%;
            padding: 2em;
            border: 1px solid #bbb;
            margin: auto;
        }
        .log-form h2 {
            color: white;
            font-size: 1.15em;
            display: block;
            background: #2a2a2a;
            width: 80%;
            text-transform: uppercase;
            padding: 0.75em 1em 0.75em 1.5em;
            box-shadow: inset 0px 1px 1px rgba(255, 255, 255, 0.05);
            border: 1px solid #1d1d1d;
            font-weight: bold;
            margin: auto;
        }

        .log-form input {
            display: block;
            margin: auto auto;
            width: 100%;
            margin-bottom: 2em;
            padding: 0.5em 0;
            border: none;
            border-bottom: 1px solid #eaeaea;
            padding-bottom: 1.25em;
            color: #757575;
        }
        .log-form input:focus {
            outline: none;
        }
        .log-form .btn {
            display: inline-block;
            background-color: Transparent;
            border: 1px solid black;
            padding: 0.5em 2em;
            color: black;
            margin-right: 0.5em;
            box-shadow: inset 0px 1px 0px rgba(255, 255, 255, 0.2);
        }
        .log-form .btn:hover {
            background: black;
            color: white;
        }
        .log-form .btn:active {
            background: #1fb5bf;
            box-shadow: inset 0px 1px 1px rgba(0, 0, 0, 0.1);
        }
        .log-form .btn:focus {
            outline: none;
        }
        .log-form .forgot {
            color: #1ba0a9;
            font-weight: bold;
            line-height: 0.5em;
            position: relative;
            text-decoration: none;
            font-size: 0.75em;
            margin: 0;
            padding: 0;
            float: right;
        }
        .log-form .forgot:hover {
            color: #33d3de;
        }
    </style>
</head>



<body>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 110px;margin-top: 3%;">
            <div class="log-form">
                <h2><?php echo 'ASSIGN YOUR CHILD' ?></h2>
                <form method="post">
                    <div class="form-group">
                        <?php getAlert(); ?>
                        <?php print_error("problem", $error, '<div class="alert alert-danger" style="margin-top:15px;">', '</div>'); ?>
                    </div>
                    <div class=" row " >
                        <div class="col-lg-6" >
                            <label for="User_ID">Username:</label>
                            <input disabled type="text" class="form-control" id="User_Id"
                                   name="User_Id" value=" <?php echo $session_user->getUsername();?>">
                        </div>
                    </div>

                    <div class=" row " >
                        <label class=" font-weight-bold">Select Child
                        <div class="col-lg-6 ">
                        <?php
                        if (empty( $students)) {
                            ?>
                            <div class="mb-3 form-check form-check-inline col-md-12">
                                <p class="mb-3 text-center col-12 font-weight-bold py-2 my-0">No Students Found</p>
                            </div>
                            <?php
                        }
                        else
                        {
                            $choices = [];
                            foreach ( $students as $ap){
                                $choices[$ap->getId()] = $ap->getUsername();
                            }
                            ?>
                            <select id="Child_Id" id="Child_Id"  class="mb-3 form-control js-example-basic-single" name="Child_Id" >
                                <?php foreach ($choices as $key => $choice) : ?>
                                    <option value="<?php echo $key ?>" <?php echo empty($key) ? 'selected' : '' ?>><?php echo $choice ?></option>
                                <?php endforeach;  ?>
                            </select>
                            <?php
                        }
                        ?>
                        </div>
                    </div>
                    <br>

                    <input type="hidden" title="balance" name="balance" value="0"/>
                    <button type="submit" class="btn" ><?php echo 'ASSIGN' ?></button><br>
                    <a href="home" class="btn btn-light-warning font-weight-bold mr-2">Finish Assignment</a>
                </form>
            </div><!--end log form -->
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });

</script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js">

</script>

<?php
include(PARENT_FOOTER);
?>
