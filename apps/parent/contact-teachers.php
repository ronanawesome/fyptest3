<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$session_user = new user($dbc);
$session_user->load($_SESSION['parent_id']);

$error = false;
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $validator = new Validator;

    $fields = array();
    $fields[] = array('index' => 'admin', 'label' => "Admin", 'required' => false);
    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    if (!$problem) {

    } else {
        $error = $problem;
    }
}



include(PARENT_HEADER);
include(PARENT_NAVBAR);
//prepare form input
$form = new FormInput();
$dbuilder = new DateBuilder();
?>
    <div class="main-container py-3">
        <div class="container py-2 md-12">
        <form id="search-form" action="" method="GET">
            <div class="float-left">
                <label style="font-size:25px "for="user" class="col-sm-12 col-lg-3 col-form-label">Teachers' contacts</label>
            </div>
            <div class="col-md-12 mb-10">
                <hr>
                <form id="search-form" action="" method="GET">
                    <div class="form-group row">
                        <label for="user" class="col-sm-3 col-lg-2 col-form-label"><?php echo "Teacher"; ?></label>
                        <div class="col-sm-9 col-lg-10">
                        <?php
                        $form->setPlaceholder("Teacher's Name / Phone No");
                        $form->createText('admin', '');
                        ?>
                            <small class="form-text text-muted"></small>
                        </div>
                    </div>
                    <br>
            <div class="form-group row">
                <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                    <input type="submit" value="<?php echo _SEARCH ?>" class="btn btn-primary btn-block" style="width: 100%"/>
                </div>
            </div>
        </form>
    </div>
   </div>
    <div class="main-container">
    <div class="container">
    <div class="row justify-content-center">
    <div class="col-12">
    <div class="card bg-light px-2 mb-2">
        <?php

        require_once('module/xcrud/xcrud.php');
        $xcrud = Xcrud::get_instance();
        $xcrud->table('user');

        $xcrud->where('Role','Teacher');
        if (!$error) {
            if ($cv['admin'] != "") {
                $xcrud->where('username LIKE "%' . $cv['admin'] . '%" OR PhoneNo LIKE "%' . $cv['admin'] . '%"');
            }
        }
        $xcrud_columns = [
            'Username',

            'Email',
            'PhoneNo',

        ];

        $xcrud_labels = [

            'PhoneNo'=>'Phone No',

        ];

        $xcrud_fields = [
            'Username',

            'Email',
            'PhoneNo',

        ];

        $xcrud->columns($xcrud_columns);
        $xcrud->fields($xcrud_fields);
        $xcrud->label($xcrud_labels);

        $xcrud->unset_add();
        //$xcrud->unset_search();
        $xcrud->unset_edit();
        $xcrud->unset_remove();
        $xcrud->unset_title();
        $xcrud->unset_view();
        $xcrud->unset_limitlist();
        $xcrud->unset_print();
        $xcrud->unset_csv();
        $xcrud->disabled('CreatedDate','edit');
        echo $xcrud->render();
        ?>
    </div>
    </div>
    </div>
    </div>
    </div>

    <script type="text/javascript">
        function agent_view($index) {
            var hiddenBtn = $("#hidden_big_button");
            hiddenBtn.attr('data-remote', '/<?php echo $role ?>/modal-agent-view/' + $index);
            hiddenBtn.attr('data-modal-title', 'View Agent Documents');
            hiddenBtn.attr('data-loading-text', 'Agent Info Loading...');
            hiddenBtn.click();
        }
    </script>


<?php
include(PARENT_FOOTER);



