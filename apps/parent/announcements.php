<?php

global $dbc, $url;
$role = $url['call_parts'][0];


$breadcrumbs = [
    _REGISTER => '/' . $role . '/registration'
];

$session_user = new user($dbc);
$session_user->load($_SESSION['parent_id']);


include(PARENT_HEADER);
include(PARENT_NAVBAR);

$error = array();
$dbuilder = new DateBuilder();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $validator = new Validator;

    // fields info container
    $fields = array();

    // fill the container with fields data

    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values



}
$form = new FormInput();
?>
<title>Announcements</title>
<style>
    .container{
        /*background-color: white;*/
        border-radius: 10px;

    }
    .grid-inner {
        background-color: lightskyblue;
        border-radius: 10px;
    }



</style>
<section id="page-title">

</section>
<!-- Content
============================================= -->
<section id="content">
    <div class="container clearfix">
        <div class="col-md-12 mb-10">
            <div class="row">
                <?php
                if (empty(Collection::getAnnouncement($dbc))):
                    ?>
                    <h3>NO ANNOUNCEMENTS</h3>
                <?php
                else:
                    foreach (Collection::getAnnouncement($dbc) as $t):
                        ?>
                        <div class="entry event col-12" >
                            <div class="grid-inner row align-items-center g-0 p-4">
                                <div class="col-md-8 ps-md-4">
                                    <div class="entry-title title-sm">
                                        <h2>
                                            <a href="<?php echo "/$role/announcement-details/" . $t->getId() ?>">
                                                <?php echo $t->getTitle() ?>
                                            </a>
                                        </h2>
                                    </div>
                                    <div class="entry-content">
                                        <div class="row">
                                            <div class="col-12 my-2 "><strong>Date Announced:</strong>
                                                <?php echo date("d/M/Y H:i:s", strtotime($t->getCreatedDatetime()))?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                        </div>
                    <?php
                    endforeach;
                endif;
                ?>

            </div>
        </div>
    </div>
</section><!-- #content end -->


<script type="text/javascript">

    function announcement_details($id) {
        var hiddenBtn = $("#hidden_big_button");
        hiddenBtn.attr('data-remote', '<?php echo "/$role/announcement-details" ?>?id=' + $id);
        hiddenBtn.attr('data-modal-title', 'Announcement Details');
        hiddenBtn.attr('data-loading-text', 'Announcement Details Loading...');
        hiddenBtn.click();
    }

</script>

<?php
include(STUDENT_FOOTER);
?>
