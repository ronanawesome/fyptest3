<?php
global $dbc, $url;
$role = $url['call_parts'][0];

if (!empty($url['call_parts'][2])) {
    $id = $url['call_parts'][2];
    $t = new Announcement($dbc);
    $t->load($id);
} else {
    redirect($role . '/tournament');
}

//$id = isset($_GET['id']) ? $_GET['id'] : '';
//$t = new Announcement($dbc);
//$t->load($id);
$user = new User($dbc);
$user->load($_SESSION['parent_id']);
//$player = $user->getPlayer();



//---------- page info --------------
//    must include before header
$title = "Announcement Details:: " . $t->getTitle();
$breadcrumbs = array(
    $title => '/' . $role . '/' . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------

$validator = new Rakit\Validation\Validator;
$fields = array();

$error = array();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $fields['tour_id'] = 'present|required';
    $fields['payment'] = 'present|required';

    $validation = $validator->make($_POST + $_GET + $_FILES, $fields);

    $validation->validate();
    $problem = $validation->errors();

    $data = $validation->getValidatedData();
}



include(PARENT_HEADER);
include(PARENT_NAVBAR);
?>

    <style>
        .entry-image {
            display: block;
            position: relative;
            width: 100%;
            height: 100% !important;
        }

        .entry-image img{
            display: block;
            position: relative;
            width: 100%;
            height: 100% !important;
            border: .5px solid #c8c8c8;
        }
        .h1{
       font-family: "Lato", sans-serif;
        }
    </style>

    <!-- Page Title
    ============================================= -->
    <section id="page-title">
        <div class="container clearfix">
            <h1 class="h1"> <?php echo $t->getTitle(); ?></h1>

        </div>

    </section>
    <!-- #page-title end -->

    <!-- Content
    ============================================= -->

        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="error-output">
                        <?php
                        getAlert();
                        ?>
                    </div>
                    <div class="single-event">
                            <div class=" align-self-center ">
                                <h3 class="mb-0">Announcement Details</h3>
                                <hr>
                                <div class="card bg-light col-6 col-md-12 col-lg-12 col-12 my-1">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-6 col-sm-6 col-lg-5 p-1"><strong>Date: </strong>
                                            </div>
                                            <div class="col-12 col-sm-6 col-lg-7 p-1"> <?php echo $t->getCreatedDatetime() ?></div>
                                            <div class="col-6 col-sm-6 col-lg-5 p-1"><strong>Description: </strong></div>
                                            <div class="col-12 col-sm-6 col-lg-7 p-1"> <?php echo $t->getDescription() ?></div>
                                            <div class="col-6 col-sm-6 col-lg-5 p-1"><strong>File
                                                   </strong>
                                            </div>
                                            <div class="col-6 col-sm-6 col-lg-7 p-1">
                                                <td>
                                                    <?php
                                                    if ($t->getFile() == NULL):
                                                        ?>
                                                        <label>No file</label><br><?php
                                                    else:
                                                        ?> <a href="<?php echo "/" . $t->getFile() ?>"
                                                              target="_blank">Open File</a><br>
                                                    <?php
                                                    endif;
                                                    ?>
                                                </td>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


        </section>

    <!-- #content end -->


<?php
include(PARENT_FOOTER);
