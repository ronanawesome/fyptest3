<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$page_title = _REGISTER;



$session_user = new user($dbc);
$session_user->load($_SESSION['parent_id']);
//$student= new student($dbc);
//$student->loadstudentinfo($_SESSION['parent_id']);

include(PARENT_HEADER);
include(PARENT_NAVBAR);
//include(PARENT_BREADCRUMB);


$error = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $validator = new Validator;

    $fields = array();

    // fill the container with fields data
    $fields[] = array('index' => 'User_Id', 'label' => 'User ID', 'required' => true);
    $fields[] = array('index' => 'Child_Id', 'label' => 'Child ID', 'required' => true);
    $fields[] = array('index' => 'start_date', 'label' => 'Start Date');
    $fields[] = array('index' => 'end_date', 'label' => 'End Date');
    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    $parentchild=new ParentChild($dbc);
    if (!$problem) {
        do {
            $parentchild->setUserId($cv['firstname'] . $cv['lastname']);
            $parentchild->setChildId($cv['firstname']);

            $parentchild->create();
            echo "<script>
                        window.location.href=('login?alert-success=Register Successfully');
                  </script>";
        } while (0);
    } else {
        $error['problem'] = $problem;
    }

}

$form = new FormInput();
$dbuilder = new DateBuilder();
?>

<title>Agent - Order History</title>

<style>
    .container{
        /*background-color: white;*/
        border-radius: 10px;
    }
</style>


<div class="main-container py-3">
    <div class="container py-2">
        <div class="col-md-16 mb-10">
            <div class="col-md-12 mb-10">
                <label style="font-size:25px "for="user" class="col-sm-12 col-lg-6 col-form-label">Child Attendance</label>
            </div>
            <hr>
            <form id="search-form" action="" method="GET">
                <br>
                <div class="form-group row pb-3">
                    <label for="user" class="col-sm-3 col-lg-2 col-form-label">Date</label>
                    <div class="col-sm-9 col-lg-10">                          <?php
                        $dbuilder->build_range_with_default("start_date", "", "end_date", "");
                        ?>

                        <small class="form-text text-muted"></small>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                        <input type="submit" value="<?php echo _SEARCH ?>" class="btn btn-primary btn-block" style="width: 100%"/>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>




</div>

<div class="main-container">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card bg-light px-2 mb-2">
                    <?php
                    require_once('module/xcrud/xcrud.php');
                    $xcrud = Xcrud::get_instance();
                    $xcrud->table('Attendance');
                    $xcrud->join('Class_Id','ClassRoom','id');

                    $xcrud->join('Class_Id','Student','Class_Id');
                    $xcrud->join('Student.User_Id','ParentChild','Child_Id');
//                    $xcrud->join('User_ID','ParentChild','Child_Id');

                    $xcrud->where('ParentChild.User_Id=', $session_user->getId() );



                    $xcrud_columns = [
                        'id',
                        'Date',
                        'ClassRoom.Class_Name',
                    ];

                    $xcrud_labels =array(
                        'ClassRoom.Class_Name'=>'ClassName',

                    );

                    $xcrud_fields = [
                        'id',
                        'Date',
                        'ClassRoom.Class_Name',
                    ];

                    $xcrud->columns($xcrud_columns);
                    // $xcrud->fields($xcrud_fields);
                    $xcrud->label($xcrud_labels);
                    $xcrud->button('attendance-detail/{id}', 'Details', 'fa fa-edit', 'btn btn-warning');
                    $xcrud->unset_add();
                    $xcrud->unset_edit();
                    $xcrud->unset_search();
                    $xcrud->unset_remove();
                    $xcrud->unset_title();
                    $xcrud->unset_view();
                    $xcrud->unset_limitlist();
                    $xcrud->unset_print();
                    $xcrud->unset_csv();

                    echo $xcrud->render();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    function sale_info($id) {
        var hiddenBtn = $("#hidden_big_button");
        hiddenBtn.attr('data-remote', '<?php echo "/$role/modal-sale-info" ?>?id=' + $id);
        hiddenBtn.attr('data-modal-title', 'Detailed Sale Information');
        hiddenBtn.attr('data-loading-text', 'Customer Info Loading...');
        hiddenBtn.click();
    }

</script>

<?php
include(PARENT_FOOTER);
?>
