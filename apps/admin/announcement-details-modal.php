<?php

global $dbc, $url;
$role = $url['call_parts'][0];


$id = isset($_GET['id']) ? $_GET['id'] : '';

$error = false;
$announcement = new Announcement($dbc);
$user = new User($dbc);
$user->load($_SESSION['admin_id']);
if (empty($announcement->load($id))) {
    $error = "Invalid ID!";
}
$uid = $user->getId();

$form = new FormInput();
?>
<form action="" id="modal-form">
    <div class="row">
        <div class="col-md-12">
            <div id="printOrder">
                <div class="row">
                    <div class="col-md-12 mb-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <input type="hidden" name="announcement"
                                           value="<?php echo $announcement->getId(); ?>">
                                    <input type="hidden"
                                           name="user_id"
                                           value="<?php echo $user->getId(); ?>">
                                    <div class="error-output">
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    Title
                                                </td>
                                                <td>
                                                    <?php
                                                    $form->setPlaceholder('Title');
                                                    $form->setClass('title');
                                                    $form->setExtra('disabled',true);
                                                    $form->createText('title', $announcement->getTitle());
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Description
                                                </td>
                                                <td>
                                                    <?php
                                                    $form->setPlaceholder('Content');
                                                    $form->setClass('content');
                                                    $form->setExtra('disabled',true);
                                                    $form->createTextarea('description', $announcement->getDescription());
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                            <tr>
                                                <td>
                                                    Date
                                                </td>
                                                <td>
                                                    <?php
                                                    $form->setPlaceholder('Date');
                                                    $form->setClass('Date');
                                                    $form->setExtra('style', 'height120px');
                                                    $form->setExtra('disabled',true);
                                                    $form->createTextarea('date', $announcement->getCreatedDatetime());
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    File
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($announcement->getFile() == NULL):
                                                        ?>
                                                        <label>No file</label><br><?php
                                                    else:
                                                        ?> <a href="<?php echo "/" . $announcement->getFile() ?>"
                                                              target="_blank">Open File</a><br>
                                                    <?php
                                                    endif;
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Year Target
                                                </td>
                                                <td>
                                                    <?php
                                                    $form->setPlaceholder('Date');
                                                    $form->setClass('Date');
                                                    $form->setExtra('disabled',true);
                                                    $form->createTextarea('date', 'Year: '.$announcement->getYearTarget());
                                                    ?>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
</form>
<!--                --><?php //endforeach; ?>


<script type="text/javascript">
    $(document).ready(function () {

        // Check admin api approve-order and pass required dat
    });
</script>
