<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$id = isset($_GET['id']) ? $_GET['id'] : '';

$error = false;
$classroom = new ClassRoom($dbc);
$classroom->load($id);
$teachers=Collection::getTeachers($dbc);
$user = new User($dbc);
$user->load($_SESSION['admin_id']);

if (empty($classroom->load($id))) {
    $error = "Invalid ID!";
}
$form = new FormInput();
?>
<form action="" id="modal-form">
    <div class="row">
        <div class="col-md-12">
            <div id="printOrder">
                <div class="row">
                    <div class="col-md-12 mb-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <input type="hidden" id="Class_Id" name="Class_Id" value="<?php echo $classroom->getId(); ?>">
                                    <div class="error-output"></div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>
                                                    Class Name
                                                </td>
                                                <td>
                                                    <?php
//                                                    $form->createHidden('Student_Id', $student->getId());
                                                    $form->setPlaceholder('Class ID');
                                                    $form->setClass('Class_ID');
                                                    $form->setExtra('disabled',true);
                                                    $form->createText('Class_Name', $classroom->getClassName());
                                                    ?>
                                                </td>
                                            </tr>
               <tr>
                                 <td>
                                                                    Class Teacher Name
                                                                </td>
                                                                <td>
                                                            <?php
                                                            if (empty( $teachers)){
                                                                ?>
                                                                <div class="mb-3 form-check form-check-inline col-md-12">
                                                                    <p class="mb-3 text-center col-12 font-weight-bold py-2 my-0">No Teachers Found</p>
                                                                </div>
                                                                <?php
                                                            }
                                                            else
                                                            {
                                                                $choices = [];
                                                                foreach ($teachers as $ap){
                                                                    $choices[$ap->getId()] = $ap->getUsername();
                                                                }
                                                                ?>
                                                                <select id="CTeacher_Id"   class="mb-3 form-control js-example-basic-single" name="CTeacher_Id" >
                                                                    <?php foreach ($choices as $teacher_id => $teacher_name) : ?>
                                                                        <option value="<?php echo $teacher_id ?>"
                                                                            <?php
                                                                            if($classroom->getCTeacherId() == $teacher_id):
                                                                                echo "selected";
                                                                            endif;
                                                                            ?>
                                                                        >
                                                                            <?php echo $teacher_name ?>
                                                                        </option>
                                                                    <?php endforeach;  ?>
                                                                    <option value="NULL">No Teacher</option>
                                                                </select>
                                                                <?php
                                                            }
                                                            ?>
                                                                </td>
                                                                </tr>
                                                <td colspan="4">
                                                    <button type="submit" class="btn btn-block btn-success update-status">Assign Class Teacher</button>
                                                </td>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
</form>
<!--                --><?php //endforeach; ?>


<script type="text/javascript">
    $(document).ready(function () {
        $('.js-example-basic-single').select2();
        // Check admin api approve-order and pass required data
        $('#modal-form').submit(function (e) {
            e.preventDefault();


            $.ajax({
                url: '/admin-API/classroom-set-teacher',
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log('Completed');
                    location.href = "<?php
                        echo "/$role/classrooms?alert-success=Update Successfully"
                        ?>";
                },
                error: function (xhr, status, error) {
                    var resp = eval("(" + xhr.responseText + ")");
                    alert(resp.error)
                },
            });
        });

    });
</script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js">
