<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$error = false;
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $validator = new Validator;

    $fields = array();
    $fields[] = array('index' => 'admin', 'label' => "Admin", 'required' => false);
    $fields[] = array('index' => 'start_date', 'label' => 'Start Date','required' => false);
    $fields[] = array('index' => 'end_date', 'label' => 'End Date','required' => false);



    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    if (!$problem) {

    } else {
        $error = $problem;
    }

}

$title = 'Exam Management';
$breadcrumbs = array(
    $title => '/' . $role . '/' . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------


include(ADMIN_HEADER);

//prepare form input
$form = new FormInput();
$dbuilder = new DateBuilder();
?>
    <div class="row">
    <div class="col-md-12">
        <form id="search-form" action="" method="GET">
            <?php
            getAlert();
            ?>
            <div class="form-group row">
                <label for="user" class="col-sm-3 col-lg-2 col-form-label">Exams</label>
                <div class="col-sm-9 col-lg-10">
                    <div class="input-group">
                        <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </span>
                        </div>

                        <?php
                        $form->setPlaceholder("Exam Name");
                        $form->createText('admin', '');
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="user" class="col-sm-3 col-lg-2 col-form-label"><?php echo "Date Range" ?></label>
                <div class="col-sm-9 col-lg-10">
                    <?php
                    $dbuilder->build_range_with_default("start_date", "", "end_date", "");
                    ?>
                    <small class="form-text text-muted"></small>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                    <input type="submit" value="Search" class="btn btn-primary btn-block">
                </div>
            </div>
            <div class="col-md-12 mt-3 d-flex flex-row-reverse">
                <a href="create-exam-modal" class="btn btn-light-warning font-weight-bold mr-2">Create Exam</a>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <?php
        require_once('module/xcrud/xcrud.php');
        $xcrud = Xcrud::get_instance();
        $xcrud->table('Exam');
        $xcrud->join('Subject_Id','Subject','id');
        if (!$error) {

            if ($cv['admin'] != "") {
                $xcrud->where('Exam_Name LIKE "%' . $cv['admin'] . '%"');
            }
        }
        if (!$error) {
            if ($cv['start_date'] != "") {
                $xcrud->where("DATE(Exam.Exam_Date) >= '" . $cv['start_date'] . "'");
            }
            if ($cv['end_date'] != "") {
                $xcrud->where("DATE(Exam.Exam_Date) <= '" . $cv['end_date'] . "'");
            }
        }
        $xcrud_columns = [
            'id',
            'Exam_Name',
            'Subject.Subject_Code',
            'Exam_Date',
        ];

        $xcrud_labels = [
            'id' => 'Exam ID',
            'Exam_Name' => 'Exam Name',
            'Subject.Subject_Code'=>'Subject Code',
            'CreatedDate' =>  'Registered Date',

        ];

        $xcrud_fields = [
            'id',
            'Exam_Name',
            'Subject.Subject_Code',
            'Exam_Date',
        ];

        $xcrud->columns($xcrud_columns);
        $xcrud->fields($xcrud_fields);
        $xcrud->label($xcrud_labels);
       $xcrud->unset_add();
        $xcrud->unset_search();
        $xcrud->unset_view();
        //$xcrud->unset_remove();
        $xcrud->unset_title();
        $xcrud->unset_edit();


        echo $xcrud->render();
        ?>
    </div>

    <script type="text/javascript">

    </script>


<?php
include(ADMIN_FOOTER);




