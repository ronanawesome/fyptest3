<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$id = isset($_GET['id']) ? $_GET['id'] : '';
$student = new Student($dbc);
$student->load($id);
$classroom = $student->getClass();
$intake=$student->getIntake();
//$classroom->
//print_array($intake);
//echo $classroom->getClassName();
//die();
$error = false;

$classrooms=Collection::getClassRoom($dbc);
$intakes=Collection::getIntake($dbc);
$user = new User($dbc);
$user->load($_SESSION['admin_id']);

if (empty($student->load($id))) {
    $error = "Invalid ID!";
}

$form = new FormInput();
?>

<form action="" id="modal-form">
    <div class="row">
        <div class="col-md-12">
            <div id="printOrder">
                <div class="row">
                    <div class="col-md-12 mb-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
<!--                                    <input type="hidden" name="user_id" value="--><?php //echo $user->getId(); ?><!--">-->
                                    <div class="error-output"></div>
                                    <div class="table-responsive">
                                      <table class="table table-bordered">
                                                  <tbody>
                                                 <tr>
                                                     <td>
                                                         ID
                                                     </td>
                                                      <td>
                                                         <?php
                                                         $form->createHidden('Student_Id',$student->getId());
                                                         $form->setPlaceholder('Student ID');
                                                         $form->setClass('Student_Id');
                                                         $form->setExtra('disabled',true);
                                                         $form->createText('Student_Id',$student->getId());

                                                         ?>
                                             </td>
                                          </tr>
                                                <td>
                                                    Class
                                                </td>
                                                <td>
                                                    <?php
                                                    if (  empty( $classrooms)){
                                                        ?>
                                                        <div class="mb-3 form-check form-check-inline col-md-12">
                                                            <p class="mb-3 text-center col-12 font-weight-bold py-2 my-0">No Classes Found</p>
                                                        </div>
                                                        <?php
                                                    }
                                                    else
                                                    {
                                                        $choices = [];
                                                        foreach ($classrooms as $ap){
                                                            $choices[$ap->getId()] = $ap->getClassName();
                                                        }
                                                        ?>
                                                        <select id="Class_Id"   class="mb-3 form-control js-example-basic-single" name="Class_Id" >
                                                            <?php foreach ($choices as $classroom_id => $classroom_name) : ?>
                                                                <option value="<?php echo $classroom_id ?>"
                                                                    <?php
                                                                        if($classroom->getId() == $classroom_id):
                                                                            echo "selected";
                                                                        endif;
                                                                    ?>
                                                                >
                                                                    <?php echo $classroom_name ?>
                                                                </option>
                                                            <?php endforeach;
                                                            ?>
                                                        </select>

                                                        <?php
                                                    }
                                                    ?>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Intake
                                                </td>
                                                <td>
                                                    <?php
                                                    if (empty( $intakes)){
                                                        ?>
                                                        <div class="mb-3 form-check form-check-inline col-md-12">
                                                            <p class="mb-3 text-center col-12 font-weight-bold py-2 my-0">No Intakes Found</p>
                                                        </div>
                                                        <?php
                                                    }
                                                    else
                                                    {
                                                        $choices = [];
                                                        foreach ($intakes as $ap){
                                                            $choices[$ap->getId()] = $ap->getIntakeCode();
                                                        }
                                                        ?>
                                                        <select id="Intake_Id"  name="Intake_Id" class="mb-3 form-control js-example-basic-single"  >
                                                            <?php foreach ($choices as $intake_id => $intake_code) : ?>
                                                                <option value="<?php echo $intake_id ?>"
                                                                    <?php
                                                                    if($intake->getId() == $intake_id):
                                                                        echo "selected";
                                                                    endif;
                                                                    ?>
                                                                >
                                                                    <?php echo $intake_code ?>
                                                                </option>
                                                            <?php endforeach;  ?>
                                                        </select>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>

                                            <tr>
                                                <td colspan="4">
                                                    <button type="submit" class="btn btn-block btn-success update-status">
                                                        Update Details
                                                    </button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
</form>
<!--                --><?php //endforeach; ?>


<script type="text/javascript">
    $(document).ready(function () {

        // Check admin api approve-order and pass required data
        $('#modal-form').submit(function (e) {
            e.preventDefault();


            $.ajax({
                url: '/admin-api/student-class-update',
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log('Completed');
                    location.href = "<?php
                        echo "/$role/student-classroom?alert-success=Update Successfully"
                        ?>";
                },
                error: function (xhr, status, error) {
                    var resp = eval("(" + xhr.responseText + ")");
                    alert(resp.error)
                },
            });
        });

    });
</script>
