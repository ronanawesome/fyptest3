<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$error = false;
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $validator = new Validator;

    $fields = array();
    $fields[] = array('index' => 'admin', 'label' => "Admin", 'required' => false);

    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    if (!$problem) {

    } else {
        $error = $problem;
    }

}

//---------- page info --------------
//    must include before header
$title = 'User Management';
$breadcrumbs = array(
    $title => '/' . $role . '/' . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------


include(ADMIN_HEADER);

//prepare form input
$form = new FormInput();
$dbuilder = new DateBuilder();
?>
    <div class="row">
    <div class="col-md-12">
        <form id="search-form" action="" method="GET">
            <div class="form-group row">
                <label for="user" class="col-sm-3 col-lg-2 col-form-label">Parent/Guardian</label>
                <div class="col-sm-9 col-lg-10">
                    <div class="input-group">
                        <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </span>
                        </div>
                        <?php
                        $form->setPlaceholder("Parent/Guardian's Name / Phone No");
                        $form->createText('admin', '');
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                    <input type="submit" value="Search" class="btn btn-primary btn-block">
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <?php

        require_once('module/xcrud/xcrud.php');
        $xcrud = Xcrud::get_instance();
        $xcrud->table('user');
        $xcrud->join('id','ParentChild','User_Id');
        $xcrud->where('is_delete',0);
        if (!$error) {

            if ($cv['admin'] != "") {
                $xcrud->where('username LIKE "%' . $cv['admin'] . '%" OR PhoneNo LIKE "%' . $cv['admin'] . '%" OR Email LIKE "%' . $cv['admin'] . '%"');       }

        }
        $xcrud->where('RegistrationApproval', 'approved');
        $xcrud->where('Role','Parent');

        $xcrud_columns = [
            'Username',
            'Fname',
            'LName',
            'Email',
            'PhoneNo',
            'CreatedDate',
            'ParentChild.Child_Id'
        ];

        $xcrud_labels = [
            'Fname' => 'First Name',
            'LName' => 'Last Name',
             'PhoneNo'=>'Phone No',
            'CreatedDate' =>  'Registered Date',

        ];

        $xcrud_fields = [
            'Username',
            'Fname',
            'Email',
            'PhoneNo',
            'CreatedDate',
        ];

        $xcrud->columns($xcrud_columns);
        $xcrud->fields($xcrud_fields);
        $xcrud->label($xcrud_labels);


        $xcrud->unset_add();
        $xcrud->unset_search();
        $xcrud->replace_remove('intake_soft_delete');
        $xcrud->unset_remove(true, 'is_delete', '=', '1');
        $xcrud->unset_title();
        $xcrud->unset_view();
        $xcrud->unset_limitlist();
        $xcrud->unset_print();
        $xcrud->unset_csv();
       // $xcrud->disabled('Email','edit');
        $xcrud->disabled('CreatedDate,Email,PhoneNo','edit');
        echo $xcrud->render();
        ?>
    </div>


    <script type="text/javascript">
        function agent_view($index) {
            var hiddenBtn = $("#hidden_big_button");
            hiddenBtn.attr('data-remote', '/<?php echo $role ?>/modal-agent-view/' + $index);
            hiddenBtn.attr('data-modal-title', 'View Agent Documents');
            hiddenBtn.attr('data-loading-text', 'Agent Info Loading...');
            hiddenBtn.click();
        }
    </script>


<?php
include(ADMIN_FOOTER);



