<?php

global $dbc;
global $url;

$role = $url['call_parts'][0];

$id = isset($_GET['id']) ? $_GET['id'] : '';
$user = new user($dbc);
$user->load($id);
$errors = array();
$form = new FormInput();
?>
<form action="" id="modal-form">
<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-8  col-sm-6 ">
                <div class="card card-custom">
                    <div class="card-header">
                        <h3 class="card-title">User Information</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>User Name</label>
                            <div class="input-group">
                                <?php
                                $form->createHidden('id',$user->getId());
                                $form->setExtra('disabled', true);
                                $form->createText('', $user->getUsername());
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <div class="input-group">
                                <?php
                                $form->setExtra('disabled', true);
                                $form->createText('', $user->getEmail());
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Address</label>
                            <div class="input-group" >
                                <?php
                                $form->setExtra('disabled', true);
                                $form->createText('', $user->getStreet().' '.$user->getCity().' '.$user->getPostCode());
                                ?>

                    </div>
                </div>

                <div class="form-group">
                    <label>Role</label>
                    <div class="input-group" >
                        <?php
                        $form->setExtra('disabled', true);
                        $form->createText('', $user->getRole());
                        ?>
                    </div>
                </div>

        </div>
                    <div class="form-group row">
                        <label for="user" class="col-sm-3 col-lg-2 col-form-label">Approval</label>
                        <div class="col-sm-9 col-lg-10">
                            <?php
                            $choices = [];
                            foreach (Common::getApprovalStatus() as  $k => $v) {
                                $choices[$k] = $v;
//                                $choices[$approval_status] = $approval_status;
                            }
                            $form->createSelect('RegistrationApproval', $choices, $user->getRegistrationApproval());
                            ?>
                            <small class="form-text text-muted"></small>
                        </div>
                    </div>
                    </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary mr-2"
                            >Approve</button>
                            <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
            </form>
<script>

    $(document).ready(function () {

        $('#modal-form').submit(function (e) {
            e.preventDefault();
            var $this = $(this);
            $.ajax({
                type: "POST",
                url: "/admin-api/user-approval",
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log('Completed');
                    location.href = "<?php
                        echo "/$role/registration-approval?alert-success=Update Successfully"
                        ?>";
                },
                error: function (xhr, status, error) {
                    var resp = eval("(" + xhr.responseText + ")");
                    alert(resp.error)
                },
            });
        });
    })

</script>
