<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$admin = new user($dbc);
$admin->load($_SESSION['admin_id']);
$announcement = new Announcement($dbc);
//$intake=Collection::getIntake($dbc);
$classroom=Collection::getClassRoom($dbc);
//---------- page info --------------
//    must include before header
$title = 'Create Announcement Report';
$breadcrumbs = array(

    $title => "/$role/" . $url['call_parts'][1]);
$card_title = "";
$card_icon = "";
//-----------------------------------

$validator = new Rakit\Validation\Validator;

$fields = array();
$error = array();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $fields['Title'] = 'required';
    $fields['Description'] = 'required';
    $fields['File'] = 'nullable';

    $fields['Year_Target'] = 'array|required';

    $validation = $validator->make($_POST + $_GET + $_FILES, $fields);

    $validation->validate();
    $problem = $validation->errors();

    $cv =   $validation->getValidatedData(); // get the form values

    //ERROR !problem is from the previous validation this one got another format \
    if (!$validation->fails()) {
        do {
            $dbc->beginTransaction();
              $File=fileUpload($_FILES['File'],'announcement');
              $announcement->createAnnouncement($cv,$admin->getId(),$File);
            $dbc->commit();
            $msg = "Announcement Created.";
            redirect($role . "/announcements?alert-success=" . $msg);
        } while (0);
    } else {
        $msg_fail = "";
        $errors = $validation->errors();
        $messages = $errors->all();
        foreach($messages as $m){
            $msg_fail = $m . "<br/>" . $msg_fail;
        }
//        this is working for me so it should work for you.

        redirect($role . "/create-announcement?alert-danger=" . $msg_fail);


    }
}

include(ADMIN_HEADER);

//prepare form input
$form = new FormInput();

?>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    </head>
    <body>
<form  class="form" method="post" enctype="multipart/form-data">
    <div class="card-body">
        <?php getAlert() ?>
        <div class="form-group">
        </div>
        <h3 class="font-size-lg text-dark font-weight-bold mb-6">Announcement Details:</h3>
        <div class="mb-15">

            <div class="row">
                <div class="form-group col-12 col-md-6">
                    <label class="col-lg-12 col-form-label">Title:</label>
                    <div class="col-lg-12">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder('Announcement Title');
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('Title', '');
                        ?>
                    </div>
                </div>
                <div class="form-group col-12 col-md-6">
                    <label class="col-lg-12 col-form-label">File:</label>
                    <div class="col-lg-12">
                        <input class="form-control" type="file" name="File">
                    </div>
                </div>

                <div class="form-group col-12 col-md-6">
                    <label class="col-lg-12 col-form-label for="textarea">Description:
                    <span class="text-danger"></span></label>
                    <textarea class="col-lg-12 form-control" id="Description" name="Description" rows="3"></textarea>
                </div>
                <div class="form-group col-12 col-md-6">
                    <label class="col-lg-12 col-form-label font-weight-bold">Year Target:</label>
                    <div class="col-lg-12  border" style="border-radius: 0.42rem;">
                                            <?php
                                                for ($x=1;$x <= 6; $x++):
                                                    ?>
                                                    <div class="form-check form-check-inline col-md-3">
                                                        <input class="col-lg-6 form-controlform-check-input" type="checkbox" id="Year_Target" name="Year_Target[]"
                                                               value="<?php echo $x ?>">
                                                        <label class="form-check-label">Year <?php echo $x?>
                                                               </label>
                                                    </div>
                                                <?php
                                                     endfor;
                                            ?>
                    </div>
                </div>
            </div>

        </div>
        </div>

        <div class="card-footer">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href="announcements" class="btn btn-secondary">
                        Cancel
                    </a>
                </div>
            </div>
        </div>

    </div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
        dropdownParent:$('#modal-container')
    });

</script>


<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<?php

include(STUDENT_FOOTER);
?>