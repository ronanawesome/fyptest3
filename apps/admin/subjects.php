<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$error = false;
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $validator = new Validator;

    $fields = array();
    $fields[] = array('index' => 'admin', 'label' => "Admin", 'required' => false);

    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    if (!$problem) {

    } else {
        $error = $problem;
    }

}


$title = 'Subject Management';
$breadcrumbs = array(
    $title => '/' . $role . '/' . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------


include(ADMIN_HEADER);

//prepare form input
$form = new FormInput();
$dbuilder = new DateBuilder();
?>
    <div class="row">
    <div class="col-md-12">
        <form id="search-form" action="" method="GET">
            <div class="form-group row">
                <label for="user" class="col-sm-3 col-lg-2 col-form-label">Subject</label>
                <div class="col-sm-9 col-lg-10">
                    <div class="input-group">
                        <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </span>
                        </div>
                        <?php
                        $form->setPlaceholder("Subject Name/ Code");
                        $form->createText('admin', '');
                        ?>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                    <input type="submit" value="Search" class="btn btn-primary btn-block">
                </div>
            </div>
            <div class="col-md-12 mt-3 d-flex flex-row-reverse">
                <a href="create-subject" class="btn btn-light-warning font-weight-bold mr-2">Create New Subject</a>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <?php

        require_once('module/xcrud/xcrud.php');
        $xcrud = Xcrud::get_instance();
        $xcrud->table('Subject');


        if (!$error) {

            if ($cv['admin'] != "") {
                $xcrud->where('Subject_Name LIKE "%' . $cv['admin'] . '%" OR Subject_Level LIKE "%' . $cv['admin']. '%"
                OR Subject_Code LIKE "%' . $cv['admin'] . '%"');
            }

        }


        $xcrud_columns = [
            'id',
            'Subject_Name',
            'Subject_Level',
            'Subject_Code'

        ];

        $xcrud_labels = [
            'id' => 'Subject ID',
            'Subject_Name' => 'Subject Name',
            'Subject_Level'=>'Subject Level',
            'Subject_Code'=>'Subject Code'

        ];

        $xcrud_fields = [
            'id',
            'Subject_Name',
            'Subject_Level',
            'Subject_Code'
        ];

        $xcrud->columns($xcrud_columns);
        $xcrud->fields($xcrud_fields);
        $xcrud->label($xcrud_labels);
        //        $xcrud->button('/' . $role . '/user-agent-login/{Id}', 'Login', 'fa fa-play', '');
        //        $xcrud->button('/' . $role . '/user-agent-password/{Id}', 'Change password', 'fa fa-lock', '');
        //        $xcrud->button('javascript:agent_view({user.id})', 'View Documents', 'fas fa-photo-video', '');
        $xcrud->unset_add();
        $xcrud->unset_search();
        //$xcrud->unset_remove();
        $xcrud->unset_title();
        $xcrud->unset_view();
        //$xcrud->unset_limitlist();
        $xcrud->unset_print();
        $xcrud->unset_csv();

        echo $xcrud->render();
        ?>
    </div>

    <script type="text/javascript">
        function add_newuser($index) {
            var hiddenBtn = $("#hidden_big_button");
            hiddenBtn.attr('data-remote', '/<?php echo $role ?>/create-examset/' + $index);
            hiddenBtn.attr('data-modal-title', 'Add New Exam Set');
            hiddenBtn.attr('data-loading-text', 'Withdraw Info Loading...');
            hiddenBtn.click();
        }

        function agent_view($index) {
            var hiddenBtn = $("#hidden_big_button");
            hiddenBtn.attr('data-remote', '/<?php echo $role ?>/modal-agent-view/' + $index);
            hiddenBtn.attr('data-modal-title', 'View Agent Documents');
            hiddenBtn.attr('data-loading-text', 'Agent Info Loading...');
            hiddenBtn.click();
        }
    </script>


<?php
include(ADMIN_FOOTER);





