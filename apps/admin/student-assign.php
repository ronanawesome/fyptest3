<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$error = false;
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $validator = new Validator;

    $fields = array();
    $fields[] = array('index' => 'admin', 'label' => "Admin", 'required' => false);
    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    if (!$problem) {
    } else {
        $error = $problem;
    }

}

//---------- page info --------------
//    must include before header
$title = 'User Management';
$breadcrumbs = array(
    $title => '/' . $role . '/' . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------


include(ADMIN_HEADER);

//prepare form input
$form = new FormInput();
$dbuilder = new DateBuilder();
?>
    <div class="row">
    <div class="col-md-12">
        <form id="search-form" action="" method="GET">
            <?php getAlert(); ?>
            <div class="form-group row">
                <label for="user" class="col-sm-3 col-lg-2 col-form-label">Student</label>
                <div class="col-sm-9 col-lg-10">
                    <div class="input-group">
                        <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </span>
                        </div>
                        <?php
                        $form->setPlaceholder("Student's Name / Phone No");
                        $form->createText('admin', '');
                        ?>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                    <input type="submit" value="Search" class="btn btn-primary btn-block">
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <?php
        require_once('module/xcrud/xcrud.php');
        $xcrud = Xcrud::get_instance();
        $xcrud->table('Student');
        $xcrud->join('User_Id','user','id');
        //$xcrud->join('Class_Name','ClassRoom','Class_Name');
        $xcrud->join('Intake_Id','Intakes','id');
        $xcrud->join('Class_Id','ClassRoom','id');
        if (!$error) {
            if ($cv['admin'] != "") {
                $xcrud->where('username LIKE "%' . $cv['admin'] . '%" OR PhoneNo LIKE "%' . $cv['admin'] . '%"');
            }
        }
        $xcrud_columns = [

            'user.Username',
            'ClassRoom.Class_Name',
            'ClassRoom.Class_Level',
            'Intakes.Intake_Code'
        ];
        $xcrud->button('javascript:student_update({Student.id})', 'Update Student', 'fa fa-edit', 'btn btn-light-warning', '',
            );
        $xcrud_labels = [

            'Username' => 'Student Name',
            'ClassRoom.Class_Name'=>'Class',
           'ClassRoom.Class_Level' =>  'Year',
            'Intakes.Intake_Code'=>'Intake Code'
        ];
        $xcrud_fields = [
            'id',
            'user.Username',
            'Student.Class_Name',
            'Student.Student_Level',
            'Intakes.Intake_Code'
        ];

        $xcrud->columns($xcrud_columns);
        $xcrud->fields($xcrud_fields);
        $xcrud->label($xcrud_labels);

        $xcrud->unset_add();
        $xcrud->unset_edit();
        $xcrud->unset_search();
        $xcrud->unset_remove();
        $xcrud->unset_title();
        $xcrud->unset_view();
        $xcrud->unset_limitlist();
        $xcrud->unset_print();
        $xcrud->unset_csv();
        $xcrud->disabled('CreatedDate','edit');
        echo $xcrud->render();
        ?>
    </div>


    <script type="text/javascript">
        function student_update($id) {
            var hiddenBtn = $("#hidden_big_button");
            hiddenBtn.attr('data-remote', '<?php echo "/$role/student-detail-modal" ?>?id=' + $id);
            hiddenBtn.attr('data-modal-title', 'Update Student Information');
            hiddenBtn.attr('data-loading-text', 'Student Info Loading...');
            hiddenBtn.click();
        }
    </script>
<?php
include(ADMIN_FOOTER);



