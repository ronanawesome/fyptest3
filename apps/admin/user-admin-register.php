<?php

global $dbc, $url;
$role = $url['call_parts'][0];

//---------- page info --------------
//    must include before header
$title = 'Registration';
$breadcrumbs = array(
    "Leader" => "javascript:void(0);",
    $title => "/$role/" . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------


$validator = new Rakit\Validation\Validator;
$fields = array();

$error = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $validator = new Validator;
    $fields[] = array('index' => 'firstname', 'label' => 'First Name', 'required' => true);
    $fields[] = array('index' => 'lastname', 'label' => 'Last Name', 'required' => true);
    $fields[] = array('index' => 'email', 'label' => 'Email', 'required' => true);
    $fields[] = array('index' => 'phoneNo', 'label' => 'Phone Number', 'required' => true);
    $fields[] = array('index' => 'password', 'label' => 'Password', 'required' => true);
    $fields[] = array('index' => 'confirm_password', 'label' => 'Confirm Password', 'required' => true);
    $fields[] = array('index' => 'street', 'label' => 'Street', 'required' => false);
    $fields[] = array('index' => 'postcode', 'label' => 'Post Code', 'required' => false);
    $fields[] = array('index' => 'city', 'label' => 'City', 'required' => false);
    $fields[] = array('index' => 'gender', 'label' => 'Gender', 'required' => true);
    $fields[] = array('index' => 'birthday', 'label' => 'Birthday', 'required' => true);
    $fields[] = array('index' => 'religion', 'label' => 'Religion', 'required' => true);


    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $data = $validator->escape_val();


    if (!$problem) {
        do {
            $dbc->beginTransaction();

            $user = new User($dbc);

            if (!is_numeric($data['phone'])) {
                $errors = "Invalid Phone Number!";
                break;
            }

            if ($data['confirm_password'] != $data['password']) {
                $errors = "Error! Confirm password is not same as password.";
                break;
            }

            if ($user->searchUser($data['username'])) {
                $errors = "Error! Username already exist! Please use different username.";
                break;
            }

            if ($user->searchEmail($data['email'])) {
                $errors = "Error! Email already exist!";
                break;
            }

            $user->registerAdmin($data['firstname'],$data['lastname'],$data['password'],$data['birthday'],$data['phoneNo'],$data['email'],$data['street'],$data['city'],$data['postcode'],$data['religion']);

            $dbc->commit();
//            $user->setUsername($cv['firstname'] . $cv['lastname']);
//            $user->setFName($cv['firstname']);
//            $user->setLName($cv['lastname']);
//            $user->setEmail($cv['email']);
//            $user->setGender($cv['gender']);
//            $user->setPhoneNo($cv['phoneNo']);
//            $user->setPassword(PasswordHasher::HashPassword($cv['password']));
//            $user->setBirthday($cv['birthday']);
//            $user->setStreet($cv['street']);
//            $user->setPostCode($cv['postcode']);
//            $user->setCity($cv['city']);
//            $user->setRole("Admin");
//            $user->setReligion($cv['religion']);
//            $user->setRegistrationApproval("pending");
//            $user->setIsDelete(0);
//            $user->create();
            $msg = "Registration Success.";
            redirect($role . "/user-admin?alert-success=" . $msg);
        } while (0);
    } else {
        $msg_fail = "Registration Failed.";
        $errors = $problem;
        redirect($role . "/user-admin-register?alert-danger=" . $msg_fail);
    }
}


include(ADMIN_HEADER);

//prepare form input
$form = new FormInput();

?>

    <form class="form" method="post" enctype="multipart/form-data">
        <div class="card-body">
            <?php getAlert() ?>
            <div class="form-group">
            </div>
            <h3 class="font-size-lg text-dark font-weight-bold mb-6">1. Admin Info:</h3>
            <div class="mb-15">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">First Name:</label>
                    <div class="col-lg-6">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder('Enter First Name');
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('firstname', '');
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Last Name:</label>
                    <div class="col-lg-6">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder('Enter Last Name');
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('lastname', '');
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Password:</label>
                    <div class="col-lg-6">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder('Enter Password');
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('password', '');
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Confirm Password:</label>
                    <div class="col-lg-6">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder('Enter Confirm Password');
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('confirm_password', '');
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Email:</label>
                    <div class="col-lg-6">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder('Enter Email');
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('email', '');
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Birthday:</label>
                    <div class="col-lg-6">
                <input type="date" name="birthday" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Gender:</label>
                    <div class="col-lg-6">
                <?php
                $choices = [];
                $choices['Male'] = _MALE;
                $choices['Female'] = _FEMALE;
                $form->setClass('form-control mb-4');
                $form->createSelect('gender', $choices,$choices['Male']);
                ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Phone No:</label>
                    <div class="col-lg-6">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder(_REGISTER_PH_NO);
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('phoneNo', '');
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Street:</label>
                    <div class="col-lg-6">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder(_ADDRESS_STREET);
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('street', '');
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">City:</label>
                    <div class="col-lg-6">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder(_ADDRESS_CITY);
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('city', '');
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Post Code:</label>
                    <div class="col-lg-6">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder(_ADDRESS_POST_CODE);
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('postcode', '');
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Religion Level:</label>
                    <div class="col-lg-6">
                        <select class="form-control"
                                name="religion" id="religion">
                            <option value=1>Christian</option>
                            <option value=2>Buddhist</option>
                            <option value=3>Sikh</option>
                            <option value=4>Hindu</option>
                            <option value=5>Muslim</option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <button type="reset" class="btn btn-secondary"
                      >Cancel</button>

                    </div>
                </div>
            </div>

        </div>
    </form>

<?php

include(ADMIN_FOOTER);
