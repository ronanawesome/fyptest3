<?php
global $dbc, $url;
$role = $url['call_parts'][0];

//---------- page info --------------
//    must include before header
$title = 'New Subject';
$breadcrumbs = array(
    "Admin" => "javascript:void(0);",
    $title => "/$role/" . $url['call_parts'][1]
);


$card_title = "";
$card_icon = "";


$error = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $fields = array();
    $validator = new Validator;
    $fields[] = array('index' => 'Subject_Name', 'label' => 'Exam Name', 'required' => true);
    $fields[] = array('index' => 'Subject_Level', 'label' => 'Subject ID', 'required' => true);
    $fields[] = array('index' => 'Subject_Code', 'label' => 'Exam_Date', 'required' => true);



    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values





    if (!$problem) {
        do {
            $dbc->beginTransaction();
            $subject = new Subject($dbc);
            if ($subject->searchSubject( $cv['Subject_Name'])) {
                $errors = "Error! Subject name already exist! Please use different subject name.";
                break;
            }
            $subject->setSubjectName($cv['Subject_Name']);
            $subject->setSubjectLevel($cv['Subject_Level']);
            $subject->setSubjectCode($cv['Subject_Code']);
            $subject->create();
            $dbc->commit();
            $errors = $problem;
            $msg_fail="Subject Successful fail";
            redirect($role . "/subjects?alert-success=" . $msg_fail.$errors);
        } while (0);
    } else {
        $msg_fail = "Subject Creation Failed.";
        $error['problem'] = $problem;
        redirect($role . "/create-subject?alert-danger=" . $msg_fail);
    }
}


include(ADMIN_HEADER);
//prepare form input
$form = new FormInput();

?>

    <form class="form" method="post" enctype="multipart/form-data">
        <div class="card-body">
            <?php getAlert() ?>
            <div class="form-group">
            </div>
            <h3 class="font-size-lg text-dark font-weight-bold mb-6">New Subject Details:</h3>
            <div class="mb-15">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Subject Name:</label>
                    <div class="col-lg-6">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder('Enter Subject Name');
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('Subject_Name', '');
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Subject Level:</label>
                    <div class="col-lg-6">
                            <select class="form-control"
                                    name="Subject_Level" id="Subject_Level">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                            </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Subject Code:</label>
                    <div class="col-lg-6">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder('Enter Subject Code.(i.e=ENGY1)');
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('Subject_Code', '');
                        ?>
                    </div>
                </div>


            </div>

            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <a  href="subjects" class="btn btn-secondary">
                            Cancel
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </form>

<?php

include(ADMIN_FOOTER);
