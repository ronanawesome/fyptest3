<?php
global $dbc, $url;
$role = $url['call_parts'][0];

//---------- page info --------------
//    must include before header
$title = 'New Classroom';
$breadcrumbs = array(
    "Admin" => "javascript:void(0);",
    //$title => "/$role/" . $url['call_parts'][1]
);


$card_title = "";
$card_icon = "";

$error = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $fields = array();
    $validator = new Validator;
    $fields[] = array('index' => 'Class_Name', 'label' => 'Class Name', 'required' => true);
    $fields[] = array('index' => 'Class_Level', 'label' => 'Class Level', 'required' => true);
    //$fields[] = array('index' => 'Capacity', 'label' => 'Exam_Date', 'required' => true);
    $fields[] = array('index' => 'CTeacher_Id', 'label' => 'CTeacher Id', 'required' => false);


    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values






    if (!$problem) {
        do {
            $dbc->beginTransaction();

            $classroom = new ClassRoom($dbc);

            if ($classroom->searchClassroom( $cv['Class_Level'].' '.$cv['Class_Name']))  { $msg_fail = "Exam Already Exist.";
                $errors = $problem;
                redirect($role . "/create-classroom?alert-danger=" . $msg_fail.$errors);
                break;
            }
            if(($classroom->checkTeacherClass($cv['CTeacher_Id'])==true)){
                $msg_fail = "Teacher is already a class teacher for another class.";
                $errors = $problem;
                redirect($role . "/create-classroom?alert-danger=" . $msg_fail.$errors);
                break;
            }
            $classroom->setClassName($cv['Class_Level'].' '.$cv['Class_Name']);
            $classroom->setClassLevel($cv['Class_Level']);
            $classroom->setCTeacherId($cv['CTeacher_Id']);
            $classroom->setCapacity(0);
            $classroom->create();
            $dbc->commit();
            $msg_fail = "Class Creation Successful.";
            $errors = $problem;
            redirect($role . "/classrooms?alert-success=" . $msg_fail.$errors);
//                        window.location.href=('/classrooms?alert-success=Successfully Created Classroom');
//                  </script>";
        } while (0);
    } else {
        $msg_fail = "Class Creation Failed.";
        $errors = $problem;
        redirect($role . "/create-classroom?alert-danger=".$msg_fail.$errors );

    }
}
include(ADMIN_HEADER);
//prepare form input
$form = new FormInput();
?>

    <form class="form" method="post" enctype="multipart/form-data">
        <div class="card-body">
            <?php getAlert() ?>
            <div class="form-group">
            </div>
            <h3 class="font-size-lg text-dark font-weight-bold mb-6">New Classroom Details:</h3>
            <div class="mb-15">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Classroom Name:</label>
                    <div class="col-lg-3">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder('Enter Classroom Name. i.e= Mars');
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('Class_Name', '');
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Class Level:</label>
                    <div class="col-lg-3">
                        <select class="form-control" id="exampleSelect1"
                                name="Class_Level" id="Class_Level">
                            <option value="" disabled selected>Select Class Level</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <label class="col-lg-3" for="Intake_Code">Class Teacher:</label>
                    <div class="col-lg-6" >
                        <select placeholder="-"class="col-lg-6 form-control js-example-basic-single" id="CTeacher_Id" name="CTeacher_Id">
                            <option value="" disabled selected>Select Class Teacher</option>
                            <option value="NULL">No Teacher</option>
                            <?php

                            $choices = [];
                            $teacher=Collection::getTeachers($dbc);
                            foreach ($teacher as $value) {
                                ?>
                                <option value="<?php echo $value->getId()?>"><?php echo $value->getUsername()?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>



            </div>

            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <a  href="classrooms" class="btn btn-secondary">
                            Cancel
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </form>
    <script type="text/javascript">

        $(document).ready(function() {
            $('.js-example-basic-single').select2();


        });

    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js">



<?php

include(ADMIN_FOOTER);
