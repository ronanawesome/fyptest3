<?php

global $dbc, $url;
$role = $url['call_parts'][0];

//---------- page info --------------
//    must include before header
$title = 'Create New Exam';
$breadcrumbs = array(
    "Leader" => "javascript:void(0);",
    $title => "/$role/" . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------


$error = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $fields = array();
    $validator = new Validator;
    $fields[] = array('index' => 'Exam_Name', 'label' => 'Exam Name', 'required' => true);
    $fields[] = array('index' => 'Subject_Id', 'label' => 'Subject ID', 'required' => true);
    $fields[] = array('index' => 'Exam_Date', 'label' => 'Exam_Date', 'required' => true);
    $fields[] = array('index' => 'Intake_Id', 'label' => 'Intake ID', 'required' => true);
  // $fields[] = array('index' => 'Intake_Code', 'label' => 'Intake Code', 'required' => true);

    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    if (!$problem) {
        do {
            $dbc->beginTransaction();
            $exam = new Exam($dbc);
            if ($exam->searchExam($cv['Exam_Name'].'-I'.$cv['Intake_Id'].'S'. $cv['Subject_Id']))
            { $msg_fail = "Exam Already Exist.";
                $errors = $problem;
                redirect($role . "/create-exam-modal?alert-danger=" . $msg_fail.$errors);
                break;
            }

            $exam->setExamName( $cv['Exam_Name'].'-I'.$cv['Intake_Id'].'S'. $cv['Subject_Id']);
            $exam->setSubjectId($cv['Subject_Id']);
            $exam->setExamDate($cv['Exam_Date']);
            $exam->setIntakeId($cv['Intake_Id']);
            $exam->create();

            if ($exam->searchExam($cv['Exam_Name'].'-I'.$cv['Intake_Id'].'S'. $cv['Subject_Id']))
            {
                $students = Collection::getStudentDetails($dbc);
                foreach ($students as $student) {
                    echo $student->getIntakeId() . "<br/>";
                    if ($student->getIntakeId() == $cv['Intake_Id']) {
                        $result = new ExamResult($dbc);
                        $result->setExamID($exam->getId());
                        $result->setUserID($student->getUserId());
                        $result->setExamMarks(0);
                        $result->setExamGrade('-');
                        $result->create();
                    }
                }
                $errors = "Exam Creation Successful.";
            }

            $dbc->commit();
            redirect($role . "/exams?alert-success=" .$errors);
        } while (0);
    } else {
        $msg_fail = "Exam Creation Failed.";
        $errors = $problem;
        redirect($role . "/create-exam-modal?alert-danger=" . $msg_fail.$errors);
    }
}


include(ADMIN_HEADER);

//prepare form input
$form = new FormInput();

?>

    <form class="form" method="post" enctype="multipart/form-data">
        <div class="card-body">
            <?php getAlert() ?>
            <div class="form-group">
            </div>
            <h3 class="font-size-lg text-dark font-weight-bold mb-6">New Exam Details:</h3>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label">Exam Semester:</label>
                <div class="col-lg-6">
                    <select class="form-control"
                            name="Exam_Name" id="Exam_Name">
                        <option value="" disabled selected>Select Exam Semester</option>
                        <option value="1st Semester">1st Semester</option>
                        <option value="2nd Semester">2nd Semester</option>
                        <option value="3rd Semester">3rd Semester</option>
                        <option value="4th Semester">4th Semester</option>
                        <option value="5th Semester">5th Semester</option>

                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-lg-3 col-form-label">Select Subject:</label>
                <div class="col-lg-6">
                    <select class="mb-6 form-control js-example-basic-single" name="Subject_Id" id="Subject_Id">
                        <?php
                        $choices = [];
                        $subject = Collection::getSubject($dbc);
                        foreach ($subject as $value) {
                            ?>
                            <option value="<?php echo $value->getId() ?>">
                                <?php echo $value->getSubjectName() ?> -
                                <?php echo $value->getSubjectCode() ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label">Exam Date:</label>
                <div class="col-lg-6">
                    <input type="datetime-local" id="Exam_Date" name="Exam_Date" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-lg-3 col-form-label" for="Intake_Code">Select Intake:</label>
                <div class="col-lg-6">
                    <select class="mb-6 form-control form-select" id="Intake_Id" name="Intake_Id">
                        <?php
                        $choices = [];
                        $intake = Collection::getIntake($dbc);
                        foreach ($intake as $value) {
                            ?>
                            <option value="<?php echo $value->getId()  ?>">
                                <?php echo $value->getIntakeCode() ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>

        </div>

        <div class="card-footer">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href="exams" class="btn btn-secondary">
                        Cancel
                    </a>
                </div>
            </div>
        </div>

        </div>
    </form>
    <script type="text/javascript">

        $(document).ready(function() {
            $('.js-example-basic-single').select2();


        });

    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js">

    </script>
<?php

include(ADMIN_FOOTER);
