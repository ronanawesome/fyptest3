<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$error = false;
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $validator = new Validator;

    $fields = array();
    $fields[] = array('index' => 'admin', 'label' => "Admin", 'required' => false);

    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    if (!$problem) {

    } else {
        $error = $problem;
    }

}


$title = 'Intake Management';
$breadcrumbs = array(
    $title => '/' . $role . '/' . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------


include(ADMIN_HEADER);

//prepare form input
$form = new FormInput();
$dbuilder = new DateBuilder();
?>
    <div class="row">
    <div class="col-md-12">
        <form id="search-form" action="" method="GET">
            <div class="form-group row">
                <label for="user" class="col-sm-3 col-lg-2 col-form-label">Intakes</label>
                <div class="col-sm-9 col-lg-10">
                    <div class="input-group">
                        <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </span>
                        </div>
                        <?php
                        $form->setPlaceholder("Intake Code/Year");
                        $form->createText('admin', '');
                        ?>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                    <input type="submit" value="Search" class="btn btn-primary btn-block">
                </div>
            </div>
            <div class="col-md-12 mt-3 d-flex flex-row-reverse">
                <a href="create-intake" class="btn btn-light-warning font-weight-bold mr-2">Create New Intake</a>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <?php
        require_once('module/xcrud/xcrud.php');
        $xcrud = Xcrud::get_instance();
        $xcrud->table('Intakes');


        if (!$error) {

            if ($cv['admin'] != "") {
                $xcrud->where('Intake_Code LIKE "%' . $cv['admin'] . '%" OR Year LIKE "%' . $cv['admin']. '%"');
            }
        }
        $xcrud->where('is_delete',0);
        $xcrud_columns = [
            'id',
            'Intake_Code',
            'Year',
            'No_of_Students',

        ];
        $xcrud_labels = [
            'id' => 'Intake ID',
            'Intake_Code' => 'Intake Code',
            'Year'=>'Intake Year',
            'No_of_Students'=>'Number of Students'
        ];
        $xcrud_fields = [
            'id',
            'Intake_Code',
            'Year',
            'No_of_Students'
        ];

        $xcrud->columns($xcrud_columns);
        $xcrud->fields($xcrud_fields);
        $xcrud->label($xcrud_labels);
     //  $xcrud->button('javascript:announcement_details({Announcement.id})', 'Details', 'flaticon2-search', 'btn btn-warning');

        $xcrud->unset_add();
        $xcrud->unset_search();
        $xcrud->replace_remove('intake_soft_delete');
        $xcrud->unset_remove(true, 'is_delete', '=', '1');
        $xcrud->unset_title();
        $xcrud->unset_view();
        $xcrud->unset_limitlist();
        $xcrud->unset_print();
        $xcrud->unset_csv();

        echo $xcrud->render();
        ?>
    </div>

    <script type="text/javascript">
        function add_newuser($index) {
            var hiddenBtn = $("#hidden_big_button");
            hiddenBtn.attr('data-remote', '/<?php echo $role ?>/create-examset/' + $index);
            hiddenBtn.attr('data-modal-title', 'Add New Exam Set');
            hiddenBtn.attr('data-loading-text', 'Withdraw Info Loading...');
            hiddenBtn.click();
        }

        function agent_view($index) {
            var hiddenBtn = $("#hidden_big_button");
            hiddenBtn.attr('data-remote', '/<?php echo $role ?>/modal-agent-view/' + $index);
            hiddenBtn.attr('data-modal-title', 'View Agent Documents');
            hiddenBtn.attr('data-loading-text', 'Agent Info Loading...');
            hiddenBtn.click();
        }
    </script>


<?php
include(ADMIN_FOOTER);





