<?php

global $dbc, $url;
$role = $url['call_parts'][0];


$error = false;
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $validator = new Validator;

    $fields = array();
    $fields[] = array('index' => 'agent', 'label' => 'user');
    $fields[] = array('index' => 'start_date', 'label' => 'Start Date');
    $fields[] = array('index' => 'end_date', 'label' => 'End Date');

    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    if (!$problem) {
    } else {

        $error = $problem;

    }
}


//---------- page info --------------
//    must include before header
$title = "Registration Approval";
$breadcrumbs = array(
    $title => "/$role/" . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------

include(ADMIN_HEADER);

//prepare form input
$form = new FormInput();
$dbuilder = new DateBuilder();

?>

    <div class="row">
        <div class="col-md-12 mb-10">
            <div class="float-left">
                <h2 class="font-weight-bold mt-1 mr-5"><?php echo $title; ?></h2>
            </div>
            <div class="float-left">
            </div>
        </div>

        <div class="col-md-12">
            <form id="search-form" action="" method="GET">
                <div class="form-group row">
                    <label for="user" class="col-sm-3 col-lg-2 col-form-label"><?php echo "User" ?></label>
                    <div class="col-sm-9 col-lg-10">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </span>
                            </div>
                            <?php
                            $form->setClass('form-control');
                            $form->setPlaceholder("User");
                            $form->createText('user', '');
                            ?>
                        </div>
                        <small class="form-text text-muted"></small>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="user" class="col-sm-3 col-lg-2 col-form-label"><?php echo "Request From" ?></label>
                    <div class="col-sm-9 col-lg-10">
                        <?php
                        $dbuilder->build_range_with_default("start_date", "", "end_date", "");
                        ?>
                        <small class="form-text text-muted"></small>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                        <input type="submit" value="<?php echo "Search" ?>" class="btn btn-primary btn-block"/>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-12">
            <div class="error-output">
                <?php
                if ($error) {
                    echo '<div class="alert alert-danger">' . $error . '</div>';
                } else {
                    getAlert();
                }
                ?>
            </div>
        </div>

        <div class="col-md-12">
            <?php

            require_once('module/xcrud/xcrud.php');
            $xcrud = Xcrud::get_instance();
            $xcrud->table('user');
            //$xcrud->join('id', 'ParentChild', 'User_Id');

            $xcrud->where('RegistrationApproval', 'pending');


            if (!$error) {

                if ($cv['start_date'] != "") {
                    $xcrud->where("DATE(user.CreatedDate) >= '" . $cv['start_date'] . "'");
                }

                if ($cv['end_date'] != "") {
                    $xcrud->where("DATE(user.CreatedDate) <= '" . $cv['end_date'] . "'");
                }
            }
            $xcrud_columns = [
                'Username',
                'RegistrationApproval',
                'Role',
                'CreatedDate',
            ];
            $xcrud->columns($xcrud_columns);

            $xcrud_labels =[
                'Username'=>'User',
                'RegistrationApproval'=>'Approval',
                'CreatedDate' =>  'Registered Date',
            ];
            $xcrud->label($xcrud_labels);
            $xcrud->highlight('RegistrationApproval', '=', 'pending', '#ffff80', '');
//            $xcrud->button('javascript:approve({user.id})', 'Approve', 'fa fa-edit', 'btn btn-light-warning', '',
//                ['RegistrationApproval', '!=', 'approved']);

            $xcrud->button('javascript:approve({user.id})', 'Approve User', 'fas fa-check-square', 'btn btn-success','',);

            $xcrud->column_callback('status', 'status_label');
            //$xcrud->unset_view();
            $xcrud->unset_limitlist();
            $xcrud->unset_print();
            $xcrud->unset_csv();
            $xcrud->unset_title();
            $xcrud->unset_search();
            $xcrud->unset_add();
            $xcrud->unset_edit();
            $xcrud->unset_remove();

            echo $xcrud->render();

            ?>
        </div>
    </div>

    <script type="text/javascript">

        function approve($id) {
            var hiddenBtn = $("#hidden_big_button");
            hiddenBtn.attr('data-remote', '<?php echo "/$role/registration-modal-approval" ?>?id=' + $id);
            hiddenBtn.attr('data-modal-title', 'Approve User');
            hiddenBtn.attr('data-loading-text', '');
            hiddenBtn.click();
        }
    </script>

<?php

include(ADMIN_FOOTER);

