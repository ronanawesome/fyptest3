<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$error = false;
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $validator = new Validator;

    $fields = array();
    $fields[] = array('index' => 'admin', 'label' => "Admin", 'required' => false);

    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    if (!$problem) {

    } else {
        $error = $problem;
    }

}


$title = 'Class Management';
$breadcrumbs = array(
    $title => '/' . $role . '/' . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------


include(ADMIN_HEADER);

//prepare form input
$form = new FormInput();
$dbuilder = new DateBuilder();
?>
    <div class="row">
    <div class="col-md-12">
        <form id="search-form" action="" method="GET">
            <div class="form-group row">
                <label for="user" class="col-sm-3 col-lg-2 col-form-label">Classroom</label>
                <div class="col-sm-9 col-lg-10">
                    <div class="input-group">
                        <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </span>
                        </div>
                        <?php
                        $form->setPlaceholder("Classroom Name");
                        $form->createText('admin', '');
                        ?>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                    <input type="submit" value="Search" class="btn btn-primary btn-block">
                </div>
            </div>
            <div class="col-md-12 mt-3 d-flex flex-row-reverse">
                <a href="create-classroom" class="btn btn-light-warning font-weight-bold mr-2">Create New Classroom</a>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <?php
        require_once('module/xcrud/xcrud.php');
        $xcrud = Xcrud::get_instance();
        $xcrud->table('ClassRoom'); //ClassRoom


        if (!$error) {

            if ($cv['admin'] != "") {
                $xcrud->where('Class_Name LIKE "%' . $cv['admin'] . '%" OR Class_Level LIKE "%' . $cv['admin']. '%"');
            }
        }
        $xcrud_columns = [
            'id',
            'Class_Name',
            'Capacity',
            'Class_Level'
        ];
        $xcrud_labels = [
            'id' => 'Class ID',
            'Class_Name' => 'Class Name',
            'Capacity'=>'Number of students',
            'Class_Level'=>'Level'
        ];
        $xcrud_fields = [
            'id',
            'Class_Name',
            'Capacity',
            'Class_Level'
        ];

        $xcrud->columns($xcrud_columns);
        $xcrud->subselect('Number of Students','SELECT COUNT(id) FROM Student WHERE Class_Name = {Class_Name}');




        $xcrud->fields($xcrud_fields);
        $xcrud->label($xcrud_labels);
        $xcrud->unset_add();
        $xcrud->unset_search();
        // $xcrud->unset_remove();
        $xcrud->unset_title();
        $xcrud->unset_view();
        $xcrud->unset_limitlist();
        $xcrud->unset_print();
        $xcrud->unset_csv();

        echo $xcrud->render();
        ?>
    </div>

    <script type="text/javascript">
       \
    </script>


<?php
include(ADMIN_FOOTER);





