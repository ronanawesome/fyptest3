<?php
global $dbc, $url;
$role = $url['call_parts'][0];

//---------- page info --------------
//    must include before header
$title = 'New Intake';
$breadcrumbs = array(
    "Admin" => "javascript:void(0);",
    //$title => "/$role/" . $url['call_parts'][1]
);


$card_title = "";
$card_icon = "";

$error = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $fields = array();
    $validator = new Validator;
    $fields[] = array('index' => 'Intake_Code', 'label' => 'Intake Code', 'required' => true);
    $fields[] = array('index' => 'Intake_Year', 'label' => 'Intake Year', 'required' => true);

    //$fields[] = array('index' => 'Capacity', 'label' => 'Exam_Date', 'required' => true);

    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values



    $problem = $validator->getErrors();
    $cv = $validator->escape_val();

    if (!$problem) {
        do {
            $dbc->beginTransaction();

            $intake = new Intakes($dbc);

            if ($intake->searchIntakes( $cv['Intake_Code'])>0) { $msg_fail = "Intake Exist.";
                $errors = $problem;
                redirect($role . "admin/create-intake?alert-danger=" . $msg_fail.$errors);
                break;
            }
            $intake->setIntakeCode($cv['Intake_Code']);
            $intake->setYear($cv['Intake_Year']);
            $intake->setNoOfStudents(0);
            $intake->setActiveStatus(1);
            $intake->setIsDelete(0);
            $intake->create();
            $dbc->commit();
            redirect($role . "/intakes?alert-success=Successfully Created Classroom" );
        } while (0);
    } else {

        $msg_fail = "Intake Creation Failed.";
        $error['problem'] = $problem;
        redirect($role . "/create intake?alert-danger=" . $msg_fail);
    }
}
include(ADMIN_HEADER);
//prepare form input
$form = new FormInput();
?>

    <form class="form" method="post" enctype="multipart/form-data">
        <div class="card-body">
            <?php getAlert() ?>
            <div class="form-group">
            </div>
            <h3 class="font-size-lg text-dark font-weight-bold mb-6">New Intake Details:</h3>
            <div class="mb-15">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Intake Code:</label>
                    <div class="col-lg-6">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder('Enter Intake Code');
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('Intake_Code', '');
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Intake Year:</label>
                    <div class="col-lg-6">
                        <input type="number" min="1900" max="2099" step="1" value="2016"
                               name="Intake_Year" id="Intake_Year"/>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <a  href="intakes" class="btn btn-secondary">
                            Cancel
                        </a>


                    </div>
                </div>
            </div>

        </div>
    </form>
<script>
   </script>
<?php

include(ADMIN_FOOTER);
