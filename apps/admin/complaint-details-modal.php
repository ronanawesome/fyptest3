<?php
global $dbc, $url;
$role = $url['call_parts'][0];
$id = isset($_GET['id']) ? $_GET['id'] : '';
$error = false;
$complaint = new ComplaintReport($dbc);
$user = new User($dbc);
$user->load($_SESSION['admin_id']);
if (empty($complaint->load($id))) {
    $error = "Invalid ID!";
}
$form = new FormInput();
?>
<form action="" id="modal-form">
    <div class="row">
        <div class="col-md-12">
            <div id="printOrder">
                <div class="row">
                    <div class="col-md-12 mb-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <input type="hidden" name="announcement" value="<?php echo $complaint->getId(); ?>">
                                    <input type="hidden" name="user_id" value="<?php echo $user->getId(); ?>">
                                    <div class="error-output"></div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    Title
                                                </td>
                                                <td>
                                                    <?php
                                                    $form->setPlaceholder('Title');
                                                    $form->setClass('Title');
                                                    $form->setExtra('disabled', true);
                                                    $form->createText('Title', $complaint->getTitle());
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Description
                                                </td>
                                                <td>
                                                    <?php
                                                    $form->setPlaceholder('Content');
                                                    $form->setClass('Content');
                                                    $form->setExtra('disabled', true);
                                                    $form->createTextarea('description', $complaint->getContent());
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Type
                                                </td>
                                                <td>
                                                    <?php
                                                    $choices = array();
                                                    foreach (Common::getComplaintType() as $k => $v){
                                                        $choices[$k] = $v;
                                                    }
                                                    $form->setExtra('disabled', true);
                                                    $form->createSelect('Type', $choices, $complaint->getType());
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Students Involved</td>
                                                <td>
                                                    <?php
                                                    $studentids=$complaint->getUserIds();
                                                    $tempstudentlist="";
                                                    $arraystudents=explode(",", $studentids);
                                                    foreach (Collection::getStudents($dbc) as $x):
                                                        if(in_array($x->getId(),$arraystudents,TRUE)){
                                                            $tempstudentlist=$x->getUsername().", ". $tempstudentlist;
                                                        }
                                                    endforeach;
                                                    $form->setClass('Student Involved');
                                                    $form->setExtra('disabled', true);
                                                    $form->createTextarea('description', $tempstudentlist);

                                                    ?>
                                                </td>
                                            </tr>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
</form>
<!--                --><?php //endforeach; ?>


<script type="text/javascript">
    $(document).ready(function () {

        // Check admin api approve-order and pass required data
        $('#modal-form').submit(function (e) {
            e.preventDefault();


            $.ajax({
                url: '/admin-api/announcement-update',
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log('Completed');
                    location.href = "<?php
                        echo "/$role/announcements-manage?alert-success=Update Successfully"
                        ?>";
                },
                error: function (xhr, status, error) {
                    var resp = eval("(" + xhr.responseText + ")");
                    alert(resp.error)
                },
            });
        });

    });
</script>
