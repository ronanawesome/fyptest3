<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$title = 'Student Login';

$errors = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $validator = new Validator;

    // fields info container
    $fields = array();

    // fill the container with fields data
    $fields[] = array('index' => 'username', 'label' => 'Username', 'required' => false);
    $fields[] = array('index' => 'password', 'label' => 'Password', 'required' => false);

    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val();

    $student=new Student($dbc);
    $user = new user($dbc);

    if (!$problem) {
        do {
            if (!$user->searchUser($cv['username'])) {
                $errors['problem'] = 'Username Not Found! ';
                break;
            }
//            if (!PasswordHasher::VerifyHashedPassword($user->getPassword(), $cv['password'])) {
//                $errors['problem'] = 'Invalid Username Or Password Combination';
//                break;
//            }
            if ($user->getRole()!=='Student') {
                $errors['problem'] = 'Student Does Not Exist';
                break;
            }

            $_SESSION['student_id'] = $user->getId();
            $user->save();
            if($student->checkStudent($user->getId())==false){
                echo  "<script>
                     window.location.href=('student-intake-assign');
                  </script>";
            }
            else{
                echo "<script>
                     window.location.href=('home');
                  </script>";}
        } while (0);

    } else {
        $errors['problem'] = $problem;
    }

}

//prepare form input
$form = new FormInput();

?>
<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->
<head>
    <base href="../../../../">
    <meta charset="utf-8"/>
    <title><?php echo SYS_NAME . " :: " . $title ?></title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link rel="canonical" href="https://keenthemes.com/metronic"/>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>
    <!--end::Fonts-->
    <!--begin::Page Custom Styles(used by this page)-->
    <link href="/template/_metronic/demo1/dist/assets/css/pages/login/classic/login-4.css" rel="stylesheet"
          type="text/css"/>
    <!--end::Page Custom Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="/template/_metronic/demo1/dist/assets/plugins/global/plugins.bundle.css" rel="stylesheet"
          type="text/css"/>
    <link href="/template/_metronic/demo1/dist/assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet"
          type="text/css"/>
    <link href="/template/_metronic/demo1/dist/assets/css/style.bundle.css" rel="stylesheet" type="text/css"/>
    <!--end::Global Theme Styles-->
    <!--begin::Layout Themes(used by all pages)-->
    <link href="/template/_metronic/demo1/dist/assets/css/themes/layout/header/base/light.css" rel="stylesheet"
          type="text/css"/>
    <link href="/template/_metronic/demo1/dist/assets/css/themes/layout/header/menu/light.css" rel="stylesheet"
          type="text/css"/>
    <link href="/template/_metronic/demo1/dist/assets/css/themes/layout/brand/dark.css" rel="stylesheet"
          type="text/css"/>
    <link href="/template/_metronic/demo1/dist/assets/css/themes/layout/aside/dark.css" rel="stylesheet"
          type="text/css"/>
    <!--end::Layout Themes-->
    <link rel="shortcut icon" type="image/png" href="/resource/assets/fav1.png"/>
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body"
      class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
<!--begin::Main-->
<div class="d-flex flex-column flex-root">
    <!--begin::Login-->
    <div class="login login-4 login-signin-on d-flex flex-row-fluid" id="kt_login">
        <div class="d-flex flex-center flex-row-fluid bgi-size-cover bgi-position-top bgi-no-repeat"
             style="background-image: url('<?php echo BRANDING_HOME_LOGIN_BG ?>');">
            <div class="login-form text-center p-7 position-relative overflow-hidden">
                <!--begin::Login Header-->
                <div class="d-flex flex-center mb-15">
                    <a href="javascript:void(0);">
                        <img src="<?php echo NEW_HOME_LOGO ?>" class="<?php echo BRANDING_LOGIN_LOGO_STYLE ?>"
                             alt=""/>
                    </a>
                </div>
                <!--end::Login Header-->
                <!--begin::Login Sign in form-->
                <div class="login-signin">
                    <div class="mb-10">
                        <h3>Student Login Portal</h3>
                        <div class="text-muted font-weight-bold">Enter your details to login to your account</div>
                    </div>
                    <a class="forgot" href="parent/login">Change to Parent Login</a>
                    <br>
                    <form class="form" action="" method="POST">
                        <div class="form-group mb-5">
                            <?php print_error("problem", $errors, '<div class="alert alert-danger" style="margin-top:15px;">', '</div>'); ?>
                        </div>
                        <div class="form-group mb-5">
                            <?php
                            $form->setClass('form-control h-auto form-control-solid py-4 px-8 text-center');
                            $form->setPlaceholder('Username');
                            $form->setExtra('autocomplete', 'off');
                            $form->createText('username', '');
                            ?>
                        </div>
                        <div class="form-group mb-5">
                            <?php
                            $form->setClass('form-control h-auto form-control-solid py-4 px-8 text-center');
                            $form->setPlaceholder('Password');
                            $form->createPassword('password', '');
                            ?>
                        </div>
                        <button type="submit" class="btn btn-block btn-primary font-weight-bold py-4">
                            Sign In
                        </button>

                    </form>
                    <div class="mt-10"></div>
                   <?php echo'  <hr>
                    <a class="forgot" href="student/forget-password">'._LOGIN_FORGOT_PASSWORD.'</a>
                         <br>
                    <a class="forgot" href="student/registration" >'._LOGIN_REGISTER.'</a>
                    <br>
                          ';?>
                </div>
                <!--end::Login Sign in form-->
            </div>
        </div>
    </div>
    <!--end::Login-->
</div>
<!--end::Main-->
<script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
<!--begin::Global Config(global config for global JS scripts)-->
<script>var KTAppSettings = {
        "breakpoints": {"sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400},
        "colors": {
            "theme": {
                "base": {
                    "white": "#ffffff",
                    "primary": "#3699FF",
                    "secondary": "#E5EAEE",
                    "success": "#1BC5BD",
                    "info": "#8950FC",
                    "warning": "#FFA800",
                    "danger": "#F64E60",
                    "light": "#E4E6EF",
                    "dark": "#181C32"
                },
                "light": {
                    "white": "#ffffff",
                    "primary": "#E1F0FF",
                    "secondary": "#EBEDF3",
                    "success": "#C9F7F5",
                    "info": "#EEE5FF",
                    "warning": "#FFF4DE",
                    "danger": "#FFE2E5",
                    "light": "#F3F6F9",
                    "dark": "#D6D6E0"
                },
                "inverse": {
                    "white": "#ffffff",
                    "primary": "#ffffff",
                    "secondary": "#3F4254",
                    "success": "#ffffff",
                    "info": "#ffffff",
                    "warning": "#ffffff",
                    "danger": "#ffffff",
                    "light": "#464E5F",
                    "dark": "#ffffff"
                }
            },
            "gray": {
                "gray-100": "#F3F6F9",
                "gray-200": "#EBEDF3",
                "gray-300": "#E4E6EF",
                "gray-400": "#D1D3E0",
                "gray-500": "#B5B5C3",
                "gray-600": "#7E8299",
                "gray-700": "#5E6278",
                "gray-800": "#3F4254",
                "gray-900": "#181C32"
            }
        },
        "font-family": "Poppins"
    };</script>
<!--end::Global Config-->
<!--begin::Global Theme Bundle(used by all pages)-->
<script src="/template/_metronic/demo1/dist/assets/plugins/global/plugins.bundle.js"></script>
<script src="/template/_metronic/demo1/dist/assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
<script src="/template/_metronic/demo1/dist/assets/js/scripts.bundle.js"></script>
<!--end::Global Theme Bundle-->
<!--begin::Page Scripts(used by this page)-->
<script src="/template/_metronic/demo1/dist/assets/js/pages/custom/login/login-general.js"></script>
<!--end::Page Scripts-->
</body>
<!--end::Body-->
</html>


