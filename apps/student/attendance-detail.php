<?php

global $dbc, $url;
$role = $url['call_parts'][0];

if (!empty($url['call_parts'][2])) {
    $id = $url['call_parts'][2];
    $attendance = new Attendance($dbc);
    $attendance->load($id);
} else {
    redirect($role . '/announcements');
}

$error = false;


$user = new User($dbc);
$user->load($_SESSION['student_id']);


$student=Collection::getStudents($dbc);


$classroom=new ClassRoom($dbc);
$classroom->load($attendance->getClassId());

$uid = $user->getId();

$form = new FormInput();
include(STUDENT_HEADER);
include(STUDENT_NAVBAR);
?>

<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="error-output">
                <?php
                getAlert();
                ?>
            </div>
            <div class="single-event">
                <div class=" align-self-center ">
                    <h3 class="mb-0">Attendance Details </h3>
                    <hr>
                    <div class="card bg-light col-6 col-md-12 col-lg-12 col-12 my-1">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6 col-sm-6 col-lg-5 p-1"><strong>Date: </strong>
                                </div>
                                <div class="col-12 col-sm-6 col-lg-7 p-1"> <?php echo $attendance->getDate() ?></div>
                                <div class="col-6 col-sm-6 col-lg-5 p-1"><strong>Class: </strong></div>
                                <div class="col-12 col-sm-6 col-lg-7 p-1"> <?php echo $classroom->getClassName() ?></div>
                                <div class="col-6 col-sm-6 col-lg-5 p-1"><strong>Students: </strong></div>
                                <div class="col-12 col-sm-6 col-lg-7 p-1">
                                                    <?php
                                                    $student_ids = explode(',', $attendance->getStudentIds());
                                                    foreach (Collection::getStudents($dbc) as $i):
                                                        $student=new Student($dbc);
                                                        $student->loadstudentinfo($i->getId());
                                                        if($student->getClassId()==$attendance->getClassId()):
                                                            ?>
                                                            <div class="form-check form-check-inline col-md-3">
                                                                <input disabled class="form-check-input" type="checkbox"
                                                                       name="Intake_Ids[]"
                                                                       id="<?php echo $i->getId() ?>"
                                                                       value="<?php echo $i->getId()?>"
                                                                    <?php
                                                                    foreach ($student_ids as $id) {
                                                                        if ($i->getId() == $id) {
                                                                            echo "checked";
                                                                        }
                                                                    }
                                                                    ?>
                                                                >
                                                                <label class="form-check-label"
                                                                       for="<?php echo $i->getId() ?>"><?php echo $i->getFname().' '.$i->getLName() ?></label>
                                                            </div>
                                                        <?php endif;
                                                    endforeach;
                                                    ?>
                                            </div
                            </div>
                        </div>
                    </div>
                </div>
            </div>


</section>

<!--                --><?php //endforeach; ?>


<script type="text/javascript">
    $(document).ready(function () {

        // Check admin api approve-order and pass required data


    });
</script>
