<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$page_title = _REGISTER;

$breadcrumbs = [
    _REGISTER => '/' . $role . '/registration'
];

$session_user = new user($dbc);
$session_user->load($_SESSION['student_id']);

include(STUDENT_HEADER);
include(STUDENT_NAVBAR);
//include(PARENT_BREADCRUMB);

$userID=$session_user->getId();
$error = array();
$int=0;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $validator = new Validator;

    // fields info container
    $fields = array();

    // fill the container with fields data
    $fields[] = array('index' => 'Class_Id', 'label' => 'Class ID', 'required' => True);
    $fields[] = array('index' => 'Intake_Id', 'label' => 'Intake ID ', 'required' => true);
    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values





    $gender=$session_user->getGender();


    $student=new Student($dbc);
    if (!$problem) {
        do {
            $student->setUserId($session_user->getId());
            $student->setClassId($cv['Class_Id']);
//            $int = (int) filter_var($cv['Class_Name'], FILTER_SANITIZE_NUMBER_INT);
//            $student->setStudentLevel($int);
            $student->setIntakeId($cv['Intake_Id']);
            $student->create();
            echo "<script>
                        window.location.href=('home?alert-success=Assignment Successfully');
                  </script>";
        } while (0);
    } else {
        $error['problem'] = $problem;
    }

}

//$servername = 'localhost';
//$username = 'root';
//$password = '';
//$dbname = "fyp3rd";
//$conn = mysqli_connect($servername, $username, $password, "$dbname");
//if (!$conn) {
//    else('Could not Connect MySql Server:' . m);
//}

$form = new FormInput();
?>
<head>

    <style>

        .log-form form {
            display: block;
            width: 80%;
            padding: 2em;
            border: 1px solid #bbb;
            margin: auto;
        }
        .log-form h2 {
            color: white;
            font-size: 1.15em;
            display: block;
            background: #2a2a2a;
            width: 80%;
            text-transform: uppercase;
            padding: 0.75em 1em 0.75em 1.5em;
            box-shadow: inset 0px 1px 1px rgba(255, 255, 255, 0.05);
            border: 1px solid #1d1d1d;
            font-weight: bold;
            margin: auto;
        }

        .log-form input {
            display: block;
            margin: auto auto;
            width: 100%;
            margin-bottom: 2em;
            padding: 0.5em 0;
            border: none;
            border-bottom: 1px solid #eaeaea;
            padding-bottom: 1.25em;
            color: #757575;
        }
        .log-form input:focus {
            outline: none;
        }
        .log-form .btn {
            display: inline-block;
            background-color: Transparent;
            border: 1px solid black;
            padding: 0.5em 2em;
            color: black;
            margin-right: 0.5em;
            box-shadow: inset 0px 1px 0px rgba(255, 255, 255, 0.2);
        }
        .log-form .btn:hover {
            background: black;
            color: white;
        }
        .log-form .btn:active {
            background: #1fb5bf;
            box-shadow: inset 0px 1px 1px rgba(0, 0, 0, 0.1);
        }
        .log-form .btn:focus {
            outline: none;
        }
        .log-form .forgot {
            color: #1ba0a9;
            font-weight: bold;
            line-height: 0.5em;
            position: relative;
            text-decoration: none;
            font-size: 0.75em;
            margin: 0;
            padding: 0;
            float: right;
        }
        .log-form .forgot:hover {
            color: #33d3de;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 110px;margin-top: 3%;">
            <div class="log-form">
                <h2><?php echo 'ASSIGN INTAKE AND CLASS' ?></h2>
                <form method="post">
                    <div class="form-group">
                        <?php getAlert(); ?>
                        <?php print_error("problem", $error, '<div class="alert alert-danger" style="margin-top:15px;">', '</div>'); ?>
                    </div>
                    <div class=" row " >
                        <div class="col-lg-6">
                            <label for="User_ID">Username:</label>
                            <input disabled type="text" class="form-control" id="User_Id"
                                   name="User_Id" value=" <?php echo $session_user->getUsername();?>">
                        </div>
                    </div>
                    <div class="row">
                        <label >Select Class & Level:</label>
                        <div class="col-6" >
                      <select class="mb-3 form-control form-select" id="Class_Id" name="Class_Id">
                                <?php
                                $choices = [];
                                $classroom=Collection::getClassRoom($dbc);
                                foreach ($classroom as $value) {
                                    ?>
                                    <option value="<?php echo $value->getId()?>">
                                        <?php echo $value->getClassName()?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <label for="Intake_Code">Select Intake:</label>
                        <div class="col-6" >
                            <select class="mb-3 form-control form-select" id="Intake_Id" name="Intake_Id">
                                <?php
                                $choices = [];
                                $intake=Collection::getIntake($dbc);
                                foreach ($intake as $value) {
                                    ?>
                                    <option value="<?php echo $value->getId()?>"><?php echo $value->getIntakeCode()?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>





                    <input type="hidden" title="balance" name="balance" value="0"/>
                    <button type="submit" class="btn"><?php echo 'ASSIGN' ?></button><br>
                </form>
            </div><!--end log form -->
        </div>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function() {

        $('.js-example-basic-single').select2();
        // $('#select-all').click(function() {
        //     $('#player').find('option').prop('selected', true);
        //     $('#player').trigger('change');
        // });

    });

</script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js">

<?php
include(STUDENT_FOOTER);
?>

