<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$page_title = _REGISTER;

$breadcrumbs = [
    _REGISTER => '/' . $role . '/registration'
];

$session_user = new user($dbc);
$session_user->load($_SESSION['student_id']);



$error = array();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $validator = new Validator;

    // fields info container
    $fields = array();
    $fields[] = array('index' => 'admin', 'label' => "Admin", 'required' => false);


    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values
    if (!$problem) {

    } else {
        $error = $problem;
    }

    include(STUDENT_HEADER);
    include(STUDENT_NAVBAR);


}

$form = new FormInput();
$dbuilder = new DateBuilder();
?>

<title>Agent - Order History</title>

<style>
    .container{
        /*background-color: white;*/
        border-radius: 10px;
    }
</style>


<div class="main-container py-3">
    <div class="container py-2">
        <div class="col-md-12 mb-10">
            <div class="float-left">
                <label style="font-size:25px "for="user" class="col-sm-12 col-lg-2 col-form-label">My Performance</label>
            </div>
<hr>
            <form id="search-form" action="" method="GET">
                <div class="form-group row">
                    <label for="user" class="col-sm-3 col-lg-2 col-form-label"><?php echo "Exam"; ?></label>
                    <div class="col-sm-9 col-lg-10">
                        <?php
                       // $form->setClass('form-control');
                        $form->setPlaceholder("Exam Info");
                        $form->createText('admin', '');
                        ?>
                        <small class="form-text text-muted"></small>
                    </div>
                </div>

                <br>
                <div class="form-group row pb-3">
                    <label for="user" class="col-sm-3 col-lg-2 col-form-label">Exam Date</label>
                    <div class="col-sm-9 col-lg-10">                          <?php
                        $dbuilder->build_range_with_default("start_date", "", "end_date", "");
                        ?>

                        <small class="form-text text-muted"></small>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                        <input type="submit" value="<?php echo _SEARCH ?>" class="btn btn-primary btn-block" style="width: 100%"/>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="main-container">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card bg-light px-2 mb-2">
                    <?php

                    require_once('module/xcrud/xcrud.php');
                    $xcrud = Xcrud::get_instance();
                    $xcrud->table('ExamResult');
                    $xcrud->join('Exam_ID','Exam','id');
                    $xcrud->join('Exam.Subject_Id','Subject','id');
                    $xcrud->join('Exam.Intake_Id','Intakes','id');
                    $xcrud->join('User_ID','Student','User_Id');
                    $xcrud->join('Student.User_Id','user','id');

                    $xcrud->where('User_ID=', $session_user->getId() );

                    if (!$error) {
                        if ($cv['admin'] != "") {
                            $xcrud->where('Exam_Name LIKE "%' . $cv['admin'] . '%"');
                        }
                    }
                    $xcrud_columns = [
                        'Exam.Exam_Name',
                        'Subject.Subject_Code',
                       // 'Student.Class_Name',
                        'Exam.Exam_Date',
                        'Exam_Grade',
                        'Exam_Marks'
                    ];
                    $xcrud_labels = [
                        //'id' => 'Exam ID',
                        'Exam.Exam_Name' => 'Exam',
                        'Subject.Subject_Code'=>'Subject Code',
                        'Exam.Exam_Date'=>'Date',
                        'Exam_Grade'=>'Grade',
                        'Exam_Marks'=>'Marks'
                    ];

                    $xcrud_fields = [

                        'Exam_Grade',
                        'Exam_Marks'
                    ];


                    $xcrud->buttons_position('left');
                    $xcrud->columns($xcrud_columns);

                    //   $xcrud->order_by('paid_datetime','desc');
                    $xcrud->unset_add();
                    $xcrud->unset_search();
                    $xcrud->unset_remove();
                    $xcrud->unset_title();
                    $xcrud->unset_view();
                    $xcrud->unset_limitlist();
                    $xcrud->unset_print();
                    $xcrud->unset_edit();
                    $xcrud->unset_csv();

                    echo $xcrud->render();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    function sale_info($id) {
        var hiddenBtn = $("#hidden_big_button");
        hiddenBtn.attr('data-remote', '<?php echo "/$role/modal-sale-info" ?>?id=' + $id);
        hiddenBtn.attr('data-modal-title', 'Detailed Sale Information');
        hiddenBtn.attr('data-loading-text', 'Customer Info Loading...');
        hiddenBtn.click();
    }

</script>

<?php
include(STUDENT_FOOTER);
?>
