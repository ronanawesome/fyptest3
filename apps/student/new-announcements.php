<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$user = new user($dbc);
$user->load($_SESSION['student_id']);
$student= new student($dbc);
$student->loadstudentinfo($_SESSION['student_id']);
//$intake=$student->getIntakeId();
$class=$student->getClassId();
$classroom=new ClassRoom($dbc);
$classroom->loadclassinfo($class);
$error = false;
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $validator = new Validator;

    $fields = array();
    $fields[] = array('index' => 'admin', 'label' => "Admin", 'required' => false);
    $fields[] = array('index' => 'start_date', 'label' => 'Start Date','required' => false);
    $fields[] = array('index' => 'end_date', 'label' => 'End Date','required' => false);



    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    if (!$problem) {

    } else {
        $error = $problem;
    }

}
//---------- page info --------------
//    must include before header
$title = 'User Management';
$breadcrumbs = array(
    $title => '/' . $role . '/' . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------

include(STUDENT_HEADER);
include(STUDENT_NAVBAR);
//prepare form input
$form = new FormInput();
$dbuilder = new DateBuilder();
?>
    <div class="main-container py-3">
    <div class="container py-2 md-12">
        <form id="search-form" action="" method="GET">
            <div class="float-left">
                <label style="font-size:25px "for="user" class="col-sm-12 col-lg-3 col-form-label">Announcements</label>
            </div>
            <div class="col-md-12 mb-10">
                <hr>
                <form id="search-form" action="" method="GET">
                    <div class="form-group row">
                        <label for="user" class="col-sm-3 col-lg-2 col-form-label"><?php echo "Teacher"; ?></label>
                        <div class="col-sm-9 col-lg-10">
                            <?php
                            $form->setPlaceholder("Announcement Title");
                            $form->createText('admin', '');
                            ?>
                            <small class="form-text text-muted"></small>
                        </div>
                    </div>
                    <br>
                    <div class="form-group row pb-3">
                        <label for="user" class="col-sm-3 col-lg-2 col-form-label">Date</label>
                        <div class="col-sm-9 col-lg-10">                          <?php
                            $dbuilder->build_range_with_default("start_date", "", "end_date", "");
                            ?>

                            <small class="form-text text-muted"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                            <input type="submit" value="Search" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
    </div>
    <div class="main-container">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="card bg-light px-2 mb-2">
                        <?php

                        require_once('module/xcrud/xcrud.php');
                        $xcrud = Xcrud::get_instance();
                        $xcrud->table('Announcement');
                        $xcrud->join('Creator_Id','user','id');

                        $xcrud->where('Year_Target LIKE "%'.$classroom->getClassLevel().'%"');

                        if (!$error) {
                            if ($cv['admin'] != "") {
                                $xcrud->where('Title LIKE "%' . $cv['admin'] . '%" OR Year_Target LIKE "%' . $cv['admin']. '%"');
                            }
                        }
                        if (!$error) {

                            if ($cv['start_date'] != "") {
                                $xcrud->where("DATE(Exam.Exam_Date) >= '" . $cv['start_date'] . "'");
                            }

                            if ($cv['end_date'] != "") {
                                $xcrud->where("DATE(Exam.Exam_Date) <= '" . $cv['end_date'] . "'");
                            }
                        }

                        $xcrud_columns = [
                            'id',
                            'Title',
                            'user.Username',
                            'Created_Datetime',
                            'Year_Target',
                        ];

                        $xcrud_labels = [
                            'id' => ' ID',
                            'user.Username'=> 'Created By',
                            'StudentName'=>'Student Involved',
                            'Year_Target'=>'Year Target',
                            'Created_Datetime'=>'Created On'
                        ];

                        $xcrud_fields = [
                            'id',
                            'Title',
                            'Date',
                            'File',
                            'Year_Target',
                            'Creator_Id',
                            'Content',
                            'Intakes.Intake_Code',
                        ];

                        $xcrud->columns($xcrud_columns);
                        $xcrud->fields($xcrud_fields);
                        $xcrud->label($xcrud_labels);
                        $xcrud->button('announcement-details/{id}', 'Details', 'flaticon2-search', 'btn btn-warning');
                        $xcrud->unset_add();
                          $xcrud->unset_search();
                        $xcrud->unset_edit();
                          $xcrud->unset_remove();
                        $xcrud->unset_title();
                        $xcrud->unset_edit();
                        $xcrud->unset_view();
                        $xcrud->unset_limitlist();
                        //$xcrud->unset_print();
                        // $xcrud->unset_csv();

                        echo $xcrud->render();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function agent_view($index) {
            var hiddenBtn = $("#hidden_big_button");
            hiddenBtn.attr('data-remote', '/<?php echo $role ?>/modal-agent-view/' + $index);
            hiddenBtn.attr('data-modal-title', 'View Agent Documents');
            hiddenBtn.attr('data-loading-text', 'Agent Info Loading...');
            hiddenBtn.click();
        }
    </script>


<?php
include(STUDENT_FOOTER);



