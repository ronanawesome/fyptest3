<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$user = new user($dbc);
$user->load($_SESSION['teacher_id']);

$attendance = new Attendance($dbc);
//$intake=Collection::getIntake($dbc);
$classroom=Collection::getClassRoom($dbc);
$teacher=new Teacher($dbc);
$class=new ClassRoom($dbc);
$class->loadclassinfo($_SESSION['teacher_id']);
$teacherclass=$class->getId();



$students=Collection::getStudents($dbc);
//---------- page info --------------
//    must include before header
$title = 'Create Attendance';
$breadcrumbs = array(

    $title => "/$role/" . $url['call_parts'][1]);
$card_title = "";
$card_icon = "";


$validator = new Rakit\Validation\Validator;

$fields = array();
$error = array();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $fields['Class_Id'] = 'required';
    $fields['Date'] = 'required';
    $fields['Student_Ids'] = 'array';
    $validation = $validator->make($_POST + $_GET + $_FILES, $fields);
    $validation->validate();
    $problem = $validation->errors();
    $cv =   $validation->getValidatedData(); // get the form values

    //ERROR !problem is from the previous validation this one got another format \
    if (!$validation->fails()) {
        do {
            $dbc->beginTransaction();
//            $File=fileUpload($_FILES['File'],'announcement');
            $attendance->createAttendance($cv,$user->getId());
            $dbc->commit();
            $msg = "Attendance Created.";
            redirect($role . "/attendance?alert-success=" . $msg);
        } while (0);
    } else {
        $msg_fail = "";
        $errors = $validation->errors();
        $messages = $errors->all();
        foreach($messages as $m){
            $msg_fail = $m . "<br/>" . $msg_fail;
        }
//        this is working for me so it should work for you.

        redirect($role . "/take-class-attendance?alert-danger=" . $msg_fail);


    }
}

include(TEACHER_HEADER);

//prepare form input
$form = new FormInput();

?>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    </head>
    <body>
<form  class="form" method="post" enctype="multipart/form-data">
    <div class="card-body">
        <?php getAlert() ?>
        <div class="form-group">
        </div>
        <h3 class="font-size-lg text-dark font-weight-bold mb-6">Attendance Details:</h3>
        <div class="mb-15">
            <div class="row">
                <div class="form-group col-12 col-md-6">
                    <label class="col-lg-12 col-form-label">ClassRoom:</label>
                    <div class="col-lg-12">
                        <?php
                        $form->createHidden('Class_Id',$class->getId());
                        $form->setClass('Class_Id');
                        $form->setExtra('disabled',true);
                        $form->createText('Class_Id',$class->getClassName());
                        ?>
                    </div>
                </div>
                <div class="form-group col-12 col-md-6">
                    <label class="col-lg-12 col-form-label">Date:</label>
                    <div class="col-lg-12">
                        <input type="date" name="Date" id="Date" class="form-control" >
                    </div>
                </div>
                                <div class="form-group col-12 col-md-6">
                                <label class="col-lg-12 col-form-label font-weight-bold">Students :</label>

                                <div class="col-lg-12  border" style="border-radius: 0.42rem;">
                                    <?php
                                    if (empty($students)) {
                                        ?>
                                        <div class="form-check form-check-inline col-lg-6">
                                            <p class="text-center col-lg-6 font-weight-bold py-2 my-0">No students found</p>
                                        </div>
                                        <?php
                                    }
                                    else
                                    {     foreach ($students=Collection::getStudents($dbc)as $c):
                                                $student= new student($dbc);
                                                $student->loadstudentinfo($c->getId());

                                                if($student->getClassId()==$teacherclass){
                                                    ?>
                                                    <div class="form-check form-check-inline col-md-3">
                                                        <input class="col-lg-6 form-controlform-check-input" type="checkbox" id="Student_Ids" name="Student_Ids[]"
                                                               id="<?php echo $c->getId() ?>"
                                                               value="<?php echo $c->getId()?>">
                                                        <label class="form-check-label"
                                                               for="<?php echo $c->getId() ?>"><?php echo $c->getUsername()?>
                                                        </label>
                                                    </div>
                                                    <?php
                                                }
                                        endforeach;
                                    }
                                    ?>
                                </div>
                                </div>
            </div>
        </div>
    </div>

    <div class="card-footer">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <button type="submit" class="btn btn-success mr-2">Submit</button>
                <a href="attendance" class="btn btn-secondary">
                    Cancel
                </a>
            </div>
        </div>
    </div>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
        dropdownParent:$('#modal-container')
    });
</script>


<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<?php

include(TEACHER_FOOTER);
?>