<?php

global $dbc, $url;
$role = $url['call_parts'][0];


$id = isset($_GET['id']) ? $_GET['id'] : '';

$error = false;
$attendance = new Attendance($dbc);
$attendance->load($id);
$user = new User($dbc);
$user->load($_SESSION['teacher_id']);
if (empty($attendance->load($id))) {
    $error = "Invalid ID!";
}
$student=Collection::getStudents($dbc);
//$teacher=new Teacher($dbc);
//$teacher->loadteacherinfo($_SESSION['teacher_id']);

$classroom=new ClassRoom($dbc);
$classroom->load($attendance->getClassId());

$uid = $user->getId();

$form = new FormInput();
?>
<form action="" id="modal-form">
    <div class="row">
        <div class="col-md-12">
            <div id="printOrder">
                <div class="row">
                    <div class="col-md-12 mb-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <div class="error-output">

                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    Class
                                                </td>
                                                <td>
                                                    <?php
                                                    $form->setPlaceholder('Title');
                                                    $form->setClass('title');
                                                    $form->setExtra('disabled',true);
                                                    $form->createText('title', $classroom->getClassName());
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Date
                                                </td>
                                                <td>
                                                    <?php
                                                    $form->setPlaceholder('Date');
                                                    $form->setClass('Date');
                                                    $form->setExtra('disabled',true);
                                                    $form->createTextarea('Date', $attendance->getDate());
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    Students
                                                </td>
                                                <td>
                                                    <?php
                                                    $student_ids = explode(',', $attendance->getStudentIds());
                                                    foreach (Collection::getStudents($dbc) as $i):
                                                         $student=new Student($dbc);
                                                         $student->loadstudentinfo($i->getId());
                                                         if($student->getClassId()==$attendance->getClassId()):
                                                        ?>
                                                        <div class="form-check form-check-inline col-md-3">
                                                            <input class="form-check-input" type="checkbox"
                                                                   name="Intake_Ids[]"
                                                                   id="<?php echo $i->getId() ?>"
                                                                   value="<?php echo $i->getId()?>"
                                                                <?php
                                                                foreach ($student_ids as $id) {
                                                                    if ($i->getId() == $id) {
                                                                        echo "checked";
                                                                    }
                                                                }
                                                                ?>
                                                            >
                                                            <label class="form-check-label"
                                                                   for="<?php echo $i->getId() ?>"><?php echo $i->getUsername() ?></label>
                                                        </div>
                                                    <?php endif;
                                                    endforeach;
                                                    ?>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
</form>
<!--                --><?php //endforeach; ?>


<script type="text/javascript">
    $(document).ready(function () {

        // Check admin api approve-order and pass required data


    });
</script>
