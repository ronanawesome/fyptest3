<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$error = false;
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $validator = new Validator;

    $fields = array();
    $fields[] = array('index' => 'admin', 'label' => "Admin", 'required' => false);

    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    if (!$problem) {

    } else {
        $error = $problem;
    }

}


$title = 'Announcement Management';
$breadcrumbs = array(
    $title => '/' . $role . '/' . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------


include(TEACHER_HEADER);

//prepare form input
$form = new FormInput();
$dbuilder = new DateBuilder();
?>
    <div class="row">
        <div class="col-md-12">
            <form id="search-form" action="" method="GET">
                <?php getAlert(); ?>
                <div class="form-group row">
                    <label for="user" class="col-sm-3 col-lg-2 col-form-label">Announcements</label>
                    <div class="col-sm-9 col-lg-10">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </span>
                            </div>
                            <?php
                            $form->setPlaceholder("Announcement Title");
                            $form->createText('admin', '');
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="user" class="col-sm-3 col-lg-2 col-form-label"><?php echo "Request From" ?></label>
                    <div class="col-sm-9 col-lg-10">
                        <?php
                        $dbuilder->build_range_with_default("start_date", "", "end_date", "");
                        ?>
                        <small class="form-text text-muted"></small>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                        <input type="submit" value="<?php echo "Search" ?>" class="btn btn-primary btn-block"/>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-12 mt-3 d-flex flex-row-reverse">
            <a href="create-announcement" class="btn btn-light-warning font-weight-bold mr-2">Create New Announcement</a>
        </div>
        </form>
    </div>
    <div class="col-md-12">
        <?php

        require_once('module/xcrud/xcrud.php');
        $xcrud = Xcrud::get_instance();
        $xcrud->table('Announcement');


        if (!$error) {
            if ($cv['admin'] != "") {
                $xcrud->where('Title LIKE "%' . $cv['admin'] . '%" OR Intakes.Intake_Code LIKE "%' . $cv['admin']. '%"');
            }
        }


        $xcrud_columns = [
            'id',
            'Title',
            'Creator_Id',
            'Created_Datetime',
            'Year_Target',
        ];

        $xcrud_labels = [
            'id' => ' ID',
            'Creator_Id'=> 'Created By',
            'StudentName'=>'Student Involved',
            'Year_Target'=>'Year Target',
            'Created_Datetime'=>'Created On'
        ];

        $xcrud_fields = [
            'id',
            'Title',
            'Date',
            'File',
            'Year_Target',
            'Creator_Id',
            'Content',
            'Intakes.Intake_Code',
        ];

        $xcrud->columns($xcrud_columns);
        $xcrud->fields($xcrud_fields);
        $xcrud->label($xcrud_labels);
        $xcrud->button('javascript:announcement_details({Announcement.id})', 'Details', 'flaticon2-search', 'btn btn-warning');
        $xcrud->unset_add();
        //  $xcrud->unset_search();
          $xcrud->unset_remove();
        $xcrud->unset_title();
        $xcrud->unset_edit();
        $xcrud->unset_view();
        $xcrud->unset_limitlist();
        //$xcrud->unset_print();
        // $xcrud->unset_csv();

        echo $xcrud->render();
        ?>
    </div>

    <script type="text/javascript">


        function announcement_details($id) {
            var hiddenBtn = $("#hidden_big_button");
            hiddenBtn.attr('data-remote', '<?php echo "/$role/announcement-details-modal" ?>?id=' + $id);
            hiddenBtn.attr('data-modal-title', 'Announcement Details');
            hiddenBtn.attr('data-loading-text', 'Announcement Details Loading...');
            hiddenBtn.click();
        }
    </script>


<?php
include(TEACHER_FOOTER);






