<?php

global $dbc, $url;
$role = $url['call_parts'][0];


if(!isset($_SESSION['teacher_id'])){
    redirect($role.'/login');
}

$error = false;
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $validator = new Validator;
    $fields = array();
    $fields[] = array('index' => 'admin', 'label' => "Admin", 'required' => false);

    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    if (!$problem) {

    } else {
        $error = $problem;
    }

}


$title = 'Complaint Report Management';
$breadcrumbs = array(
    $title => '/' . $role . '/' . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------


include(TEACHER_HEADER);

//prepare form input
$form = new FormInput();
$dbuilder = new DateBuilder();

$admin = new user($dbc);
$admin->load($_SESSION['teacher_id']);
?>
    <div class="row">
    <div class="col-md-12">
        <form id="search-form" action="" method="GET">
            <?php getAlert(); ?>
                      <div class="form-group row">
                        <label for="user" class="col-sm-3 col-lg-2 col-form-label"><?php echo "Date Range" ?></label>
                        <div class="col-sm-9 col-lg-10">
                            <?php
                            $dbuilder->build_range_with_default("start_date", "", "end_date", "");
                            ?>
                            <small class="form-text text-muted"></small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                            <input type="submit" value="<?php echo "Search" ?>" class="btn btn-primary btn-block"/>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-12 mt-3 d-flex flex-row-reverse">
                <a href="create-complaint" class="btn btn-light-warning font-weight-bold mr-2">Create New Complaint</a>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <?php

        require_once('module/xcrud/xcrud.php');
        $xcrud = Xcrud::get_instance();
        $xcrud->table('ComplaintReport');


        if (!$error) {
            if ($cv['admin'] != "") {
                $xcrud->where('Title LIKE "%' . $cv['admin'] . '%" OR Type LIKE "%' . $cv['admin']. '%"');
            }
        }
        $xcrud_columns = [
            'id',
            'Title',
            'User_Ids',
            'Type',
            'Content',
            'CreatedDate'
        ];

        $xcrud_labels =array(
            'id' => ' ID',
            'Type'=>'Report Type',
              'CreatedDate'=>'Date'
            );

        $xcrud_fields = [
            'id',
            'Title',
            'user.Username',
            'Student_Ids',
            'Type',
            'Content'
        ];

        $xcrud->columns($xcrud_columns);
       // $xcrud->fields($xcrud_fields);
        $xcrud->label($xcrud_labels);
        $xcrud->button('javascript:complaint_details({ComplaintReport.id})', 'Details', 'fa fa-edit', 'btn btn-warning');
        $xcrud->unset_add();
        $xcrud->unset_edit();
        //$xcrud->unset_search();
        $xcrud->unset_remove();
        $xcrud->unset_title();
       $xcrud->unset_view();
        $xcrud->unset_limitlist();
        $xcrud->unset_print();
        $xcrud->unset_csv();

        echo $xcrud->render();
        ?>
    </div>

    <script type="text/javascript">
        function complaint_update($id) {
            var hiddenBtn = $("#hidden_big_button");
            hiddenBtn.attr('data-remote', '<?php echo "/$role/complaint-details-modal" ?>?id=' + $id);
            hiddenBtn.attr('data-modal-title', 'Update Complaint');
            hiddenBtn.attr('data-loading-text', '');
            hiddenBtn.click();
        }

        function complaint_details($id) {
            var hiddenBtn = $("#hidden_big_button");
            hiddenBtn.attr('data-remote', '<?php echo "/$role/complaint-details-modal" ?>?id=' + $id);
            hiddenBtn.attr('data-modal-title', 'Complaint Details');
            hiddenBtn.attr('data-loading-text', 'Complaint Details Loading...');
            hiddenBtn.click();
        }
    </script>


<?php
include(ADMIN_FOOTER);






