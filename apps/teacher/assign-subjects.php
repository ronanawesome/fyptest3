<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$page_title = _REGISTER;

$breadcrumbs = [
    _REGISTER => '/' . $role . '/registration'
];
$intake = Collection::getIntake($dbc);
$teacher = new Teacher($dbc);

$session_user = new user($dbc);
$session_user->load($_SESSION['teacher_id']);

include(REGISTRATION_HEADER);
//include(STUDENT_NAVBAR);
//include(PARENT_BREADCRUMB);

$userID = $session_user->getId();
$error = array();
$int = 0;
$validator = new Rakit\Validation\Validator;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

//    $validator = new Validator;

    // fields info container
    $fields = array();

    $fields['Subjects_Taught'] = 'array|required';
    // fill the container with fields data
//    $fields[] = array('index' => 'Main_Subject', 'label' => 'Main Subject', 'required' => True);
//    $fields[] = array('index' => 'Secondary_Subject', 'label' => 'Secondary Subject', 'required' => true);
   // $fields[] = array('index' => 'Subjects_Taught', 'label' => 'Subjects Taught', 'required' => true);

    $validation = $validator->make($_POST + $_GET + $_FILES, $fields);

    $validation->validate();
    $problem = $validation->errors();

    $cv = $validation->getValidatedData(); // get the form values

    //ERROR !problem is from the previous validation this one got another format \
    if (!$validation->fails()) {
        do {
            $dbc->beginTransaction();

            $teacher->createSubjectsTaught($cv, $session_user->getId());
            $dbc->commit();
            $msg = "Subjects Assigned.";
            redirect($role . "/home?alert-success=" . $msg);
        } while (0);
    } else {
        $msg_fail = "";
        $errors = $validation->errors();
        $messages = $errors->all();
        foreach ($messages as $m) {
            $msg_fail = $m . "<br/>" . $msg_fail;
        }
//        this is working for me so it should work for you.

        redirect($role . "/assign-subjects?alert-danger=" . $msg_fail);


    }
}
//    $validator->formHandle($fields);
//    $problem = $validator->getErrors();
//    $cv = $validator->escape_val(); // get the form values
//
//
//
//    $teacher=new Teacher($dbc);
//    if (!$problem) {
//        do {
//            if($cv['Main_Subject']==$cv['Secondary_Subject'])
//            { $msg_fail = "Main And Secondary Subjects Taught Cannot Be The Same.";
//                $errors = $problem;
//                redirect($role . "/assign-subjects?alert-danger=" . $msg_fail.$errors);
//                break;
//            }
//            $teacher->setTeacherId($session_user->getId());
//            $teacher->setMainSubject($cv['Main_Subject']);
//            $teacher->setSecondarySubject($cv['Secondary_Subject']);
//            $teacher->setSubjectsTaught();
//            $teacher->create();
//            echo "<script>
//                        window.location.href=('home?alert-success=Assignment Successfully');
//                  </script>";
//        } while (0);
//    } else {
//        $error['problem'] = $problem;
//    }
//}


$form = new FormInput();
?>
<head>

    <style>

        .log-form form {
            display: block;
            width: 80%;
            padding: 2em;
            border: 1px solid #bbb;
            margin: auto;
        }

        .log-form h2 {
            color: white;
            font-size: 1.15em;
            display: block;
            background: #2a2a2a;
            width: 80%;
            text-transform: uppercase;
            padding: 0.75em 1em 0.75em 1.5em;
            box-shadow: inset 0px 1px 1px rgba(255, 255, 255, 0.05);
            border: 1px solid #1d1d1d;
            font-weight: bold;
            margin: auto;
        }

        .log-form input {
            display: block;
            margin: auto auto;
            width: 100%;
            margin-bottom: 2em;
            padding: 0.5em 0;
            border: none;
            border-bottom: 1px solid #eaeaea;
            padding-bottom: 1.25em;
            color: #757575;
        }

        .log-form input:focus {
            outline: none;
        }

        .log-form .btn {
            display: inline-block;
            background-color: Transparent;
            border: 1px solid black;
            padding: 0.5em 2em;
            color: black;
            margin-right: 0.5em;
            box-shadow: inset 0px 1px 0px rgba(255, 255, 255, 0.2);
        }

        .log-form .btn:hover {
            background: black;
            color: white;
        }

        .log-form .btn:active {
            background: #1fb5bf;
            box-shadow: inset 0px 1px 1px rgba(0, 0, 0, 0.1);
        }

        .log-form .btn:focus {
            outline: none;
        }

        .log-form .forgot {
            color: #1ba0a9;
            font-weight: bold;
            line-height: 0.5em;
            position: relative;
            text-decoration: none;
            font-size: 0.75em;
            margin: 0;
            padding: 0;
            float: right;
        }

        .log-form .forgot:hover {
            color: #33d3de;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 110px;margin-top: 3%;">
            <div class="log-form">
                <h2><?php echo 'ASSIGN SUBJECTS TAUGHT' ?></h2>
                <form method="post">
                    <div class="form-group">
                        <?php getAlert(); ?>
                        <?php print_error("problem", $error, '<div class="alert alert-danger" style="margin-top:15px;">', '</div>'); ?>
                    </div>
                    <div class=" row ">
                        <div class="col-lg-20">
                            <label for="User_ID">Username:</label>
                            <input disabled type="text" class="form-control" id="User_Id"
                                   name="User_Id" value=" <?php echo $session_user->getUsername(); ?>">
                        </div>
                    </div>
                    <label class="col-lg-20 col-form-label font-weight-bold">Chosen Subjects You Teach:</label>
                    <div class="col-lg-12 py-2 border border-secondary m-0 row"
                         style="border-radius: 0.42rem;min-height: 250px;">
                        <?php
                        //                        $form->createHidden('coach_id', $_SESSION['coach_id']);
                        //                        $form->createHidden('tournament_id', $id);
                        $choices = [];
                        if (empty(Collection::getSubject($dbc))):
                            ?>
                            <h5>No Subjects</h5>
                        <?php
                        else:
                        foreach (Collection::getSubject($dbc) as $singlesub):
                            ?>
                            <div class="col-12 col-md-4 m-0 row p-2" style="border: 2px solid black; border-radius: 5px;">
                                <div class="col-3 col-md-2">
                                    <input class="form-check-input m-0" type="checkbox"
                                           name="Subjects_Taught[]"
                                           id="<?php echo $singlesub->getId() ?>"
                                           value="<?php echo $singlesub->getId() ?>"
                                    >
                                </div>
                                <div class="col-9 col-md-10">
                                    <label class="form-check-label"
                                           for="<?php echo $singlesub->getId() ?>"><?php echo $singlesub->getSubjectName() ?>
                                    </label>
                                </div>
                            </div>
                        <?php
                        endforeach;
                        ?>
                    </div>
                    <?php
                    endif;
                    ?>
                    <button type="submit" class="btn btn-primary"><?php echo 'ASSIGN' ?></button>
            </div>
            <br>
            </form>
        </div><!--end log form -->
    </div>
</div>
</div>
</body>


<script type="text/javascript">

    $(document).ready(function () {

        $('.js-example-basic-single').select2();
        // $('#select-all').click(function() {
        //     $('#player').find('option').prop('selected', true);
        //     $('#player').trigger('change');
        // });

    });

</script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js">

    <?php
    include(STUDENT_FOOTER);
    ?>

