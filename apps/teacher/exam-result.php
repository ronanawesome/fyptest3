<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$error = false;
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $validator = new Validator;

    $fields = array();
    $fields[] = array('index' => 'admin', 'label' => "Admin", 'required' => false);
    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    if (!$problem) {

    } else {
        $error = $problem;
    }

}


$title = 'Exam Result Management';
$breadcrumbs = array(
    $title => '/' . $role . '/' . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------


include(TEACHER_HEADER);

//prepare form input
$form = new FormInput();
$dbuilder = new DateBuilder();
?>
    <div class="row">
        <div class="col-md-12">
            <form id="search-form" action="" method="GET">
                <?php
                getAlert();
                ?>
                <div class="form-group row">
                    <label for="user" class="col-sm-3 col-lg-2 col-form-label">Parent/Guardian</label>
                    <div class="col-sm-9 col-lg-10">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </span>
                            </div>

                            <?php
                            $form->setPlaceholder("Exam Name");
                            $form->createText('admin', '');
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="user" class="col-sm-3 col-lg-2 col-form-label"><?php echo "Date Range" ?></label>
                    <div class="col-sm-9 col-lg-10">
                        <?php
                        $dbuilder->build_range_with_default("start_date", "", "end_date", "");
                        ?>
                        <small class="form-text text-muted"></small>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                        <input type="submit" value="Search" class="btn btn-primary btn-block">
                    </div>
                </div>
                <div class="col-md-12 mt-3 d-flex flex-row-reverse">
                    <a href="create-exam-modal" class="btn btn-light-warning font-weight-bold mr-2">Create Exam</a>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-12">
        <?php

        require_once('module/xcrud/xcrud.php');
        $xcrud = Xcrud::get_instance();
        $xcrud->table('ExamResult');
        $xcrud->join('Exam_ID','Exam','id');
        $xcrud->join('Exam.Subject_Id','Subject','id');
        $xcrud->join('Exam.Intake_Id','Intakes','id');
        $xcrud->join('User_ID','Student','User_Id');
        $xcrud->join('Student.Class_Id','ClassRoom','id');
        $xcrud->join('Student.User_Id','user','id');


        if (!$error) {

            if ($cv['admin'] != "") {
                $xcrud->where('Exam_Name LIKE "%' . $cv['admin'] . '%"');
            }

        }


        $xcrud_columns = [
            //'id',
            'Exam.Exam_Name',
            'Subject.Subject_Code',
            'Intakes.Intake_Code',
            'ClassRoom.Class_Name',

            'Exam.Exam_Date',
            'user.Username',
            'Exam_Grade',
            'Exam_Marks'
        ];
        $xcrud_labels = [
            //'id' => 'Exam ID',
            'Exam.Exam_Name' => 'Exam',
            'Subject.Subject_Code'=>'Subject Code',
            'user.Username'=>'Name',
            'Exam.Exam_Date'=>'Date',
            'Exam_Grade'=>'Grade',
            'Exam_Marks'=>'Marks'
        ];

        $xcrud_fields = [
            //'id',
            'Exam_Grade',
            'Exam_Marks'
        ];

        $xcrud->columns($xcrud_columns);
        $xcrud->fields($xcrud_fields);
        $xcrud->label($xcrud_labels);

        //$xcrud->unset_add();

        $xcrud->unset_title();
       $xcrud->unset_view();
        //   $xcrud->unset_limitlist();
        // $xcrud->unset_print();
        //$xcrud->unset_csv();

        echo $xcrud->render();
        ?>
    </div>

    <script type="text/javascript">

    </script>


<?php
include(ADMIN_FOOTER);




