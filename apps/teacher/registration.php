<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$page_title = _REGISTER;

$breadcrumbs = [
    _REGISTER => '/' . $role . '/registration'
];
include(REGISTRATION_HEADER);
//include(PARENT_HEADER);
//include(PARENT_NAVBAR);
//include(PARENT_BREADCRUMB);


$error = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $validator = new Validator;

    // fields info container
    $fields = array();

    // fill the container with fields data
    $fields[] = array('index' => 'firstname', 'label' => 'First Name', 'required' => true);
    $fields[] = array('index' => 'lastname', 'label' => 'Last Name', 'required' => true);
    $fields[] = array('index' => 'email', 'label' => 'Email', 'required' => true);
    $fields[] = array('index' => 'phoneNo', 'label' => 'Phone Number', 'required' => true);
    $fields[] = array('index' => 'password', 'label' => 'Password', 'required' => true);
    $fields[] = array('index' => 'confirmpassword', 'label' => 'Confirm Password', 'required' => true);
    $fields[] = array('index' => 'street', 'label' => 'Street', 'required' => false);
    $fields[] = array('index' => 'postcode', 'label' => 'Post Code', 'required' => false);
    $fields[] = array('index' => 'city', 'label' => 'City', 'required' => false);
    $fields[] = array('index' => 'gender', 'label' => 'Gender', 'required' => true);
    $fields[] = array('index' => 'birthday', 'label' => 'Birthday', 'required' => true);
    $fields[] = array('index' => 'religion', 'label' => 'Religion', 'required' => true);

    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    $user=new user($dbc);
    if (!$problem) {
        do {
            if (!($cv["confirmpassword"] === $cv['password'])) {
                $error['problem'] = 'confirm password must be same';
                break;
            }
            if (  $user->searchEmail($cv['email'])>0) {
                $error['problem'] = 'Email is already taken ';
                break;
            }
            if (strlen($cv['password']) < 6) {
                $error['problem'] = 'Minimum 6 length';
                break;
            }
            $user->setUsername($cv['firstname'] . $cv['lastname']);
            $user->setFName($cv['firstname']);
            $user->setLName($cv['lastname']);
            $user->setEmail($cv['email']);
            $user->setGender($cv['gender']);
            $user->setPhoneNo($cv['phoneNo']);
            $user->setPassword(PasswordHasher::HashPassword($cv['password']));
            $user->setBirthday($cv['birthday']);
            $user->setStreet($cv['street']);
            $user->setPostCode($cv['postcode']);
            $user->setCity($cv['city']);
            $user->setRole("Teacher");
            $user->setReligion($cv['religion']);
            $user->setRegistrationApproval("pending");
            $user->setIsDelete(0);
            $user->create();
            $msg = "Announcement Created.";
            redirect($role . "/login?alert-success=Register" . $msg);
        } while (0);
    } else {
        $error['problem'] = $problem;
    }
}

$form = new FormInput();
?>
<head>

    <style>

        .log-form form {
            display: block;
            width: 80%;
            padding: 2em;
            border: 1px solid #bbb;
            margin: auto;
        }
        .log-form h2 {
            color: aliceblue;
            font-size: 1.15em;
            display: block;
            background: darkblue;
            width: 80%;
            text-transform: uppercase;
            padding: 0.75em 1em 0.75em 1.5em;
            box-shadow: inset 0px 1px 1px rgba(255, 255, 255, 0.05);
            border: 1px solid #1d1d1d;
            font-weight: bold;
            margin: auto;
        }
        .log-form input {
            display: block;
            margin: auto auto;
            width: 100%;
            margin-bottom: 2em;
            padding: 0.5em 0;
            border: none;
            border-bottom: 1px solid #eaeaea;
            padding-bottom: 1.25em;
            color: #757575;
        }
        .log-form input:focus {
            outline: none;
        }
        .log-form .btn {
            display: inline-block;
            background-color: dodgerblue;
            border: 1px solid black;
            padding: 0.5em 2em;
            color: black;
            margin-right: 0.5em;
            box-shadow: inset 0px 1px 0px rgba(255, 255, 255, 0.2);
        }
        .log-form .btn:hover {
            background: black;
            color: white;
        }
        .log-form .btn:active {
            background: #1fb5bf;
            box-shadow: inset 0px 1px 1px rgba(0, 0, 0, 0.1);
        }
        .log-form .btn:focus {
            outline: none;
        }
        .log-form .forgot {
            color: #1ba0a9;
            font-weight: bold;
            line-height: 0.5em;
            position: relative;
            text-decoration: none;
            font-size: 0.75em;
            margin: 0;
            padding: 0;
            float: right;
        }
        .log-form .forgot:hover {
            color: #33d3de;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 110px;margin-top: 3%;"><a class="btn btn-secondary"  href="
        login">Back to login</a>
            <?php  echo "<br><br>";?>
            <div class="log-form">
                <h2>Teacher Registration</h2>
                <form method="post">
                    <div class="form-group">
                        <?php getAlert(); ?>
                        <?php print_error("problem", $error, '<div class="alert alert-danger" style="margin-top:15px;">', '</div>'); ?>
                    </div>
                    <?php
                    $form->setClass('form-control');
                    $form->setPlaceholder(_REGISTER_YOUR_FIRST_NAME);
                    $form->setExtra('autocomplete', 'off');
                    $form->createText('firstname', '');
                    ?>
                    <?php
                    $form->setClass('form-control');
                    $form->setPlaceholder(_REGISTER_YOUR_LAST_NAME);
                    $form->setExtra('autocomplete', 'off');
                    $form->createText('lastname', '');
                    ?>
                    <?php
                    $form->setClass('form-control');
                    $form->setPlaceholder(_REGISTER_EMAIL);
                    $form->setExtra('autocomplete', 'off');
                    $form->createText('email', '');
                    ?>

                    <input type="date" name="birthday" class="form-control">
                    <?php
                    $choices = [];
                    $choices['Male'] = _MALE;
                    $choices['Female'] = _FEMALE;
                    $form->setClass('form-control mb-4');
                    $form->createSelect('gender', $choices,$choices['Male']);
                    ?>
                    <div class="row">
                        <div class="col-9">
                            <?php
                            $form->setClass('form-control');
                            $form->setPlaceholder(_REGISTER_PH_NO);
                            $form->setExtra('autocomplete', 'off');
                            $form->createText('phoneNo', '');
                            ?>
                        </div>
                    </div>
                    <?php
                    $form->setClass('form-control');
                    $form->setPlaceholder(_ADDRESS_STREET);
                    $form->setExtra('autocomplete', 'off');
                    $form->createText('street', '');
                    ?>
                    <?php
                    $form->setClass('form-control');
                    $form->setPlaceholder(_ADDRESS_CITY);
                    $form->setExtra('autocomplete', 'off');
                    $form->createText('city', '');
                    ?>
                    <?php
                    $form->setClass('form-control');
                    $form->setPlaceholder(_ADDRESS_POST_CODE);
                    $form->setExtra('autocomplete', 'off');
                    $form->createText('postcode', '');
                    ?>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Religion Level:</label>
                        <div class="col-lg-6">
                            <select class="form-control"
                                    name="religion" id="religion">
                                <option value=1>Christian</option>
                                <option value=2>Buddhist</option>
                                <option value=3>Sikh</option>
                                <option value=4>Hindu</option>
                                <option value=5>Muslim</option>
                            </select>
                        </div>
                    </div><?php

                    ?>
                    <div class="form-group text-start mb-4"><span></span>
                        <label for="password"><i class="lni lni-lock"></i></label>
                        <input class="form-control" name="password" id="password" type="password" placeholder="Password">
                    </div>
                    <div class="form-group text-start mb-4"><span></span>
                        <label for="confirmpassword"><i class="lni lni-lock"></i></label>
                        <input class="form-control" name="confirmpassword" id="confirmpassword" type="password" placeholder="Confirm Password">
                    </div>
                    <!--                    --><?php
                    //                    $form->setClass('form-control');
                    //                    $form->setPlaceholder(_REGISTER_CONFIRM_PASSWORD);
                    //                    $form->setExtra('autocomplete', 'off');
                    //                    $form->createText('confirmpassword', '');
                    //                    ?>
                    <input type="hidden" title="balance" name="balance" value="0"/>
                    <button type="submit" class="btn"><?php echo _REGISTER_NOW ?></button><br>
                </form>
            </div><!--end log form -->
        </div>
    </div>
</div>
