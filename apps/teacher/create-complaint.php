<?php

global $dbc, $url;
$role = $url['call_parts'][0];

$admin = new user($dbc);
$admin->load($_SESSION['teacher_id']);
$complaintReport = new ComplaintReport($dbc);
$students=Collection::getStudents($dbc);

//---------- page info --------------
//    must include before header
$title = 'Create Complaint Report';
$breadcrumbs = array(

    $title => "/$role/" . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------

$validator = new Rakit\Validation\Validator;

$fields = array();
$error = array();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $fields['Title'] = 'required';
    $fields['Complaint_Date'] = 'required';
    $fields['Type'] = 'required';
    $fields['Student_Ids'] = 'array|required';
    $fields['Content'] = 'required';

    $validation = $validator->make($_POST + $_GET + $_FILES, $fields);

    $validation->validate();
    $problem = $validation->errors();

    $cv =   $validation->getValidatedData(); // get the form values

    //ERROR !problem is from the previous validation this one got another format refer to my login-->

    if (!$validation->fails()) {
        do {
            $dbc->beginTransaction();
            $complaintReport->createComplaintReport($cv,$admin->getId());

            $dbc->commit();
            $msg = "Complaint Created.";
            redirect($role . "/complaint?alert-success=" . $msg);
        } while (0);
    } else {
        $msg_fail = "";
        $errors = $validation->errors();
        $messages = $errors->all();
        foreach($messages as $m){
            $msg_fail = $m . "<br/>" . $msg_fail;
        }
//        this is working for me so it should work for you.

        redirect($role . "/create-complaint?alert-danger=" . $msg_fail);


    }
}

include(TEACHER_HEADER);

//prepare form input
$form = new FormInput();

?>
<head>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<!--    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"/>-->
<!--    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"/>-->
  </head>
<body>
    <form class="form" method="post" enctype="multipart/form-data">
        <div class="card-body">
            <?php getAlert() ?>
            <div class="form-group">
            </div>
            <h3 class="font-size-lg text-dark font-weight-bold mb-6">Complaint Report Details:</h3>
            <div class="mb-15">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Title:</label>
                    <div class="col-lg-6">
                        <?php
                        $form->setClass('form-control');
                        $form->setPlaceholder('Enter Report Title');
                        $form->setExtra('autocomplete', 'off');
                        $form->createText('Title', '');
                        ?>
                    </div>
                </div>
            <div class="form-group row">
                <label class="col-lg-3 col-form-label">Complaint Date:</label>
                <div class="col-lg-6">
                    <input type="datetime-local" id="Complaint_Date" name="Complaint_Date" class="form-control">
                </div>
            </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Complaint Type:</label>
                    <select class="col-lg-6 form-control form-select" id="Type" name="Type">
                        <option value="Bullying">Bullying</option>
                        <option value="Vandalism">Vandalism</option>
                        <option value="Cheating">Cheating</option>
                        <option value="Skipping">Skipping</option>
                        <option value="Disruptive Behaviour">Disruptive Behaviour</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <div class="form-group row" >
                    <label class="col-lg-3 ">Students Involved:</label>
                    <div class="col-lg-6 ">
                        <?php
                        if (empty( $students)) {
                            ?>r
                            <div class="form-check form-check-inline col-md-12">
                                <p class=" text-center col-12 font-weight-bold py-2 my-0">No Students Found</p>
                            </div>
                            <?php
                        }
                        else
                        {
                            $choices = [];
                            foreach ( $students as $ap){
                                $choices[$ap->getId()] = $ap->getUsername();
                            }
                            ?>
                            <select id="Student_Ids"  class="form-control js-example-basic-multiple" name="Student_Ids[]"  multiple="multiple" >
                                <?php foreach ($choices as $key => $choice) : ?>
                                    <option value="<?php echo $key ?>" <?php echo empty($key) ? 'selected' : '' ?>><?php echo $choice ?></option>
                                <?php endforeach;  ?>
                            </select>
                            <?php
                        }
                        ?> </div>
                    </div>




                <div class="form-group row">
                    <label class="col-lg-3 col-form-label for="exampleTextarea">Extra Details:
                    <span class="text-danger">*</span></label>
                    <textarea class="col-lg-6 form-control" id="Content" name="Content" rows="3"></textarea>
                </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                    <a href="complaint" class="btn btn-secondary">
                        Cancel
                    </a>
                </div>
            </div>
        </div>

        </div>
    </form>
    <script type="text/javascript">

        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();


        });

    </script>






<?php

include(TEACHER_FOOTER);
