<?php

global $dbc, $url;
$role = $url['call_parts'][0];


if(!isset($_SESSION['teacher_id'])){
    redirect($role.'/login');
}
$class=new ClassRoom($dbc);
$class->loadclassinfo($_SESSION['teacher_id']);
$error = false;
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $validator = new Validator;
    $fields = array();
    $fields[] = array('index' => 'admin', 'label' => "Admin", 'required' => false);
    $fields[] = array('index' => 'start_date', 'label' => 'Start Date');
    $fields[] = array('index' => 'end_date', 'label' => 'End Date');
    $validator->formHandle($fields);
    $problem = $validator->getErrors();
    $cv = $validator->escape_val(); // get the form values

    if (!$problem) {

    } else {
        $error = $problem;
    }

}


$title = 'Complaint Report Management';
$breadcrumbs = array(
    $title => '/' . $role . '/' . $url['call_parts'][1]
);
$card_title = "";
$card_icon = "";
//-----------------------------------

include(TEACHER_HEADER);

//prepare form input
$form = new FormInput();
$dbuilder = new DateBuilder();

$admin = new user($dbc);
$admin->load($_SESSION['teacher_id']);
?>
    <div class="row">
        <div class="col-md-12">
            <form id="search-form" action="" method="GET">
                <div class="form-group row">
                    <label for="user" class="col-sm-3 col-lg-2 col-form-label"><?php echo "Date Range" ?></label>
                    <div class="col-sm-9 col-lg-10">
                        <?php
                        $dbuilder->build_range_with_default("start_date", "", "end_date", "");
                        ?>
                        <small class="form-text text-muted"></small>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-9 offset-sm-3 col-lg-10 offset-lg-2">
                        <input type="submit" value="<?php echo "Search" ?>" class="btn btn-primary btn-block"/>
                    </div>
                </div>
            </form>
        </div>



        <?php
                $class=new ClassRoom($dbc);
                if( $class->loadclassinfo($_SESSION['teacher_id'])==true){  ?>
                     <div class="col-md-12 mt-3 d-flex flex-row-reverse">
                        <a href="take-class-attendance" class="btn btn-light-warning font-weight-bold mr-2">Create New Attendance</a>
        </div>

            <?php

                }else
        ?>

    </div>
    <div class="col-md-12">
        <?php

        require_once('module/xcrud/xcrud.php');
        $xcrud = Xcrud::get_instance();
        $xcrud->table('Attendance');
        $xcrud->join('Class_Id','ClassRoom','id');
        $xcrud->where('Class_Id=',$class->getId());

        if (!$error) {

            if ($cv['start_date'] != "") {
                $xcrud->where("DATE(Attendance.Date) >= '" . $cv['start_date'] . "'");
            }

            if ($cv['end_date'] != "") {
                $xcrud->where("DATE(Attendance.Date) <= '" . $cv['end_date'] . "'");
            }
        }

        $xcrud_columns = [
            'id',
            'Date',
            'ClassRoom.Class_Name',
        ];

        $xcrud_labels =array(
            'ClassRoom.Class_Name'=>'ClassName',

        );

        $xcrud_fields = [
            'id',
            'Date',
            'ClassRoom.Class_Name',
        ];

        $xcrud->columns($xcrud_columns);
        // $xcrud->fields($xcrud_fields);
        $xcrud->label($xcrud_labels);
        $xcrud->button('javascript:attendance_details({Attendance.id})', 'Details', 'fa fa-edit', 'btn btn-warning');
        $xcrud->unset_add();
        $xcrud->unset_edit();
        $xcrud->unset_search();
//        $xcrud->unset_remove();
        $xcrud->unset_title();
        $xcrud->unset_view();
        $xcrud->unset_limitlist();
        $xcrud->unset_print();
        $xcrud->unset_csv();

        echo $xcrud->render();
        ?>
    </div>


    <script type="text/javascript">
        function complaint_update($id) {
            var hiddenBtn = $("#hidden_big_button");
            hiddenBtn.attr('data-remote', '<?php echo "/$role/complaint-details-modal" ?>?id=' + $id);
            hiddenBtn.attr('data-modal-title', 'Update Complaint');
            hiddenBtn.attr('data-loading-text', '');
            hiddenBtn.click();
        }

        function attendance_details($id) {
            var hiddenBtn = $("#hidden_big_button");
            hiddenBtn.attr('data-remote', '<?php echo "/$role/attendance-details" ?>?id=' + $id);
            hiddenBtn.attr('data-modal-title', ' Attendance Details');
            hiddenBtn.attr('data-loading-text', 'Attendance Details Loading...');
            hiddenBtn.click();
        }

    </script>


<?php
include(TEACHER_FOOTER);






