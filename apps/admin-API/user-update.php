<?php

global $dbc;
$response_data = [];

$validator = new Validator;

$fields = array();

$fields[] = array('index' => 'FName', 'label' => 'First Name', 'required' => true);
$fields[] = array('index' => 'LName', 'label' => 'Last Name', 'required' => true);
$fields[] = array('index' => 'Email', 'label' => 'Email', 'required' => true);
$fields[] = array('index' => 'Birthday', 'label' => 'Birthday', 'required' => true);
$fields[] = array('index' => 'Gender', 'label' => 'Gender', 'required' => true);
$fields[] = array('index' => 'PhoneNo', 'label' => 'PhoneNo', 'required' => true);
$fields[] = array('index' => 'Street', 'label' => 'Street Start Time', 'required' => true);
$fields[] = array('index' => 'City', 'label' => 'City', 'required' => true);
$fields[] = array('index' => 'PostCode', 'label' => 'PostCode', 'required' => true);

$validator->formHandle($fields);
$problem = $validator->getErrors();
$cv = $validator->escape_val(); // get the form values

if ($problem) throwError(400, $problem);

$user = new user($dbc);


if (!$user->load($cv['user_id'])) throwError(400, "User Not Found.");
if (!$user->load($cv['user_id']) || ($user->getRole() != 'Admin')) throwError(400, "Invalid User.");


$dbc->beginTransaction();


$user->updateTournament($cv,$user->getId());

throwSuccess($response_data);
