<?php

global $dbc;
$response_data = [];

$validator = new Validator;

$fields = array();
$fields[] = array('index' => 'Student_Id', 'label' => 'Student ID', 'required' => true);
$fields[] = array('index' => 'Class_Id', 'label' => 'Class ID', 'required' => true);
$fields[] = array('index' => 'Intake_Id', 'label' => 'Intake ID', 'required' => true);


$validator->formHandle($fields);
$problem = $validator->getErrors();
$cv = $validator->escape_val(); // get the form values


if ($problem) throwError(400, $problem);

$student = new Student($dbc);

if (!$student->load($cv['Student_Id'])) throwError(400, "Student ID - Not Found.");


$dbc->beginTransaction();

$student->updateStudentInfo($cv);
//$student->setClassId($cv['Class_Id']);
//$student->setIntakeId($cv['Class_Level']);

$student->save();



throwSuccess($response_data);




