<?php

global $dbc;
$response_data = [];

$validator = new Validator;

$fields = array();


$fields[] = array('index' => 'Creator_Id', 'label' => 'Creator ID', 'required' => true);
$fields[] = array('index' => 'Title', 'label' => 'Title', 'required' => true);
$fields[] = array('index' => 'Complaint_Date', 'label' => 'Complaint Date', 'required' => true);
$fields[] = array('index' => 'Type', 'label' => 'Type', 'required' => true);
$fields[] = array('index' => 'Content', 'label' => 'Content', 'required' => true);
$fields[] = array('index' => 'Student_Ids', 'label' => 'Student_Ids', 'required' => true);
$fields[] = array('index' => 'Status', 'label' => 'Status', 'required' => true);


$validator->formHandle($fields);
$problem = $validator->getErrors();
$cv = $validator->escape_val(); // get the form values

if ($problem) throwError(400, $problem);

$complaint = new ComplaintReport($dbc);
$user = new User($dbc);

if (!$complaint->load($cv['announcement'])) throwError(400, "Invalid Announcement - Not Found.");
if (!$user->load($cv['user_id']) || ($user->getType() != 'Admin')) throwError(400, "Invalid User.");

$dbc->beginTransaction();

$image = fileUpload($_FILES['image'], 'announcement');

$complaint->setTitle($cv['Title']);
$complaint->setContent($cv['Content']);
$complaint->setCreatorId($cv['Creator_Id']);
$complaint->setStudentIds(str_replace($_SERVER['DOCUMENT_ROOT'] . DS, "", $image));
$complaint->setType($cv['Type']);
$complaint->setCreatedDate(today_format(false));
$complaint->save();



throwSuccess($response_data);




