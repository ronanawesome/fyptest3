<?php

global $dbc;
$response_data = [];

$validator = new Validator;
$teachers=Collection::getTeachersInClass($dbc);
$fields = array();
$fields[] = array('index' => 'Class_Id', 'label' => 'Class ID', 'required' => true);
$fields[] = array('index' => 'CTeacher_Id', 'label' => 'Class Teacher ID', 'required' => false);


$validator->formHandle($fields);
$problem = $validator->getErrors();
$cv = $validator->escape_val(); // get the form values

if ($problem) throwError(400, $problem);

$classroom = new ClassRoom($dbc);

if (!$classroom->load($cv['Class_Id'])) throwError(400, "Class ID - Not Found.");

foreach ($teachers as $ap){
    if ($ap->getID()==$cv['CTeacher_Id']) throwError(400, "Teacher already belongs to another class.");
}

$dbc->beginTransaction();

$classroom->updateClassTeacher($cv);
$classroom->save();



throwSuccess($response_data);




