<?php

global $dbc;
$response_data = [];

$validator = new Validator;
$fields = array();

$fields[] = array('index' => 'id', 'label' => 'ID', 'required' => true);
$fields[] = array('index' => 'RegistrationApproval', 'label' => 'Registration Approval', 'required' => true);

//'min_filesize' => 1, 'max_filesize' => 8024,

$validator->formHandle($fields);
$problem = $validator->getErrors();
$cv = $validator->escape_val(); // get the form values

if ($problem) throwError(400, $problem);


$user = new user($dbc);


if (!$user->load($cv['id'])) throwError(400, "User ID - Not Found.");

$dbc->beginTransaction();


$user->approve($cv);
$user->save();



throwSuccess($response_data);


