<?php

require_once (ROOT . DS . 'package' . DS . 'vendor' . DS . 'autoload.php');

require_once (ROOT . DS . 'include' . DS . 'shared_function.inc.php');

require_once (ROOT . DS . 'config'  . DS . 'app.config.inc.php');
require_once (ROOT . DS . 'config'  . DS . 'config.'.SERVER_STATE.'.inc.php');

require_once (ROOT . DS . 'include' . DS . 'common.inc.php');
require_once (ROOT . DS . 'include' . DS . 'mysql.inc.php');

//require_once 'module/kint/Kint.class.php';

$session=new Session();
$session->setCookie();
$url=parse_path();

$language=new Language($dbc);
$language->call();

//router must be the last to call
require_once (ROOT . DS . 'include' . DS . 'router.inc.php');
