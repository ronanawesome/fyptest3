<?php

if (PROJECT_PRIVACY == TRUE) {
    if (!isset($_SESSION['viewer_id'])) {
        find_route('project', 'login', false);
        exit();
    }
}//authethication for project viewer

if (SITE_AVAILABILITY == FALSE) {
    find_route('project', 'maintenance', false);
    exit();
}//authethication for project viewer


$url = parse_path();

if (isset($url['call_parts'])) {

    $lastElement = end($url['call_parts']);
    if ($lastElement == "") {
        $key = key($url['call_parts']);
        unset($url['call_parts'][$key]);
    }

}//trim last array if the value is ""

if (isset($url['call_parts'][0]) && isset($url['call_parts'][1])) { //when url is complete , do the routing logic

    switch ($url['call_parts'][0]) {
        case "admin" :
            if (!isset($_SESSION['admin_id']) && $url['call_parts'][1] != 'login' && $url['call_parts'][1] != '_api') { //when user is not logged
                redirect('admin/login');
            }// redirect non logged user
            break;
        case "parent" :
            $exception = ['login', 'registration', 'forgot-password','forgot-username', 'modal-package-info'];
            if (!isset($_SESSION['parent_id']) && !in_array($url['call_parts'][1], $exception)) { //when user is not logged
                redirect('parent/login');
            }// redirect non logged user
            break;
        case "student" :
            $exception = ['login', 'registration', 'forgot-password','forgot-username', 'modal-package-info','register-complete'];
            if (!isset($_SESSION['student_id']) && !in_array($url['call_parts'][1], $exception)) { //when user is not logged
                redirect('student/login');
            }// redirect non logged user
            break;
        case "Teacher" :
            $exception = ['login', 'registration', 'forgot-password','forgot-username', 'modal-package-info','register-complete'];
            if (!isset($_SESSION['teacher_id']) && !in_array($url['call_parts'][1], $exception)) { //when user is not logged
                redirect('teacher/login');
            }// redirect non logged user
            break;

        default :
            break;
    }

    find_route($url['call_parts'][0], $url['call_parts'][1]);

} else if (isset($url['call_parts'][0])) {

//    echo $url['call'];

    switch ($url['call_parts'][0]) {

        case "admin" :
            redirect('admin/home');
            break;
        case "parent" :
            redirect('parent/home');
            break;
        case "student" :
            redirect('student/home');
            break;
        default :
            redirect('student/home');

    }


} else {

    //default url when  user enter link without any informaion. eg. www.link.com
    redirect('student/home');

}

## End for frontend route #################################

call404();

##Please note find_route only be call when there are valid route
function find_route($dir, $filename)
{
    if (file_exists('apps/' . $dir . "/" . $filename . ".php")) {
        include_once('apps/' . $dir . "/" . $filename . ".php");
    } else {
        call404($dir);
    }
    exit();
}

#whole application will only redirect to 404 page when there are no valid route
function call404($dir)
{
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    if (file_exists('apps/' . $dir . "/404.php")) {
        find_route($dir, '404');
    } else {
        redirect('');
    }
}//end of call404
