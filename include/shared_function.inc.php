<?php

function get_mtd_w_months($interval = 12) {
    $result = [];
    for ($i = 0; $i <= ($interval - 1); $i++) {
//        if ($i == 0 && date('d') < 1) continue;
        $value = date("Y-m", strtotime(date('Y-m-01') . " -$i months"));
        $display = date("M Y", strtotime(date('Y-m-01') . " -$i months"));

        $result[$value] = $display;
    }

    return $result;
}

function get_mtd_w_month($month) {
    $sd = new DateTime($month . "-01");
    $ed = new DateTime($month . "-01");
    return ['sd' => $sd->format('Y-m-01'), 'ed' => $ed->format('Y-m-t')];
}

function startsWith($string, $startString) {
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
}

function log2Txt($status, $title, $url = "")
{
    /** START of Logging Region **/
    $datetime = date('Y-m-d h:i:s A');
    $d_year = date('Y');
    $d_month = date('m');
    $d_date = date('d');
    $log_file_name = ROOT . DS . 'logs' . DS . $title . DS . $d_year . DS . $d_month . DS . $d_date . '.txt';

    $content = $datetime . PHP_EOL;
    $content .= $url . PHP_EOL;

    $data = file_get_contents('php://input');
    $data = json_encode(json_decode($data), JSON_PRETTY_PRINT);;
    $data = str_replace('json=', '', $data);
    $data = $data . PHP_EOL;

    $content .= '[POST] ' . PHP_EOL;
    $content .= $data . PHP_EOL;
    $content .= '[STATUS]: ' . boolval($status) . PHP_EOL;

    logToFile($log_file_name, $content, true);
    /** END of Logging Region **/
}

//this function create folder if not exist
//append the content to the file if exist
function logToFile($file, $content, $append = false)
{
    if (!file_exists(dirname($file))) mkdir(dirname($file), 0777, true);
    if ($append) {
        file_put_contents($file, $content . PHP_EOL, FILE_APPEND | LOCK_EX);
    } else {
        file_put_contents($file, $content);
    }
}

function randomDate($start_date, $end_date)
{
    // Convert to timetamps
    $min = strtotime($start_date);
    $max = strtotime($end_date);

    // Generate random number using above bounds
    $val = rand($min, $max);

    // Convert back to desired date format
    return date('Y-m-d H:i:s', $val);
}

function thirdPartyQr($text, $size = 350)
{
    return "https://api.qrserver.com/v1/create-qr-code/?size=" . $size . "x" . $size . "&data=" . $text;
}

function redirect2($url)
{
    header('Location: ' . $url);
    exit();
}

function column_2_orders($columns, $order)
{
    $orders = [];
    foreach ($columns as $column) {
        if (isset($column['orderable']) && $column['orderable']) {
            $array_key = $columns[$order[0]['column']]['data'];
            $orders[$array_key] = $order[0]['dir'];
        }
    }
    return $orders;
}

function throwSuccess($response_data) {
    $response = array();
    $response['code'] = 200;
    $response['success'] = true;
    $response = array_merge($response, $response_data);

    http_response_code(200);
    $encoded_json = json_encode($response);
    header('Content-Type: application/json');
    die($encoded_json);
}


function throwError($code, $msg, $logout = false)
{
    $response = array();
    $response['code'] = $code;
    $response['success'] = false;
    $response['error'] = $msg;
    if ($logout) $response['logout'] = true;

    http_response_code($code);
    $encoded_json = json_encode($response);
    header('Content-Type: application/json');
    die($encoded_json);
}


function slug_2_text($string)
{
    $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', ' ', $string)));
    return $slug;
}

function text_2_slug($string)
{
    $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
    return $slug;
}

function format_date($date, $format = "Y-m-d H:i A")
{
    $dt = new DateTime($date);
    return $dt->format($format);
}

function generate_day_index($start_date, $end_date, $inclusive = true)
{

    $duration = get_days($start_date, $end_date);

    //create index of days
    $day_index = array();
    $i = 1;
    if ($inclusive) {
        $i = 0;
    }
    while ($i <= $duration) {
        $day_index[date('Y-m-d', strtotime($start_date . "+" . $i . " days"))] = true;
        $i++;
    }

    return $day_index;

}

function get_days($start_date, $end_date)
{
    $start = new DateTime($start_date);
    $end = new DateTime($end_date);
    return $start->diff($end)->format("%a");
}

function is_dev()
{
    if (SERVER_STATE != 'production') {
        return true;
    }
    return false;
}

function getDomain()
{

    // so called safe version from stackoverflow
    if ((!empty($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https') || (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (!empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443')) {
        $protocol = 'https';
    } else {
        $protocol = 'http';
    }

    return $protocol . '://' . $_SERVER['HTTP_HOST'];

}

function isWalao($supplier_id = NULL)
{
    if (is_null($supplier_id)) {
        return in_array($_SESSION['supplier_id'], ANTHUB_WALAOS);
    } else {
        return in_array($supplier_id, ANTHUB_WALAOS);
    }
}

function get_request_curl_string($url)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,//url to get access token
        CURLOPT_RETURNTRANSFER => true,//return the response instead of output
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, CURLOPT_CUSTOMREQUEST => "GET",
    ));

    $response = curl_exec($curl);
    $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    $responseArray = json_decode($response, true);

    return array($responseArray, $responseCode);
}

function post_request_curl_string($url, array $query)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,//url to get access token
        CURLOPT_RETURNTRANSFER => true,//return the response instead of output
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, CURLOPT_CUSTOMREQUEST => "POST", CURLOPT_POSTFIELDS => http_build_query($query) //the info to passed on during the POST request
    ));

    $response = curl_exec($curl);
    $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    $responseArray = json_decode($response, true);

    return array($responseArray, $responseCode);
}

function group_array_by($array, $key)
{
    $return = array();
    foreach ($array as $val) {
        $return[$val[$key]][] = $val;
    }
    return $return;
}

function getUri()
{
    return strtok($_SERVER["REQUEST_URI"], '?');
}

function generateHash($value)
{
    return md5($value . SALT);
}

function startMemberPortlet($title = "", $subtitle = "")
{

    ?>
    <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?php echo $title; ?>
                    <small><?php echo $subtitle; ?></small>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
    <?php

}

function endMemberPortlet()
{

    ?>
    </div>
    </div>
    <?php

}

function checkExtraParameter($extra_parameter_count)
{

    global $url;

    $require_parameter = 1 + $extra_parameter_count;

    if (isset($url['call_parts'])) {
        if (count($url['call_parts']) >= $require_parameter) {
            return true;
        }
    }

    return false;

}

function getExtraParameter($position)
{ // start from 0

    global $url;

    return $url['call_parts'][$position];

}

function getAlert()
{

    global $url;

    if (isset($url['query_vars']['alert-success'])) {

        echo '<div class="alert alert-success">' . $url['query_vars']['alert-success'] . '</div>';;

    }

    if (isset($url['query_vars']['alert-danger'])) {

        echo '<div class="alert alert-danger">' . $url['query_vars']['alert-danger'] . '</div>';;

    }

    if (isset($url['query_vars']['alert-warning'])) {

        echo '<div class="alert alert-warning">' . $url['query_vars']['alert-warning'] . '</div>';;

    }

    if (isset($url['query_vars']['alert-info'])) {

        echo '<div class="alert alert-info">' . $url['query_vars']['alert-info'] . '</div>';;

    }

}

function startMemberContent($title = "", $subtitle = "", $description = "")
{
    ?>
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-page__container m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <?php if ($title != "" && $subtitle != "") { ?>
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title">
                    <span class="font-weight: normal !important;"> <?php echo $title ?> - </span> <?php echo $subtitle; ?>
                    <small> <?php echo $description ?> </small>
                </h3>
            </div>
        </div>
    </div>
<?php } ?>
    <!-- END: Subheader -->
    <div class="m-content" <?php if ($title == "" && $subtitle == "") {
    echo 'style="padding-top:0"';
} ?>>
    <?php
}

function endMemberContent()
{
    ?>
    </div>
    </div>
    </div>
    <?php
}

function startAdminContent($title, $subtitle = "")
{
    ?>
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- END: Subheader -->
    <div class="m-content">
    <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text"> <?php echo $title ?>
                    <small> <?php echo $subtitle ?> </small>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
    <?php

}

function endAdminContent()
{

    ?>
    </div>
    </div>
    </div>
    </div>
    <?php

}

/**
 *    Print error if exist
 * @param $array - array to print
 **/
function print_error($name, $array, $open = "", $close = "")
{

    if (in_array($name, array_keys($array))) {
        echo $open . $array[$name] . $close;
    }

}


function print_msg($name, $array, $open = "", $close = "")
{

    if (in_array($name, array_keys($array))) {
        echo $array[$name];
    }

}

function check_msg($name, $array)
{
    if (in_array($name, array_keys($array))) {
        return true;
    } else {
        return false;
    }
}

function get_msg($name, $array)
{
    if (in_array($name, array_keys($array))) {
        return $array[$name];
    }
}

function check_error($name, $array)
{
    if (in_array($name, array_keys($array))) {
        return true;
    } else {
        return false;
    }
}

function get_error($name, $array)
{
    if (in_array($name, array_keys($array))) {
        return $array[$name];
    }
}

/**
 *    Print readable array
 * @param $array - array to print
 **/
function print_array($array, $color = "black")
{

    echo '<pre style="color:' . $color . '">';
    print_r($array);
    echo '</pre>';

}

/**
 *    Print with javascript alert
 **/
function js_print($string)
{

    echo '<script type="text/javascript">';
    echo 'alert("' . addslashes($string) . '")';
    echo '</script>';

}

/**
 *    Print with javascript alert
 **/
function js_print_array($array, $color = "black")
{

    echo '<script type="text/javascript">';
    echo 'alert("<pre style="color:' . $color . '">' . print_r($array) . '</pre>")';
    echo '</script>';

}

/**
 *    Remove bad chars from input
 * @param $str_words - input
 * @param $escape
 * @param $level
 **/
function prepare_input($str_words, $escape = false, $level = "high")
{
    global $dbc;
    $found = false;
    if ($level == "low") {
        $bad_string = array("%20union%20", "/*", "*/union/*", "+union+", "load_file", "outfile", "document.cookie", "onmouse", "<applet", "<meta", "<style", "<form", "<body", "<link", "_GLOBALS", "_REQUEST", "_GET", "include_path", "prefix", "ftp://", "smb://", "onmouseover=", "onmouseout=");
    } else if ($level == "medium") {
        $bad_string = array("xp_", "%20union%20", "/*", "*/union/*", "+union+", "load_file", "outfile", "document.cookie", "onmouse", "<script", "script>", "<iframe", "<applet", "<meta", "<style", "<form", "<body", "<link", "_GLOBALS", "_REQUEST", "_GET", "_POST", "include_path", "prefix", "ftp://", "smb://", "onmouseover=", "onmouseout=");
    } else if ($level == "high") {
        $bad_string = array("select", "drop", "--", "insert", "xp_", "%20union%20", "/*", "*/union/*", "+union+", "load_file", "outfile", "document.cookie", "onmouse", "<script", "script>", "<iframe", "<applet", "<meta", "<style", "<form", "<img", "<body", "<link", "_GLOBALS", "_REQUEST", "_GET", "_POST", "include_path", "prefix", "http://", "https://", "ftp://", "smb://", "onmouseover=", "onmouseout=");
    } else if ($level == "extra") {
        $bad_string = array("select", "drop", "--", "insert", "xp_", "%20union%20", "/*", "*/union/*", "+union+", "load_file", "outfile", "document.cookie", "onmouse", "<script", "script>", "<iframe", "<applet", "<meta", "<style", "<form", "<img", "<body", "<link", "_GLOBALS", "_REQUEST", "_GET", "_POST", "include_path", "prefix", "http://", "https://", "ftp://", "smb://", "onmouseover=", "onmouseout=", "<", ">", "'", '"');
    }
    for ($i = 0; $i < count($bad_string); $i++) {
        $str_words = str_ireplace($bad_string[$i], "", $str_words);
    }

    if ($escape) {
        $str_words = mysqli_real_escape_string($dbc, $str_words);
    }

    return $str_words;
}

/**
 *    macth array in multiarray
 * @param $elem
 * @param $multiarray
 **/

function in_multiarray($elem, $array)
{
    $top = sizeof($array) - 1;
    $bottom = 0;
    while ($bottom <= $top) {
        if ($array[$bottom] == $elem) return true; else if (is_array($array[$bottom])) if (in_multiarray($elem, ($array[$bottom]))) return true;

        $bottom++;
    }
    return false;
}

/**
 *    Password harsh with SHA256
 * @param $password
 **/

//function get_password_hash($password) {
//	return hash_hmac('sha256',$password , SALT , true);
//}

// Generate a random IV using openssl_random_pseudo_bytes()
// random_bytes() or another suitable source of randomness
function get_password_hash($password, $iterations = "10000")
{
    return hash_pbkdf2("sha256", $password, SALT, $iterations, 32);
}

function redirect($destination, $protocol = '')
{

    $url = $protocol . BASE_URL . '/' . $destination; // Define the URL.
    header("Location: $url");
    exit(); // Quit the script.

} // End of redirect() function.

function alink($destination, $protocol = 'http://')
{
    $url = $protocol . BASE_URL . '/' . $destination; // Define the URL.
    return $url;
}

function draw_navigation($navigation)
{

    $path = parse_path();
    $current_route = "/" . $path['call'];
    $current = false;

    echo '<ul>';

    foreach ($navigation as $k => $v) {

        if (is_array($v)) {

            $nav = '';
            $subnav = '';
            $title = explode('{i}', $k);

            foreach ($v as $sk => $sv) {
                $subnav .= '<li class="';
                if ($current_route == $sv) {
                    $current = true;
                    $subnav .= 'active';
                }
                $subnav .= '"> <a href="' . $sv . '"> ' . $sk . ' </a>';
                $subnav .= '</li>';
            }//end of foreach

            $nav .= '<li class="';
            if ($current == true) {
                $nav .= "start active";

            }
            $nav .= '">
					<a href="javascript:;"> <i class="' . $title[1] . '"></i> <span class="title">' . $title[0] . '</span>';

            if ($current == true) {
                $nav .= '<span class="selected"></span>';
                $current = false;
            }

            $nav .= '<span class="arrow "></span> </a>';
            $nav .= '<ul class="sub-menu">';
            $nav .= $subnav;
            $nav .= '</ul></li>';
            echo $nav;
        } else {//else not array

            $title = explode('{i}', $k);

            if ($current_route == $v) {
                $current = true;
            }

            echo '<li class="';

            if ($current == true) {
                echo "start active";
                $current = false;
            }

            echo '"> <a href="' . $v . '"> <i class="' . $title[1] . '"></i> <span class="title">' . $title[0] . '</span> </a> </li>';


        }//end of is_array


    }//end of foreach
    echo '</ul>';

}

//please use after session_start
function logout()
{

    // Destroy the session:
    $exception = array('viewer_id', 'lang');

    foreach ($_SESSION as $key => $value) {
        if (!in_array($key, $exception)) {
            unset($_SESSION[$key]);
        }
    }

}

function debug_query($dbc)
{

    echo '<br/>' . mysqli_error($dbc) . '<br/>';

}

function digit_word($number)
{
    switch ($number) {
        case 0:
            $word = "Zero";
            break;
        case 1:
            $word = "First";
            break;
        case 2:
            $word = "Second";
            break;
        case 3:
            $word = "Third";
            break;
        case 4:
            $word = "Forth";
            break;
        case 5:
            $word = "Fifth";
            break;
        case 6:
            $word = "Sixth";
            break;
        case 7:
            $word = "Seventh";
            break;
        case 8:
            $word = "Eight";
            break;
        case 9:
            $word = "Ninth";
            break;
    }
    return $word;
}

function getMatchedArray($array, $search)
{
    $returnArray = array();
    foreach ($array as $k => $v) {
        if ($v == $search) {
            $returnArray[] = $k;
        }
    }
    return $returnArray;
}

function trimArrayIndex($array, $keyword)
{
    $returnArray = array();
    foreach ($array as $k => $v) {
        $returnArray[] = str_replace($keyword, "", $v);
    }
    return $returnArray;
}

function yesterday_format($type = true)
{
    if ($type) {
        return date("Y-m-d", strtotime("-1 days"));
    } else {
        return date("Y-m-d H:i:s", strtotime("-1 days"));
    }
}

function today_format($type = true)
{
    if ($type) {
        return date("Y-m-d");
    } else {
        return date("Y-m-d H:i:s");
    }
}

function today($type = true)
{
    if ($type) {
        return date("Y/m/d");
    } else {
        return date("Y/m/d H:i:s");
    }
}

function camelCase($str, $exclude = array())
{
    // replace accents by equivalent non-accents
    $str = replaceAccents($str);
    // non-alpha and non-numeric characters become spaces
    $str = preg_replace('/[^a-z0-9' . implode("", $exclude) . ']+/i', ' ', $str);
    // uppercase the first character of each word
    $str = ucwords(trim($str));
    return lcfirst(str_replace(" ", "", $str));
}

function replaceAccents($str)
{
    $search = explode(",", "ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,ø,Ø,Å,Á,À,Â,Ä,È,É,Ê,Ë,Í,Î,Ï,Ì,Ò,Ó,Ô,Ö,Ú,Ù,Û,Ü,Ÿ,Ç,Æ,Œ");
    $replace = explode(",", "c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,o,O,A,A,A,A,A,E,E,E,E,I,I,I,I,O,O,O,O,U,U,U,U,Y,C,AE,OE");
    return str_replace($search, $replace, $str);
}

function get_title_from_filename()
{
    $cur_page = CUR_PAGE;
    $cur_dir = CUR_DIR;

    return ucfirst(strtolower($cur_dir)) . '::' . ucfirst(strtolower($cur_page));
}

function crypto_rand_secure($min, $max)
{
    $range = $max - $min;
    if ($range < 0) return $min; // not so random...
    $log = log($range, 2);
    $bytes = (int)($log / 8) + 1; // length in bytes
    $bits = (int)$log + 1; // length in bits
    $filter = (int)(1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd >= $range);
    return $min + $rnd;
}

function getToken($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet .= "0123456789";
    for ($i = 0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, strlen($codeAlphabet))];
    }
    return $token;
}

function sendMail($subject, $message, $fromName, $mailAddress)
{

    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=utf-8\r\nContent-Transfer-Encoding: 8bit\r\nX-Priority: 1\r\nX-MSMail-Priority: High\r\n";
    $headers .= "From:$fromName\r\n" . "Reply-To:$fromName\r\n";

    //send email using server
    mail($mailAddress, $subject, $message, $headers, "-f$fromName");
}

function parse_path()
{

    $path = array();

    if (isset($_SERVER['REQUEST_URI'])) {

        $request_path = explode('?', $_SERVER['REQUEST_URI']);

        $path['base'] = rtrim(dirname($_SERVER['SCRIPT_NAME']), '\/');

        $path['call_utf8'] = substr(urldecode($request_path[0]), strlen($path['base']) + 1);

        $path['call'] = utf8_decode($path['call_utf8']);

        if ($path['call'] == basename($_SERVER['PHP_SELF'])) {
            $path['call'] = '';
        }

        $path['call_parts'] = explode('/', $path['call']);

        if (isset($request_path[1])) $path['query_utf8'] = urldecode($request_path[1]);
        if (isset($request_path[1])) $path['query'] = utf8_decode(urldecode($request_path[1]));

        if (isset($path['query'])) {

            $vars = explode('&', $path['query']);
            foreach ($vars as $var) {
                $t = explode('=', $var);
                if (isset($t[0]) && isset($t[1])) {
                    $path['query_vars'][$t[0]] = $t[1];
                }
            }
        }//isset query

    }//end of isset request url

    return $path;

}

function form_self($home = true)
{
    $params = parse_path();
    return ($home == true ? ABS_URL . '/' : '') . $params['call'] . ((isset($params['query'])) ? '?' . $params['query'] : '');
}

function convertDatepicker($date)
{
    $time = strtotime($date);
    return date('Y-m-d', $time);
}

function lang()
{

    $url = parse_path();
    $lang = $url['call_parts'][0];
    return $lang;

}

function getLang()
{
    if (!isset($_SESSION['lang'])) {
        return 'en';
    }

    return $_SESSION['lang'];
}

function getAdminLang()
{
    if (!isset($_SESSION['admin_lang'])) {
        return 'en';
    }

    return $_SESSION['admin_lang'];
}


function dismount($object)
{
    $reflectionClass = new ReflectionClass(get_class($object));
    $array = array();
    foreach ($reflectionClass->getProperties() as $property) {
        $property->setAccessible(true);
        $array[$property->getName()] = $property->getValue($object);
        $property->setAccessible(false);
    }
    return $array;
}

function generateRandomString($length)
{
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);

    $randomString = '';

    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;

}

function whatsapp_no($value)
{

    $value = preg_replace('/[^0-9]/', '', $value);
    if (!empty($value) && $value[0] == "0") {
        $value = "6" . $value;
    }

    return $value;
}

function fileUpload($file, $destination)
{
    $uid = md5(uniqid());
    $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
    $file_name = "upload" . DS . $destination . DS . $uid . "." . $ext;
    $real_file_name = $_SERVER['DOCUMENT_ROOT'] . DS . $file_name;

    $temp_name = $file['tmp_name'];

    if (!move_uploaded_file($temp_name, $real_file_name)) {
        return false;
    } else {
        return $file_name;
    }
}

function invoice_num($input, $pad_len = 7, $prefix = null)
{
    if ($pad_len <= strlen($input))
        trigger_error('<strong>$pad_len</strong> cannot be less than or equal to the length of <strong>$input</strong> to generate invoice number', E_USER_ERROR);

    if (is_string($prefix))
        return sprintf("%s%s", $prefix, str_pad($input, $pad_len, "0", STR_PAD_LEFT));

    return str_pad($input, $pad_len, "0", STR_PAD_LEFT);
}

function numtowords($num){
    $decones = array(
        '01' => "One",
        '02' => "Two",
        '03' => "Three",
        '04' => "Four",
        '05' => "Five",
        '06' => "Six",
        '07' => "Seven",
        '08' => "Eight",
        '09' => "Nine",
        10 => "Ten",
        11 => "Eleven",
        12 => "Twelve",
        13 => "Thirteen",
        14 => "Fourteen",
        15 => "Fifteen",
        16 => "Sixteen",
        17 => "Seventeen",
        18 => "Eighteen",
        19 => "Nineteen"
    );
    $ones = array(
        0 => " ",
        1 => "One",
        2 => "Two",
        3 => "Three",
        4 => "Four",
        5 => "Five",
        6 => "Six",
        7 => "Seven",
        8 => "Eight",
        9 => "Nine",
        10 => "Ten",
        11 => "Eleven",
        12 => "Twelve",
        13 => "Thirteen",
        14 => "Fourteen",
        15 => "Fifteen",
        16 => "Sixteen",
        17 => "Seventeen",
        18 => "Eighteen",
        19 => "Nineteen"
    );
    $tens = array(
        0 => "",
        2 => "Twenty",
        3 => "Thirty",
        4 => "Forty",
        5 => "Fifty",
        6 => "Sixty",
        7 => "Seventy",
        8 => "Eighty",
        9 => "Ninety"
    );
    $hundreds = array(
        "Hundred",
        "Thousand",
        "Million",
        "Billion",
        "Trillion",
        "Quadrillion"
    ); //limit t quadrillion
    $num = number_format($num,2,".",",");
    $num_arr = explode(".",$num);
    $wholenum = $num_arr[0];
    $decnum = $num_arr[1];
    $whole_arr = array_reverse(explode(",",$wholenum));
    krsort($whole_arr);
    $rettxt = "";
    foreach($whole_arr as $key => $i){
        if($i < 20){
            $rettxt .= $ones[$i];
        }
        elseif($i < 100){
            $rettxt .= $tens[substr($i,0,1)];
            $rettxt .= " ".$ones[substr($i,1,1)];
        }
        else{
            $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0];
            $rettxt .= " ".$tens[substr($i,1,1)];
            $rettxt .= " ".$ones[substr($i,2,1)];
        }
        if($key > 0){
            $rettxt .= " ".$hundreds[$key]." ";
        }

    }
    $rettxt = $rettxt." Ringgit";

    if($decnum > 0){
        $rettxt .= " and ";
        if($decnum < 20){
            $rettxt .= $decones[$decnum];
        }
        elseif($decnum < 100){
            $rettxt .= $tens[substr($decnum,0,1)];
            $rettxt .= " ".$ones[substr($decnum,1,1)];
        }
        $rettxt = $rettxt." Cent";
    }

    $rettxt = $rettxt . " Only";
    return $rettxt;}