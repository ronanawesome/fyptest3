<?php

define('SERVER_STATE', 'testbed');//testbed , staging , production
define('SYS_NAME', 'SMSP');
define("PG_TESTING", FALSE);
define("CURRENCY", "RM");
define('BRANDING_FAVICO', '/resource/branding/favicon.png');
define('BRANDING_LOGIN_LOGO', '/resource/branding/logo-dark.png');
define('BRANDING_LOGIN_LOGO_STYLE', 'max-h-180px');
define('BRANDING_HOME_LOGO', '/resource/branding/logo-dark.png');
define('NEW_HOME_LOGO', '/resource/assets/SMSPlogoclearbig.png');
define('NEW_HOME_LOGO_BORDER', '/resource/assets/SMSPlogoclearbigwithborder.png');
define('SMALL_HOME_LOGO_BORDER', '/resource/assets/SMSPlogoclearsmallwithborder.png');
define('BRANDING_HOME_LOGO_HEADER_MOBILE_STYLE', 'max-h-25px');
define('BRANDING_HOME_LOGO_HEADER_STYLE', 'max-h-40px');
define('BRANDING_HOME_LOGIN_BG', '/template/_metronic/demo1/dist/assets/media/bg/bg-3.jpg');
define('BRANDING_LOGO_LOGIN_STYLE', 'max-h-150px');
define('BRANDING_LOGO_PORTAL_DESKTOP_STYLE', 'max-h-50px');
define('BRANDING_LOGO_PORTAL_MOBILE_STYLE', 'max-h-35px');
define('ADMIN_HEADER', 'template/admin/header.inc.php');
define('ADMIN_FOOTER', 'template/admin/footer.inc.php');
define('TEACHER_HEADER', 'template/teacher/header.inc.php');
define('TEACHER_FOOTER', 'template/teacher/footer.inc.php');
define('PYMT_TEST_ENV', false);
define('PYMT_CLIENT_ID', '1633600123932130334');
define('PYMT_CLIENT_SECRET', 'cFpfGActXXPzWHoTOrFQbJIurpNgELRv');
define('PYMT_CLIENT_P_KEY', 'MIIEpAIBAAKCAQEArwhpeLNjcI3pxu0wownfssMEwM1AhFfvlGylB5wxTbSkSy4YtvRAP3KcmjFm6IO+Xl69Wom8b33kYglYfbfhp5op5Jxx26YwPbxbJGHpwsP2DTWynxhf1WJyzTQdEg3dkGWHR4OHJGoGPaThyV/Rv7KuuOb3q2ao1oHuamOf/swCc9C00IxraqVxeJbaZkUzgiJb6CyAlnhiBHY5yAclu46WjRzcaRr7NEYT9b3o6nC0BcD45CqVoqpa/UFgfVObZFLeKKqQP++7vfc7VXwQJTSe/sslxuNQ+HQyAazFFcMuxg20gLpOqDbm0Xde/2iWyBqJk/ewAGOfLjHsGOTnsQIDAQABAoIBAQCcDUXrxRQ9P2lUYBhi4+S7fj/4nYaN99C0ZgDkvg7/FrT8CkciprW5W7ffiXgUiZDqllEALZkBVh6OgYKR46WeRkZfelPQ32RXRl2EHH/Lk+cFdACi85pNpw5vga0Bcbc+jCaGTe/pOVYDE8PyRCm/XdGFLPdXq5mQVht4YwY8pa6ldH0HxbfL8aUAW/PhUzi25KHUB6zI55lmyM7M3eHmGNhhAEwFkKrSQ9Q6Qn+dN9omkmL0IcRd+sLdDt+Mvy6+x2uRnKOzPWGJ4Sbmw/uNHfuYZkua1F9xjDgtPl/g5ubdnRb1NvtB+RxBN8XjiVTlGYZs7l9gjNixY8FkeF45AoGBAO9OzhP7HLPAYLtnoFnBqNcrJ+nGydhi/dVVLKuarO/aVH4DpotyTtBCffIrMFqUoIa57/uMAUeXrFYmR5z3uwd+H7VCOEA12U7kTrnNtmOc+/96WXb5Ve6ARd4l4GvBgy3SLSBcgKCrxl1hFnmq5MRO9/oaaX48vsMwrL/uXWFfAoGBALs94eOT1gyaSa8pSuKw71nwNFHdEL2P04has2jgSDnpIuYripsj54ybypMZrR6Yck5EnrG6vNkCriuSQqPLSCyUTbw6+ofXHpRs63mrr6/o3/xvPS+Zi9Ucds3u2sIvkPZtLa2xJWRgOgqHSuoQomJhPMw92io7rGPGXcW4qQDvAoGBAM9mriYVCXAkN/HTwt5ngAxTo7jxh3gZe0LMGfX0DROnVnkMcugCPpoyNnOOyM6IZ4O8XGfXlDhXFI5piMFa1uaMgbCXXqcPxHGiXzSbPAqjQKfPgwAg4j/dPjBlBVZ+lVE+Ccw1P2mA1l+RyGfUG4ajpjlFcM5umSoT4nRJgvbjAoGAHVVNfRSc9O562qkSZT8o0CM0XxrN05Sjo/npn2V7iy7926c4mKNgVAPYEYF1QJTLW32eiuNhUHH+DS9lpzlifjFhmDp5IyzNSp1hqnb+GAXYiTh4EmvwnxtNdWeU99Tx1dk2zb4xyG7WyO13DAI9HkJzft+1vOYKVLsv15jdSRsCgYAK/f2jcWi6Jtfi28JkEYKR+KjsE3boRmVonVafAUYIv+AE68DdWqe4y/liz9dzd0wzPHvVB6GtBDW9N4FrnlzLwtmhrmNku9Dib+EJrvUO6/7n8XpXXj4AVxW78xavESYZ57fR7hHVqzS7iUdJQK4hpIDVO8IL3rdna4IckgIQFQ==');
define('PYMT_STORE_ID', '1631514216482827390');
define('PYMT_CONFIG', [
    'client_id' => PYMT_CLIENT_ID,
    'client_secret' => PYMT_CLIENT_SECRET,
    'private_key' => PYMT_CLIENT_P_KEY,
    'store_id' => PYMT_STORE_ID,
]);



//define('AGENT_HEADER', 'template/agent-test/header.inc.php');
//define('AGENT_NAVBAR', 'template/agent-test/navbar.inc.php');
//define('AGENT_FOOTER', 'template/agent-test/footer.inc.php');

define('PARENT_HEADER', 'template/parent/header.inc.php');
define('PARENT_NAVBAR', 'template/parent/navbar.inc.php');
define('PARENT_FOOTER', 'template/parent/footer.inc.php');
define('NO_USER_HEADER', 'template/parent/no-user-header.php');
define('WHOLESALE_HEADER', 'template/wholesale/header.inc.php');
define('WHOLESALE_NAVBAR', 'template/wholesale/navbar.inc.php');
define('WHOLESALE_FOOTER', 'template/wholesale/footer.inc.php');

define('STUDENT_HEADER', 'template/student/header.inc.php');
define('STUDENT_NEW_HEADER', 'template/student/new-header.inc.php');
define('REGISTRATION_HEADER', 'template/student/registration-header.php');
define('STUDENT_NAVBAR', 'template/student/navbar.inc.php');
define('STUDENT_FOOTER', 'template/student/footer.inc.php');

define('STUDENT_BREADCRUMB', 'template/student/breadcrumb.inc.php');
