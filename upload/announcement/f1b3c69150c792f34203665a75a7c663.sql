USE [master]
GO
/****** Object:  Database [ADVBSG2]    Script Date: 7/7/2022 8:36:07 PM ******/
CREATE DATABASE [ADVBSG2]
GO
ALTER DATABASE [ADVBSG2] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ADVBSG2].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ADVBSG2] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ADVBSG2] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ADVBSG2] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ADVBSG2] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ADVBSG2] SET ARITHABORT OFF 
GO
ALTER DATABASE [ADVBSG2] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ADVBSG2] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ADVBSG2] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ADVBSG2] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ADVBSG2] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ADVBSG2] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ADVBSG2] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ADVBSG2] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ADVBSG2] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ADVBSG2] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ADVBSG2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ADVBSG2] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ADVBSG2] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ADVBSG2] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ADVBSG2] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ADVBSG2] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ADVBSG2] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ADVBSG2] SET RECOVERY FULL 
GO
ALTER DATABASE [ADVBSG2] SET  MULTI_USER 
GO
ALTER DATABASE [ADVBSG2] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ADVBSG2] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ADVBSG2] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ADVBSG2] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ADVBSG2] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ADVBSG2] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'ADVBSG2', N'ON'
GO
ALTER DATABASE [ADVBSG2] SET QUERY_STORE = OFF
GO
USE [ADVBSG2]
GO
/****** Object:  Table [dbo].[Airline]    Script Date: 7/7/2022 8:36:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Airline](
	[Id] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[MaximumBookingCapacity] [tinyint] NOT NULL,
	[WheelChairService] [tinyint] NOT NULL,
	[ChildTravelSolo] [tinyint] NOT NULL,
	[MandatoryUnaccompaniedService] [tinyint] NULL,
	[MandatoryUnaccompaniedServiceCharge] [int] NULL,
 CONSTRAINT [PK_Airline] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 7/7/2022 8:36:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [int] NOT NULL,
	[Name] [varchar](125) NOT NULL,
	[PassportNumber] [nvarchar](125) NOT NULL,
	[DateOfBirth] [date] NOT NULL,
	[CustomerTypeId] [int] NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerType]    Script Date: 7/7/2022 8:36:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerType](
	[Id] [int] NOT NULL,
	[Name] [varchar](125) NOT NULL,
 CONSTRAINT [PK_CustomerType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Destination]    Script Date: 7/7/2022 8:36:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Destination](
	[Id] [int] NOT NULL,
	[CityName] [varchar](125) NOT NULL,
	[AirportName] [varchar](125) NOT NULL,
 CONSTRAINT [PK_Destination] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FlightBooking]    Script Date: 7/7/2022 8:36:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FlightBooking](
	[Id] [int] NOT NULL,
	[FlightDestinationId] [int] NOT NULL,
	[PurchaseCustomerId] [int] NOT NULL,
	[FlightWayId] [int] NOT NULL,
	[LinkFlightDestinationId] [int] NULL,
	[Total] [decimal](16, 2) NOT NULL,
	[IsCancel] [tinyint] NOT NULL,
	[IsChange] [tinyint] NOT NULL,
	[ChangeFlightDestinationId] [int] NULL,
	[ChangeFlightChargesTotal] [decimal](16, 2) NULL,
	[PurchasedDateTime] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_FlightBooking] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FlightBookingDetail]    Script Date: 7/7/2022 8:36:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FlightBookingDetail](
	[Id] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[FlightBookingId] [int] NOT NULL,
	[FlightClassId] [int] NOT NULL,
	[MealChildCare] [tinyint] NULL,
	[SeatNo] [nvarchar](256) NOT NULL,
	[InLap] [tinyint] NULL,
	[FlightBookingDetailTotal] [decimal](16, 2) NOT NULL,
 CONSTRAINT [PK_FlightBookingDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FlightClass]    Script Date: 7/7/2022 8:36:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FlightClass](
	[Id] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[MaxCapacity] [int] NOT NULL,
	[AdditionalCharges] [int] NOT NULL,
 CONSTRAINT [PK_FlightClass] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FlightCreditPoints]    Script Date: 7/7/2022 8:36:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FlightCreditPoints](
	[Id] [int] NOT NULL,
	[FlightBookingId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CreditPoints] [decimal](16, 2) NOT NULL,
	[ValidTillDateTime] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_FlightCreditPoints] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FlightDestination]    Script Date: 7/7/2022 8:36:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FlightDestination](
	[Id] [int] NOT NULL,
	[AirlineId] [int] NOT NULL,
	[DestinationFromId] [int] NOT NULL,
	[DestinationToId] [int] NOT NULL,
	[TakeOffDateTime] [smalldatetime] NOT NULL,
	[Duration] [int] NOT NULL,
	[MealTypeId] [int] NOT NULL,
	[BasePrice] [decimal](16, 2) NOT NULL,
 CONSTRAINT [PK_FlightDestination] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FlightWay]    Script Date: 7/7/2022 8:36:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FlightWay](
	[Id] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_FlightWay] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MealType]    Script Date: 7/7/2022 8:36:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MealType](
	[Id] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Vegan] [tinyint] NOT NULL,
	[ChildCare] [tinyint] NOT NULL,
 CONSTRAINT [PK_MealType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Staff]    Script Date: 7/7/2022 8:36:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[Id] [int] NOT NULL,
	[Name] [varchar](256) NOT NULL,
 CONSTRAINT [PK_Staff] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_CustomerType] FOREIGN KEY([CustomerTypeId])
REFERENCES [dbo].[CustomerType] ([Id])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_CustomerType]
GO
ALTER TABLE [dbo].[FlightBooking]  WITH CHECK ADD  CONSTRAINT [FK_FlightBooking_Customer] FOREIGN KEY([PurchaseCustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[FlightBooking] CHECK CONSTRAINT [FK_FlightBooking_Customer]
GO
ALTER TABLE [dbo].[FlightBooking]  WITH CHECK ADD  CONSTRAINT [FK_FlightBooking_FlightDestination] FOREIGN KEY([FlightDestinationId])
REFERENCES [dbo].[FlightDestination] ([Id])
GO
ALTER TABLE [dbo].[FlightBooking] CHECK CONSTRAINT [FK_FlightBooking_FlightDestination]
GO
ALTER TABLE [dbo].[FlightBooking]  WITH CHECK ADD  CONSTRAINT [FK_FlightBooking_FlightDestination1] FOREIGN KEY([LinkFlightDestinationId])
REFERENCES [dbo].[FlightDestination] ([Id])
GO
ALTER TABLE [dbo].[FlightBooking] CHECK CONSTRAINT [FK_FlightBooking_FlightDestination1]
GO
ALTER TABLE [dbo].[FlightBooking]  WITH CHECK ADD  CONSTRAINT [FK_FlightBooking_FlightDestination2] FOREIGN KEY([LinkFlightDestinationId])
REFERENCES [dbo].[FlightDestination] ([Id])
GO
ALTER TABLE [dbo].[FlightBooking] CHECK CONSTRAINT [FK_FlightBooking_FlightDestination2]
GO
ALTER TABLE [dbo].[FlightBooking]  WITH CHECK ADD  CONSTRAINT [FK_FlightBooking_FlightWay] FOREIGN KEY([FlightWayId])
REFERENCES [dbo].[FlightWay] ([Id])
GO
ALTER TABLE [dbo].[FlightBooking] CHECK CONSTRAINT [FK_FlightBooking_FlightWay]
GO
ALTER TABLE [dbo].[FlightBookingDetail]  WITH CHECK ADD  CONSTRAINT [FK_FlightBookingDetail_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[FlightBookingDetail] CHECK CONSTRAINT [FK_FlightBookingDetail_Customer]
GO
ALTER TABLE [dbo].[FlightBookingDetail]  WITH CHECK ADD  CONSTRAINT [FK_FlightBookingDetail_FlightBooking] FOREIGN KEY([FlightBookingId])
REFERENCES [dbo].[FlightBooking] ([Id])
GO
ALTER TABLE [dbo].[FlightBookingDetail] CHECK CONSTRAINT [FK_FlightBookingDetail_FlightBooking]
GO
ALTER TABLE [dbo].[FlightBookingDetail]  WITH CHECK ADD  CONSTRAINT [FK_FlightBookingDetail_FlightClass] FOREIGN KEY([FlightClassId])
REFERENCES [dbo].[FlightClass] ([Id])
GO
ALTER TABLE [dbo].[FlightBookingDetail] CHECK CONSTRAINT [FK_FlightBookingDetail_FlightClass]
GO
ALTER TABLE [dbo].[FlightCreditPoints]  WITH CHECK ADD  CONSTRAINT [FK_FlightCreditPoints_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[FlightCreditPoints] CHECK CONSTRAINT [FK_FlightCreditPoints_Customer]
GO
ALTER TABLE [dbo].[FlightCreditPoints]  WITH CHECK ADD  CONSTRAINT [FK_FlightCreditPoints_FlightBooking] FOREIGN KEY([FlightBookingId])
REFERENCES [dbo].[FlightBooking] ([Id])
GO
ALTER TABLE [dbo].[FlightCreditPoints] CHECK CONSTRAINT [FK_FlightCreditPoints_FlightBooking]
GO
ALTER TABLE [dbo].[FlightDestination]  WITH CHECK ADD  CONSTRAINT [FK_FlightDestination_Airline] FOREIGN KEY([AirlineId])
REFERENCES [dbo].[Airline] ([Id])
GO
ALTER TABLE [dbo].[FlightDestination] CHECK CONSTRAINT [FK_FlightDestination_Airline]
GO
ALTER TABLE [dbo].[FlightDestination]  WITH CHECK ADD  CONSTRAINT [FK_FlightDestination_MealType] FOREIGN KEY([MealTypeId])
REFERENCES [dbo].[MealType] ([Id])
GO
ALTER TABLE [dbo].[FlightDestination] CHECK CONSTRAINT [FK_FlightDestination_MealType]
GO
ALTER TABLE [dbo].[FlightDestination]  WITH CHECK ADD  CONSTRAINT [FK_FlightDestinationFrom_Destination] FOREIGN KEY([DestinationFromId])
REFERENCES [dbo].[Destination] ([Id])
GO
ALTER TABLE [dbo].[FlightDestination] CHECK CONSTRAINT [FK_FlightDestinationFrom_Destination]
GO
ALTER TABLE [dbo].[FlightDestination]  WITH CHECK ADD  CONSTRAINT [FK_FlightDestinationTo_Destination] FOREIGN KEY([DestinationToId])
REFERENCES [dbo].[Destination] ([Id])
GO
ALTER TABLE [dbo].[FlightDestination] CHECK CONSTRAINT [FK_FlightDestinationTo_Destination]
GO
USE [master]
GO
ALTER DATABASE [ADVBSG2] SET  READ_WRITE 
GO
