-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 21, 2022 at 05:31 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fyp3rd`
--

-- --------------------------------------------------------

--
-- Table structure for table `Absence`
--

DROP TABLE IF EXISTS `Absence`;
CREATE TABLE IF NOT EXISTS `Absence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Attendance_Id` int(11) NOT NULL,
  `Reason` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Announcement`
--

DROP TABLE IF EXISTS `Announcement`;
CREATE TABLE IF NOT EXISTS `Announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Creator_Id` int(11) NOT NULL,
  `Title` longtext NOT NULL,
  `Description` longtext NOT NULL,
  `Created_Datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `File` varchar(256) DEFAULT NULL,
  `Year_Target` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Announcement`
--

INSERT INTO `Announcement` (`id`, `Creator_Id`, `Title`, `Description`, `Created_Datetime`, `File`, `Year_Target`) VALUES
(15, 1, 'Year 6 Exams', 'asefsdafasdsadf', '2022-07-11 08:59:53', '', '6'),
(18, 1, 'Extra Classes for Year 5 Exam Revision', 'Exam Revision classes have been scheduled for year 5 students to better prepare for their upcoming and future exams in the next year. The is attached schedule can be downloaded. ', '2022-07-17 04:57:44', 'upload\\announcement\\f7b7ba45abe0321c3b3f8abe672a7e54.pdf', '5'),
(17, 1, 'Year 5 Exam Timetables', 'Year 5 Exam Timetables have been prepared and can be downloaded. Please contact the admin for any enquiries. ', '2022-07-17 04:24:09', 'upload\\announcement\\9187ae749bfa841a38d0c833b8798657.mp4', '5'),
(21, 1, 'test', 'tset', '2022-07-18 02:22:53', 'upload\\announcement\\f6baba1ce5ff7abeb8455853504c3796.pdf', '4'),
(22, 1, 'Exam ScheDule year 4', 'etstsetse', '2022-07-18 02:23:35', 'upload\\announcement\\4e39c30ec3b9b2850a5f641909e0313c.docx', '4'),
(23, 1, 'Year 4 Exam Question Sample', 'testsetse', '2022-07-18 03:27:58', 'upload\\announcement\\0d0d7142c53797f4f22cda0269e66b0f.pdf', '5,4'),
(24, 1, 'ttesting title', 'title', '2022-07-21 00:15:30', 'upload\\announcement\\85a790ad18658d8403b592f97dbc662b.pdf', '2'),
(25, 1, 'Year 6 Extra class', 'w et sdffgsdgfdsgdsfdgf', '2022-07-21 00:46:17', 'upload\\announcement\\91e4f1e111bbeda6c567f921ad0fc8fc.pdf', '6'),
(26, 1, 'Extra Class on Saturday', 'as sfasfsadfdasdsd', '2022-07-21 01:04:30', 'upload\\announcement\\87b5239734bd3f0a26c29932936e858b.pdf', '6'),
(14, 1, 'test year 1', 'tset', '2022-07-11 08:37:06', '', '1'),
(20, 1, 'Year 4 exam Scedule', 'testse', '2022-07-18 02:22:33', '', '4');

-- --------------------------------------------------------

--
-- Table structure for table `Attendance`
--

DROP TABLE IF EXISTS `Attendance`;
CREATE TABLE IF NOT EXISTS `Attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  `Student_ids` mediumtext NOT NULL,
  `Class_Id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Attendance`
--

INSERT INTO `Attendance` (`id`, `Date`, `Student_ids`, `Class_Id`) VALUES
(1, '2022-07-13', '29,28,27,26', 28),
(2, '2022-07-13', '27,26', 28),
(3, '2022-07-11', '27,26', 28),
(4, '2022-07-11', '29,28,27,26', 28),
(5, '2022-07-12', '4', 3),
(6, '2022-07-20', '20,3', 4),
(7, '2022-07-27', '20', 4),
(8, '2022-07-05', '3', 4),
(9, '2022-07-12', '18,7,6', 6),
(10, '2022-07-22', '44,18,7,6', 6),
(11, '2022-07-23', '44', 6);

-- --------------------------------------------------------

--
-- Table structure for table `ClassRoom`
--

DROP TABLE IF EXISTS `ClassRoom`;
CREATE TABLE IF NOT EXISTS `ClassRoom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Class_Name` varchar(11) NOT NULL,
  `Capacity` int(11) DEFAULT NULL,
  `Class_Level` int(11) NOT NULL,
  `CTeacher_Id` int(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ClassRoom`
--

INSERT INTO `ClassRoom` (`id`, `Class_Name`, `Capacity`, `Class_Level`, `CTeacher_Id`) VALUES
(1, '1 Mercury', 0, 1, 42),
(2, '2 Mercury', 0, 2, NULL),
(3, '3 Mercury', 0, 3, 34),
(4, '4 Mercury', 0, 4, 30),
(5, '5 Mercury', 0, 5, NULL),
(6, '6 Mercury', 0, 6, 46),
(7, '1 Mars', 0, 1, NULL),
(8, '2 Mars', 0, 2, NULL),
(9, '3 Mars', 0, 3, NULL),
(10, '4 Mars', 0, 4, NULL),
(11, '5 Mars', 0, 5, NULL),
(12, '6 Mars', 0, 6, NULL),
(13, '1 Earth', 0, 1, NULL),
(14, '2 Earth', 0, 2, NULL),
(15, '3 Earth', 0, 3, NULL),
(16, '4 Earth', 0, 4, NULL),
(17, '5 Earth', 0, 5, NULL),
(18, '6 Earth', 0, 6, NULL),
(22, '5 Saturn', 0, 5, NULL),
(23, '2 Saturn', 0, 2, NULL),
(24, '1 Uranus', 0, 1, NULL),
(25, '2 Uranus', 0, 2, NULL),
(26, '3 Uranus', 0, 3, NULL),
(28, '4 Uranus', 0, 4, 16),
(29, '6 Uranus', 0, 6, 25),
(30, '6 Pluto', 0, 6, 15),
(31, '4 Pluto', 0, 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ComplaintReport`
--

DROP TABLE IF EXISTS `ComplaintReport`;
CREATE TABLE IF NOT EXISTS `ComplaintReport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Creator_Id` int(45) NOT NULL,
  `User_Ids` mediumtext,
  `Type` varchar(50) NOT NULL,
  `Content` longtext NOT NULL,
  `Title` varchar(150) NOT NULL,
  `CreatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ComplaintReport`
--

INSERT INTO `ComplaintReport` (`id`, `Creator_Id`, `User_Ids`, `Type`, `Content`, `Title`, `CreatedDate`, `is_delete`) VALUES
(15, 1, '6,3', 'Bullying', 'student5 has often been reported by various students to constantly tease and harass student3. Most recent student 5 broken all of student3&#39;s bag and equipment which were given to him by his late grandparents', 'Bullying of Student3', '2022-07-12 00:00:00', 0),
(16, 1, '3', 'Cheating', 'Student3 was caught cheating by bringing a note which had the answers to the exam', 'Cheating during Mid-semester math exam', '2022-07-12 07:31:24', 0),
(17, 1, '4', 'Vandalism', 'student4 was caught vandalizing the school with doors, and the pump malfunctioning', 'Vandalizing school toilet', '2022-07-12 07:32:47', 0),
(18, 1, '17', 'Skipping', 'student10 was caught skipping class and playing soccer outside of his scheduled period', 'Student10 skipping class for soccer', '2022-07-12 07:33:55', 0),
(19, 1, '9,7,6,4,3', 'Cheating', 'Drugs were found in the school sports equipment store which was brought by student8 and distributing to other students as well as started to spread it around the basketball court', 'Students caught doing drugs in the school sports equipment store', '2022-07-12 00:00:00', 0),
(27, 1, '3', 'Cheating', 'test', 'test renpui', '2022-07-17 18:58:39', 0),
(28, 1, '27', 'Bullying', 'Bullied kid', 'Adam Bully', '2022-07-18 10:18:05', 0),
(29, 1, '28', 'Cheating', 'Cheated in math class', 'Rey Cheated ', '2022-07-18 10:21:01', 0),
(30, 1, '29', 'Bullying', 'Brought cheat sheet', 'Math Exam Cheating', '2022-07-18 10:21:35', 0),
(31, 1, '29,28', 'Skipping', 'Sky sibling cheat', 'Sky sibling cheat', '2022-07-18 10:22:04', 0),
(32, 1, '28', 'Cheating', 'trestets', 'Cheat exam science paper', '2022-07-18 11:29:51', 0),
(33, 1, '29,28', 'Vandalism', 'test', 'Vandalizying property', '2022-07-18 11:31:58', 0),
(34, 1, '35', 'Vandalism', 'testsetset', 'broke door', '2022-07-21 08:17:31', 0),
(35, 1, '40', 'Vandalism', 'broke keyboard and everything', 'Breaking a computer', '2022-07-21 08:48:03', 0),
(36, 1, '44', 'Vandalism', 'detailsfsdf', 'break phone', '2022-07-21 09:05:25', 0),
(37, 1, '18', 'Disruptive Behaviour', 'tewtwetwe', 'acting weird', '2022-07-21 09:13:35', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Exam`
--

DROP TABLE IF EXISTS `Exam`;
CREATE TABLE IF NOT EXISTS `Exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Exam_Name` varchar(50) NOT NULL,
  `Subject_Id` int(11) NOT NULL,
  `Exam_Date` datetime NOT NULL,
  `Intake_Id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Exam`
--

INSERT INTO `Exam` (`id`, `Exam_Name`, `Subject_Id`, `Exam_Date`, `Intake_Id`) VALUES
(74, '1st Semester-I3S3', 3, '2022-07-05 09:06:00', '3'),
(73, '5th Semester-I3S3', 3, '2022-07-20 08:49:00', '3'),
(72, '2nd Semester-I3S22', 22, '2022-07-18 08:18:00', '3'),
(70, '5th Semester-I3S16', 16, '2022-07-20 11:23:00', '3'),
(69, '4th Semester-I3S39', 39, '2022-07-12 10:29:00', '3'),
(67, '1st Semester-I3S5', 5, '2022-07-17 00:00:00', '3'),
(66, '1st Semester-I1S1', 1, '2022-07-04 09:02:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `ExamResult`
--

DROP TABLE IF EXISTS `ExamResult`;
CREATE TABLE IF NOT EXISTS `ExamResult` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Exam_ID` int(11) NOT NULL,
  `User_ID` int(11) NOT NULL,
  `Exam_Grade` varchar(2) DEFAULT NULL,
  `Exam_Marks` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ExamResult`
--

INSERT INTO `ExamResult` (`id`, `Exam_ID`, `User_ID`, `Exam_Grade`, `Exam_Marks`) VALUES
(63, 71, 29, '-', 0),
(62, 71, 28, '-', 0),
(61, 71, 27, '-', 0),
(60, 71, 2, '-', 0),
(59, 71, 8, '-', 0),
(58, 71, 18, '-', 0),
(57, 70, 29, '-', 0),
(56, 70, 28, '-', 0),
(55, 70, 27, 'B', 70),
(54, 70, 2, '-', 0),
(53, 70, 8, '-', 0),
(52, 70, 18, 'B', 66),
(51, 69, 29, 'B', 67),
(50, 69, 28, '-', 0),
(49, 69, 27, 'A', 90),
(48, 69, 2, '-', 0),
(47, 69, 8, '-', 0),
(46, 69, 18, '-', 0),
(45, 67, 2, '-', 0),
(44, 67, 8, '-', 0),
(43, 67, 18, '-', 0),
(42, 67, 17, '-', 0),
(41, 66, 14, '-', 0),
(40, 66, 7, '-', 0),
(39, 66, 3, '75', 57567),
(64, 72, 18, '-', 0),
(65, 72, 8, '-', 0),
(66, 72, 2, '-', 0),
(67, 72, 27, '-', 0),
(68, 72, 28, '-', 0),
(69, 72, 29, '-', 0),
(70, 72, 35, 'B', 75),
(71, 73, 18, '-', 0),
(72, 73, 8, '-', 0),
(73, 73, 2, '-', 0),
(74, 73, 27, '-', 0),
(75, 73, 28, '-', 0),
(76, 73, 29, '-', 0),
(77, 73, 35, '-', 0),
(78, 73, 38, '-', 0),
(79, 73, 40, 'B', 67),
(80, 74, 18, '-', 0),
(81, 74, 8, '-', 0),
(82, 74, 2, '-', 0),
(83, 74, 27, '-', 0),
(84, 74, 28, '-', 0),
(85, 74, 29, '-', 0),
(86, 74, 35, '-', 0),
(87, 74, 38, '-', 0),
(88, 74, 40, '-', 0),
(89, 74, 44, 'C', 55);

-- --------------------------------------------------------

--
-- Table structure for table `Inbox`
--

DROP TABLE IF EXISTS `Inbox`;
CREATE TABLE IF NOT EXISTS `Inbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Announcement_Id` int(11) NOT NULL,
  `Checked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Intakes`
--

DROP TABLE IF EXISTS `Intakes`;
CREATE TABLE IF NOT EXISTS `Intakes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Intake_Code` varchar(50) NOT NULL,
  `Year` year(4) NOT NULL,
  `No_of_Students` int(5) NOT NULL DEFAULT '0',
  `Active_Status` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7236 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Intakes`
--

INSERT INTO `Intakes` (`id`, `Intake_Code`, `Year`, `No_of_Students`, `Active_Status`, `is_delete`) VALUES
(1, 'JAN16', 2016, 0, 1, 0),
(2, 'JAN17', 2017, 0, 1, 0),
(3, 'JAN18', 2018, 0, 1, 0),
(21, 'JAN19', 2019, 0, 1, 0),
(18, 'JAN21', 2021, 0, 1, 0),
(72, 'JAN20', 2020, 0, 1, 0),
(9, 'JAN22', 2022, 0, 1, 0),
(73, 'test22', 2021, 0, 0, 1),
(74, 'rresrser', 2013, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ParentChild`
--

DROP TABLE IF EXISTS `ParentChild`;
CREATE TABLE IF NOT EXISTS `ParentChild` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `User_Id` int(11) NOT NULL,
  `Child_Id` int(11) NOT NULL,
  `Relationship` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ParentChild`
--

INSERT INTO `ParentChild` (`id`, `User_Id`, `Child_Id`, `Relationship`) VALUES
(1, 12, 3, 'Mother'),
(2, 13, 7, 'Mother'),
(3, 23, 7, 'Mother'),
(4, 19, 6, 'Father'),
(5, 24, 18, 'Mother'),
(6, 10, 3, 'Mother'),
(7, 10, 2, 'Mother'),
(9, 31, 28, 'Father'),
(10, 31, 29, 'Father'),
(11, 36, 35, 'Mother'),
(12, 39, 38, 'Mother'),
(13, 41, 40, 'Father');

-- --------------------------------------------------------

--
-- Table structure for table `Student`
--

DROP TABLE IF EXISTS `Student`;
CREATE TABLE IF NOT EXISTS `Student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `User_Id` int(50) NOT NULL,
  `Class_Id` int(45) DEFAULT NULL,
  `Intake_Id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Student`
--

INSERT INTO `Student` (`id`, `User_Id`, `Class_Id`, `Intake_Id`) VALUES
(13, 3, 4, 21),
(14, 7, 6, 2),
(15, 14, 2, 1),
(16, 6, 6, 7),
(17, 17, 1, 2),
(18, 18, 6, 3),
(19, 9, 10, 83),
(20, 4, 3, 9),
(21, 8, 9, 3),
(22, 20, 4, 9),
(23, 2, 5, 3),
(24, 26, 28, 18),
(25, 27, 28, 3),
(26, 28, 10, 3),
(27, 29, 28, 3),
(28, 35, 14, 3),
(29, 38, 14, 3),
(30, 40, 12, 2),
(31, 44, 6, 3);

-- --------------------------------------------------------

--
-- Table structure for table `Subject`
--

DROP TABLE IF EXISTS `Subject`;
CREATE TABLE IF NOT EXISTS `Subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Subject_Name` varchar(50) NOT NULL,
  `Subject_Level` int(11) NOT NULL,
  `Subject_Code` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Subject`
--

INSERT INTO `Subject` (`id`, `Subject_Name`, `Subject_Level`, `Subject_Code`) VALUES
(3, 'Science 2', 2, 'SC-Y2'),
(40, 'Chemistry 4', 4, 'CHEM Y4'),
(4, 'English 5', 5, 'ENG-Y5'),
(5, 'Science 5', 5, 'SC-Y5'),
(39, 'Science 4', 4, 'SC-Y4'),
(7, 'Mathematics 5', 5, 'MATH-Y5'),
(8, 'Mathematics 4', 4, 'MATH-Y4'),
(9, 'Bahasa Malaysia 3', 3, 'BM-Y3'),
(10, 'Science 3', 3, 'SC-Y3'),
(11, 'English 1', 1, 'ENG-Y1'),
(12, 'Science 1', 1, 'SC-Y1'),
(13, 'Science 6', 6, 'SC-Y6'),
(14, 'English 2', 2, 'ENG-Y2'),
(15, 'English 3', 3, 'ENG-Y3'),
(16, 'English 4', 4, 'ENG-Y4'),
(17, 'English 6', 6, 'ENG-Y6'),
(18, 'Bahasa Malaysia 1', 1, 'BM-Y1'),
(19, 'Bahasa Malaysia 2', 2, 'BM-Y2'),
(20, 'Bahasa Malaysia 4', 4, 'BM-Y4'),
(21, 'Bahasa Malaysia 6', 6, 'BM-Y6'),
(22, 'Mathematics 2', 2, 'MATH-Y2'),
(23, 'Mathematics 6', 6, 'MATH-Y6'),
(24, 'Bahasa Malaysia 5', 5, 'BM-Y5'),
(25, 'Mathematics 3', 3, 'MATH-Y3'),
(26, 'Pendidikan Moral 1', 1, 'PM-Y1'),
(27, 'Pendidikan Moral 2', 2, 'PM-Y2'),
(28, 'Pendidikan Moral 3', 3, 'PM-Y3'),
(29, 'Pendidikan Moral 4', 4, 'PM-Y4'),
(30, 'Pendidikan Moral 5', 5, 'PM-Y5'),
(31, 'Pendidikan Moral 6', 6, 'PM-Y6'),
(32, 'Pendidikan Islam 1', 1, 'PI-Y1'),
(33, 'Pendidikan Islam 2', 2, 'PI-Y2'),
(34, 'Pendidikan Islam 3', 3, 'PI-Y3'),
(35, 'Pendidikan Islam 4', 4, 'PI-Y4'),
(36, 'Pendidikan Islam 5', 5, 'PI-Y5'),
(37, 'Pendidikan Islam 6', 6, 'PI-Y6'),
(38, 'Advance Maths 2', 2, 'AMATH-2');

-- --------------------------------------------------------

--
-- Table structure for table `Subject_List`
--

DROP TABLE IF EXISTS `Subject_List`;
CREATE TABLE IF NOT EXISTS `Subject_List` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Student_id` int(11) NOT NULL,
  `Subject_ids` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Teacher`
--

DROP TABLE IF EXISTS `Teacher`;
CREATE TABLE IF NOT EXISTS `Teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `User_Id` int(11) NOT NULL,
  `Subjects_Taught` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Teacher`
--

INSERT INTO `Teacher` (`id`, `User_Id`, `Subjects_Taught`) VALUES
(1, 16, NULL),
(2, 21, '8,5,1'),
(3, 25, '15,14,12,11'),
(4, 30, '16,9,8,6,5,3,1'),
(5, 34, '39,23,13,10,8,7,5'),
(6, 15, '15,12,9,5'),
(7, 37, '25,23,22,8,7'),
(8, 42, '24,21,20,19,18'),
(9, 46, '13,10,7');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `Username` varchar(60) NOT NULL,
  `Fname` varchar(30) NOT NULL,
  `Lname` varchar(30) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(256) NOT NULL,
  `Birthday` date NOT NULL,
  `Gender` varchar(6) NOT NULL,
  `PhoneNo` varchar(12) NOT NULL,
  `City` varchar(15) NOT NULL,
  `PostCode` int(7) NOT NULL,
  `Street` varchar(50) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Role` varchar(10) NOT NULL,
  `RegistrationApproval` varchar(10) NOT NULL DEFAULT 'pending',
  `Religion` int(2) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `Username`, `Fname`, `Lname`, `Email`, `Password`, `Birthday`, `Gender`, `PhoneNo`, `City`, `PostCode`, `Street`, `CreatedDate`, `Role`, `RegistrationApproval`, `Religion`, `is_delete`) VALUES
(1, 'admin1', 'admin', '1', 'admin1@gmail.com', 'admin1', '1998-02-02', 'Male', '0123456', 'Kuala Lumpur', 88300, 'Bukit Jalil', '2022-07-17 07:24:21', 'admin', 'approved', 2, 0),
(2, 'student1', 'student', '1', 'student1@gmail.com', 'student1', '2014-07-10', 'Female', '012345678', 'Kuala Lumpur', 55700, 'Sri Kembangan', '2022-07-01 15:01:38', 'Student', 'approved', 1, 0),
(3, 'student3', 'student', '3', 'student3@gmail.com', 'AQAAAAEAACcQAAAAEIQJG19vINEuPB9Yoeer3UFFouJCq4XojjcEO96GwdX3ez5/TeUUJoC4Sjusfs1Jvg==', '2022-07-05', 'Male', '012345678', 'Kuala Lumpur', 88744, 'Sri Kembanngan', '2022-07-02 06:02:38', 'Student', 'approved', 5, 0),
(4, 'student4', 'student', '4', 'student4@gmail.com', 'AQAAAAEAACcQAAAAECP4vK30UXjbKygejgBh687i8Cb4UCjCQ+o5A77d3lLraTbi0hxSllnjlOMrBN0+lg==', '2022-07-12', 'Female', '012345678', 'Kuala Lumpur', 88744, 'Sri Kembanngan', '2022-07-02 06:03:59', 'Student', 'approved', 4, 0),
(6, 'student5', 'student', '5', 'student5@gmail.com', 'AQAAAAEAACcQAAAAEA2a6Jj94TKN3K/OXom98N4Hn+Cdybpp35wYrDCvjROTLklYA7NZqorK8J+bEjqAGA==', '2022-06-05', 'Female', '0121345687', 'Kuala Lumpur', 98111, 'Ampang', '2022-07-02 07:28:34', 'Student', 'approved', 2, 0),
(7, 'student6', 'student', '6', 'student6@gmail.com', 'AQAAAAEAACcQAAAAEKXYFq86M46mxIiqDFKJM2Hgyc3Dkwx1nAt+TRNbC5q5bAmsDv2HWuK6SMx016I9lA==', '2018-05-29', 'Female', '0121345687', 'Kuala Lumpur', 98222, 'Jalan Bangsar', '2022-07-02 07:50:10', 'Student', 'approved', 1, 0),
(8, 'student7', 'student', '7', 'student7@gmail.com', 'AQAAAAEAACcQAAAAEPQUZWgwKUTo1KaCw5rZ6jvWuSHlwp47d0k4+zc6khFSkLTnXo2CsTjIYBHLRMfVeg==', '2022-06-05', 'Female', '0123546879', 'Kuala Lumpur', 98877, 'Jalan Bukit Bintang', '2022-07-02 07:57:12', 'Student', 'approved', 2, 0),
(9, 'student8', 'student', '8', 'student8@gmail.com', 'AQAAAAEAACcQAAAAEM9usrUnEUJ2yBh2lCdiin6QzeBAU7e4frflt7wUfuq+b88L1ynVvXXf3yizpB1cGQ==', '2015-02-02', 'Male', '0123546879', 'Kuala Lumpur', 98877, 'Jalan Chow Kit', '2022-07-02 07:58:40', 'Student', 'approved', 1, 0),
(10, 'parent2', 'parent', '2', 'parent2@gmail.com', 'AQAAAAEAACcQAAAAEEBkDXaM9QdDkWn/NLUxNZpx7KY6Fa1VJUHuwB9NzL2k1toEu1I5Vrq7bo3hYoEC+Q==', '2022-07-19', 'Female', '015445778', 'Kuala Lumpur', 58877, 'Jalan Damansara', '2022-07-02 08:22:46', 'Parent', 'approved', 1, 0),
(11, 'parent1', 'parent', '1', 'parent1@gmail.com', 'AQAAAAEAACcQAAAAEKptHYtGfSX5446kVThg1oe6PW2wnb4he4ehi/sixD817hrsL2u+zX0FeDr3ocFXmg==', '1998-02-02', 'Female', '012345644', 'Kuala Lumpur', 98554, 'Jalan Damansara', '2022-07-02 08:30:08', 'Parent', 'approved', 4, 0),
(12, 'parent3', 'parent', '3', 'parent3@gmail.com', 'AQAAAAEAACcQAAAAEBlEXTaOItOAeNEJuIIUrR6+ZhBDYNDT7fowQVvFyGemaMLOT+1/Yz9mR/7dwJjgcg==', '1998-12-04', 'Female', '0123545554', 'Kuala Lumpur', 54488, 'Jalan Ipoh', '2022-07-02 08:48:37', 'Parent', 'approved', 2, 0),
(13, 'parent4', 'parent', '4', 'parent4@gmail.com', 'AQAAAAEAACcQAAAAEIBlrIDPdznJirIpXuN1XE2JWqsik5smv88XgUxPDqlV1d2q/WzOnr0K5l6ixTsdUw==', '1998-07-12', 'Female', '012345884', 'Kuala Lumpur', 99887, 'Jalan Kinabalu', '2022-07-02 08:50:38', 'Parent', 'approved', 2, 0),
(14, 'student9', 'student', '9', 'student9@gmail.com', 'AQAAAAEAACcQAAAAEFlrWp8SfuCBi9zjDCyhssr51vs4iNYPWx5e2Tr08t1w0cT5PGRMVjK1VOq63KbwIw==', '2011-02-11', 'Female', '01234567', 'Kajang', 43000, 'Jalan Impian Makmur', '2022-07-02 13:26:46', 'Student', 'approved', 5, 0),
(15, 'teacher1', 'teacher', '1', 'teacher1@gmail.com', 'AQAAAAEAACcQAAAAEO/ue1/q3in2ImBU05YpcBeLo2muy6PImj8DNqX2VoSzqFN+Mf4lBXKkq0AR8EEbAw==', '2022-07-06', 'Female', '012345677', 'Kuala Lumpur', 88774, 'Sri Kembangan', '2022-07-02 15:51:20', 'Teacher', 'approved', 1, 0),
(16, 'teacher2', 'teacher', '2', 'teacher2@gmail.com', 'AQAAAAEAACcQAAAAEExBjlo0au2ikdvfY5OVtkIUTG6ChP97iK3U9PbPt519WhPHY20y6ZGd4hLLwrJ1Fw==', '1973-06-03', 'Male', '0123456878', 'Kuala Lumpur', 87744, 'Sri Petaling', '2022-07-03 00:45:51', 'Teacher', 'approved', 1, 0),
(17, 'student10', 'student', '10', 'student10@gmail.com', 'AQAAAAEAACcQAAAAEO5Nh7eU+T+KFCTbXGa6aXX82SIqj/U/lSp7xetQsoJC7ZDrXkrRN/O5Y7New2kMwA==', '2019-06-03', 'Male', '012334522', 'Kuala Lumpur', 87445, 'Sri Petaling', '2022-07-03 00:47:46', 'Student', 'rejected', 5, 0),
(18, 'RonanIrvine', 'Ronan', 'Irvine', 'ronanirvine@gmail.com', 'AQAAAAEAACcQAAAAEIYKluJQQzcnICQ+0or+AyXATlg2zGdLRByaUsFrEBrCUWzFYGfUgPDVVx4BLAEU2A==', '2008-06-27', 'Male', '0133662883', 'Kuala Lumpur', 88774, 'Bukit Jalliil', '2022-07-03 00:54:56', 'Student', 'approved', 1, 0),
(19, 'parent5', 'parent', '5', 'parent5@gmail.com', 'AQAAAAEAACcQAAAAEIBlLU/cDLM9ys1H9VbBJtr7HnXyzx2KesQuMnoavPsg0aXysqxlPRPODqyAS8z8wg==', '1995-02-03', 'Male', '012345677', 'Kuala Lumpur', 98445, 'Bukit Jalil', '2022-07-03 01:00:43', 'Parent', 'pending', 2, 0),
(20, 'student12', 'student', '12', 'student12', 'AQAAAAEAACcQAAAAENQAa+YetqLcI2x+gV1gqDc85EJ+uq08VwIN80YwMtqWf7sUaFLKsxj4HWKY7/j7Ww==', '2013-11-03', 'Male', '01233456634', 'Kota Kinabalu', 88300, 'Jalan Cantek', '2022-07-03 06:21:56', 'Student', 'approved', 1, 0),
(21, 'teacher3', 'teacher', '3', 'teacher3@gmail.com', 'AQAAAAEAACcQAAAAEFkVNiU4PU6I2JW1JLbIIFQOONXgdlvQbk4rfr+/M2g7CNfYAG431aVoKf3VR9ATUQ==', '2000-02-03', 'Female', '012345687', 'Kuala Lumpur', 95221, 'Jalan Genting Klang', '2022-07-03 06:35:12', 'Teacher', 'approved', 3, 0),
(22, 'teacher4', 'teacher', '4', 'teacher4@gmail.com', 'AQAAAAEAACcQAAAAEMFQ+yI4GRJ6UN/Mw+6ErsVXyCRz1WrsKGVnbbSol3LreRmp+Q93INavZSfN4oywaQ==', '2000-06-03', 'Male', '012345678', 'Kuala Lumpur', 98000, 'Jalan Genting Klang', '2022-07-03 06:48:48', 'Teacher', 'approved', 4, 0),
(23, 'parent6', 'parent', '6', 'parent6@gmail.com', 'AQAAAAEAACcQAAAAEOH34+RRhLIoJrOvKPEPHfy7/6NCvgp9TOSMXTazG+shrmjM8lTDzHcEDfAYdnztuQ==', '2022-07-18', 'Female', '01234564564', 'Kuala Lumpur', 33441, 'Sri Petaling', '2022-07-04 17:14:09', 'Parent', 'rejected', 4, 0),
(24, 'ParentTest', 'Parent', 'Test', 'parenttest@gmail.com', 'AQAAAAEAACcQAAAAEHGHQ5BhpgnAVJrAOORvD7YNSAtGeTkO5fGkniwx8AW6eR0Vc/QVGhMh93oak5qHPg==', '2019-06-05', 'Female', '1234565645', 'Kuala Lumpur', 88766, 'Sri Petaling', '2022-07-04 17:19:06', 'Parent', 'pending', 1, 0),
(25, 'PeterParker', 'Peter', 'Parker', 'teachertest@gmail.com', 'AQAAAAEAACcQAAAAEGOFxWZPwGJrguf8McypDrfxVEPJa6yjTBsOI6DkZrkZLKSjEVLp/H+9VCpcKuCfQw==', '2000-06-07', 'Male', '0123546878', 'Bukit Jalil', 88550, 'Jalan Persekutuan', '2022-07-06 02:13:26', 'Teacher', 'approved', 1, 0),
(26, 'KentWong', 'Kent', 'Wong', 'kentwong@gmail.com', 'AQAAAAEAACcQAAAAELvzqzlL3J9vfKorDLlK4jCZhLo6hmAQE1n++5a2As6loWhOXKxQJ5g75nxBsOaGIA==', '2013-06-11', 'Male', '0123456', 'Kuala Lumpur', 123333, 'Sri Kembangan', '2022-07-12 16:24:26', 'Student', 'approved', 2, 0),
(27, 'AdamDriver', 'Adam', 'Driver', 'adamdriver@gmail.com', 'AQAAAAEAACcQAAAAENpMPF8QOkfLq2GmyiY3P8vnEYRRFDK5pI9vcVuAIDHACXa8ykbgX4RYHNu88tnugg==', '2010-06-05', 'Male', '01234567', 'Test', 4648989, 'Test', '2022-07-12 16:26:28', 'Student', 'approved', 1, 0),
(28, 'ReySky', 'Rey', 'Sky', 'rey@gmail.com', 'AQAAAAEAACcQAAAAEBtgGthNaKOkWJwjZdmWy0Gy5r03gIWK1WvVUWRZpoKE9MiC7HJI+0oN965/RRGJ4Q==', '2022-07-05', 'Female', '0123456', 'tset', 1231234, 'test', '2022-07-12 16:29:11', 'Student', 'approved', 1, 0),
(29, 'LukeSky', 'Luke', 'Sky', 'luke@gmail.com', 'AQAAAAEAACcQAAAAEEQrPiqdqdN0i34MhC3AGfYGCsI30xaY+zCXXImlm5DNzrfzNxXgrfyTB/sOdv2+lA==', '2007-07-26', 'Male', '234234523', 'tset', 24244, 'test', '2022-07-12 16:32:30', 'Student', 'approved', 1, 0),
(30, 'EddyBrock', 'Eddy', 'Brock', 'eddy@gmail.com', 'AQAAAAEAACcQAAAAEPegnAUpi+Oq0rCbyykJF3dIoH3agQBpHzcqubVmVdq6sd8gM5meS8pHFOs9Ft+iIg==', '2022-07-11', 'Male', '21312', 'test', 894332, 'test', '2022-07-12 17:10:49', 'Teacher', 'pending', 3, 0),
(31, 'AnakinSky', 'Anakin', 'Sky', 'anakin@gmail.com', 'AQAAAAEAACcQAAAAENOJrSb3eGxvt98OqM5D/umcpC+cV8Qd4mVLTop7vZciv1RZhrNKrCsIGw+9hlUH7g==', '1997-06-13', 'Male', '01234564', 'Kuala Lumpur', 88500, 'Bespin', '2022-07-13 07:34:33', 'Parent', 'pending', 1, 0),
(32, 'BobMarley', 'Bob', 'Marley', 'test@gmail.com', 'AQAAAAEAACcQAAAAEFhO6uvlOlentIvc6FOy6lc+PbrOfBVPoGAVxDwYuW2xCvrkoFFVinM3fyanbjoe8g==', '2022-07-07', 'Female', '123123', '234234', 234324, '324', '2022-07-18 12:21:01', 'Student', 'pending', 1, 0),
(33, 'MichaelScott', 'Michael', 'Scott', 'testMS@gmail.com', 'AQAAAAEAACcQAAAAEJqmyDftX9/XbyV4NzyiaeFmqOduyTxYSk+1ww/59AzbCb/1ELQaGJ3XuloYOD1JFA==', '2017-06-18', 'Female', '123', '2345234', 234523345, '23452345', '2022-07-18 12:21:58', 'Parent', 'pending', 1, 0),
(34, 'DwightSchrute', 'Dwight', 'Schrute', 'DS@gmail.com', 'AQAAAAEAACcQAAAAEBIh7hLbM4N5GvDuYh/FbmC+C1iAPQfq9ltX63FZtCnOoLgPtZIxkt4tptcTVKYmow==', '2022-06-27', 'Male', '0123432', '435235', 23452345, '342532', '2022-07-18 12:27:53', 'Teacher', 'pending', 1, 0),
(35, 'ClarkKent', 'Clark', 'Kent', 'clark@gmail.com', 'AQAAAAEAACcQAAAAEKtJGWbn9zSB/tipXZDgV+LTA1c8fUXjZwHu9+GdXrvFe064rVx6FsAP3jCpjcQjMA==', '2022-07-05', 'Male', '0123456', 'City', 23423532, 'Street', '2022-07-20 16:39:04', 'Student', 'pending', 1, 0),
(36, 'MarthaKent', 'Martha', 'Kent', 'Martha@gmail.com', 'AQAAAAEAACcQAAAAEH+8mpVIuRMqdCXswl1AECNA4vREIeXsfOiUGaKrRoOAX591FlfwZLtIrKcNN0p+Tg==', '2022-07-20', 'Female', '45688799', 'City', 23446343, 'Street', '2022-07-20 16:43:22', 'Parent', 'pending', 1, 0),
(37, 'teacher5', 'teacher', '5', 'teacher5@gmail.com', 'AQAAAAEAACcQAAAAENbkVb9X1CVeJBtXRIsYz5nAVDkN/t+vSYA36KD0+Z4qnACfdTJI7pI+OCPqqYG8cQ==', '2022-07-03', 'Male', '012345435', 'city', 97869234, 'street', '2022-07-20 16:53:35', 'Teacher', 'pending', 1, 0),
(38, 'batman', 'bat', 'man', 'man@gmail.com', 'AQAAAAEAACcQAAAAEH9NWkCZFmCbucrzY2A87wyVtG80jv/c54ySw3zjIuGsVsEaT+X5yl6G8W/+iG++XA==', '2022-07-20', 'Male', '45645899', 'asdf', 234, 'asdf', '2022-07-21 00:27:47', 'Student', 'pending', 1, 0),
(39, 'Martha', 'Mar', 'tha', 'mar@gmail.com', 'AQAAAAEAACcQAAAAEHArQ+jeJZMlPOTnPN8xGBN/2WQXI3/drnS4G96JDok6pDBk0NscihiciXTgF+AZ8w==', '2022-07-17', 'Female', '23452345', 'egewtrr', 564, 'wetwe', '2022-07-21 00:28:43', 'Parent', 'pending', 1, 0),
(40, 'milo', 'mi', 'lo', 'milo@gmail.com', 'AQAAAAEAACcQAAAAECNEp2+jgtJU9Q74WQhlDi5hbZRr866/e1vRouUViANgR0rzV/WLAbezL1QZPk6vWw==', '2022-06-28', 'Male', '1231231', '234235rqa3', 234234, 'stree', '2022-07-21 00:33:39', 'Student', 'pending', 1, 0),
(41, 'KenTeo', 'Ken', 'Teo', 'cof@gmail.com', 'AQAAAAEAACcQAAAAEOyIZK7SRF4+KKgRq+xLtGNWLBkTDCzoSRIK+jnzZfHOccRLYua8j1GeRqUIjRf5Rg==', '2022-07-12', 'Male', '234234', 'asfasd', 23423, 'wearef', '2022-07-21 00:40:33', 'Parent', 'pending', 1, 0),
(42, 'JasonTodd', 'Jason', 'Todd', 'asdfsdf@gmail.com', 'AQAAAAEAACcQAAAAENJVQJJL/MOCG7xAtBgrwVB6pjuTDSvUnkVgCL+DHoGhZKi55bQ/RIqHT3Z8mVI7xA==', '2022-07-13', 'Male', '541564685', 'asasfasf', 123213, 'wsefavsdf', '2022-07-21 00:41:33', 'Teacher', 'pending', 1, 0),
(43, 'jokerx', 'joker', 'x', 'aoisfjoasd@gmail', 'AQAAAAEAACcQAAAAEKGwpz4tNXFltgPcHZNrv/GtPh51/huTtak/o+h/F8ggDBAhlacolYz3st54mDgLoQ==', '2022-07-20', 'Male', '32423', 'esfser', 32423, 'werwr', '2022-07-21 00:58:19', 'Teacher', 'pending', 1, 0),
(44, 'studenttest', 'student', 'test', 'gukyh@gmail.com', 'AQAAAAEAACcQAAAAEE04yxy/j7poHLas0rIoox9J9lThxO+M9nNaE0DVI85jDxzhS4JHYEnTgoTrtcVSQQ==', '2022-07-12', 'Male', '23423', 'sfweafew', 32423, 'wraweraw', '2022-07-21 00:59:03', 'Student', 'pending', 1, 0),
(45, 'parenttest', 'parent', 'test', 'aifh wioh@gmail.com', 'AQAAAAEAACcQAAAAELA8D3xREtcvxbtOGkMQbs+l7xmPW4Cye6ZP2Cr+EMXPUwAPQ+VK8U16STzC1cTpjQ==', '2022-07-06', 'Male', '234234', 'sef', 124, 'sf', '2022-07-21 01:00:34', 'Parent', 'pending', 1, 0),
(46, 'teachertest', 'teacher', 'test', 'sadf@gmail.com', 'AQAAAAEAACcQAAAAEK3VtrApRiVc3WoOAOOJf/z03Z+xbek1ft7AOQ24yVXOoqtaYx77SnUGysuHTuw/mA==', '2022-07-12', 'Male', '3424', 'gsfg', 234432, 'sdgsgdf', '2022-07-21 01:01:10', 'Teacher', 'pending', 1, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
