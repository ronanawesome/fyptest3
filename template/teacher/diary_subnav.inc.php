<?php

$active_path = '/' . $url['call'];//game/change-password

$sub_navs = array(
    '/portal/my-diary-overview' => array(
        'name' => _menu_overview,
    ),
    '/portal/my-diary-followup' => array(
        'name' => _diary_follow_up,
    ),
    '/portal/my-diary-past-transaction' => array(
        'name' => _diary_past_transaction,
    ),
    '/portal/my-diary-past-complaint-feedback' => array(
        'name' => _diary_past_complaint_feedback,
    ),
    '/portal/my-diary-task' => array(
        'name' => _diary_task,
    ),
    '/portal/my-diary-sale-performance' => array(
        'name' => _diary_sale_performance,
    ),
);

//$sub_navs = array(
//    '/portal/my-diary-overview' => array(
//        'name' => 'Overview', 'is_default' => true,
//    ),
//    'customer' => array(
//        'name' => 'Customer', 'is_default' => false,
//    ), 'customer-past-txn' => array(
//        'name' => 'Customer Past Transaction', 'is_default' => false,
//    ), 'customer-past-complaint-feedback' => array(
//        'name' => 'Customer Past Complaint & Feedback', 'is_default' => false,
//    ), 'task' => array(
//        'name' => 'Task', 'is_default' => false,
//    ), 'sale-performance' => array(
//        'name' => 'Sale Performance', 'is_default' => false,
//    ),
//);

?>
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <?php
    $current_active = (isset($_GET['active']) ? $_GET['active'] : "overview");
    $current_sub_active = (isset($_GET['sub_active']) ? $_GET['sub_active'] : "");

    foreach ($sub_navs as $key => $sub_nav) {

        $is_active = ($active_path == $key) ? ' active' : '';

        echo '<li class="nav-item" role="presentation">';
        echo '<a class="nav-link' . $is_active . '" href="' . $key . '" aria-controls="general" aria-selected="true">';
        echo $sub_nav['name'];
        echo '</a>';
        echo '</li>';

    }

    ?>
</ul>
