<?php
global $dbc, $url;

$role = $url['call_parts'][0];
?>

<?php

$cart_items = [];
$cart_total = 0;

//if (isset($_SESSION['customer_id'])) {
//    $current_user = new Customer($dbc);
//    $current_user->load($_SESSION['customer_id']);
//
//    $cart_items = $current_user->getCartItems();
//    $cart_total = $current_user->getCartTotal();
//}

?>



<script type="text/javascript">
    $(document).ready(function () {



        $('#hidden_general_button, #hidden_lg_button, #hidden_big_button').on('click', function (e) {

            var modal = $(this).attr('data-target');

            modal = document.getElementById(modal.replace("#", ""));

            modal.addEventListener('show.bs.modal', function (e) {
                console.log('show modal');
                console.log('target : ');
                console.log(e.relatedTarget);

                var anchor = $(e.relatedTarget);
                var modal = $(this);

                modal.find('.modal-title').html(anchor.attr('data-modal-title'));
                modal.find('.modal-body').html('<div class="m-loader m-loader--brand m-loader--lg w-100  d-block p-5"></div>');

                if (anchor.attr("data-remote")) {
                    modal.find('.modal-body').load(anchor.attr("data-remote"));

                }
            });

            var modal_2 = new bootstrap.Modal(modal);
            modal_2.toggle($(this));

        });

    });
</script>


<script type="text/javascript">
    $(document).ready(function () {
        console.log("DOCUMENT READY");

        $('body').on('click', '.remove-cart', function () {

            $.ajax({
                type: "POST",
                url: "/shop-api/cart-remove",
                data: {
                    cart_item_id: $(this).attr('data-attr-id'),
                },
                success: function (response, $status, xhr) {
                    // location.replace("/shop/cart?alert-success=" + response['message']);
                    toastr.success(response['message'], 'Removed Successfully');

                    $('#cart-item-count').html(response.data.cart_item_size);
                    $('#cart-item-total').html(response.data.cart_item_total);

                    $html = '';

                    $.each(response.data.cart_items,function (i, e) {

                        console.log(e);

                        $html += '<div class="cart-item">';
                        $html += '<div class="cart-item__image">';
                        $html += '<img src="/upload/product/' + e.product_photo + '" alt="Product image"/>';
                        $html += '</div>';
                        $html += '<div class="cart-item__info">';
                        $html += ' <a href="/student/product-detail/'+ e.product_id +'" >' + e.product_name + '</a>';
                        $html += '<h5>RM ' + e.product_selling + '</h5>';
                        $html += '<p>Quantity: &nbsp;<span>' + e.cart_item_quantity + '</span></p>';
                        $html += '</div>';
                        $html += '<button class="cart-item__remove remove-cart" data-attr-id="' + e.cart_item_id + '"><i class="fal fa-times"></i></button>';
                        $html += '</div>';
                    });

                    $('#cart-item-area').html($html);
                },
            });

        });

    });
</script>

</div>
</body>
</html>