<?php
global $dbc, $url;


$role = $url['call_parts'][0];

$login = isset($_SESSION['parent_id']);



$navbars = [];
$navbars[] = [
    'title' => _MENU_HOME,
    'link' => '/parent/home',
];
$navbars[] = [
    'title' => _MENU_CHILD_PERFORMANCE,
    'link' => '/parent/child-performance',
];
$navbars[] = [
    'title' => MENU_ANNOUCEMENTS,
    'link' => '/parent/announcements',
];
$navbars[] = [
    'title' => 'Child Reports',
    'link' => '/parent/reports',
];
$navbars[] = [
    'title' => _MENU_CONTACT,
    'link' => '/parent/contact-teachers',
];



?>

<style>
    .language{
        background-color: #000000;
        border-color: #000000;
        color: white;
    }
    .login-div{
        text-align: end;
    }
    .language:hover{
        color: #e1e1e1 !important;
    }
</style>

<div class="header -one">
    <div style="background-color: black;" class="top-nav -style-1">
        <div class="container">
            <div class="row text-white">
                <div class="top-nav__wrapper">
                    <div class="col-md-8">
                        <div class="dropdown">
                            <button class="btn language dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                Language
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" onclick="changeLang('en')">English</a></li>
                                <li><a class="dropdown-item" onclick="changeLang('my')">Malay</a></li>
                                <li><a class="dropdown-item" onclick="changeLang('zs')">中文</a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-md-4 top-nav-selection login-div"> <?php
                        if ($login === true) {
                            echo '
                        <div class="top-nav-selections__item"><a href="/'. $role .'/dashboard">'._FOOTER_MYACCOUNT.' |</a>  <a href="/'. $role .'/logout">'._LOGOUT.'</a></div>';
                        } else {
                            echo '
                        <div class="top-nav-selections__item"><a href="/'. $role .'/login">'._LOGIN.'</a></div>';
                        }
                        ?></div>
                </div>
            </div>
            <div class="top-nav__wrapper">
                <div class="top-nav-social">
                </div>
                <div class="top-nav-selections">
<!--                    <div class="top-nav-selections__item"><a href="/--><?php //echo $role ?><!--/login">Login</a></div>-->


                </div>
            </div>
        </div>
    </div>


    <div class="menu -style-1">
        <div class="container">

            <div class="menu__wrapper">
                <a class="menu-logo" href="home"><img style="height: 100%;" src="/resource/assets/SMSPlogoclear.png" alt="Logo"/></a>


                <div class="navigator">
                    <ul>
                        <?php foreach ($navbars as $navbar) : ?>
                            <li class="relative" style="  margin-left : 1.5em; margin-right : 1.5em;margin-top: 3%;">
                                <a href="<?php echo $navbar['link'] ?>">
                                    <?php echo $navbar['title'] ?>

                                    <?php if (isset($navbar['dropdown'])): ?>
                                        <span class="dropable-icon"><i class="fas fa-angle-down"></i>
                                        </span>
                                    <?php endif; ?>
                                </a>

                                <?php if (isset($navbar['dropdown'])): ?>
                                    <ul class="dropdown-menu">
                                        <?php foreach ($navbar['dropdown'] as $dropdown) : ?>
                                            <li>
                                                <a href="<?php echo $dropdown['link'] ?>"><?php echo $dropdown['title'] ?></a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <div class="menu-functions ">

                    <a class="menu-icon -navbar" href="#">
                        <div class="bar"></div>
                        <div class="bar"></div>
                        <div class="bar"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

