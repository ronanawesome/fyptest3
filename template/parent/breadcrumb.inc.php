<div class="breadcrumb">
    <div class="container" style="padding-top: 2%;">
        <h2><?php echo $page_title ?></h2>
        <ul>
            <?php

            $total_breadcrumbs = count($breadcrumbs);

            $count = 1;
            ?>
            <?php foreach ($breadcrumbs as $name => $link) : ?>
                <?php
                $href = 'javascript:void(0)';

                if (!empty($link)) {
                    $href = $link;
                }
                ?>

                <li style="margin-right: 2%;" <?php echo (!empty($link))? 'onclick="location.href=\''. $link .'\'" style=\'cursor: pointer\' ' : '' ?> class="<?php echo ($count == $total_breadcrumbs) ? 'active' : '' ?>"><?php echo $name ?></li>
                <?php $count++; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>