<?php
global $dbc,$url;
$role = $url['call_parts'][0];

require('module/mobiledetect/mobiledetectlib/Mobile_Detect.php');
$detect = new Mobile_Detect;

$parent = new user($dbc);
$parent->load($_SESSION['parent_id']);

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title><?php echo SYS_NAME ?></title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Spartan:wght@300;400;500;700;900&amp;display=swap"/>
    <link rel="shortcut icon" type="image/png" href="/resource/assets/SMSPlogoclear.png"/>
    <!--build:css assets/css/styles.min.css-->
    <link rel="stylesheet" href="/template/student/assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" href="/template/student/assets/css/slick.min.css"/>
    <link rel="stylesheet" href="/template/student/assets/css/fontawesome.css"/>
<!--    <link rel="stylesheet" href="/template/shop/assets/css/jquery.modal.min.css"/>-->
    <link rel="stylesheet" href="/template/student/assets/css/bootstrap-drawer.min.css"/>
    <link rel="stylesheet" href="/template/student/assets/css/style.css"/>

    <script src="/template/student/assets/js/jquery-3.5.1.min.js"></script>
    <script src="/template/student/assets/js/parallax.min.js"></script>
    <script src="/template/student/assets/js/slick.min.js"></script>
    <script src="/template/student/assets/js/jquery.validate.min.js"></script>
<!--    <script src="/template/shop/assets/js/jquery.modal.min.js"></script>-->
    <script src="/template/student/assets/js/bootstrap-drawer.min.js"></script>
    <script src="/template/student/assets/js/jquery.countdown.min.js"></script>
    <script src="/template/student/assets/js/main.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

    <!--js modal-->
<!--    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>-->
<!--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>-->

    <!-- Bootstrap 5 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Toast Library for css and javascript -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <style>
        body {
            font-family: 'Poppins', sans-serif;
        }

        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:70px;
            right:40px;
            background-color:#25d366;
            color:#FFF;
            border-radius:50px;
            text-align:center;
            font-size:30px;
            box-shadow: 2px 2px 3px #999;
            z-index:100;
        }

        .float1{
            position:fixed;
            width:60px;
            height:60px;
            bottom:120px;
            right:40px;
            margin-bottom: 1.5%;
            background-color: #00b7ff;
            color:white;
            border-radius:50px;
            text-align:center;
            font-size:30px;
            box-shadow: 2px 2px 3px #999;
            z-index:100;
        }

        .my-float{
            margin-top:16px;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-212462584-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-212462584-1');
    </script>
</head>

<?php
if ($detect->isMobile()){
    echo'
          ';
}else{
    echo'
         
';
}
?>
<body>

<div id="content">

