<?php

$active_path = '/portal/' . $url['call_parts'][1];//game/change-password

$sub_navs = [];

if ($obj->getCreatedBy() == $_SESSION['user_id']):
    $sub_navs['/portal/my-campaign-edit'] = array('name' => _campaign_settings,);
endif;

$sub_navs['/portal/my-campaign-customer-list'] = array('name' => _customer_list,);
$sub_navs['/portal/my-campaign-response'] = array('name' => _response,);

if ($obj->getCreatedBy() == $_SESSION['user_id']):
    $sub_navs['/portal/my-campaign-activity-log'] = array('name' => _activity_log,);
endif;


if (!checkExtraParameter(2)) {

    echo _invalid_request;
    exit();

} else {

    $id = getExtraParameter(2);

    $obj = new UserCampaign($dbc);
    if (!$obj->load($id)) {
        echo _invalid_request;
        exit();
    }

}

?>
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <?php
    $current_active = (isset($_GET['active']) ? $_GET['active'] : "overview");
    $current_sub_active = (isset($_GET['sub_active']) ? $_GET['sub_active'] : "");

    foreach ($sub_navs as $key => $sub_nav) {

        $is_active = ($active_path == $key) ? ' active' : '';

        echo '<li class="nav-item" role="presentation">';
        echo '<a class="nav-link' . $is_active . '" href="' . $key . '/' . $obj->getId() . '" aria-controls="general" aria-selected="true">';
        echo $sub_nav['name'];
        echo '</a>';
        echo '</li>';

    }

    ?>
</ul>