</div>
</div>

</div>
<!--end::Container-->

</div>
<!--end::Entry-->
</div>
<!--end::Content-->
<!--begin::Footer-->
<div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
    <!--begin::Container-->
    <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
        <!--begin::Copyright-->
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted font-weight-bold mr-2"><?php echo date("Y") . " @ "; ?></span>
            <a href="javascript:void(0);" class="text-dark-75 text-hover-primary">
                <?php echo SYS_NAME; ?>
            </a>
        </div>
        <!--end::Copyright-->
        <!--begin::Nav-->
        <!-- <div class="nav nav-dark">
            <a href="http://keenthemes.com/metronic" target="_blank" class="nav-link pl-0 pr-5">About</a>
            <a href="http://keenthemes.com/metronic" target="_blank" class="nav-link pl-0 pr-5">Team</a>
            <a href="http://keenthemes.com/metronic" target="_blank" class="nav-link pl-0 pr-0">Contact</a>
        </div> -->
        <!--end::Nav-->
    </div>
    <!--end::Container-->
</div>
<!--end::Footer-->
</div>
<!--end::Wrapper-->
</div>
<!--end::Page-->
</div>
<!--end::Main-->

<!-- <a href="#" class="nav-link" data-remote="/url/url" data-toggle="modal" data-title="My title" data-target="#general-modal">Modal</a> -->
<!-- <a href="#" class="nav-link" data-remote="/url/url" data-toggle="modal" data-title="My title" data-target="#big-modal">Big Modal</a> -->

<button id="hidden_general_button" style="     display: none;" data-remote=""
        data-toggle="modal" data-target="#general-modal" data-modal-title="">
</button>

<button id="hidden_lg_button" style="display: none;" data-remote=""
        data-toggle="modal" data-target="#lg-modal" data-modal-title="">
</button>

<button id="hidden_big_button" style="display: none;" data-remote=""
        data-toggle="modal" data-target="#big-modal" data-modal-title="">
</button>

<div class="modal fade" id="general-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="min-height:400px;">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="lg-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered mx-auto" role="document" style="max-width: 50% !important;">
        <div class="modal-content" style="min-height:400px;">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="big-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered mx-auto" role="document" style="max-width: 70% !important;">
        <div class="modal-content" style="min-height:400px;">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<style>
    /*@media print {*/
    /*    .myDivToPrint {*/
    /*        background-color: white;*/
    /*        height: 100%;*/
    /*        width: 100%;*/
    /*        position: fixed;*/
    /*        top: 0;*/
    /*        left: 0;*/
    /*        margin: 0;*/
    /*        padding: 15px;*/
    /*        font-size: 14px;*/
    /*        line-height: 18px;*/
    /*    }*/
    /*}*/
</style>


<script type="text/javascript">
    function printContent(el) {
        var DocumentContainer = document.getElementById(el);
        var WindowObject = window.open("", "PrintWindow",
            "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
        WindowObject.document.write('<link href="/template/_metronic/demo1/dist/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />')
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        setTimeout(function(){
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        },1000);

        // var restorepage = $('body').html();
        // var printcontent = $('#' + el).clone();
        // $('body').empty().html(printcontent);
        // window.print();
        // $('body').html(restorepage);

        // var mywindow = window.open('', 'PRINT', 'height=400,width=600');
        // mywindow.document.write($('#' + el).innerHTML);
        // mywindow.print();

        // var mywindow = window.open('', 'PRINT', 'height=400,width=600');
        // mywindow.document.write('<html><head><title>123</title>');
        // mywindow.document.write('<body>');
        // mywindow.document.write($('#' + el).clone());
        // mywindow.document.write('</body>');
        //
        // mywindow.document.close(); // necessary for IE >= 10
        // mywindow.focus(); // necessary for IE >= 10*/
        //
        // mywindow.print();
        // mywindow.close();
        // var headstr = "<html><head><title></title></head><body>";
        // var footstr = "</body>";
        // var newstr = document.all.item(printpage).innerHTML;
        // var oldstr = document.body.innerHTML;
        // document.body.innerHTML = headstr + newstr + footstr;
        // window.print();
        // document.body.innerHTML = oldstr;
        // return false;


    }

    function currentDT() {
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        return date + ' ' + time;
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function show_modal_message($success, $msg) {
        $value = "";
        if ($success) {
            $value = '<div class="alert alert-success">' + $msg + '</div>';
        } else {
            $value = '<div class="alert alert-danger">' + $msg + '</div>';
        }
        $("#modal-form").find(".error-output").html($value);
    }

    function show_message($success, $msg) {
        $value = "";
        if ($success) {
            $value = '<div class="alert alert-success">' + $msg + '</div>';
        } else {
            $value = '<div class="alert alert-danger">' + $msg + '</div>';
        }
        $(".error-output").html($value);
    }

    function clear_message() {
        console.log()
        $(".alert").remove();
        $(".error-output").html("");
    }

    function reinitDatepicker() {
        $('.c_future_datepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            orientation: "bottom",
            format: "yyyy-mm-dd",
        }).on('show.bs.modal', function (e) {
            e.preventDefault();
            e.stopPropagation();
        }).on('hide.bs.modal', function (e) {
            e.preventDefault();
            e.stopPropagation();
        });

        $('.c_datepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            orientation: "bottom",
            format: "yyyy-mm-dd",
            endDate: '+0d',
        }).on('show.bs.modal', function (e) {
            e.preventDefault();
            e.stopPropagation();
        }).on('hide.bs.modal', function (e) {
            e.preventDefault();
            e.stopPropagation();
        });

        $('.c_timepicker').timepicker({
            minuteStep: 1,
        });
    }

    var unsaved = false;

    function unloadPage() {
        if (unsaved) {
            return "You have unsaved changes, Do you want to leave this page and discard your changes?";
        }
    }

    function changeLang(lang) {
        $.get("/admin-api/change-language?lang=" + lang, function (data, status) {

            window.location.href = "";

        });
    }

    $(document).ready(function () {
        $('#general-modal, #big-modal, #lg-modal').on('show.bs.modal', function (e) {

            var anchor = $(e.relatedTarget);
            var modal = $(this);

            modal.find('.modal-title').html(anchor.attr('data-modal-title'));
            modal.find('.modal-body').html('<div class="m-loader m-loader--brand m-loader--lg w-100  d-block p-5"></div>');

            if (anchor.attr("data-remote")) {
                modal.find('.modal-body').load(anchor.attr("data-remote"));
            }

        });

        $('#general-modal, #big-modal, #lg-modal').on('hide.bs.modal', function (e) {
            if (unsaved) {
                var confirm = window.confirm("You have unsaved changes, Do you want to leave this page and discard your changes?");
                if (confirm) {
                    unsaved = false;
                    window.onbeforeunload = null;
                } else {
                    e.preventDefault();
                    e.stopPropagation();
                }
            }
        });
    });

</script>



<div id="kt_quick_user" class="offcanvas offcanvas-right p-10">
    <!--begin::Header-->
    <div class="offcanvas-header d-flex align-items-center justify-content-between pb-5" kt-hidden-height="40" style="">
        <h3 class="font-weight-bold m-0"><?php echo 'User Profile'; ?>
            <small class="text-muted font-size-sm ml-2"></small>
        </h3>
        <a href="#" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_user_close">
            <i class="ki ki-close icon-xs text-muted"></i>
        </a>
    </div>
    <!--end::Header-->
    <!--begin::Content-->
    <div class="offcanvas-content pr-5 mr-n5 scroll" style="height: auto; overflow: hidden;">
        <!--begin::Header-->
        <div class="d-flex align-items-center mt-5">
            <div class="symbol symbol-100 mr-5">
                <div class="symbol-label" style="background-image:url('/resource/assets/user-alt-300.png')"></div>
            </div>
            <div class="d-flex flex-column">
                <a href="#" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">
                    <?php echo (isset($_SESSION['user_displayname'])) ? $_SESSION['user_displayname'] : "" ?>
                </a>
                <div class="text-muted mt-1"></div>
                <div class="navi mt-2">
                    <a href="javascript:;" class="navi-item">
                        <span class="navi-link p-0 pb-2">
                            <span class="navi-icon mr-1">
                                <span class="svg-icon svg-icon-lg svg-icon-primary">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-notification.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"></rect>
                                            <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z"
                                                  fill="#000000"></path>
                                            <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5"></circle>
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                            </span>
                            <span class="navi-text text-muted text-hover-primary">
                                <?php echo 'test@test.com' ?>
                            </span>
                        </span>
                    </a>
                    <a href="<?php echo "/{$role}/logout" ?>"
                       class="btn btn-sm btn-light-primary font-weight-bolder py-2 px-5">
                        Sign Out
                    </a>
                </div>
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Separator-->
        <div class="separator separator-dashed mt-8 mb-5"></div>
        <!--end::Separator-->

    </div>
    <!--end::Content-->
</div>

<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop">
        <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                 height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24"/>
                    <rect fill="#000000" opacity="0.3" x="11" y="10" width="2" height="10" rx="1"/>
                    <path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z"
                          fill="#000000" fill-rule="nonzero"/>
                </g>
            </svg>
            <!--end::Svg Icon-->
        </span>
</div>
<!--end::Scrolltop-->

</body>
<!--end::Body-->
</html>
