<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-main',
    'version' => 'dev-main',
    'aliases' => 
    array (
    ),
    'reference' => 'c308fe50eee74b87ce3a0f7b837122eae2ae32c5',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
      ),
      'reference' => 'c308fe50eee74b87ce3a0f7b837122eae2ae32c5',
    ),
    'dimabdc/php-fast-simple-html-dom-parser' => 
    array (
      'pretty_version' => '1.4',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a247567567a512a2c7ba819898e00a9207f47c3',
    ),
    'nategood/httpful' => 
    array (
      'pretty_version' => '0.3.2',
      'version' => '0.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '0cded3ea97ba905600de9ceb9ef13f3ab681587c',
    ),
    'phpmailer/phpmailer' => 
    array (
      'pretty_version' => 'v6.5.0',
      'version' => '6.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a5b5c43e50b7fba655f793ad27303cd74c57363c',
    ),
    'rakit/validation' => 
    array (
      'pretty_version' => 'v1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ff003a35cdf5030a5f2482299f4c93f344a35b29',
    ),
    'revenuemonster/sdk' => 
    array (
      'pretty_version' => 'v1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d6454e2cee103bba7073f4f6c8f193d8e935d33',
    ),
    'sylouuu/php-curl' => 
    array (
      'pretty_version' => '0.8.1',
      'version' => '0.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3f3c06ae719e1ff2db01912c3f357a249a8f9936',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fcd0b29a7a0b1bb5bfbedc6231583d77fea04814',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2df51500adbaebdc4c38dea4c89a2e131c45c8a1',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eca0bf41ed421bed1b57c4958bab16aa86b757d0',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v5.3.2',
      'version' => '5.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '905a22c68b292ffb6f20d7636c36b220d1fba5ae',
    ),
    'tightenco/collect' => 
    array (
      'pretty_version' => 'v8.34.0',
      'version' => '8.34.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b069783ab0c547bb894ebcf8e7f6024bb401f9d2',
    ),
  ),
);
